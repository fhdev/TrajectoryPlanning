#pragma once
#ifndef CTRLLATSTY_HPP
#define CTRLLATSTY_HPP

#include "Eigen/Dense"
#include "TypesTp.hpp"
#include "Ode.hpp"


namespace TrajectoryPlanning
{
	class CtrlLatSty : public DynamicSystem
	{
	private:
		// look ahead factor
		const Float64_T lookAheadFactor = 0.8;

		// lon. velocity of linearization [m/s]
		Float64_T velLin;
		// lon. velocity sites for velocity dependent saturation of driving / braking torque [m/s]
		Eigen::VectorXd satVel;
		// saturation steering angle values [rad]
		Eigen::VectorXd satSteerAng;

		// Stanley gain
		Float64_T gainSty;
		// horizontal distance from front wheel center point to center of gravity [m]
		Float64_T distanceFrontCOG;
		// time constant (settling time) of steering actuator [s]
		Float64_T timeConstant;

		// way points x coordinates
		Eigen::VectorXd posXRef;
		// way points y coordinates
		Eigen::VectorXd posYRef;
		// way points yaw angles
		Eigen::VectorXd yawZRef;

		// previously found reference point index
		mutable Int64_T iClosestPrev;

		std::tuple<Float64_T, Float64_T>  getReferenceValues(
			const Eigen::Ref<const Eigen::VectorXd> xGRefPts,
			const Eigen::Ref<const Eigen::VectorXd> yGRefPts,
			const Eigen::Ref<const Eigen::VectorXd> yawZRefPts,
			Float64_T xGAct,
			Float64_T yGAct,
			Float64_T yawZAct) const;

	public:

		Int64_T nP() const override;
		Int64_T nX() const override;
		Int64_T nU() const override;
		Int64_T nY() const override;

		CtrlLatSty(
			Float64_T velLin,
			const Eigen::Ref<const Eigen::VectorXd> satVel,
			const Eigen::Ref<const Eigen::VectorXd> satSteerAng,
			Float64_T gainSty,
			Float64_T distanceFrontCOG,
			Float64_T timeConstant,
			const Eigen::Ref<const Eigen::VectorXd> posXRef,
			const Eigen::Ref<const Eigen::VectorXd> posYRef,
			const Eigen::Ref<const Eigen::VectorXd> yawZRef);

		Int32_T	Derivatives(
			Float64_T t,
			const Eigen::Ref<const Eigen::VectorXd> X,
			const Eigen::Ref<const Eigen::VectorXd> U,
			Eigen::Ref<Eigen::VectorXd> dX,
			Eigen::Ref<Eigen::VectorXd> Y) const override;

		Int32_T Initialize(
			const Eigen::Ref<const Eigen::VectorXd> Xi,
			Eigen::Ref<Eigen::VectorXd> X) const;
	};
}

#endif // !CTRLLATSTY_HPP

