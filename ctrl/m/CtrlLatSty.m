%% CtrlLatSty **************************************************************************************
% [Summary]
%   This class represents a nonlinear Stanley lateral (path tracking) control. 
%
% [Used in]
%   VehStCl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef CtrlLatSty < CtrlLat
    %% Constant properties =========================================================================
    properties (Constant)
        lookAheadFactor = 0.8;
    end
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------        
        % Controller base parameters
        % Stanley gain
        GainSty
        
        % Controlled vehicle related parameters
        DistanceFrontCOG % [m] distance of cog and front axis
        TimeConstant     % [s] settling time of steering actuator
        
        % Reference trajectory
        PosXGRef
        PosYGRef
        YawZRef
                
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------        
        function set.GainSty(self, value)
            if ~isequal(self.GainSty, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: GainSty must be a numeric scalar!')
                end
                self.GainSty = value(:);
            end
        end
        
        function set.DistanceFrontCOG(self, value)
            if ~isequal(self.DistanceFrontCOG, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DistanceFrontCOG must be a numeric scalar!')
                end
                self.DistanceFrontCOG = value(:);
            end
        end
        
        function set.PosXGRef(self, value)
            if ~isequal(self.PosXGRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PosXGRef must be a numeric vector!')
                end
                self.PosXGRef = value(:);
            end
        end

        function set.PosYGRef(self, value)
            if ~isequal(self.PosYGRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PosYGRef must be a numeric vector!')
                end
                self.PosYGRef = value(:);
            end
        end
        
        function set.YawZRef(self, value)
            if ~isequal(self.YawZRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawZRef must be a numeric vector!')
                end
                self.YawZRef = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = CtrlLatSty(chassis, steering, varargin)
            %% Assemble superclass arguments
            if ~isa(chassis, 'ChassisSt')
                error('ERROR: chassis must be a ChassisSt object!');
            end
            if nargin == 3
                superArgs = varargin(1);
            elseif nargin == 6
                superArgs = varargin(1:3);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            
            %% Call superclass constructor
            self@CtrlLat(superArgs{:});
            
            %% Set subclass properties
            if nargin == 3
                self.GainSty = varargin{1}.GainSty;
            elseif nargin == 6
                self.GainSty = varargin{4};
            end
            
            self.DistanceFrontCOG = chassis.DistanceFrontCOG;
            self.TimeConstant = steering.TimeConstant;
            
            % Default values for references
            self.PosXGRef    = [0; 100];
            self.PosYGRef    = [0; 0];
            self.YawZRef    = [0; 0];
        end
        
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of the nonlinear Stanley lateral path
        %   tracking controller. References:
        %       - Jarrod M. Snider: Automatic Steering Methods for Autonomous Automobile Path 
        %         Tracking, Chapter 2.3
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => arbitrary scalar value, this controller has no state
        %
        %   U - input vector
        %       U(1) => PosXG:    longitudinal position in ground-fixed coordinate system [m]
        %       U(2) => PosYG:    lateral position in ground-fixed coordinate system [m]
        %       U(3) => YawZ:     yaw (heading) angle in ground-fixed coordinate system [rad]
        %       U(4) => VelXV:    longitudinal velocity in vehicle-fixed cs. [m/s]
        %
        % [Output]
        %   dX - state derivative vector
        %       dX(1) => arbitrary scalar value, this controller has no state
        %
        %   Y  - output vector
        %       Y(1) => SteerAng:      steering angle output of controller [rad]
        %       Y(2) => ErrCrossTrack: cross track error [m]
        %       Y(3) => ErrHeading:    heading error [rad]
        % ------------------------------------------------------------------------------------------         
        function [dX, Y] = StateEquation(self, t, X, U) %#ok<INUSL> t used for debugging
            %% Calculate tracking errors
            % Reference point for traditional Stanley controller is the front axis centerpoint. It
            % is however beneficial to define a lookahead considering the settling time of steering
            % actuator.
            lookAhead = self.DistanceFrontCOG + self.lookAheadFactor * U(4) * self.TimeConstant;
            xfG = U(1) + lookAhead * cos(U(3));
            yfG = U(2) + lookAhead * sin(U(3));
            
            [latErr, yawErr] = ...
                self.GetReferenceValues(self.PosXGRef, self.PosYGRef, self.YawZRef, [], xfG, yfG, U(3));
            
            %% Calculate output (Stanley control law)            
            steerAng = yawErr;
            if U(4) ~= 0
                steerAng = yawErr + atan(self.GainSty * latErr / U(4));        
            end
            
            % Saturation
            [~, steerAng] = self.AntiWindup(U(4), steerAng, []);
            
            % Stanley controller has no state
            dX = 0;
            
            %% Set output
            % Set output tracking errors calculated in cog position
            [latErr, yawErr] = ...
                self.GetReferenceValues(self.PosXGRef, self.PosYGRef, self.YawZRef, [], U(1), U(2), U(3));
            
            
            Y = [steerAng; latErr; yawErr];
        end
        
        %% Initialize -----------------------------------------------------------------------------
        % [Summary]
        %   This function calculates initial value of state vector.
        %
        % [Input]
        %   None
        %
        % [Output]
        %   X0 -  initial state vector
        %       X0(1) => Dummy, Stanley controller has no state.
        % ------------------------------------------------------------------------------------------          
        function X0 = Initialize(self)
            %% Reset invernal variables
            self.iClosestPrev = 0;
            
            %% Set initial state vector
            X0 = 0;
            
        end 
    end
end
