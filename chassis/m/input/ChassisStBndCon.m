%% ChassisStBndCon *********************************************************************************
% [Summary]
%   This class represents boundary conditions for single track vehicle chassis model.
%
% [Used in]
%   VehStBndCon
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef ChassisStBndCon < ChassisQrBndCon
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % yaw angle in ground-fixed cs. [rad]
        YawZ
        % yaw velovity in ground-fixed cs. [rad/s]
        YawVelZ
        
    end
    
    %% Instance methods ============================================================================    
    methods
        %% Property getter and setter methods ------------------------------------------------------       
        function set.YawZ(self, value)
            if ~isequal(self.YawZ, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: YawZ must be a numeric scalar!')
                end
                self.YawZ = value;
            end
        end
        
        function set.YawVelZ(self, value)
            if ~isequal(self.YawVelZ, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: YawVelZ must be a numeric scalar!')
                end
                self.YawVelZ = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = ChassisStBndCon(varargin)
            if nargin == 0
                superArgs = {};
            elseif nargin == 1
                superArgs = varargin(1);
            elseif nargin == 6
                superArgs = varargin(1:4);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@ChassisQrBndCon(superArgs{:});
            if nargin == 0
                self.YawZ = 0;
                self.YawVelZ = 0;
            elseif nargin == 1
                self.YawZ = varargin{1}.YawZ;
                self.YawVelZ = varargin{1}.YawVelZ;
            elseif nargin == 6
                self.YawZ = varargin{5};
                self.YawVelZ = varargin{6}; 
            end
        end
        
    end
end