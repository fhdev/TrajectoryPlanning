%% CtrlLat *****************************************************************************************
% [Summary]
%   This class represents common features for lateral (path tracking) control. 
%
% [Used in]
%   none
%
% [Subclasses]
%   CtrlLqsYr
%   CtrlLatTrk
%   CtrlLatSty
% **************************************************************************************************
classdef CtrlLat < handle
    
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % index of last closest point
        iClosestPrev
        
        %% Properties set in constructor -----------------------------------------------------------
        % Linearization velocity        
        VelLin        
        % Saturation velocity and steering angle
        SatVel
        SatSteerAng
        
    end
    
    %% Instance methods ============================================================================
    methods        
        %% Property getter and setter methods ------------------------------------------------------        
        function set.VelLin(self, value)
            if ~isequal(self.VelLin, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: VelLin must be a numeric scalar!')
                end
                self.VelLin = value(:);
            end
        end
        
        function set.SatVel(self, value)
            if ~isequal(self.SatVel, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: SatVel must be a numeric vector!')
                end
                self.SatVel = value(:);
            end
        end
        
        function set.SatSteerAng(self, value)
            if ~isequal(self.SatSteerAng, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: SatSteerAng must be a numeric vector!')
                end
                self.SatSteerAng = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------        
        function self = CtrlLat(varargin)
            if nargin == 1
                self.VelLin      = varargin{1}.VelLin;
                self.SatVel      = varargin{1}.SatVel;
                self.SatSteerAng = varargin{1}.SatSteerAng;
            elseif nargin == 3
                self.VelLin      = varargin{1};
                self.SatVel      = varargin{2};
                self.SatSteerAng = varargin{3};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self.iClosestPrev = 0;
        end
        
        %% GetReferenceValues ----------------------------------------------------------------------
        % [Summary]
        %   This function determines the tracking errors of the vehicle compared to the track in
        %   the reference point. The reference point is the closest point of the track to vehicle 
        %   center of gravity (CtrlLatTrk) or front wheel axis (CtrlLatSty).
        %
        % [Input]
        %   xGRefPts      - x coordinates of the path points [m]
        %   yGRefPts      - y coordinates of the path points [m]
        %   yawZRefPts    - yaw angle in path points [rad]
        %   yawVelZRefPts - yaw rate in path points [rad/s]
        %
        %   xGAct         - x coordinate of vehicle position [m]
        %   yGAct         - y coordinate of vehicle position [m]
        %   yawZAct       - yaw angle of vehicle [rad]
        %  yawVelZAct     - yaw rate of vehicle [rad/s]
        %
        % [Output]
        %   Tracking errors. Positive value means positive steering angle is needed. 
        %   error = reference (track) - actual (vehicle)
        %   eLat        - cross track error [m]
        %   eYawZ       - yaw angle error [rad]
        %   eYawVelZ    - yaw rate error [rad/s]
        % ------------------------------------------------------------------------------------------         
        function [eLat, eYawZ, eYawVelZ] = GetReferenceValues(self, xGRefPts, yGRefPts, yawZRefPts, yawVelZRefPts, ...
                xGAct, yGAct, yawZAct, yawVelZAct)
            %% Find reference point (the closest sample to vehicle) 
            % Find optimal length for subsampling: number of checks is N(s) = n/x + 2x
            % N'(x) = -n/x^2 + 2 = 0   -> x = sqrt(n/2)
            nRef = length(xGRefPts);
            nSub = round(sqrt(nRef / 2));
            
            % Step 1: find closest point in subsampled trajectory if no previously found is present
            if (self.iClosestPrev < 1) || (self.iClosestPrev > nRef)
                xGRel = xGRefPts(1:nSub:end) - xGAct;
                yGRel = yGRefPts(1:nSub:end) - yGAct;
                dSqr = xGRel.^2 + yGRel.^2;
                [~, iSubClosestStep1] = min(dSqr);
                iClosestStep1 = nSub * (iSubClosestStep1 - 1) + 1;
            else
                iClosestStep1 = self.iClosestPrev;
            end
            iLeftStep1 = max(1, iClosestStep1 - nSub);
            iRightStep1 = min(iClosestStep1 + nSub, nRef);
            
            % Step 2: find closest point in the closest region of trajectory
            xGRel = xGRefPts(iLeftStep1 : iRightStep1) - xGAct;
            yGRel = yGRefPts(iLeftStep1 : iRightStep1) - yGAct;
            dSqr = xGRel.^2 + yGRel.^2;
            [~, iSubClosestStep2] = min(dSqr);
            if iSubClosestStep2 == 1
                iSubNextClosestStep2 = iSubClosestStep2 + 1;
            elseif iSubClosestStep2 == (iRightStep1 - iLeftStep1 + 1)
                iSubNextClosestStep2 = iSubClosestStep2 - 1;
            elseif (dSqr(iSubClosestStep2 - 1) < dSqr(iSubClosestStep2 + 1))
                iSubNextClosestStep2 = iSubClosestStep2 - 1;
            else
                iSubNextClosestStep2 = iSubClosestStep2 + 1;
            end
            iClosestStep2 = iLeftStep1 + iSubClosestStep2 - 1;
            iNextClosestStep2 = iLeftStep1 + iSubNextClosestStep2 - 1;
            self.iClosestPrev = iClosestStep2;
            
            %% Calculate tracking errors
            % Lets approximate linearly between points: y(x) = a.x + b
            % Squared distance function: x^2 + y^2 = (a^2 + 1).x^2 + 2ab.x + b^2
            % Distance is minimal where distance function's derivative is zero: x = (-2ab) / (2(a^2 +1))
            
            % Lateral error
            if yGRel(iSubClosestStep2) == yGRel(iSubNextClosestStep2)
                a = 0;
            else
                a = (yGRel(iSubClosestStep2) -  yGRel(iSubNextClosestStep2)) / ...
                    (xGRel(iSubClosestStep2) -  xGRel(iSubNextClosestStep2));
            end
            b = yGRel(iSubClosestStep2) - a * xGRel(iSubClosestStep2);
            xGRelOpt = - 2 * a * b / (2 * (a^2 + 1));
            yGRelOpt = a * xGRelOpt + b;
            yVRelOpt = - sin(yawZAct) * xGRelOpt + cos(yawZAct) * yGRelOpt;
            eLat = sign(yVRelOpt) * sqrt(xGRelOpt^2 + yGRelOpt^2);
            
            % Yaw angle error
            if yawZRefPts(iClosestStep2) ==  yawZRefPts(iNextClosestStep2)
                a = 0;
            else
                a = (yawZRefPts(iClosestStep2) -  yawZRefPts(iNextClosestStep2)) / ...
                    (xGRel(iSubClosestStep2) -  xGRel(iSubNextClosestStep2));
            end
            b = yawZRefPts(iClosestStep2) - a * xGRel(iSubClosestStep2);
            yawZRefOpt = a * xGRelOpt + b;
            eYawZ = yawZRefOpt - yawZAct;
            
            % Yaw velocity error
            if ~isempty(yawVelZRefPts)
                if yawVelZRefPts(iClosestStep2) == yawVelZRefPts(iNextClosestStep2)
                    a = 0;
                else
                    a = (yawVelZRefPts(iClosestStep2) -  yawVelZRefPts(iNextClosestStep2)) / ...
                        (xGRel(iSubClosestStep2) -  xGRel(iSubNextClosestStep2));
                end
                b = yawVelZRefPts(iClosestStep2) - a * xGRel(iSubClosestStep2);
                yawVelZRefOpt = a * xGRelOpt + b;
                eYawVelZ = yawVelZRefOpt - yawVelZAct;
            end
            
        end
        
        %% AntiWindup ------------------------------------------------------------------------------
        % [Summary]
        %   This function calculates saturation and anti-windup by clamping for lateral path
        %   tracking control.
        %
        % [Input]
        %   velXV      - longitudinal velocity in vehicle-fixed cs. [m/s]
        %   steerAng   - steering angle (unsaturated) [rad] 
        %   yawVelZErr - error in yaw rate tracking [rad/s]
        %
        % [Output]
        %   dX - state derivative vector
        %       dX(1) => YawVelZErr: yaw rate tracking error [rad/s]
        %
        %   Y  - output vector
        %       Y(1) => SteerAng: saturated steering angle [rad] 
        % ------------------------------------------------------------------------------------------                
        function [dX, Y] = AntiWindup(self, velXV, steerAng, yawVelZErr)
            %% Saturation with anti-windup (clamping)
            steerAngMax = Util.LinInterp(self.SatVel, self.SatSteerAng, abs(velXV));            
            if steerAng > steerAngMax
                Y = steerAngMax;
                dX = 0;
            elseif steerAng < -steerAngMax
                Y = -steerAngMax;
                dX = 0;
            else
                Y = steerAng;
                dX = yawVelZErr;
            end
        end
        
    end
    
end