%% PlannerOptFcnGeom *******************************************************************************
% [Summary]
%   This class represents state related cost function for PlannerOpt and PlannerCopt motion 
%   planners. State related cost function is the weighted sum of average absolute state values
%   along the trajectory. 
%
% [Used in]
%   PlannerOpt
%   PlannerCopt
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef PlannerOptFcnGeom < handle
    
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % proeprty names of ChassisStOut
        propNames
        
        %% Properties set in constructor -----------------------------------------------------------
        % cost function weights
        Weight
        % actual average absolute state values along trajectory
        Value
    end

    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        
        function set.Weight(self, value)
            if ~isequal(self.Weight, value)
                if isstruct(value)
                    value = PlannerGeomOut(value);
                end
                if ~isa(value, 'PlannerGeomOut')
                    error('ERROR: Weight must be a PlannerGeomOut object!');
                end
                self.Weight = value;
            end
        end
        
        function set.Value(self, value)
            if ~isequal(self.Value, value)
                if isstruct(value)
                    value = PlannerGeomOut(value);
                end
                if ~isa(value, 'PlannerGeomOut')
                    error('ERROR: Value must be a PlannerGeomOut object!');
                end
                self.Value = value;
            end
        end
                
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerOptFcnGeom(weight)
            %% Check arguments
            if ~(isa(weight, 'PlannerGeomOut'))
                error('ERROR: finalCon must be a PlannerGeomOut object!');
            end
            %% Initialize
            self.Weight = PlannerGeomOut(weight);
            nulls = repmat({0}, 1, 8);
            self.Value = PlannerGeomOut(nulls{:});
            self.propNames = properties('PlannerGeomOut');
        end
        
        %% Update ----------------------------------------------------------------------------------
        % [Summary]
        %   This function updates state state related cost values according to given vehicle 
        %   simulation output.
        %
        % [Input]
        %   chassisOut - output of current vehicle simulation as ChassisStOut object
        %
        % [Output]
        %   none
        % ------------------------------------------------------------------------------------------
        function Update(self, plannerGeomOut)
            %% Check arguments
            if ~(isa(plannerGeomOut, 'PlannerGeomOut'))
                error('ERROR: chassisOut most be a PlannerGeomOut object!');
            end
            %% Calculate state constraints
            for iProp = 1 : length(self.propNames)
                % There is weight specified
                if ~isnan(self.Weight.(self.propNames{iProp}))
                    % Calculate average value along path
                    self.Value.(self.propNames{iProp}) = ...
                        sqrt(trapz(plannerGeomOut.Time, plannerGeomOut.(self.propNames{iProp}).^2)) / ...
                        (plannerGeomOut.Time(end) - plannerGeomOut.Time(1));                
                else
                    self.Value.(self.propNames{iProp}) = nan;                    
                end
            end
        end
        
        %% ToScalar --------------------------------------------------------------------------------
        % [Summary]
        %   This method provides scalar cost value as the weighted sum of average absolute state
        %   values.
        %
        % [Input]
        %   none
        %
        % [Output]
        %   cost - scalar cost value
        % ------------------------------------------------------------------------------------------   
        function cost = ToScalar(self)
            cost = 0;
            for iProp = 1 : length(self.propNames)
                % There is weight specified
                if ~isnan(self.Weight.(self.propNames{iProp}))
                    cost = cost + self.Weight.(self.propNames{iProp}) * self.Value.(self.propNames{iProp});
                end
            end
        end

    end
end
