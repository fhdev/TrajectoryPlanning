clear busElements;

busElements(1) = Simulink.BusElement;
busElements(1).Name = 'subBusElement1';
busElements(1).Dimensions = 1;
busElements(1).DimensionsMode = 'Fixed';
busElements(1).DataType = 'double';
busElements(1).SampleTime = 0;
busElements(1).Complexity = 'real';

busElements(2) = Simulink.BusElement;
busElements(2).Name = 'subBusElement2';
busElements(2).Dimensions = 1;
busElements(2).DimensionsMode = 'Fixed';
busElements(2).DataType = 'double';
busElements(2).SampleTime = 0;
busElements(2).Complexity = 'real';

subBus = Simulink.Bus;
subBus.Elements = busElements;

clear busElements;
busElements(1) = Simulink.BusElement;
busElements(1).Name = 'busElement1';
busElements(1).DataType = 'Bus: subBus';
busElements(1).SampleTime = -1;

busElements(2) = Simulink.BusElement;
busElements(2).Name = 'busElement2';
busElements(2).Dimensions = 1;
busElements(2).DimensionsMode = 'Fixed';
busElements(2).DataType = 'double';
busElements(2).SampleTime = 0;
busElements(2).Complexity = 'real';

bus = Simulink.Bus;
bus.Elements = busElements;

uin = [];
uin.busElement1.subBusElement1 = timeseries(0:10,0:10);
uin.busElement1.subBusElement2 = timeseries(0:2:20,0:10);
uin.busElement2 = timeseries(0:3:30,0:10);