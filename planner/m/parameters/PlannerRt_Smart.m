function planner = PlannerRt_Smart()
planner.Planner           = PlannerCon_Smart();
planner.SampleTimeCtrl    = 0.02;
planner.SampleTimePlanner = 2;
planner.MaxRunTime        = 40;
planner.VirtualCan        = false;
planner.Wheel2SteerWheel  = 17;
end
