%% ChassisQrBndCon *********************************************************************************
% [Summary]
%   This class represents boundary conditions for quarter vehicle chassis model.
%
% [Used in]
%   VehQrBndCon
%
% [Subclasses]
%   ChassisStBndCon
% **************************************************************************************************
classdef ChassisQrBndCon < ChassisQrLonBndCon
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % lateral position in ground-fixed cs. [m]
        PosYG
        % lateral velocity in vehicle-fixed cs. [m/s]
        VelYV
        
    end
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.PosYG(self, value)
            if ~isequal(self.PosYG, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: PosYG must be a numeric scalar!')
                end
                self.PosYG = value;
            end
        end
        
        function set.VelYV(self, value)
            if ~isequal(self.VelYV, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: VelYV must be a numeric scalar!')
                end
                self.VelYV = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = ChassisQrBndCon(varargin)
            if nargin == 0
                superArgs = {};
            elseif nargin == 1
                superArgs = varargin(1);
            elseif nargin == 4
                superArgs = varargin(1:2);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@ChassisQrLonBndCon(superArgs{:});
            if nargin == 0
                self.PosYG = 0;
                self.VelYV = 0;
            elseif nargin == 1
                self.PosYG = varargin{1}.PosYG;
                self.VelYV = varargin{1}.VelYV;
            elseif nargin == 4
                self.PosYG = varargin{3};
                self.VelYV = varargin{4}; 
            end
        end
        
    end
end