% Parameters
W = 7;
H = 4;
F = 8;

% Create new figure
fig = nicefigure('VelRef', W, H);
ax = niceaxis(fig, F);
% Plot trapezoidal profile
plot(ax, [0 2 4 5], [3 5 5 1], 'LineStyle', '-', 'Color', nicecolors.BlueBelizeHole, 'Marker', 'o', ...
    'MarkerFaceColor',  nicecolors.BlueBelizeHole, 'MarkerSize', 3.0);
% Set axis limits
axis(ax, [0, 5, 0, 6]);
% Axis labels
xlabel(ax, '$t_v$ (time) [s]');
ylabel(ax, '$\dot{x}_{ref}$ (vel. ref.) [m/s]');
% Text labels for acceleration parameters
text(ax, 1, 3.4, '$\ddot{x}^i_{ref}$','FontSize', F, 'Interpreter', 'Latex');
text(ax, 4, 2.7, '$\ddot{x}^f_{ref}$','FontSize', F, 'Interpreter', 'Latex');
% X tick label
ax.XTick = [0 1 2 3 4 5];
ax.XTickLabel = {'$0$', '', '', '', '','$t_f$'};
% Y tick labels
ax.YTick = [1 3 5];
ax.YTickLabels = {'$\dot{x}^f_{ref}$', '$\dot{x}^i_{ref}$', '$\dot{x}^t_{ref}$'};

name = 'VelRef';
savefig(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');