channelList = canChannelList;

%% Switches
USE_VIRTUAL = 1;

%% Constants
CANCASE_RX_CH_NUM = 1;
CANCASE_TX_CH_NUM = 2;
VIRTUAL_RX_CH_NUM = 1;
VIRTUAL_TX_CH_NUM = 1;

%% Create CAN channels
virtualChannel1 = canChannel('Vector', 'Virtual 1', 1);
virtualChannel2 = canChannel('Vector', 'Virtual 1', 2);

if USE_VIRTUAL
    device = 'Virtual 1';
    % Receive channels
    channelRxImar  = canChannel('Vector', device, VIRTUAL_RX_CH_NUM);
    channelRxImu   = canChannel('Vector', device, VIRTUAL_RX_CH_NUM);
    channelRxVBox  = canChannel('Vector', device, VIRTUAL_RX_CH_NUM);
    % Transmit channels
    channelTxSmart = canChannel('Vector', device, VIRTUAL_TX_CH_NUM);
    channelTxImar  = canChannel('Vector', device, VIRTUAL_TX_CH_NUM);
else
    device = 'CANcaseXL 1';
    % Receive channels
    channelRxImar  = canChannel('Vector', device, CANCASE_RX_CH_NUM);
    channelRxImu   = canChannel('Vector', device, CANCASE_RX_CH_NUM);
    channelRxVBox  = canChannel('Vector', device, CANCASE_RX_CH_NUM);
    % Transmit channels
    channelTxSmart = canChannel('Vector', device, CANCASE_TX_CH_NUM);
end

%% Load dbc
dbcImar  = canDatabase('imar.dbc');
dbcImu   = canDatabase('MM5_10_Default.dbc');
dbcVbox  = canDatabase('VBOX3iSL_ADAS_VCI_VEHICO.dbc');
dbcSmart = canDatabase('smart_control.dbc');

%% Assign database to channels
% Receive channels
channelRxImar.Database = dbcImar;
channelRxImu.Database = dbcImu;
channelRxVBox.Database = dbcVbox;
% Transmit channels
channelTxSmart.Database = dbcSmart;
channelTxImar.Database = dbcImar;

%% Filter necessary data
filterAllowOnly(channelRxImar, {'Attitude', 'Body_Velocity', 'INS_Latitude', 'INS_Longitude'});
% Accelerations and angular rates
filterAllowOnly(channelRxImu, {'TX1', 'TX2', 'TX3'});
% Control signals
filterAllowOnly(channelTxSmart, 'Control_message');

%% Start channels
start(channelTxSmart);

%% Build dummy message
msgCtrl = canMessage(dbcSmart, 'Control_message');
msgCtrl.Signals.Speed_demand = 90;
msgCtrl.Signals.Steering_wheel_angle = 0;
transmitPeriodic(channelTxSmart, msgCtrl, 'On', 0.01);

% while true
%     msgCtrl.Signals.Speed_demand = msgCtrl.Signals.Speed_demand - 2;
%     pause(1);
% end