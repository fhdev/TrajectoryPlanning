#pragma once
#ifndef CHASSISST_HPP
#define CHASSISST_HPP

#include "../../ext//eigen-3.3.7/Eigen/Dense"
#include "../../com/inc/TypesTp.hpp"
#include "../../com/inc/Ode.hpp"


namespace TrajectoryPlanning
{
	class ChassisSt : DynamicSystem
	{
	private:
		// mass [kg]
		Float64_T mass;
		// moment of inertia about vertical axis [kgm^2]
		Float64_T momentOfInertiaZ;
		// horizontal distance from front wheel center point to center of gravity [m]
		Float64_T distanceFrontCOG;
		// horizontal distance from rear wheel center point to center of gravity [m]
		Float64_T distanceRearCOG;
		// center of gravity height [m]
		Float64_T heightCOG;
		// aerodynamic drag coefficient [1]
		Float64_T dragCoefficient;
		// frontal area [m^2]
		Float64_T frontalArea;
		// driving torque distribution between front and rear axles [1]
		Float64_T drivingTorqueSplit;

	public:
		Float64_T Mass() const;
		Float64_T DistanceFrontCOG() const;
		Float64_T DistanceRearCOG() const;
		Float64_T HeightCOG() const;

		Int64_T nP() const override;
		Int64_T nX() const override;
		Int64_T nU() const override;
		Int64_T nY() const override;

		ChassisSt(
			Float64_T mass,
			Float64_T momentOfInertiaZ,
			Float64_T distanceFrontCOG,
			Float64_T distanceRearCOG,
			Float64_T heightCOG,
			Float64_T dragCoefficient,
			Float64_T frontalArea,
			Float64_T drivingTorqueSplit);

		ChassisSt(const Eigen::Ref<const Eigen::VectorXd> P);

		Int32_T Derivatives(Float64_T t, 
			const Eigen::Ref<const Eigen::VectorXd> X,
			const Eigen::Ref<const Eigen::VectorXd> U,
			Eigen::Ref<Eigen::VectorXd> dX,
			Eigen::Ref<Eigen::VectorXd> Y) const override;
	};
}

#endif // !CHASSISST_HPP

