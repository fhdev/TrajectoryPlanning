#pragma once
#ifndef MATHTP_HPP
#define MATHTP_HPP

#include "TypesTp.hpp"
#include "Eigen/Dense"

namespace TrajectoryPlanning
{
    // Constant definitions
    #define G 9.80665
    #define PI 3.14159265358979323846
    #define RHOA 1.225
    // Macros
    #define SQR(x) ((x)*(x))
    // Function declarations
    Float64_T max3(Float64_T x, Float64_T y, Float64_T z);
    Float64_T sign(Float64_T x);
    Float64_T interpolate(const Eigen::Ref<const Eigen::VectorXd> x, const Eigen::Ref<const Eigen::VectorXd> y, Float64_T xq);

    Int32_T interp4spline5(const Eigen::Ref<const Eigen::VectorXd> x,
                           const Eigen::Ref<const Eigen::VectorXd> y,
                           const Eigen::Ref<const Eigen::VectorXd> dyb,
                           const Eigen::Ref<const Eigen::VectorXd> ddyb,
                           const Eigen::Ref<const Eigen::VectorXd> xq,
                           Eigen::Ref<Eigen::VectorXd> yq,
                           Eigen::Ref<Eigen::VectorXd> dyq,
                           Eigen::Ref<Eigen::VectorXd> ddyq);

    Int32_T interp1(const Eigen::Ref<const Eigen::VectorXd> x,
                    const Eigen::Ref<const Eigen::VectorXd> y,
                    const Eigen::Ref<const Eigen::VectorXd> xq,
                    Eigen::Ref<Eigen::VectorXd> yq);
    
    Eigen::VectorXd diff(const Eigen::VectorXd & v);

    Eigen::VectorXd cumsum(const Eigen::VectorXd & v);
}

#endif // !MATHTP_HPP