% Clear and close all
clear all;
close all;
% Set default interpreter to LaTeX
set(0,'DefaultTextInterpreter','latex');
% Create new figure
figure('Name', 'Parametrized velocity input', 'NumberTitle', 'off', 'Color', [1 1 1]);
% Set plot size (9.25 cm witdh to fit two column article page)
set(gcf,'Units','Centimeters','Position',[10, 10, 9.25, 3.5])
% Plot trapezoidal profile
plot([0 2 4 5],[3 5 5 1], 'LineStyle', '-', 'LineWidth', 1.0, 'Color', [0 0 1], 'Marker', 'o', 'MarkerFaceColor',  [0 0 1], 'MarkerSize', 2.0);
% Set axis limits
axis([0, 5, 0, 6]);
% Grid and box on
grid on; 
box on;
% Axis labels
xlabel('Time','FontSize',10);
ylabel('Long. velocity','FontSize',10);
% Text labels for acceleration parameters
text(1,3.4,'$a_0$','FontSize',10);
text(4.3,2.5,'$a_1$','FontSize',10);
% X tick label
xdata = [0 1 2 3 4 5];
xlabels = {'$0$', '', '', '', '','$t_f$'};
% Y tick labels
ydata = [0 1 2 3 4 5 6];
ylabels = {'', '$v_f$', '', '$v_0$', '', '$v_t$', ''};
% Set tick labels
set(gca, 'Xtick', xdata, 'XtickLabel', xlabels, 'TickLabelInterpreter', 'latex');
set(gca, 'Ytick', ydata, 'YtickLabel', ylabels, 'TickLabelInterpreter', 'latex');