function [input, initCon] = Maneuver_AccelerateBrake()

%% Parameters that define the trajectory
% Open loop (constant braking torque): have to make this time dependent
Md = 800;
% Closed loop (accelerate from 0.1 to 20 mps in 200 m)
sp = [ 0; 20; 50; 90; 120; 140];
kp = [ 0;  0;  0;  0;   0;   0];
vp = [10; 10; 20; 20;  10;  10];
[t, ~, x, y, r, v, dr] = PlannerGeom.CurvatureTraj(sp, kp, vp);

%% Initial conditions
% Do not reset these
initCon.PosXG   = 0;
initCon.PosYG   = 0;
initCon.YawZ    = 0;
% Adjustable indeed
initCon.VelXV   = vp(1);
initCon.VelYV   = 0;
initCon.YawVelZ = 0;

%% Inputs
% Common
input.Time = t;
input.Friction = ones(size(input.Time));
input.FrictionFront = ones(size(input.Time));
input.FrictionRear = ones(size(input.Time));
% Open loop
input.TorqueDrv = Md * double(input.Time > 0.5);
input.TorqueBrk = zeros(size(input.Time));
input.SteerWhlAng = zeros(size(input.Time));
input.SteerAng = input.SteerWhlAng / 18;
% Closed loop with yaw rate tracking or path tracking
input.VelXVRef = v;
input.YawVelZRef = dr;
input.PosXGRef = x;
input.PosYGRef = y;
input.YawZRef = r;
end
