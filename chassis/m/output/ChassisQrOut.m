%% ChassisQrOut ************************************************************************************
% [Summary]
%   This class represents outputs for quarter vehicle chassis model.
%
% [Used in]
%   VehQrOut
%   VehQr
%
% [Subclasses]
%   ChassisStOut
% **************************************************************************************************
classdef ChassisQrOut < ChassisQrLonOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % longitudinal velocity in vehicle-fixed cs. [m/s]
        VelXV
        % longitudinal inertial acceleration in vehicle-fixed cs. [m/s^2]
        AccInrtXV
        % lateral position in ground-fixed cs. [m]
        PosYG
        % lateral velocity in ground-fixed cs. [m/s]
        VelYG
        % lateral acceleration in ground-fixed cs. [m/s^2]
        AccYG
        % lateral velocity in vehicle-fixed cs. [m/s]
        VelYV
        % lateral intertial acceleration in vehicle-fixed cs. [m/s^2]
        AccInrtYV

    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.VelXV(self, value)
            if ~isequal(self.VelXV, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: VelXV must be a numeric vector!')
                end
                self.VelXV = value(:);
            end
        end
        
        function set.AccInrtXV(self, value)
            if ~isequal(self.AccInrtXV, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: AccInrtXV must be a numeric vector!')
                end
                self.AccInrtXV = value(:);
            end
        end
        
        function set.PosYG(self, value)
            if ~isequal(self.PosYG, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PosYG must be a numeric vector!')
                end
                self.PosYG = value(:);
            end
        end
        
        function set.VelYG(self, value)
            if ~isequal(self.VelYG, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: VelYG must be a numeric vector!')
                end
                self.VelYG = value(:);
            end
        end
        
        function set.AccYG(self, value)
            if ~isequal(self.AccYG, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: AccYG must be a numeric vector!')
                end
                self.AccYG = value(:);
            end
        end
        
        function set.VelYV(self, value)
            if ~isequal(self.VelYV, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: VelYV must be a numeric vector!')
                end
                self.VelYV = value(:);
            end
        end
        
        function set.AccInrtYV(self, value)
            if ~isequal(self.AccInrtYV, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: AccInrtYV must be a numeric vector!')
                end
                self.AccInrtYV = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = ChassisQrOut(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 11
                superArgs = varargin(1:4);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@ChassisQrLonOut(superArgs{:});
            if nargin == 1
                self.VelXV     = varargin{1}.VelXV;
                self.AccInrtXV = varargin{1}.AccInrtXV;
                self.PosYG     = varargin{1}.PosYG;
                self.VelYG     = varargin{1}.VelYG;
                self.AccYG     = varargin{1}.AccYG;
                self.VelYV     = varargin{1}.VelYV;
                self.AccInrtYV = varargin{1}.AccInrtYV;
            elseif nargin == 11
                self.VelXV     = varargin{5};
                self.AccInrtXV = varargin{6};
                self.PosYG     = varargin{7};
                self.VelYG     = varargin{8};
                self.AccYG     = varargin{9};
                self.VelYV     = varargin{10};
                self.AccInrtYV = varargin{11};
            end
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the quarter vehicle chassis model.
        %
        % [Input]
        %       name - name for the plots
        %
        % [Output]
        %       figGlob  - figure handle of plot containing global cs. quantities
        %       figLoc   - figure handle of plot containing vehicle cs. quantities
        %       figPath  - figure handle of plot containing vehicle path
        % ------------------------------------------------------------------------------------------
        function [figGlob, figLoc, figPath] = DisplayPlots(self, name)
            %% Constants
            FONTSIZE = 8;
            WIDTH = 10;
            HEIGHT = 10;
            WIDTH_PATH = 15;
            HEIGHT_PATH = 10;
            %% Default arguments
            if nargin < 2
                name = class(self);
            end
            
            %% Create plots
            
            % Plot superclass properties
            figGlob = DisplayPlots@ChassisQrLonOut(self, [name, '/GlobalDynamics']);
            
            % Plot own properties
            % --------------------------------------------------------------------------------------
            
                        
            ax = niceaxis(figGlob, FONTSIZE);
            subplot(3, 3, 2, ax);
            plot(ax, self.Time, self.PosYG, 'Color', Colors.BelizeHole);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$y^G \mathrm{[m]}$');
            title(ax, 'global lat. position');
            
            ax = niceaxis(figGlob, FONTSIZE);
            subplot(3, 3, 5, ax);
            plot(ax, self.Time, self.VelYG, 'Color', Colors.BelizeHole);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$v_y^G \mathrm{[m/s]}$');
            title(ax, 'global lat. velocity');
            
            ax = niceaxis(figGlob, FONTSIZE);
            subplot(3, 3, 8, ax);
            plot(ax, self.Time, self.AccYG, 'Color', Colors.BelizeHole);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$a_y^G \mathrm{[m/s^2]}$');
            title(ax, 'global lat. acceleration');
            
            % --------------------------------------------------------------------------------------
            figLoc = nicefigure([name, '/LocalDynamics'], WIDTH, HEIGHT);
            
            ax = niceaxis(figLoc, FONTSIZE);
            subplot(2, 2, 1, ax);
            plot(ax, self.Time, self.VelXV, 'Color', Colors.Amethyst);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$v_y^G \mathrm{[m/s]}$');
            title(ax, 'local lon. velocity');
            
            ax = niceaxis(figLoc, FONTSIZE);
            subplot(2, 2, 3, ax);
            plot(ax, self.Time, self.AccInrtXV, 'Color', Colors.Amethyst);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$a_y^G \mathrm{[m/s^2]}$');
            title(ax, 'local lon. acceleration');
            
            ax = niceaxis(figLoc, FONTSIZE);
            subplot(2, 2, 2, ax);
            plot(ax, self.Time, self.VelYV, 'Color', Colors.Alizarin);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$v_y^G \mathrm{[m/s]}$');
            title(ax, 'local lat. velocity');
            
            ax = niceaxis(figLoc, FONTSIZE);
            subplot(2, 2, 4, ax);
            plot(ax, self.Time, self.AccInrtYV, 'Color', Colors.Alizarin);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$a_y^G \mathrm{[m/s^2]}$');
            title(ax, 'local lat. acceleration');
            
            % --------------------------------------------------------------------------------------
            figPath = nicefigure([name, '/Path'], WIDTH_PATH, HEIGHT_PATH);
            
            ax = niceaxis(figPath, FONTSIZE);
            plot(ax, self.PosXG, self.PosYG, 'Color', Colors.Pomegrante);
            axis(ax, 'equal');
            view(ax, -90, 90);
            xlabel(ax, '$x^G \mathrm{[m]}$');
            ylabel(ax, '$y^G \mathrm{[m]}$');
            title(ax, 'path');
        end
    end
end