%% PlannerCopt ***********************************************************************************
% [Summary]
%   This class represents a constrained nonlinear optimization based motion planner.
%   The planner aims to reach a prescribed end state from the current initial state, while 
%   minimizing the trajectory tracking error and dynamical quantities which cause discomfort. 
%   This happens with the constrained optimization solver Matlab fmincon.
%
% [Used in]
%   user
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef PlannerCopt < PlannerOpt
    
    %% Static methods ==============================================================================
    methods (Static)
        
        %% Property checker methods ----------------------------------------------------------------
        function value = setSolverOptions(value)
            if ~isa(value, 'optim.options.Fmincon')
                error('ERROR: SolverOptions must be an optim.options.Fmincon object!')
            end
        end
   
    end
    
    %% Instance methods ============================================================================
    methods
 
        %% Constructor  ----------------------------------------------------------------------------

        function self = PlannerCopt(varargin)
            self@PlannerOpt(varargin{:});
        end
        
        %% conFcnState ----------------------------------------------------------------------
        % [Summary]
        %   This function calculates and provides state constraint values in format required by 
        %   fmincon.
        %   
        % [Input]
        %   input    - planner input as PlannerCoptIn object
        %   optVarV  - optimization variable in a vector as provided by fsolve
        %   
        % [Output]
        %   neqCon   - nonlinear inequality constraint vector as required by fmincon
        %   eqCon    - nonlinear equality constraint vector as required by fmincon
        % ------------------------------------------------------------------------------------------
        function [neqCon, eqCon] = conFcnState(self, input, optVarV)
            % Update optimized variable
            newOptVarFlag = self.optVar.Update(optVarV);
            % Run only at first time or if optimized variable changed
            if newOptVarFlag || (isempty(self.vehIn) && isempty(self.vehOut))
                % Perform new simulation
                self.predictMotion(input);
            end
            %% Return with updated state constraint vector
            self.conState.Update(self.vehOut.Chassis, true);
            [neqCon, eqCon] = self.conState.ToVector(true);
        end

        %% CalculateTrajectory ---------------------------------------------------------------------
        % [Summary]
        %   This function calculates a vehicle trajectory according to the provided input.
        %
        % [Input]
        %   input  - planner input signals as PlannerCoptIn object
        %   
        % [Output]
        %   output - planner output signals as PlannerCoptOut object
        % ------------------------------------------------------------------------------------------
        function output = CalculateTrajectory(self, input)
            %% Check arguments
            % Serialize input
            input = input(:);
            if ~(isa(input, 'PlannerCoptIn') && isscalar(input))
                error('ERROR: input must be a PlannerCoptIn object!');
            end
            %% Plan trajectory
            % Message to Command Window
            fprintf([repmat('=', 1, 120), '\n']);
            fprintf('Planning a constrained optimal trajectory to x = %s m, y = %s m, r = %s rad, dr = %s rad ...\n', ...
                mat2str(input.FinalCon.PosXG, 4), ...
                mat2str(input.FinalCon.PosYG, 4), ...
                mat2str(input.FinalCon.YawZ, 4), ...
                mat2str(input.FinalCon.YawVelZ, 4));
            % Initialize
            self.conState = PlannerConFcnVehState(input.FinalCon);
            self.optVar = PlannerOptVarVelPathFree(input.OptVarInit);
            self.costTrack = PlannerOptFcnTrack(input.CostTrackWeight);
            self.costState = PlannerOptFcnVehState(input.CostStateWeight);
            % Reset storage for vehicle input and output
            self.vehIn = [];
            self.vehOut = [];
            % Run solver
            optVarInitV = input.OptVarInit.ToVector();
            F = @(optVarV)self.optFcnStateAndTrack(input, optVarV);
            C = @(optVarV)self.conFcnState(input, optVarV);
            tic;
            [optVarOptV, optFcnOpt, exitFlag] = fmincon(F, optVarInitV, [], [], [], [], [], [], C, self.SolverOptions);
            self.optFcnStateAndTrack(input, optVarOptV);
            [neqConOpt, eqConOpt] = self.conFcnState(input, optVarOptV);
            ellapsedTime = toc;
            % 
            if exitFlag > 0
                fprintf('Success.\n');
            else
                fprintf('Failure.\n');
            end
            fprintf('Planning time:          %.4g\n', ellapsedTime);
            fprintf('Equality constraint:    %s\n', mat2str(eqConOpt, 4));
            fprintf('Inequality constraint:  %s\n', mat2str(neqConOpt, 4));
            fprintf('Optimization variable:  %s\n', mat2str(optVarOptV, 4));
            fprintf('Optimization function:  %.4g\n', optFcnOpt);
            %% Set output
            % Update state constraint
            self.conState.Update(self.vehOut.Chassis);
            output = PlannerOptOut(ellapsedTime, exitFlag > 0, self.optVar, ...
                self.vehIn, self.vehOut, self.conState, self.costTrack, self.costState);
        end

    end
end
