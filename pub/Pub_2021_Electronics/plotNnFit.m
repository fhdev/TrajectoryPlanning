% Run vehTestNnFit first! (with MotionDeltasV00, n256l4v1_relu_00050, START_INDEX = 1000; SAMPLE_SIZE = 10000;

H = 2.5 * 7; % cm
W = 7; % cm
F = 8; % pt 
PLOT_PER_FIG = 5;

pldata = {'$\Delta\hat{\dot{x}}^V_v$ (lon. vel.) [1]',        1; ...
          '$\Delta\hat{\dot{y}}^V_v$ (lat. vel.) [1]',        2; ...
          '$\Delta\hat{\dot{\psi}}_v$ (yaw rate) [1]',         3; ...
          '$\Delta\hat{\dot{\rho}}_f$ (f. wheel speed) [1]',4; ...
          '$\Delta\hat{s}_{f,x}$ (f. lon. slip) [1]',       6; ...
          '$\Delta\hat{s}_{f,y}$ (f. lat. slip) [1]',       7; ...
          '$\Delta\hat{\dot{\rho}}_r$ (r. wheel speed) [1]',5; ...
          '$\Delta\hat{s}_{r,x}$ (r. lon. slip) [1]',       8; ...
          '$\Delta\hat{s}_{r,y}$ (r. lat. slip) [1]',       9; ...
          '$\Delta\hat{\delta_f}$ (steering angle) [1]',    10};
indices = START_INDEX : 50 : START_INDEX + SAMPLE_SIZE;

fig(1) = nicefigure('nn', W, H);
fig(2) = nicefigure('nn', W, H);

for iOut = 1 : metadata.NumOutputs
    iFig = floor((pldata{iOut, 2} - 1) / PLOT_PER_FIG) + 1;
    iPlot = floor(mod(pldata{iOut, 2} - 1, PLOT_PER_FIG)) + 1;
    ax(iFig, iPlot) = niceaxis(fig(iFig), F, true);
    subplot(PLOT_PER_FIG, 1, iPlot, ax(iFig, iPlot));
    yyaxis(ax(iFig, iPlot), 'left');
    plot(ax(iFig, iPlot), matOut(indices, iOut),  'Color', nicecolors.BlueDeepKoamaru);
    plot(ax(iFig, iPlot), nnOut(indices, iOut), 'Color', nicecolors.PurpleAmethyst, 'LineWidth', 2,  'LineStyle', ':');
    ax(iFig, iPlot).YAxis(1).Color = nicecolors.BlueDeepKoamaru;
    ax(iFig, iPlot).YAxis(1).Label.String = pldata{iOut, 1};
    yyaxis(ax(iFig, iPlot), 'right');
    plot(ax(iFig, iPlot), matOut(indices, iOut) - nnOut(indices, iOut), 'Color', nicecolors.GreenSea);
    ax(iFig, iPlot).YAxis(2).Color = nicecolors.GreenSea;
    ax(iFig, iPlot).YAxis(2).Label.String = 'pred. error [1]';
    
    xlabel(ax(iFig, iPlot), 'sample [0.001s]');
end
for iFig = 1 : 2
ax(iFig, 1).Position(1) = 0.17;
ax(iFig, 1).Position(2) = 0.81;
ax(iFig, 1).Position(3) = 0.65;

ax(iFig, 2).Position(1) = 0.17;
ax(iFig, 2).Position(2) = 0.62;
ax(iFig, 2).Position(3) = 0.65;

ax(iFig, 3).Position(1) = 0.17;
ax(iFig, 3).Position(2) = 0.43;
ax(iFig, 3).Position(3) = 0.65;

ax(iFig, 4).Position(1) = 0.17;
ax(iFig, 4).Position(2) = 0.24;
ax(iFig, 4).Position(3) = 0.65;

ax(iFig, 5).Position(1) = 0.17;
ax(iFig, 5).Position(2) = 0.05;
ax(iFig, 5).Position(3) = 0.65;

leg = nicelegend({'actual', 'predicted'}, ax(iFig, end), 8);
leg.Orientation = 'horizontal';
end

fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

%% Save plot
name = [NAME, '_', TAG, '_FitNice_1'];
savefig(fig(1), fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig(1), fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig(1), fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

name = [NAME, '_', TAG, '_FitNice_2'];
savefig(fig(2), fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig(2), fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig(2), fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');
