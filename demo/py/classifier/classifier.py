# Dependencies
import numpy as np 
import pandas as pd 
import tensorflow as tf 
import os
import sklearn.preprocessing
import sklearn.model_selection
import sklearn.metrics
import keras
import matplotlib.pyplot as plt

# Read in datasets
datasetName = os.path.join(os.path.dirname(os.path.abspath(__file__)), "train.csv")
dataset = pd.read_csv(datasetName)

# Convert to input and output matrices
X = dataset.iloc[:, :20].values
y = dataset.iloc[:, 20:21].values

# Normalize input
sc = sklearn.preprocessing.StandardScaler()
Xs = sc.fit_transform(X)

# One-Hot encoding of output categories
ohe = sklearn.preprocessing.OneHotEncoder()
ye = ohe.fit_transform(y).toarray()

# Split into random test and train dataset
# specify argument random_state to do the randomization reproducably 
XsTrain, XsTest, yeTrain, yeTest = sklearn.model_selection.train_test_split(Xs, ye, test_size = 0.1)

# Build neural network (fully connected sequation)
model = keras.models.Sequential()
# Input layer + first hidden layer with 16 neurons and 20 inputs
model.add(keras.layers.Dense(16, input_dim=20, activation="relu"))
model.add(keras.layers.Dense(12, activation="relu"))
# Output layer with 4 output neurons as we have 4 output
# Activation functions (see in Keras Docs):
#   softmax:     converts a real vector to a vector of categorical probabilities (for classification problems)
#   relu:        rectified linear unit [max(x, 0)]
model.add(keras.layers.Dense(4, activation="softmax"))

# Specify loss function and optimizer
# Losses [minimization objective function] (see in Keras Docs): 
#   categorical_crossentropy: crossentropy loss between the labels and predictions (for classification problems)
#   mean_squared_error:       mean of squares of errors between the labels and predictions (for regression problems)
# Optimizers (see in Keras Docs):
#   sgd:     gradient descent
#   adam:    stochastic gradient descent with adaptive estimation of first and second order moments
#   Specify learning rates in optimizer constructor.
# Metrics [to judge the performance of the model] (see in Keras Docs): 
#   accuracy:           calculates how often predictions equals labels (for classification problems)
#   mean_squared_error: as before
#   any loss function can be used as metrics as well
optim = keras.optimizers.Adam(learning_rate=0.001)
model.compile(loss = "categorical_crossentropy", optimizer=optim, metrics=["accuracy"])

# Train
# Specify validation data to calculate the metrics in every epoch also to the test data set.
# This way you can avoid overfitting.
history = model.fit(XsTrain, yeTrain, validation_data=(XsTest, yeTest), epochs=100, batch_size=64)

# Save model
modelName = os.path.join(os.path.dirname(os.path.abspath(__file__)), "model.tf")
model.save(modelName)
del model
print("Model saved successfully.")

# Load model
model = keras.models.load_model(modelName)
print("Model loaded successfully.")

# Test accuracy
yePred = model.predict(XsTest)
yPred = np.argmax(yePred, 1)
yTest = np.argmax(yeTest, 1)
accuracy = sklearn.metrics.accuracy_score(yPred, yTest)
print("Test accuracy is: %g" % accuracy)

# Plot results
# Accuracy
plt.plot(history.history["accuracy"])
plt.plot(history.history["val_accuracy"])
plt.title("Model accuracy")
plt.ylabel("Accuracy")
plt.xlabel("Epoch")
plt.legend(["Train", "Test"], loc="upper left")
plt.savefig("accuracy.png")

# Loss
plt.plot(history.history["loss"])
plt.plot(history.history["val_loss"])
plt.title("Model loss")
plt.ylabel("Loss")
plt.xlabel("Epoch")
plt.legend(["Train", "Test"], loc="upper left")
plt.savefig("loss.png")

