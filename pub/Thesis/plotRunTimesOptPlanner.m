% Parameters
W = 7;
H = 4;
F = 8;


% Data
load(fullfile(pwd, 'data', 'Pub_2020_IFACWC2020', 'sampleTrajectories.mat'), 'plannerOpt', 'samplesOpt');
m = length(samplesOpt);
indices_lanechange = 1 : 2 : m;
indices_lanekeep = 2 : 2 : m;

%% Measure run-time
for i = 1 : m
    samplesOpt(i).In.OptVarInit.YRef = [0.1, -0.1];
end

N = 20;

% Measure time
planner = PlannerOpt(PlannerOpt_Default);
% lane change
n = length(indices_lanechange);
t_lanechange = zeros(n, 1);
for i = 1:n
    for j = 1 : 2
        out = planner.CalculateTrajectory(PlannerOptIn(samplesOpt(indices_lanechange(i)).In));
    end
    for j = 1 : N
        out = planner.CalculateTrajectory(PlannerOptIn(samplesOpt(indices_lanechange(i)).In));
        t_lanechange(i) = t_lanechange(i) + out.CalcTime;
    end
    t_lanechange(i) = t_lanechange(i) / N;
end
% lane keep
n = length(indices_lanekeep);
t_lanekeep = zeros(n, 1);
for i = 1:n
    for j = 1 : 2
        out = planner.CalculateTrajectory(PlannerOptIn(samplesOpt(indices_lanekeep(i)).In));
    end
    for j = 1 : N
        out = planner.CalculateTrajectory(PlannerOptIn(samplesOpt(indices_lanekeep(i)).In));
        t_lanekeep(i) = t_lanekeep(i) + out.CalcTime;
    end
    t_lanekeep(i) = t_lanekeep(i) / N;
end

%% Plot
name = 'RuntimesOptPlanner';

% Plot
fig1 = nicefigure(name, W, H);
ax1 = niceaxis(fig1, F);
hold(ax1, 'on');
plot(ax1, 1 : length(t_lanechange), t_lanechange * 1000, 'Color', nicecolors.PurpleAmethyst, 'MarkerSize', 3.0, 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.PurpleAmethyst, 'MarkerFaceColor', nicecolors.PurpleAmethyst);
plot(ax1, 1 : length(t_lanekeep), t_lanekeep * 1000, 'Color', nicecolors.OrangePumpkin,  'MarkerSize', 3.0, 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.OrangePumpkin, 'MarkerFaceColor', nicecolors.OrangePumpkin);
xlabel(ax1, '$i$ (no. of trajectory) [1]');
ylabel(ax1, '$t_r$ (run-time) [ms]');
l = nicelegend({'lane change', 'lane keep'}, ax1, F);

savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');