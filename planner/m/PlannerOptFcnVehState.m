%% PlannerOptFcnVehState ********************************************************************************
% [Summary]
%   This class represents state related cost function for PlannerOpt and PlannerCopt motion 
%   planners. State related cost function is the weighted sum of average absolute state values
%   along the trajectory. 
%
% [Used in]
%   PlannerOpt
%   PlannerCopt
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef PlannerOptFcnVehState < handle
    
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % proeprty names of ChassisStOut
        propNames
        
        %% Properties set in constructor -----------------------------------------------------------
        % cost function weights
        Weight
        % actual average absolute state values along trajectory
        Value
    end

    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        
        function set.Weight(self, value)
            if ~isequal(self.Weight, value)
                if isstruct(value)
                    value = ChassisStOut(value);
                end
                if ~isa(value, 'ChassisStOut')
                    error('ERROR: Weight must be a ChassisStOut object!');
                end
                self.Weight = value;
            end
        end
        
        function set.Value(self, value)
            if ~isequal(self.Value, value)
                if isstruct(value)
                    value = ChassisStOut(value);
                end
                if ~isa(value, 'ChassisStOut')
                    error('ERROR: Value must be a ChassisStOut object!');
                end
                self.Value = value;
            end
        end
                
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerOptFcnVehState(weight)
            %% Check arguments
            if ~(isa(weight, 'ChassisStOut'))
                error('ERROR: finalCon must be a ChassisStOut object!');
            end
            %% Initialize
            self.Weight = ChassisStOut(weight);
            nulls = repmat({0}, 1, 14);
            self.Value = ChassisStOut(nulls{:});
            self.propNames = properties('ChassisStOut');
        end
        
        %% Update ----------------------------------------------------------------------------------
        % [Summary]
        %   This function updates state state related cost values according to given vehicle 
        %   simulation output.
        %
        % [Input]
        %   chassisOut - output of current vehicle simulation as ChassisStOut object
        %
        % [Output]
        %   none
        % ------------------------------------------------------------------------------------------
        function Update(self, chassisOut)
            %% Check arguments
            if ~(isa(chassisOut, 'ChassisStOut'))
                error('ERROR: chassisOut most be a ChassisStOut object!');
            end
            %% Calculate state constraints
            for iProp = 1 : length(self.propNames)
                % There is weight specified
                if ~isnan(self.Weight.(self.propNames{iProp}))
                    % Calculate average value along path
                    self.Value.(self.propNames{iProp}) = ...
                        sqrt(trapz(chassisOut.Time, chassisOut.(self.propNames{iProp}).^2)) / ...
                        (chassisOut.Time(end) - chassisOut.Time(1));                
                else
                    self.Value.(self.propNames{iProp}) = nan;                    
                end
            end
        end
        
        %% ToScalar --------------------------------------------------------------------------------
        % [Summary]
        %   This method provides scalar cost value as the weighted sum of average absolute state
        %   values.
        %
        % [Input]
        %   none
        %
        % [Output]
        %   cost - scalar cost value
        % ------------------------------------------------------------------------------------------   
        function cost = ToScalar(self)
            cost = 0;
            for iProp = 1 : length(self.propNames)
                % There is weight specified
                if ~isnan(self.Weight.(self.propNames{iProp}))
                    cost = cost + self.Weight.(self.propNames{iProp}) * self.Value.(self.propNames{iProp});
                end
            end
        end

    end
end
