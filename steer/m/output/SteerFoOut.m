%% SteerFoOut **************************************************************************************
% [Summary]
%   This class represets outputs of a simple steering model which is modelled as a first order 
%   transfer function. 
%
% [Used in]
%   VehStOlOut
%
% [Subclasses]
%   none
%
% **************************************************************************************************
classdef SteerFoOut < ComTimeInOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Wheel level steering angle [rad]
        SteerAng
        % Derivative of wheel level steering angle [rad]
        DSteerAng

    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.SteerAng(self, value)
            if ~isequal(self.SteerAng, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: SteerAng must be a numeric vector!')
                end
                self.SteerAng = value(:);
            end            
        end
        
        function set.DSteerAng(self, value)
            if ~isequal(self.DSteerAng, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: DSteerAng must be a numeric vector!')
                end
                self.DSteerAng = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = SteerFoOut(varargin)
            if nargin == 1
                comTimeInArgs = varargin(1);
            elseif nargin == 3
                comTimeInArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
           self@ComTimeInOut(comTimeInArgs{:});
            if nargin == 1
                self.SteerAng  = varargin{1}.SteerAng;
                self.DSteerAng = varargin{1}.VelXG;
            elseif nargin == 3
                self.SteerAng  = varargin{2};
                self.DSteerAng = varargin{3};
            end
        end
        
    end
end