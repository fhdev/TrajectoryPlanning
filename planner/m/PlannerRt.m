classdef PlannerRt < handle
    
    properties (Constant)
        
        LAT_REF = 46.895038; % [�]
        LON_REF = 16.843526; % [�]
        
        CANCASE_RX_CH_NUM = 1;
        CANCASE_TX_CH_NUM = 1;
        VIRTUAL_RX_CH_NUM = 1;
        VIRTUAL_TX_CH_NUM = 1;
        
        N_SIGNALS = 20;
        
        SECONDS_BEFORE_ACC = 8;
        
    end
    
    properties (Access = public)
        
        %% CAN
        % Transmit channels 
        channelRxImar
        channelRxImu
        channelRxVBox
        % Receive channels
        channelTxSmart
        % CAN databases
        dbcImar
        dbcImu
        dbcVbox
        dbcSmart
        % CAN messages
        msgCtrl
        
        %% Timer
        timerPlanner
        timerCtrl
        cntPlanner
        cntCtrl
        
        %% Control
        
        % Timing
        tStartAbs
        tStartManAbs
        tActAbs
        tActRel
        
        % Is maneuver running?
        maneuverOn
        started
        
        % Temp state and output storage
        S
        t
        
        %% Trajectory
        plannerIn
        plannerOut
        
    end
    
    properties
        
        Planner
         
        SampleTimeCtrl
        
        SampleTimePlanner
        
        MaxRunTime
        
        VirtualCan
                
        Wheel2SteerWheel
        
    end
    
    methods
        
        function set.Planner(self, value)
            if ~isequal(self.Planner, value)
                if isstruct(value)
                    switch value.Type
                        case 'PlannerCon'
                            value = PlannerCon(value);
                        case 'PlannerOpt'
                            value = PlannerOpt(value);
                        case 'PlannerCopt'
                            value = PlannerCopt(value);
                    end
                end
                if ~(isa(value, 'PlannerCon') || isa(value, 'PlannerOpt') || isa(value, 'PlannerCopt'))
                    error('ERROR: Planner must be a PlannerCon, PlannerOpt or PlannerCopt object!');
                end
                self.Planner = value;
            end
        end
        
        function set.SampleTimeCtrl(self, value)
            if ~isequal(self.SampleTimeCtrl, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SampleTimeCtrl must be a numeric scalar!')
                end
                self.SampleTimeCtrl = value;
            end
        end
         
        function set.SampleTimePlanner(self, value)
            if ~isequal(self.SampleTimePlanner, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SampleTimePlanner must be a numeric scalar!')
                end
                self.SampleTimePlanner = value;
            end
        end
        
        function set.MaxRunTime(self, value)
            if ~isequal(self.MaxRunTime, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MaxRunTime must be a numeric scalar!')
                end
                self.MaxRunTime = value;
            end
        end
        
        function set.VirtualCan(self, value)
            if ~isequal(self.VirtualCan, value)
                if ~(islogical(value) && isscalar(value))
                    error('ERROR: VirtualCan must be a logical scalar!')
                end
                self.VirtualCan = value;
            end
        end
        
        function set.Wheel2SteerWheel(self, value)
            if ~isequal(self.Wheel2SteerWheel, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: Wheel2SteerWheel must be a numeric scalar!')
                end
                self.Wheel2SteerWheel = value;
            end
        end
        
        function self = PlannerRt(varargin)
            if nargin == 1
                self.Planner           = varargin{1}.Planner;
                self.SampleTimeCtrl    = varargin{1}.SampleTimeCtrl;
                self.SampleTimePlanner = varargin{1}.SampleTimePlanner;
                self.MaxRunTime        = varargin{1}.MaxRunTime;
                self.VirtualCan        = varargin{1}.VirtualCan;
                self.Wheel2SteerWheel  = varargin{1}.Wheel2SteerWheel;
            elseif nargin == 6
                self.Planner           = varargin{1};
                self.SampleTimeCtrl    = varargin{2};
                self.SampleTimePlanner = varargin{3};
                self.MaxRunTime        = varargin{4};
                self.VirtualCan        = varargin{5};
                self.Wheel2SteerWheel  = varargin{6};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
                        
            assert(mod(self.MaxRunTime / self.SampleTimeCtrl, 1) == 0, ...
                'Maximal run time must be an integer multiple of contoller time step!');
            assert(mod(self.MaxRunTime / self.SampleTimePlanner, 1) == 0, ...
                'Maximal run time must be an integer multiple of planner time step!')
            
            self.S = nan(self.N_SIGNALS, self.MaxRunTime / self.SampleTimeCtrl + 1);
            self.t = nan(7, self.MaxRunTime / self.SampleTimeCtrl + 1);
            self.S(:, 1) = 0;
            self.t(:, 1) = 0;

            
            self.cntCtrl = 1;
            self.cntPlanner = 1;
            self.maneuverOn = false;
            
            self.InitializeCAN();
                        
            self.timerCtrl = timer('Name', 'TimerCtrl', ...
                'BusyMode', 'drop', ...
                'ExecutionMode', 'fixedRate', ...
                'Period', self.SampleTimeCtrl, ...
                'TasksToExecute', self.MaxRunTime / self.SampleTimeCtrl, ...
                'TimerFcn', @self.TimerFcnCtrl, ...
                'StopFcn', @self.TimerStopFcnCtrl);
            
            self.timerPlanner = timer('Name', 'TimerPlanner', ...
                'BusyMode', 'drop', ...
                'ExecutionMode', 'fixedRate', ...
                'Period', self.SampleTimeCtrl, ...
                'TasksToExecute', self.MaxRunTime / self.SampleTimePlanner, ...
                'TimerFcn', @self.TimerFcnPlanner);

            
        end
        
        function InitializeCAN(self)
            
            %% Create CAN channels
            
            if self.VirtualCan
                device = 'Virtual 1';
                % Receive channels
                self.channelRxImar  = canChannel('Vector', device, self.VIRTUAL_TX_CH_NUM);
                self.channelRxImu   = canChannel('Vector', device, self.VIRTUAL_TX_CH_NUM);
                self.channelRxVBox  = canChannel('Vector', device, self.VIRTUAL_TX_CH_NUM);
                % Transmit channels
                self.channelTxSmart = canChannel('Vector', device, self.VIRTUAL_RX_CH_NUM);
            else
                device = 'CANcaseXL 1';
                % Receive channels
                self.channelRxImar  = canChannel('Vector', device, self.CANCASE_TX_CH_NUM);
                self.channelRxImu   = canChannel('Vector', device, self.CANCASE_TX_CH_NUM);
                self.channelRxVBox  = canChannel('Vector', device, self.CANCASE_TX_CH_NUM);
                % Transmit channels
                self.channelTxSmart = canChannel('Vector', device, self.CANCASE_RX_CH_NUM);
            end
            
            %% Load dbc
            self.dbcImar  = canDatabase('imar.dbc');
            self.dbcImu   = canDatabase('MM5_10_Default.dbc');
            self.dbcVbox  = canDatabase('VBOX3iSL_ADAS_VCI_VEHICO.dbc');
            self.dbcSmart = canDatabase('smart_control.dbc');
            
            %% Assign database to channels   
            % Receive channels
            self.channelRxImar.Database  = self.dbcImar;
            self.channelRxImu.Database   = self.dbcImu;
            self.channelRxVBox.Database  = self.dbcVbox;
            % Transmit channels
            self.channelTxSmart.Database = self.dbcSmart;
            
            %% Filter necessary data
            filterBlockAll(self.channelRxImar, 'Standard');
            filterAllowOnly(self.channelRxImar, {'Attitude', 'Body_Velocity', 'INS_Latitude', 'INS_Longitude'});
            % Accelerations VBOX
            filterBlockAll(self.channelRxVBox, 'Extended');
            filterAllowOnly(self.channelRxVBox, 'VBOX_4');
            % Accelerations and angular rates
            filterAllowOnly(self.channelRxImu, {'TX1', 'TX2', 'TX3'});
            filterBlockAll(self.channelRxImu, 'Extended');
            % Control signals
            filterAllowOnly(self.channelTxSmart, 'Control_message');
            filterBlockAll(self.channelTxSmart, 'Extended');
            
            %% Create CAN messages
            self.msgCtrl = canMessage(self.dbcSmart, 'Control_message');
            
        end
        
        function Start(self, input)
            % Check arguments
            if isa(self.Planner, 'PlannerCon') && ~isa(input, 'PlannerConIn')
                error('ERROR: input must be a PlannerConIn object if planner is PlannerCon!');
            end
            if isa(self.Planner, 'PlannerOpt') && ~isa(input, 'PlannerOptIn')
                error('ERROR: input must be a PlannerOptIn object if planner is PlannerOpt!');
            end
            if isa(self.Planner, 'PlannerCopt') && ~isa(input, 'PlannerCoptIn')
                error('ERROR: input must be a PlannerCoptIn object if planner is PlannerCopt!');
            end
            % Reset counters and flags
            self.cntCtrl = 1;
            self.cntPlanner = 1;
            self.maneuverOn = false;
            self.started = false;
            % Plan trajectory
            self.plannerIn  = input;
            self.plannerOut = self.Planner.CalculateTrajectory(self.plannerIn);
            % Reset signals
            self.S(:, 1) = 0;
            self.S(:, 2:end) = nan;
            self.t(:, 1) = 0;
            self.t(:, 2:end) = nan;
            % Close plot
            close(gcf);
            % Open plot
            figure('Color', 'White', 'Unit', 'Centimeters', 'Position', [2, 2, 18, 18]);
            plot([0, 10], [0, 10], 'gx');
            xlabel('X [m]');
            ylabel('Y [m]');
            axis equal;
            drawnow();
            % Start CAN channels
            if ~self.channelRxImar.Running
                start(self.channelRxImar);
            end
            if ~self.channelRxVBox.Running
                start(self.channelRxVBox);
            end
            if ~self.channelRxImu.Running
                start(self.channelRxImu);
            end
            if ~self.channelTxSmart.Running
                start(self.channelTxSmart);
            end
            % Clear buffer and send out zero command
            receive(self.channelRxImar, Inf);
            receive(self.channelRxImu, Inf);
            receive(self.channelRxVBox, Inf);
            self.OutputProvider(0, 0);
            % Clear command window
            clc();
            % Start timer
            start(self.timerCtrl);
        end
        
        function [S, t] = Stop(self)
            % Stop timers
            stop(self.timerCtrl);
            stop(self.timerPlanner);
            
            % Clear buffer
            receive(self.channelRxImar, Inf);
            receive(self.channelRxImu, Inf);
            receive(self.channelRxVBox, Inf);
            
            % Stop CAN channels
            if self.channelRxImar.Running
                stop(self.channelRxImar);
            end
            if self.channelRxImu.Running
                stop(self.channelRxImu);
            end
            if self.channelRxVBox.Running
                stop(self.channelRxVBox);
            end
            if self.channelTxSmart.Running
                stop(self.channelTxSmart);
            end
            
            % Output
            S = self.S;
            t = self.t;
            
        end
        
        function OutputProvider(self, speed, steerAng)
            % [m/s] -> [km/h]
            self.msgCtrl.Signals.Speed_demand = speed * 3.6; 
            self.S(5, self.cntCtrl + 1) = self.msgCtrl.Signals.Speed_demand;
            % wheel level [rad] -> steering wheel level [deg]
            self.msgCtrl.Signals.Steering_wheel_angle = rad2deg(-steerAng) * self.Wheel2SteerWheel;
            self.S(6, self.cntCtrl + 1) = self.msgCtrl.Signals.Steering_wheel_angle;
            % Transmit messages
            try
                transmit(self.channelTxSmart, self.msgCtrl);
            catch ex
                fprintf('Transmission error in OutputProvider! Cause: %s\n', ex.message);
            end
        end
        
        function [Uctrl, start] = InputProvider(self, speed)
            % U(1) => PosXG:    longitudinal position in ground-fixed coordinate system [m]
            % U(2) => PosYG:    lateral position in ground-fixed coordinate system [m]
            % U(3) => YawZ:     yaw (heading) angle in ground-fixed coordinate system [rad]
            % U(4) => VelXV:    longitudinal velocity in vehicle-fixed cs. [m/s]
            Uctrl = zeros(4, 1);
            start = false;
            
            msgAllImar  = receive(self.channelRxImar, Inf);
            msgAllVBox  = receive(self.channelRxVBox, Inf);
            msgAllImu   = receive(self.channelRxImu, Inf);
            
            msgAttitude = extractRecent(msgAllImar, 'Attitude');
            msgBodyVel  = extractRecent(msgAllImar, 'Body_Velocity');
            msgLat      = extractRecent(msgAllImar, 'INS_Latitude');
            msgLon      = extractRecent(msgAllImar, 'INS_Longitude');
            
            msgAccVBox  = extractRecent(msgAllVBox, 'VBOX_4');
            msgImuTx1 = extractRecent(msgAllImu, 'TX1');
            msgImuTx2 = extractRecent(msgAllImu, 'TX2');
            msgImuTx3 = extractRecent(msgAllImu, 'TX3');
            
            % Positions
            if ~isempty(msgLat) && ~isempty(msgLon)
                flat = lla2flat([msgLat.Signals.Latitude, msgLon.Signals.Longitude, 0], [self.LAT_REF, self.LON_REF], 0, 0);
                Uctrl(1:2) = flat(1:2);
            else
                Uctrl(1:2) = self.S(1:2, self.cntCtrl);
            end
            self.S(1:2, self.cntCtrl + 1) = Uctrl(1:2);
            
            % Heading
            if ~isempty(msgAttitude)
                heading = msgAttitude.Signals.Yaw;
%                 heading = heading + 90;
%                 if heading < 0
%                     heading = heading + 360;
%                 end
                %heading = heading - 90;
                Uctrl(3) = deg2rad(heading); % [�] -> [rad] 
            else
                Uctrl(3) = self.S(3, self.cntCtrl);
            end
            self.S(3, self.cntCtrl + 1) = Uctrl(3);
            
            % Speed
            if ~isempty(msgBodyVel)
                Uctrl(4) = msgBodyVel.Signals.V_X / 3.6; % [km/h] -> [m/s]
                if abs(Uctrl(4) - speed) < 0.5
                    start = true;
                end
            else
                Uctrl(4) = self.S(4, self.cntCtrl);
            end
            self.S(4, self.cntCtrl  + 1) = Uctrl(4);
            
            % Accelerations VBOX
            if ~isempty(msgAccVBox)
                ax = msgAccVBox.Signals.Longitudinal_acceleration;
                ay = msgAccVBox.Signals.Lateral_acceleration;
            else
                ax = self.S(9, self.cntCtrl);
                ay = self.S(10, self.cntCtrl);
            end
            self.S(9, self.cntCtrl + 1) = ax;
            self.S(10, self.cntCtrl + 1) = ay;
            
            % Accelerations and turn rates MM5        
            if ~isempty(msgImuTx1)
                ay = msgImuTx1.Signals.ACC_F_Y * 9.81;
                oz = deg2rad(msgImuTx1.Signals.OMEGA_F_Z);   
            else
                ay = self.S(11, self.cntCtrl);
                oz = self.S(12, self.cntCtrl);
            end
            self.S(11, self.cntCtrl + 1) = ay;
            self.S(12, self.cntCtrl + 1) = oz;
            
            if ~isempty(msgImuTx2)
                ax = msgImuTx2.Signals.ACC_F_X * 9.81;
                ox = deg2rad(msgImuTx2.Signals.OMEGA_F_X);
            else
                ax = self.S(13, self.cntCtrl);
                ox = self.S(14, self.cntCtrl);
                
            end
            self.S(13, self.cntCtrl + 1) = ax;
            self.S(14, self.cntCtrl + 1) = ox;
            
            if ~isempty(msgImuTx3)
                az = msgImuTx3.Signals.ACC_F_Z * 9.81;
                oy = deg2rad(msgImuTx3.Signals.OMEGA_F_Y);
            else
                az = self.S(15, self.cntCtrl);
                oy = self.S(16, self.cntCtrl);
            end
            self.S(15, self.cntCtrl + 1) = az;
            self.S(16, self.cntCtrl + 1) = oy;
            
        end
  
        function TimerFcnCtrl2(self, obj, event) %#ok<INUSD>
            if self.cntCtrl < 400
                self.OutputProvider(0, 0);
            elseif self.cntCtrl < 800
                self.OutputProvider(20/3.6, 0);
            elseif self.cntCtrl < 1000
                self.OutputProvider(20/3.6, deg2rad(5));
            end
            self.cntCtrl = self.cntCtrl + 1;
        end
        
        function TimerFcnCtrl(self, obj, event) %#ok<INUSD>
            self.S(17, self.cntCtrl + 1) = self.cntCtrl;
            try
                
                %% Base task
                % Set absolute time of start
                if self.cntCtrl == 1
                    % start absolute time
                    self.tStartAbs = clock();
                    self.tStartManAbs  = clock();
                    self.tActRel = 0;
                end
                if self.maneuverOn
                    % Get input
                    Uctrl = self.InputProvider(self.plannerIn.OptVarInit.VelXVRef);
                    % Set and store absolute time
                    self.tActAbs = clock();
                    self.t(1:6, self.cntCtrl + 1) = self.tActAbs';
                    % Transform trajectory
                    if ~self.started
                        % set controller reference
                        sinYaw = sin(Uctrl(3));
                        cosYaw = cos(Uctrl(3));
                        self.Planner.Vehicle.CtrlLat.PosXGRef = + cosYaw * self.plannerOut.VehIn.PosXGRef ...
                            - sinYaw * self.plannerOut.VehIn.PosYGRef + Uctrl(1);
                        self.Planner.Vehicle.CtrlLat.PosYGRef = + sinYaw * self.plannerOut.VehIn.PosXGRef ...
                            + cosYaw * self.plannerOut.VehIn.PosYGRef + Uctrl(2);
                        self.Planner.Vehicle.CtrlLat.YawZRef = self.plannerOut.VehIn.YawZRef + Uctrl(3);
                        % Set time
                        self.tStartManAbs = self.tActAbs;
                        % A maneuver has been started
                        self.started = true;
                    end
                    % Set and store relative time
                    self.tActRel = etime(self.tActAbs, self.tStartAbs);
                    self.t(7, self.cntCtrl + 1) = self.tActRel;
                    % run control loop
                    [~, Yctrl] = self.Planner.Vehicle.CtrlLat.StateEquation(self.tActRel, 1, Uctrl);
                    self.OutputProvider(self.plannerIn.OptVarInit.VelXVRef, Yctrl(1));
                    self.S(7, self.cntCtrl + 1) = Yctrl(2);
                    self.S(8, self.cntCtrl + 1) = Yctrl(3);
                    % trajectory is over
                    if etime(self.tActAbs, self.tStartManAbs) > self.plannerOut.OptVar.TravelTime
                        self.maneuverOn = false;
                    end
                else
                    % Get input
                    [~, startIt] = self.InputProvider(self.plannerIn.OptVarInit.VelXVRef);
                    % Set and store absolute time
                    self.tActAbs = clock();
                    self.t(1:6, self.cntCtrl + 1) = self.tActAbs';
                    % Set and store relative time
                    self.tActRel = etime(self.tActAbs, self.tStartAbs);
                    self.t(7, self.cntCtrl + 1) = self.tActRel;
                    % Start maneuver if ready
                    if startIt && ~self.started
                        self.maneuverOn = true;
                    end
                    % Set output
                    if self.cntCtrl < floor(self.SECONDS_BEFORE_ACC * (1 / self.SampleTimeCtrl))
                        self.OutputProvider(0, 0);
                    else
                        self.OutputProvider(self.plannerIn.OptVarInit.VelXVRef, 0);
                    end
                    self.S(7, self.cntCtrl + 1) = 0;
                    self.S(8, self.cntCtrl + 1) = 0;
                end
                %% Task 10x
                if mod(self.cntCtrl, 10) == 0
                    fprintf('C = %8.4f [s] | t = %8.4f [s] | V = %8.4f / %8.4f [km/h] | Yr = %8.4f [deg/s] | X = %9.4f [m] | Y = %9.4f [m] | Hd = %9.4f [deg] | St= %8.4f [deg]\n', ...
                        self.cntCtrl * self.SampleTimeCtrl, ...
                        self.t(7, self.cntCtrl), ...
                        self.S(4, self.cntCtrl) * 3.6, self.S(5, self.cntCtrl), ...
                        0, ...
                        self.S(1, self.cntCtrl), ...
                        self.S(2, self.cntCtrl), ...
                        rad2deg(self.S(3, self.cntCtrl)), ...
                        self.S(6, self.cntCtrl));
                end
                %% Task 25x
                if mod(self.cntCtrl, 25) == 0
                    self.DebugPlotRt();
                end
                
                self.cntCtrl = self.cntCtrl + 1;
            catch ex
                fprintf('Exception in TimerFcnCtrl. Cause: %s. Location: %s line %d.\n', ...
                    ex.message, ex.stack(1).name, ex.stack(1).line);
            end
        end
        
        function TimerStopFcnCtrl(self, obj, event) %#ok<INUSD>
            % Clear buffer
            receive(self.channelRxImar, Inf);
            receive(self.channelRxVBox, Inf);
            receive(self.channelRxImu, Inf);
            % Stop CAN channels
            if self.channelRxImar.Running
                stop(self.channelRxImar);
            end
            if self.channelRxVBox.Running
                stop(self.channelRxVBox);
            end
            if self.channelRxImu.Running
                stop(self.channelRxImu);
            end
            if self.channelTxSmart.Running
                stop(self.channelTxSmart);
            end
            
            % Save everyting
            fileName = [datestr(now, 'yyyymmdd_hhMMss'), '.mat'];
            if self.VirtualCan
                fileName = ['SIM_', fileName];
            else
                fileName = ['MEAS_', fileName];
            end
            save(fullfile('.', '_data', fileName), 'self');
            
        end
        
        function DebugPlot(self)
            figure('Color', 'White', 'Unit', 'Centimeters', 'Position', [2, 2, 18, 18]);
            hold on;
            plot(self.Planner.Vehicle.CtrlLat.PosXGRef, self.Planner.Vehicle.CtrlLat.PosYGRef, 'r-', 'LineWidth', 2);
            plot(self.S(1, 2:end), self.S(2, 2:end), 'b-');
            legend({'planned', 'actual'});
            axis equal;
            xlabel('X [m]');
            ylabel('Y [m]');
            
            figure('Color', 'White', 'Unit', 'Centimeters', 'Position', [2, 2, 18, 9]);
            hold on;
            plot(self.t(end, 2:end), self.S(6, 2:end), 'r-');
            xlabel('t [s]');
            ylabel('steering wheel angle [deg]');
            
            figure('Color', 'White', 'Unit', 'Centimeters', 'Position', [2, 2, 18, 9]);
            hold on;
            plot(self.t(end, 2:end), self.S(7, 2:end) * 100, 'r-');
            plot(self.t(end, 2:end), rad2deg(self.S(8, 2:end)), 'b-');
            xlabel('t [s]');
            ylabel('tracking errors [cm] / [deg]');
            legend({'cross track error [cm]', 'heading error [deg]'});
        end
        
        function DebugPlotRt(self)
            if self.maneuverOn
                plot(self.Planner.Vehicle.CtrlLat.PosXGRef, self.Planner.Vehicle.CtrlLat.PosYGRef, 'r-', 'LineWidth', 2);
            end
            plot(self.S(1, self.cntCtrl), self.S(2, self.cntCtrl), 'bo');
            axis equal;
            hold on;
            drawnow();
        end
        
        function TimerFcnPlanner(self, obj, event) %#ok<INUSD>
%             self.plannerOut = planner.CalculateTrajectory(self.plannerIn);
%             self.cntPlanner = self.cntPlanner + 1;
        end
        
    end
    
end
