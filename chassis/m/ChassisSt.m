%% ChassisSt ***************************************************************************************
% [Summary]
%   This class represents a chassis coupled to a planar single track vehicle model.
%
% [Used in]
%   VehQr
%
% [Subclasses]
%   ChassisSt
%
% **************************************************************************************************
classdef ChassisSt < ChassisQr
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------        
        % moment of inertia about vertical axis [kgm^2]
        MomentOfIntertiaZ
        % horizontal distance from front wheel center point to center of gravity [m]
        DistanceFrontCOG
        % horizontal distance from rear wheel center point to center of gravity [m]
        DistanceRearCOG
        % center of gravity height [m]
        HeightCOG
        % aerodynamic drag coefficient [1]
        DragCoefficient
        % frontal area [m^2]
        FrontalArea
        % driving torque distribution between front and rear axles [1]
        DrivingTorqueSplit
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.MomentOfIntertiaZ(self, value)
            if ~isequal(self.MomentOfIntertiaZ, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MomentOfIntertiaZ must be a numeric scalar!')
                end
                self.MomentOfIntertiaZ = value;
            end
        end
        
        function set.DistanceFrontCOG(self, value)
            if ~isequal(self.DistanceFrontCOG, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DistanceFrontCOG must be a numeric scalar!')
                end
                self.DistanceFrontCOG = value;
            end
        end
        
        function set.DistanceRearCOG(self, value)
            if ~isequal(self.DistanceRearCOG, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DistanceRearCOG must be a numeric scalar!')
                end
                self.DistanceRearCOG = value;
            end
        end
        
        function set.HeightCOG(self, value)
            if ~isequal(self.HeightCOG, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: HeightCOG must be a numeric scalar!')
                end
                self.HeightCOG = value;
            end
        end
        
        function set.DragCoefficient(self, value)
            if ~isequal(self.DragCoefficient, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DragCoefficient must be a numeric scalar!')
                end
                self.DragCoefficient = value;
            end
        end
        
        function set.FrontalArea(self, value)
            if ~isequal(self.FrontalArea, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: FrontalArea must be a numeric scalar!')
                end
                self.FrontalArea = value;
            end
        end
        
        function set.DrivingTorqueSplit(self, value)
            if ~isequal(self.DrivingTorqueSplit, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DrivingTorqueSplit must be a numeric scalar!')
                end
                self.DrivingTorqueSplit = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = ChassisSt(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 8
                superArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@ChassisQr(superArgs{:});
            if nargin == 1
                self.MomentOfIntertiaZ  = varargin{1}.MomentOfIntertiaZ;
                self.DistanceFrontCOG   = varargin{1}.DistanceFrontCOG;
                self.DistanceRearCOG    = varargin{1}.DistanceRearCOG;
                self.HeightCOG          = varargin{1}.HeightCOG;
                self.DragCoefficient    = varargin{1}.DragCoefficient;
                self.FrontalArea        = varargin{1}.FrontalArea;
                self.DrivingTorqueSplit = varargin{1}.DrivingTorqueSplit;
            elseif nargin == 8
                self.MomentOfIntertiaZ  = varargin{2};
                self.DistanceFrontCOG   = varargin{3};
                self.DistanceRearCOG    = varargin{4};
                self.HeightCOG          = varargin{5};
                self.DragCoefficient    = varargin{6};
                self.FrontalArea        = varargin{7};
                self.DrivingTorqueSplit = varargin{8};
            end
        end
        
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of chassis coupled to a planar single track 
        %   vehicle model.
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => PosXG:   longitudinal position in ground-fixed coordinate system [m]
        %       X(2) => VelXG:   longitudinal velocity in ground-fixed coordinate system [m/s]
        %       X(3) => PosYG:   lateral position in ground-fixed coordinate system [m]
        %       X(4) => VelYG:   lateral velocity in ground-fixed coordinate system [m/s]
        %       X(5) => YawZ:    yaw (heading) angle in ground-fixed coordinate system [rad]
        %       X(6) => YawVelZ: yaw (heading) angular velocity in ground-fixed system [rad/s]
        %
        %   U - input vector
        %       U(1) => ForceFrontXV: longitudinal front tire force in vehicle-fixed coordinate system [N]
        %       U(2) => ForceFrontYV: lateral front tire force in vehicle-fixed coordinate system [N]
        %       U(3) => ForceRearXV:  longitudinal rear tire force in vehicle-fixed coordinate system [N]
        %       U(4) => ForceRearYV:  lateral rear tire force in vehicle-fixed coordinate system [N]
        %       U(5) => VelXV:        longitudinal velocity in vehicle-fixed coordintate system [m/s]
        %       U(6) => VelYV:        lateral velocity in vehicle-fixed coordintate system [m/s]
        %       U(7) => CosYawV:      cosine of yaw (heading) angle in ground-fixed coordinate system [1]
        %       U(8) => SinYawV:      sine of yaw (heading) angle in ground-fixed coordinate system [1]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %       Y(1) => AccXG: longitudinal acceleration in ground-fixed cs. [m/s^2]
        %       Y(2) => AccYG: lateral acceleration in ground-fixed cs. [m/s^2]
        %       Y(3) => AccInrtXV: longitudinal inertial acceleration in vehicle-fixed cs. [m/s^2]
        %       Y(4) => AccInrtYV: lateral inertial acceleration in vehicle-fixed cs. [m/s^2]
        %       Y(5) => YawAccZ: yaw (heading) angular acceleration in ground-fixed system [rad/s^2]
        % ------------------------------------------------------------------------------------------        
        function [dX, Y] = StateEquation(self, ~, X, U)
            %% Calculation of necessary variables
            % Tire forces in ground-fixed coordinate system
            forceFrontXG = + U(7) * U(1) - U(8) * U(2);
            forceFrontYG = + U(8) * U(1) + U(7) * U(2);
            forceRearXG = + U(7) * U(3) - U(8) * U(4);
            forceRearYG = + U(8) * U(3) + U(7) * U(4);
            % Aerodynamic drag force in vehicle-fixed and then in ground-fixed coordinate system
            velAbs = sqrt(U(5)^2 + U(6)^2);
            forceDragXV = 0.5 * self.DragCoefficient * self.FrontalArea * Com.AirDensity * U(5) * velAbs;
            forceDragYV = 0.5 * self.DragCoefficient * self.FrontalArea * Com.AirDensity * U(6) * velAbs;
            forceDragXG = + U(7) * forceDragXV - U(8) * forceDragYV;
            forceDragYG = + U(8) * forceDragXV + U(7) * forceDragYV;
            %% Calculation of state derivatives
            % Principle of linear momentum for the chassis
            accXG = (forceFrontXG + forceRearXG - forceDragXG) / self.Mass;
            accYG = (forceFrontYG + forceRearYG - forceDragYG) / self.Mass;
            % Principle of angular momentum for the chassis around z-axis
            yawAccZ = (self.DistanceFrontCOG * U(2) - self.DistanceRearCOG * U(4)) / self.MomentOfIntertiaZ;
            %% Calculation of additional output quantitites
            % Inertial acceleration in vehicle-fixed coordinate system
            accInrtXV = + U(7) * accXG + U(8) * accYG;
            accInrtYV = - U(8) * accXG + U(7) * accYG;
            %% State derivative varaibles
            dX = [X(2);     % VelXG:   longitudinal velocity in ground-fixed coordinate system [m/s]
                  accXG;    % AccXG:   longitudinal acceleration in ground-fixed cs. [m/s^2]
                  X(4);     % VelYG:   lateral velocity in ground-fixed coordinate system [m/s]
                  accYG;    % AccYG:   lateral acceleration in ground-fixed cs. [m/s^2]
                  X(6);     % YawVelZ: yaw (heading) angular velocity in ground-fixed system [rad/s]
                  yawAccZ]; % YawAccZ: yaw (heading) angular acceleration in ground-fixed system [rad/s^2]
            %% Output variables
            Y = [accXG;      % AccXG: longitudinal acceleration in ground-fixed cs. [m/s^2]
                 accYG;      % AccYG: lateral acceleration in ground-fixed cs. [m/s^2]
                 accInrtXV;  % AccInrtXV: longitudinal inertial acceleration in vehicle-fixed cs. [m/s^2]
                 accInrtYV;  % AccInrtYV: lateral inertial acceleration in vehicle-fixed cs. [m/s^2]
                 yawAccZ];   % YawAccZ: yaw (heading) angular acceleration in ground-fixed system [rad/s^2]
        end
    end
end