%% PlannerOptIn *************************************************************************************
% [Summary]
%   This class represents inputs for optimal nonlinear optimization based motion planner PlannerOpt.
%
% [Used in]
%   PlannerOpt
%
% [Subclasses]
%   PlannerCoptIn
% **************************************************************************************************
classdef PlannerOptIn < PlannerConIn
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Weights of tracking objective function
        CostTrackWeight     % CtrlLatOut object
        % Weights of vehicle dynamics objective function
        CostStateWeight     % ChassisStOut object
    end
    
    %% Static methods ==============================================================================
    methods (Static)
        
        %% Property checker methods ----------------------------------------------------------------
        function value = setOptVarInit(value)
            if isstruct(value)
                value = PlannerOptVarVelPath(value);
            end
            if ~isa(value, 'PlannerOptVarVelPath')
                error('ERROR: OptVarInit must be a PlannerOptVarVelPath object!');
            end
        end
        
    end
    
    %% Instance methods ============================================================================    
    methods      
        %% Property getter and setter methods ------------------------------------------------------
        function set.CostTrackWeight(self, value)
            if ~isequal(self.CostTrackWeight, value)
                if isstruct(value)
                    value = CtrlLatOut(value);
                end
                if ~isa(value, 'CtrlLatOut')
                    error('ERROR: CostTrackWeight must be a CtrlLatOut object!');
                end
                self.CostTrackWeight = value;
            end
        end
        
        function set.CostStateWeight(self, value)
            if ~isequal(self.CostStateWeight, value)
                if isstruct(value)
                    value = ChassisStOut(value);
                end
                if ~isa(value, 'ChassisStOut')
                    error('ERROR: CostStateWeight must be a ChassisStOut object!');
                end
                self.CostStateWeight = value;
            end
        end       

        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerOptIn(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 6
                superArgs = varargin(1:4);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@PlannerConIn(superArgs{:});
            if nargin == 1
                self.CostTrackWeight = varargin{1}.CostTrackWeight;
                self.CostStateWeight = varargin{1}.CostStateWeight;
            elseif nargin == 6
                self.CostTrackWeight = varargin{5};
                self.CostStateWeight = varargin{6};
            end
        end
    end
end
