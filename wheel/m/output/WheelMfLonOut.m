%% WheelMfLonOut ***********************************************************************************
% [Summary]
%   This class represents outputs for longitudinal wheel model with Magic Formula tire.
%
% [Used in]
%   VehQrLonOut
%   VehQrLon
%
% [Subclasses]
%   WheelMfOut
% **************************************************************************************************
classdef WheelMfLonOut < ComTimeInOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Pitch (turn) angle of the wheel [rad]
        PitchY
        % Pitch (turn) rate of the wheel [rad/s]
        PitchVelY
        % Pitch (turn) acceleration of the wheel [rad/s^2]
        PitchAccY
        % Longitudinal slip [1]
        SlipX
        % Longitudinal slip derivative [1]
        DSlipX
        % Longitudinal tire force [N]
        ForceXW
        % Vertical tire force (tire load) [N]
        ForceZW
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.PitchY(self, value)
            if ~isequal(self.PitchY, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PitchY must be a numeric vector!')
                end
                self.PitchY = value(:);
            end
        end
        
        function set.PitchVelY(self, value)
            if ~isequal(self.PitchVelY, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PitchVelY must be a numeric vector!')
                end
                self.PitchVelY = value(:);
            end
        end
        
        function set.PitchAccY(self, value)
            if ~isequal(self.PitchAccY, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PitchAccY must be a numeric vector!')
                end
                self.PitchAccY = value(:);
            end
        end
        
        function set.SlipX(self, value)
            if ~isequal(self.SlipX, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: SlipX must be a numeric vector!')
                end
                self.SlipX = value(:);
            end
        end
        
        function set.DSlipX(self, value)
            if ~isequal(self.DSlipX, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: DSlipX must be a numeric vector!')
                end
                self.DSlipX = value(:);
            end
        end
        
        function set.ForceXW(self, value)
            if ~isequal(self.ForceXW, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: ForceXW must be a numeric vector!')
                end
                self.ForceXW = value(:);
            end
        end
        
        function set.ForceZW(self, value)
            if ~isequal(self.ForceZW, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: ForceZW must be a numeric vector!')
                end
                self.ForceZW = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = WheelMfLonOut(varargin)
            if nargin == 1
                comTimeInArgs = varargin(1);
            elseif nargin == 8
                comTimeInArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
           self@ComTimeInOut(comTimeInArgs{:});
            if nargin == 1
                self.PitchY     = varargin{1}.PitchY;
                self.PitchVelY  = varargin{1}.PitchVelY;
                self.PitchAccY  = varargin{1}.PitchAccY;
                self.SlipX      = varargin{1}.SlipX;
                self.DSlipX     = varargin{1}.DSlipX;
                self.ForceXW    = varargin{1}.ForceXW;
                self.ForceZW    = varargin{1}.ForceZW;
            elseif nargin == 8
                self.PitchY     = varargin{2};
                self.PitchVelY  = varargin{3};
                self.PitchAccY  = varargin{4};
                self.SlipX      = varargin{5};
                self.DSlipX     = varargin{6};
                self.ForceXW    = varargin{7};
                self.ForceZW    = varargin{8};
            end
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the longitudinal wheel model.
        %
        % [Input]
        %       name - name for the plots
        %
        % [Output]
        %       fig  - figure handle of plot
        % ------------------------------------------------------------------------------------------
        function fig = DisplayPlots(self, name)
            %% Constants
            WIDTH = 15;
            HEIGHT = 15;
            FONTSIZE = 8;
            
            %% Default arguments
            if nargin < 2
                name = class(self);
            end
            
            %% Create plots
            fig = nicefigure(name, WIDTH, HEIGHT);
          
            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 1, ax);
            plot(ax, self.Time, self.PitchY, 'Color', Colors.Emerald);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$\phi \mathrm{[rad]}$');
            title(ax, 'angle');
            
            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 4, ax);
            plot(ax, self.Time, self.PitchVelY, 'Color', Colors.Emerald);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$\omega \mathrm{[rad/s]}$');
            title(ax, 'angular velocity');
            
            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 7, ax);
            plot(ax, self.Time, self.PitchAccY, 'Color', Colors.Emerald);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$\alpha \mathrm{[rad/s^2]}$');
            title(ax, 'angular acceleration');

            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 2, ax);
            plot(ax, self.Time, self.SlipX * 100, 'Color', Colors.BelizeHole);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$s_x \mathrm{[\%]}$');
            title(ax, 'lon. slip');
            
            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 5, ax);
            plot(ax, self.Time, self.DSlipX * 100, 'Color', Colors.BelizeHole);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$\dot{s}_x \mathrm{[\%/s]}$');
            title(ax, 'lon. slip derivative');
            
            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 8, ax);
            plot(ax, self.Time, self.ForceXW, 'Color', Colors.Amethyst);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$F_x \mathrm{[N]}$');
            title(ax, 'lon. force');
        end
        
    end
end