% To get the data, run model with CtrlLatSty controller and set lookAheadFactor to 0.8 and 0.0.
%
% withLookahead = obj2struct(VehTest.TestManeuver('VehStCl_Default', 'Maneuver_SCurve', false, false)); % 0.8
% withoutLookahead = obj2struct(VehTest.TestManeuver('VehStCl_Default', 'Maneuver_SCurve', false, false)); % 0.0

% Subsampling factor
subs = @(x) x(1:50:end);

% Create plot
W = 10;
H = 8;
F = 8;
fig = nicefigure('StanleyLookahead', W, H);

ax1 = niceaxis(fig, F);
subplot(2, 1, 1, ax1);
hold(ax1, 'on');
plot(ax1, subs(withoutLookahead.in.PosXGRef), subs(withoutLookahead.in.PosYGRef), 'Color', nicecolors.YellowSunFlower, ...
    'LineStyle', '-', 'LineWidth', 3);
plot(ax1, subs(withoutLookahead.out.Chassis.PosXG), subs(withoutLookahead.out.Chassis.PosYG), 'Color', nicecolors.RedAlizarin, ...
    'LineStyle', '-');
plot(ax1, subs(withLookahead.out.Chassis.PosXG), subs(withLookahead.out.Chassis.PosYG), 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-');
xlabel(ax1, '$x^G_v$ (pos. north) [m]');
ylabel(ax1, '$y^G_v$ (pos. west) [m]');
axis(ax1, 'equal');
xlim(ax1, [-10,330]);
ylim(ax1, [-20, 130]);
l = nicelegend({'Reference', 'original', 'lookahead'}, ax1, F);
l.Orientation = 'vertical';
l.Location = 'northeastoutside';
l.Position = [0.715, 0.64, 0.267, 0.20];
ax1.Position = [0.11, 0.64, 0.58, 0.32];

ax2 = niceaxis(fig, F);
subplot(2, 1, 2, ax2);
hold(ax2, 'on');
yyaxis(ax2, 'left');
plot(ax2, subs(withoutLookahead.out.CtrlLat.Time), subs(withoutLookahead.out.CtrlLat.CrossTrackErr * 100), 'Color', nicecolors.RedAlizarin, ...
    'LineStyle', '-');
plot(ax2, subs(withLookahead.out.CtrlLat.Time), subs(withLookahead.out.CtrlLat.CrossTrackErr * 100), 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-');
ylim(ax2, [-30, 30]);
xlabel(ax2, '$t_v$ (time) [s]');
ylabel(ax2, '$y_e$ (cross track err.) [cm]');
ax2.YAxis(1).Color = [0, 0, 0];
yyaxis(ax2, 'right');
plot(ax2, subs(withoutLookahead.out.CtrlLat.Time), subs(rad2deg(withoutLookahead.out.CtrlLat.HeadingErr)), 'Color', nicecolors.RedAlizarin, ...
    'LineStyle', '-.', 'LineWidth', .75);
plot(ax2, subs(withLookahead.out.CtrlLat.Time), subs(rad2deg(withLookahead.out.CtrlLat.HeadingErr)), 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-.', 'LineWidth', .75);
ylim(ax2, [-1, 1]);
xlabel(ax2, '$t_v$ (time) [s]');
ylabel(ax2, '$\psi_e$ (yaw angle err.) [$^\circ$]');
ax2.YAxis(2).Color = [0, 0, 0];
xlim(ax2, [0, 18]);
l = nicelegend({sprintf('original\ncross track'), sprintf('lookahead\ncross track'), sprintf('original\nheading'), sprintf('lookahead\nheading')}, ax2, F);
l.Orientation = 'vertical';
l.Location = 'northeastoutside';
l.Position = [0.715, 0.10, 0.267, 0.39];
ax2.Position = [0.11, 0.10, 0.495, 0.39];

% Save
name = 'StanleyLookahead';
savefig(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');