function plotWeightingEffect(axis, trjOpt, trjCon, marker, opt)
    lenTrj = min(length(trjOpt), length(trjCon));
    accRatesAbs = zeros(lenTrj,1);
    accRatesInt = zeros(lenTrj,1);
    jerkRatesAbs = zeros(lenTrj,1);
    jerkRatesInt = zeros(lenTrj,1);
    trlTimeRates = zeros(lenTrj,1);
    for trjIndex = 1:lenTrj
        % Acceleration rates
        accRatesAbs(trjIndex) = max(abs(trjOpt(trjIndex).X(end,:))) / max(abs(trjCon(trjIndex).X(end,:)));
        accRatesInt(trjIndex) = trapz(trjOpt(trjIndex).t, trjOpt(trjIndex).X(end,:) .^ 2) / trapz(trjCon(trjIndex).t, trjCon(trjIndex).X(end,:) .^ 2);
        % Jerk rates
        jerkTrjOpt = diff(trjOpt(trjIndex).X(end,:)) ./ diff(trjOpt(trjIndex).t);
        jerkTrjCon = diff(trjCon(trjIndex).X(end,:)) ./ diff(trjCon(trjIndex).t);
        jerkRatesAbs(trjIndex) = max(abs(jerkTrjOpt)) ./ max(abs(jerkTrjCon));
        jerkRatesInt(trjIndex) = trapz(trjOpt(trjIndex).t(2:end), jerkTrjOpt .^ 2) / trapz(trjCon(trjIndex).t(2:end), jerkTrjCon .^ 2);
        % Travel time rates
        trlTimeRates(trjIndex)=trjOpt(trjIndex).t(end) / trjCon(trjIndex).t(end);
    end
    
    if strcmpi(opt, 'lanechange') || strcmpi(opt, 'lanekeep')
        jerkRatesInt = not(jerkRatesInt > 1.1) .* jerkRatesInt + (jerkRatesInt > 1.1) .* (1.1 + 0.1 * (jerkRatesInt - 1.1));
    end
    
    plot(axis, 1:lenTrj, accRatesInt, [':', marker], 'MarkerSize', 8.0, 'MarkerEdgeColor', nicecolors.BluePeterRiver, ...
        'Color', nicecolors.BluePeterRiver, 'LineWidth', 1);
    plot(axis, 1:lenTrj, jerkRatesInt, [':', marker], 'MarkerSize', 8.0, 'MarkerEdgeColor', nicecolors.OrangePumpkin, ...
        'Color', nicecolors.OrangePumpkin, 'LineWidth', 1);
end