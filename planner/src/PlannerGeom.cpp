#include <chrono>
#include "PlannerGeom.hpp"
#include "MathTp.hpp"

using namespace Eigen;
using namespace alglib;
using namespace std;

namespace TrajectoryPlanning
{
		

	Int32_T PlannerGeom::SplineTrajLinVel(const Float64_T xf,
							const VectorXd & yp0,
							const Float64_T rf,
							const Float64_T vi,
							const Float64_T vf,
							const Float64_T dri,
							const Float64_T drf,
							VectorXd & ts,
							VectorXd & s,
							VectorXd & xs,
							VectorXd & ys,
							VectorXd & rs,
							VectorXd & vs,
							VectorXd & drs,
							VectorXd & acps,
							Boolean_T optimflag)
	{
		// Constants
		const Float64_T dx = 0.01;
		const Float64_T ds = 0.1;

		// Check arguments
		if (yp0.size() != 3)
			throw std::invalid_argument("Vectpr yp must have 3 elements!");

		// Get equidistant samples in local coordinates
		Int64_T nyp0 = yp0.size();
		VectorXd xp = VectorXd::LinSpaced(nyp0 + 1, 0, xf);
        // trajectory begins at (0,0) CoG of ego vehicle
		VectorXd yp(nyp0 + 1);
		yp << 0, yp0;

		// Evaluate path and curvature as function of longitudinal coordinate [ y(x), k(x) ]
        // longitudinal coordinate domain x
        Int64_T nx = (Int64_T)(ceil(xf / dx) + 1);
        Float64_T dxa = xf / (nx - 1);
        VectorXd  x = VectorXd::LinSpaced(nx, 0, xf);
            
        // path y(x), path derivative y'(x), and path second derivative y''(x) functions
		VectorXd dyb(2); dyb << 0, tan(rf);
        VectorXd ddyb(2); ddyb << dri / vi, drf / vf * pow(1 + pow(tan(rf), 2.0), 3.0/2.0);

        VectorXd yx = VectorXd::Zero(nx);
		VectorXd dyx_dx = VectorXd::Zero(nx);
		VectorXd ddyx_dx = VectorXd::Zero(nx);
		
		interp4spline5(xp, yp, dyb, ddyb, x, yx, dyx_dx, ddyx_dx);

		VectorXd kx = ddyx_dx.array() / pow(1 + dyx_dx.array().square(), 3.0 / 2.0);
		
        // Calculate position and heading as function of arc length [ x(s), y(s), r(s) ]
        // calculate arc length as function of lon. position s(x)
		VectorXd sx = VectorXd::Zero(nx);
		sx.tail(nx - 1) = cumsum(sqrt(pow(dxa, 2) + diff(yx).array().square()));
		
		// arc length domain s
		Int64_T ns = (Int64_T)(ceil(sx.tail(1)(0) / ds) + 1);
        Float64_T dsa = sx.tail(1)(0) / (ns - 1);
		s = VectorXd::LinSpaced(ns, 0, sx.tail(1)(0));

		// calculate curvature as function of arc length k(s)
		VectorXd ks = VectorXd::Zero(ns);
		interp1(sx, kx, s, ks);

		// Calculation of these quntities is not needed in case of optimization
		if (!optimflag)
		{
			// calculate lon. and lat. positions as function of arc length x(s) and y(s)
			xs = VectorXd::Zero(ns);
			interp1(sx, x, s, xs);
			ys = VectorXd::Zero(ns);
			interp1(sx, yx, s, ys);
			
			rs = VectorXd::Zero(ns);
			rs.tail(ns - 1) = atan(diff(ys).array() / diff(xs).array());
		}

 		// Calculate time, velocity and yaw rate as function of arc length [ t(s), v(s), dr(s) ]   
		// calculate time as function of arc length t(s)
		Float64_T va = (vi + vf) / 2;
		Float64_T tf = s.tail(1)(0) / va;
		Float64_T a = (vf - vi) / tf;
		if (abs(a) > 1e-6)
		{
			// (a/2).t^2 + v0.t - s = 0 -> t
			ts = (-vi + sqrt(pow(vi, 2) + 2 * a * s.array())) / a;
		}
		else
		{
			ts = s.array() / vi;
		}
        // calculate velocity and yaw rate as function of arc length dr(s), v(s)
		vs = vi + a * ts.array();
		drs = vs.array() * ks.array();
		acps = vs.array() * drs.array();
		
		return 0;	
	}

	void PlannerGeom::cost_fcn(const real_1d_array &x, double &fval,  real_1d_array &grad, void *ptr)
	{
		// finite difference step size: trajectory y points [m]
		const Float64_T dx = 1e-2;

		// Get planner
		VectorXd yp(3);
		Int64_T l = 0;

		PlannerGeom *p = (PlannerGeom *)ptr;
		
		// Evaluate function value
		yp << x[0], x[1], p->yf_;
		p->SplineTrajLinVel(p->xf_, yp, p->rf_, p->vi_, p->vf_, p->dri_, p->drf_, p->t_, p->s_, 
			p->x_, p->y_, p->r_, p->v_, p->dr_, p->acp_, TRUE);
		l = p->acp_.size();
		fval = (p->acp_.tail(l - 1).array().square() / diff(p->t_).array()).sum() / p->t_.tail(1)(0);

		// Evaluate gradient
		yp << x[0] + dx, x[1], p->yf_;
		p->SplineTrajLinVel(p->xf_, yp, p->rf_, p->vi_, p->vf_, p->dri_, p->drf_, p->t_, p->s_, 
			p->x_, p->y_, p->r_, p->v_, p->dr_, p->acp_, TRUE);
		l = p->acp_.size();
		grad[0] = (((p->acp_.tail(l - 1).array().square() / diff(p->t_).array()).sum() / p->t_.tail(1)(0)) - fval) / dx;

		yp << x[0], x[1] + dx, p->yf_;
		p->SplineTrajLinVel(p->xf_, yp, p->rf_, p->vi_, p->vf_, p->dri_, p->drf_, p->t_, p->s_, 
			p->x_, p->y_, p->r_, p->v_, p->dr_, p->acp_, TRUE);
		l = p->acp_.size();
		grad[1] = (((p->acp_.tail(l - 1).array().square() / diff(p->t_).array()).sum() / p->t_.tail(1)(0)) - fval) / dx;

		p->nfeval_+=3;
	}

	std::tuple<Int32_T, Int32_T> PlannerGeom::SplineTrajLinVelOpt(const Float64_T &xf,
											 const Float64_T &yf,
											 const Float64_T &rf,
											 const Float64_T &vi,
											 const Float64_T &vf,
											 const Float64_T &dri,
											 const Float64_T &drf,
											 VectorXd &t,
											 VectorXd &s,
											 VectorXd &x,
											 VectorXd &y,
											 VectorXd &r,
											 VectorXd &v,
											 VectorXd &dr,
											 VectorXd &acp)
	{
		// Save inputs so that it can be used in cost_fcn
		xf_ = xf;
		yf_ = yf;
		rf_ = rf;
		vi_ = vi;
		vf_ = vf;
		dri_ = dri;
		drf_ = drf;
		nfeval_ = 0;

		// Set up optimiztion problem
		// initial quesss
		Float64_T y0ptr[] = {0, 0};
		real_1d_array y0;
		y0.setcontent(2, y0ptr);
		// gradient value tolerance: change in average lateral acceleration [m/s^2]
		double epsg = 1.0e-5;
		// function value tolerance: average lateral acceleratio [m/s^2]
		double epsf = 1.0e-3;
		// step size tolerance: trajectory y points [m]
		double epsx =  1.0e-3;
		// maximum iterations
		ae_int_t maxits = 10;
		// scale for coordinates
		Float64_T yscaleptr[] = {1, 1};
		real_1d_array yscale;
		yscale.setcontent(2, yscaleptr);

		minlbfgsstate state;
		minlbfgscreate(1, y0, state);
		minlbfgssetcond(state, epsg, epsf, epsx, maxits);
		minlbfgssetscale(state, yscale);

		// Run optimization
		minlbfgsoptimize(state, &cost_fcn, nullptr, this);

		// Get optimization result
		minlbfgsreport rep;
		minlbfgsresults(state, y0, rep);

		// Calculate optimal result
		VectorXd yp = VectorXd::Zero(3);
		yp << y0[0], y0[1], yf;
		SplineTrajLinVel(xf, yp, rf, vi, vf, dri, drf, t, s, x, y, r, v, dr, acp);
		nfeval_++;

		return make_tuple(rep.terminationtype, nfeval_);
	}
}