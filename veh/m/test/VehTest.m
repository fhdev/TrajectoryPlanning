classdef VehTest
    methods (Static)
        
        function TestAll()
            %% VehQrLon
            VehTest.TestManeuver('VehQrLon_Default', 'Maneuver_BrakeStill', true);
            
            VehTest.TestManeuver('VehQrLon_Default', 'Maneuver_DriveOff', true);
            
            %% VehQr
            VehTest.TestManeuver('VehQr_Default', 'Maneuver_BrakeStill', true);
            
            VehTest.TestManeuver('VehQr_Default', 'Maneuver_DriveOff', true);
            
            VehTest.TestManeuver('VehQr_Default', 'Maneuver_Circle', true);
            
            VehTest.TestManeuver('VehQr_Default', 'Maneuver_LaneChange', true);
            
            %% VehStOl
            VehTest.TestManeuver('VehStOl_Default', 'Maneuver_BrakeStill', true);
            VehTest.TestManeuver('VehStOl_Default', 'Maneuver_BrakeStill', true, 1);
            VehTest.TestManeuver('VehStOl_Default', 'Maneuver_BrakeStill', true, 2);
            
            VehTest.TestManeuver('VehStOl_Default', 'Maneuver_DriveOff', true);
            VehTest.TestManeuver('VehStOl_Default', 'Maneuver_DriveOff', true, 1);
            
            VehTest.TestManeuver('VehStOl_Default', 'Maneuver_Circle', true);
            VehTest.TestManeuver('VehStOl_Default', 'Maneuver_Circle', true, 1);
            
            VehTest.TestManeuver('VehStOl_Default', 'Maneuver_LaneChange', true);
            VehTest.TestManeuver('VehStOl_Default', 'Maneuver_LaneChange', true, 1);
            
            %% VehStCl 
            VehTest.TestManeuver('VehStCl_Default', 'Maneuver_BrakeStill', true);
            VehTest.TestManeuver('VehStCl_Default', 'Maneuver_BrakeStill', true, 1);
            VehTest.TestManeuver('VehStCl_Default', 'Maneuver_BrakeStill', true, 2);
            
            VehTest.TestManeuver('VehStCl_Default', 'Maneuver_DriveOff', true);
            VehTest.TestManeuver('VehStCl_Default', 'Maneuver_DriveOff', true, 1);
            
            VehTest.TestManeuver('VehStCl_Default', 'Maneuver_Circle', true);
            VehTest.TestManeuver('VehStCl_Default', 'Maneuver_Circle', true, 1);
            
            VehTest.TestManeuver('VehStCl_Default', 'Maneuver_LaneChange', true);
            VehTest.TestManeuver('VehStCl_Default', 'Maneuver_LaneChange', true, 1);
        end
        
        function test = TestManeuver(paramSet, inputName, doPlot, useMex)
            if (nargin < 4) || isempty(useMex)
                useMex = 0;
            end
            if (nargin < 3) || isempty(doPlot)
                doPlot = true;
            end
            if (nargin < 2) || isempty(inputName)
                inputName = 'Maneuver_BrakeStill';
            end
            if (nargin < 1) || isempty(paramSet)
                paramSet = 'VehStOl_Default';
            end
            
            
            
            veh = VehTest.CreateVehicle(paramSet);
            [in, initCon] = VehTest.CreateVehicleInput(paramSet, inputName);
            
            if (useMex > 0)  && ~ismethod(veh, 'SimulateMex')
                error('Unable to simulate mex for %s!', paramSet);
            end
            
            switch useMex
                case 0
                    tic;
                    out = veh.Simulate(initCon, in);
                    t = toc;
                    fprintf(1, 'MATLAB simulation finished in %g s.\n', t);
                    fprintf(1, 'MATLAB simulation speed is %g x RT.\n', out.Time(end) / t);
                    if doPlot
                        out.DisplayPlots(inputName);
                        if ismethod(veh, 'DisplayPlots')
                            veh.DisplayPlots(in, out, inputName);
                        end
                    end
                    test.out = out;
                case 1
                    tic;
                    out = veh.SimulateMex(initCon, in);
                    t = toc;
                    fprintf(1, 'MEX simulation finished in %g s.\n', t);
                    fprintf(1, 'MEX simulation speed is %g x RT.\n', out.Time(end) / t);
                    if doPlot
                        out.DisplayPlots(inputName);
                        if ismethod(veh, 'DisplayPlots')
                            veh.DisplayPlots(in, out, inputName);
                        end
                    end
                    test.out = out;
                case 2
                    tic;
                    outMl = veh.Simulate(initCon, in);
                    tMl = toc;
                    fprintf(1, 'MATLAB simulation finished in %g s.\n', tMl);
                    fprintf(1, 'MATLAB simulation speed is %g x RT.\n', outMl.Time(end) / tMl);
                    tic;
                    outMex = veh.SimulateMex(initCon, in);
                    tMex = toc;
                    fprintf(1, 'MEX simulation finished in %g s.\n', tMex);
                    fprintf(1, 'MEX simulation speed is %g x RT.\n', outMex.Time(end) / tMex);
                    fprintf('MEX to MATLAB speed factor is %g.\n', tMl / tMex);
                    % Compare results
                    names = properties('ChassisStOut');
                    for name = names'
                        assert(max(abs(outMex.Chassis.(name{:}) - outMl.Chassis.(name{:}))) < 1e-9);
                    end
                    names = properties('WheelMfExtOut');
                    for name = names'
                        assert(max(abs(outMex.WheelFront.(name{:}) - outMl.WheelFront.(name{:}))) < 1e-9);
                        assert(max(abs(outMex.WheelRear.(name{:}) - outMl.WheelRear.(name{:}))) < 1e-9);
                    end
                    names = properties('SteerFoOut');
                    for name = names'
                        assert(max(abs(outMex.Steering.(name{:}) - outMl.Steering.(name{:}))) < 1e-9);
                    end
                    if isprop(veh, 'CtrlLon')
                        names = properties('CtrlLonOut');
                        for name = names'
                            assert(max(abs(outMex.CtrlLon.(name{:}) - outMl.CtrlLon.(name{:}))) < 1e-9);
                        end
                    end
                    if isprop(veh, 'CtrlLat')
                        names = properties('CtrlLatOut');
                        for name = names'
                            assert(max(abs(outMex.CtrlLat.(name{:}) - outMl.CtrlLat.(name{:}))) < 1e-9);
                        end
                    end
                    fprintf('The two outputs are equal!\n');
                    test.outMl = outMl;
                    test.outMex = outMex;
            end
            
            test.veh = veh;
            test.in = in;
            test.initCon = initCon;
            
        end        
        
        function CompareMlMex(paramSet)
            switch paramSet
                case 'VehStOl_Default'
                    veh = VehStOl(Vehicle_Default());
                case 'VehStCl_Default'
                    veh = VehStCl(Vehicle_Default());
                otherwise
                    error('Unknown param set %s.', paramSet);
            end
            in = VehStClIn(Maneuver_Circle);
            initCon = VehStBndCon(InitCon_20mps_Straight);
            tic;
            outMex = veh.SimulateMex(initCon, in);
            mexTime = toc;
            fprintf('Mex finished in %g s.\n', mexTime);
            tic;
            outMl = veh.Simulate(initCon, in);
            mlTime = toc;
            fprintf('Matlab finished in %g s.\n', mlTime);
            % Compare times
            fprintf('Mex speed factor is %g.\n', mlTime / mexTime);

        end
        
        
        function vehicle = CreateVehicle(paramSet)
            switch paramSet
                case 'VehQrLon_Default'
                    vehicle = VehQrLon(Vehicle_Default());
                case 'VehQr_Default'
                    vehicle = VehQr(Vehicle_Default());
                case 'VehStOl_Default'
                    vehicle = VehStOl(Vehicle_Default());
                case 'VehStOl_Smart'
                    vehicle = VehStOl(Vehicle_Smart());
                case 'VehStCl_Default'
                    vehicle = VehStCl(Vehicle_Default());
                case 'VehStCl_Smart'
                    vehicle = VehStCl(Vehicle_Smart());
                otherwise
                    error('Unknown param set %s.', paramSet);
            end
        end
        
        function [in, inCon] = CreateVehicleInput(paramSet, inputName)
            [in, inCon] = feval(inputName);
                    
            switch paramSet
                case 'VehQrLon_Default'
                    in = VehQrLonIn(in);
                    inCon = VehQrLonBndCon(inCon);
                case 'VehQr_Default'
                    in = VehQrIn(in);
                    inCon = VehQrBndCon(inCon);
                case {'VehStOl_Default', 'VehStOl_Smart'}
                    in = VehStOlIn(in);
                    inCon = VehStBndCon(inCon);
                case {'VehStCl_Default', 'VehStCl_Smart'}
                    in = VehStClIn(in);
                    inCon = VehStBndCon(inCon);
                otherwise
                    error('Unknown param set %s.', paramSet);
            end
        end
    end
end