import os
import sklearn.preprocessing
import matplotlib.pyplot as plt
import pandas
import numpy
import tensorflow
import json

# Parameters to change
DATAFOLDER = os.path.join("data", "VehModel_NN_2020", "StateEq_V1")
TAG = "big3_relu_2"

# Read in datasets
inputMetadataPath = os.path.join(DATAFOLDER, "input_metadata.json")
with open(inputMetadataPath, "r") as inputMetadataFile:
    inputMetadata = json.load(inputMetadataFile)
inputDataPath = os.path.join(DATAFOLDER, "input.bin")
with open(inputDataPath, "r") as inputDataFile:
    matIn = numpy.fromfile(inputDataFile, "<d", numpy.prod(inputMetadata["SampleSize"]))
matIn = matIn.reshape(inputMetadata["SampleSize"], order="F")


outputMetadataPath = os.path.join(DATAFOLDER, "output_metadata.json")
with open(outputMetadataPath, "r") as outputMetadataFile:
    outputMetadata = json.load(outputMetadataFile)
outputDataPath = os.path.join(DATAFOLDER, "output.bin")
with open(outputDataPath, "r") as outputDataFile:
    matOut = numpy.fromfile(outputDataFile, "<d", numpy.prod(outputMetadata["SampleSize"]))
matOut = matOut.reshape(outputMetadata["SampleSize"], order="F")

# Normalize input
scIn = sklearn.preprocessing.MaxAbsScaler()
matInScaled = scIn.fit_transform(matIn)
del matIn

# Normalize output
scOut = sklearn.preprocessing.MaxAbsScaler()
matOutScaled = scOut.fit_transform(matOut)
del matOut

# Create training folder name
trainingFolder = os.path.join(DATAFOLDER, "training_%s" % TAG)

# Load model
modelPath = os.path.join(trainingFolder, "model_%s.json" % TAG)
with open(modelPath, "r") as modelFile:
    modelJson = modelFile.read()
model = tensorflow.keras.models.model_from_json(modelJson)

# Load weights
weightPath = os.path.join(trainingFolder, "weights_%s.h5" % TAG)
model.load_weights(weightPath)
print("Model loaded successfully.")

# Show error for a few samples
Ynet = model.predict(matInScaled[0:3, :], batch_size=4096)
print(Ynet)

# # Load log
# logPath = os.path.join(trainingFolder, "log_%s.csv" % DATETAG)
# logDataSet = pandas.read_csv(logPath)

# # Plot results
# # Metrics
# fig1, ax1 = plt.subplots()
# ax1.plot(logDataSet["epoch"].values, logDataSet["loss"].values)
# ax1.plot(logDataSet["epoch"].values, logDataSet["val_loss"].values)
# ax1.set_title("Training process")
# ax1.set_ylabel("Mean square error")
# ax1.set_xlabel("Epoch")
# ax1.legend(["Train", "Test"], loc="upper left")
# plt.show()