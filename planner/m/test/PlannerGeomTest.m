classdef PlannerGeomTest
    methods (Static)
        
        function TestPolyTraj()
            xf = 100;
            yf = 10;
            rf = 0;
            vi = 20;
            vf = 10;
            tic;
            [t, s, x, y, r, v, dr] = PlannerGeom.PolyTrajLinVel(xf, yf, rf, vi, vf);
            toc;
            PlannerGeomTest.DisplayPlots(t, s, x, y, r, v, dr);
        end
        
        function TestCurvatureTraj()
            sp = [ 0, 10,   20,  120, 130,   140,   240, 250,  300];
            kp = [ 0,  0, 1e-2, 1e-2,   0, -5e-3, -5e-3,   0,    0];
            vp = [15, 20,   20,   30,  30,    30,    15,   15,   0];
            tic;
            [t, s, x, y, r, v, dr, acp] = PlannerGeom.CurvatureTraj(sp, kp, vp);
            toc;
            PlannerGeomTest.DisplayPlots(t, s, x, y, r, v, dr, acp);
        end
        
        function TestSplineTrajLinVel(doPlots, doMex)
            if nargin < 2
                doMex = false;
            end
            if nargin < 1
                doPlots = true;
            end
            
            clear('PlannerGeomSplineTrajLinVelMex');
            
            xf = 100;
            yp = [1.5, 8.5, 10];
            rf = 0;
            vi = 10;
            vf = 10;
            dri = 0;
            drf = 0;
            
            if doMex
                tic;
                [t, s, x, y, r, v, dr, acp] = PlannerGeomSplineTrajLinVelMex(xf, yp', rf, vi, vf, dri, drf);
                toc;
            else
                tic;
                [t, s, x, y, r, v, dr, acp] = PlannerGeom.SplineTrajLinVel(xf, yp, rf, vi, vf, dri, drf);
                toc;
            end
            if doPlots
                PlannerGeomTest.DisplayPlots(t, s, x, y, r, v, dr, acp);
            end
        end
        
        function TestSplineTrajLinVelOpt(doPlots, doMex)
            if nargin < 2
                doMex = false;
            end
            if nargin < 1
                doPlots = true;
            end
            
            clear('PlannerGeomSplineTrajLinVelMex');
            
            xf = 100;
            yf = 10;
            rf = 0;
            vi = 10;
            vf = 10;
            dri = 0;
            drf = 0;
            
            if doMex
                tic;
                [t, s, x, y, r, v, dr, acp] = PlannerGeomSplineTrajLinVelOptMex(xf, yf, rf, vi, vf, dri, drf);
                toc;
            else
                tic;
                [t, s, x, y, r, v, dr, acp] = PlannerGeom.SplineTrajLinVelOpt(xf, yf, rf, vi, vf, dri, drf);
                toc;
            end
            if doPlots
                PlannerGeomTest.DisplayPlots(t, s, x, y, r, v, dr, acp);
            end
        end
        
        function TestSplineTrajLinVelOptTime()
            N = 10;
            
            clear('PlannerGeomSplineTrajLinVelOptMex');
            
            xf = 100;
            yf = 10;
            rf = 0;
            vi = 10;
            vf = 10;
            dri = 0;
            drf = 0;
            
            tic;
            for i = 1:N
                [tm, sm, xm, ym, rm, vm, drm, acpm] = PlannerGeom.SplineTrajLinVelOpt(xf, yf, rf, vi, vf, dri, drf);
            end
            trm = toc;
            
            tic;
            for i = 1:N
                [tc, sc, xc, yc, rc, vc, drc, acpc] = PlannerGeomSplineTrajLinVelOptMex(xf, yf, rf, vi, vf, dri, drf);
            end
            trc = toc;
            
            assert(max(abs(tm - tc)) < 1e-3);
            assert(max(abs(sm - sc)) < 1e-3);
            assert(max(abs(xm - xc)) < 1e-3);
            assert(max(abs(ym - yc)) < 5e-3);
            assert(max(abs(rm - rc)) < 1e-3);
            assert(max(abs(vm - vc)) < 1e-3);
            assert(max(abs(drm - drc)) < 1e-3);
            assert(max(abs(acpm - acpc)) < 5e-3);
            

            fprintf('tM = %g ms, tC = %g ms, f = %g\n', trm / N * 1000, trc / N * 1000, trm / trc);
        end
        
        function TestSplineTrajLinVelTime()
            N = 500;
            
            clear('PlannerGeomSplineTrajLinVelOptMex');
            
            xf = 100;
            yp = [1.5, 8.5, 10];
            rf = deg2rad(45);
            vi = 10;
            vf = 20;
            dri = 0.1;
            drf = 0.1;
            
            tic;
            for i = 1:N
                [tm, sm, xm, ym, rm, vm, drm, acpm] = PlannerGeom.SplineTrajLinVel(xf, yp, rf, vi, vf, dri, drf);
            end
            trm = toc;
            
            tic;
            for i = 1:N
                [tc, sc, xc, yc, rc, vc, drc, acpc] = PlannerGeomSplineTrajLinVelMex(xf, yp', rf, vi, vf, dri, drf);
            end
            trc = toc;
            
            assert(max(abs(tm - tc)) < 1e-9);
            assert(max(abs(sm - sc)) < 1e-9);
            assert(max(abs(xm - xc)) < 1e-9);
            assert(max(abs(ym - yc)) < 1e-9);
            assert(max(abs(rm - rc)) < 1e-9);
            assert(max(abs(vm - vc)) < 1e-9);
            assert(max(abs(drm - drc)) < 1e-9);
            assert(max(abs(acpm - acpc)) < 1e-9);
            

            fprintf('tM = %g ms, tC = %g ms, f = %g\n', trm / N * 1000, trc / N * 1000, trm / trc);
        end
        
        function DisplayPlots(t, s, x, y, r, v, dr, acp)
            
            figure('Name', 'Path', 'Color', 'White');
            plot(x, y, 'r-');
            axis equal;
            grid on;
            box on;
            xlabel('x [m]');
            ylabel('y [m]');
            
            
            figure('Name', 'Velocity and path', 'Color', 'White');
            subplot(2,1,1);
            plot(t, v, 'r-');
            grid on;
            box on;
            xlabel('t [s]');
            ylabel('v(t) [m/s]');
            subplot(2,1,2);
            plot(t, s, 'r-');
            grid on;
            box on;
            xlabel('t [s]');
            ylabel('s(t) [m]');
            
            figure('Name', 'Yaw rate and angle', 'Color', 'White');
            subplot(3,1,1);
            plot(t, rad2deg(dr), 'r-', t(1:end-1), rad2deg(diff(r) ./ diff(t)), 'b-');
            grid on;
            box on;
            xlabel('t [s]');
            ylabel('dr(t) [o/s]');
            subplot(3,1,2);
            plot(t, rad2deg(r), 'r-', t(1:end-1), rad2deg(atan(diff(y) ./ diff(x))), 'b-', ...
                t(1:end-1), rad2deg(cumsum(dr(2:end) .* diff(t))), 'g-');
            grid on;
            box on;
            xlabel('t [s]');
            ylabel('r(t) [o]');
            subplot(3,1,3);
            plot(t, acp, 'r-');
            grid on;
            box on;
            xlabel('t[s]');
            ylabel('acp(t) [m/s^2]');
            
        end
        
    end
end