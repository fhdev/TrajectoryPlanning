#include <cstdlib>
#include "CtrlLqsYr.hpp"
#include "MathTp.hpp"

using namespace Eigen;

// Parameters ======================================================================================

// number of parameters
#define NP 8

// Inputs ==========================================================================================
// longitudinal velocity in vehicle - fixed coordintate system [m/s]
#define VelXV_U      (U(0))
// lateral velocity in vehicle - fixed coordintate system [m/s]
#define VelYV_U      (U(1))
// yaw (heading) angular velocity in ground - fixed system [rad/s]
#define YawVelZ_U    (U(2))
// yaw rate tracking error [rad/s]
#define YawVelZErr_U (U(3))    
// number of inputs
#define NU 4

// States and derivatives ==========================================================================
// integral of longitudinal velocity tracking error [m]
#define YawVelZErrInt_X     (X(0))
#define DYawVelZErrInt_dX   (dX(0))
// number of states
#define NX 1

// Outputs =========================================================================================
// driving (> 0) or braking (< 0) torque [Nm]
#define SteerAng_Y     (Y(0))
// number of outputs
#define NY 1

namespace TrajectoryPlanning
{
	Int64_T CtrlLqsYr::nP() const { return NP; }
	Int64_T CtrlLqsYr::nX() const { return NX; }
	Int64_T CtrlLqsYr::nU() const { return NU; }
	Int64_T CtrlLqsYr::nY() const { return NY; }

	CtrlLqsYr::CtrlLqsYr(
		Float64_T velLin,
		const Ref<const VectorXd> satVel,
		const Ref<const VectorXd> satSteerAng,
		Float64_T gainVelY,
		Float64_T gainYawVelZ_,
		Float64_T gainYawVelZTrackErrInt,
		Float64_T gainVelYInit,
		Float64_T gainYawVelZInit) :

		velLin(velLin),
		satVel(satVel),
		satSteerAng(satSteerAng),
		gainVelY(gainVelY),
		gainYawVelZ(gainYawVelZ),
		gainYawVelZTrackErrInt(gainYawVelZTrackErrInt),
		gainVelYInit(gainVelYInit),
		gainYawVelZInit(gainYawVelZInit)
	{
	}


	Int32_T CtrlLqsYr::Derivatives(
		Float64_T t,
		const Ref<const VectorXd> X,
		const Ref<const VectorXd> U,
		Ref<VectorXd> dX, 
		Ref<VectorXd> Y) const
	{
        // Check sizes
		CheckSizes(X, U, dX, Y);

		// State feedback equation
		SteerAng_Y = -gainVelY * VelYV_U - gainYawVelZ * YawVelZ_U - gainYawVelZTrackErrInt * YawVelZErrInt_X;

		// Integration with anti-windup
		Float64_T maxSteerAng = interpolate(satVel, satSteerAng, VelXV_U);
		if (SteerAng_Y > maxSteerAng)
		{
			SteerAng_Y = maxSteerAng;
			DYawVelZErrInt_dX = 0.0;
		}
		else if (SteerAng_Y < -maxSteerAng)
		{
			SteerAng_Y = -maxSteerAng;
			DYawVelZErrInt_dX = 0.0;
		}
		else
		{
			DYawVelZErrInt_dX = YawVelZErr_U;
		}

		return 0;
	}
}
