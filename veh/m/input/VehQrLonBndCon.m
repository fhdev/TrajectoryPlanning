%% VehQrLonIn **************************************************************************************
% [Summary]
%   This class represents boundary conditions for longitudinal quarter vehicle model.
%
% [Used in]
%   VehQrLon
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehQrLonBndCon < ChassisQrLonBndCon
    %% Instance methods ============================================================================
    methods
        %% Constructor -----------------------------------------------------------------------------
        function self = VehQrLonBndCon(varargin)
            self@ChassisQrLonBndCon(varargin{:});
        end

    end
end