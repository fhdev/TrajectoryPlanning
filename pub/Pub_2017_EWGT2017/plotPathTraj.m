% Parameters
W = 7;
H = 4;
F = 8;

type = 'LaneChange';

% Data
if strcmpi(type, 'LaneChange')
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'fc3_lanechange.mat'), 'T_fc_3');
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'co5_lanechange_1_1_1.mat'), 'T_co_5');
elseif strcmpi(type, 'LaneKeep')
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'fc3_lanekeep.mat'), 'T_fc_3');
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'co5_lanekeep_1_1_1.mat'), 'T_co_5');
else
    error('Non-existing type.');
end

%% Path
fig1 = nicefigure('Path', W, H);
ax1 = niceaxis(fig1, F);

% Dummy for legends
plot(ax1, nan, nan, 'Color', nicecolors.BlueBelizeHole);
plot(ax1, nan, nan, 'Color', nicecolors.GreenEmerald);

grad = @(lb, ub, n, i) lb + (ub - lb) * (i - 1) / (n - 1);
n = length(T_fc_3);
for i=1:n
    t = T_fc_3(i);
    plot(ax1, t.X(1,:), t.X(3,:), 'Color', nicecolors.BlueBelizeHole * grad(1.35, 0.55, n, i));
    
    plot(ax1, t.X_0(1), t.X_0(3), 'MarkerSize', 2.0, 'Marker', 'o', 'MarkerEdgeColor', 'Black', 'MarkerFaceColor', 'Black');
    plot(ax1, t.X_f(1), t.X_f(2), 'MarkerSize', 2.0, 'Marker', 'o', 'MarkerEdgeColor', 'Black', 'MarkerFaceColor', 'Black');
end
for i=1:n
    t = T_co_5(i);
    plot(ax1, t.X(1,:), t.X(3,:), 'Color', nicecolors.GreenEmerald * grad(1.25, 0.45, n, i));
end
axis(ax1, 'equal');

if strcmpi(type, 'LaneChange')
    xlim(ax1, [-5, 85]);
    ylim(ax1, [-5, 35]);
elseif strcmpi(type, 'LaneKeep')
    xlim(ax1, [-5, 85]);
    ylim(ax1, [-5, 60]);
end

xlabel(ax1, '$x^G_v$ (pos. north) [m]');
ylabel(ax1, '$y^G_v$ (pos. west) [m]');

l = nicelegend({'Con', 'ConOpt'}, ax1, F);

% Adjust by hand
fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

% Save
name = sprintf('Path_%s', type);
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

%% Path + heading
fig2 = nicefigure('Traj', W, H);
ax2 = niceaxis(fig2, F);

% Dummy for legends
plot(ax2, nan, nan, 'Color', nicecolors.BlueBelizeHole);
plot(ax2, nan, nan, 'Color', nicecolors.GreenEmerald);

grad = @(lb, ub, n, i) lb + (ub - lb) * (i - 1) / (n - 1);
n = length(T_fc_3);
for i=1:n
    t = T_fc_3(i);
    plot3(ax2, t.X(1,:), t.X(3,:), rad2deg(t.X(5,:)), 'Color', nicecolors.BlueBelizeHole * grad(1.35, 0.55, n, i) );
    
    plot3(ax2, t.X_0(1), t.X_0(3), rad2deg(t.X_0(5)), 'MarkerSize', 2.0, 'Marker', 'o', 'MarkerEdgeColor', 'Black', 'MarkerFaceColor', 'Black');
    plot3(ax2, t.X_f(1), t.X_f(2), rad2deg(t.X_f(3)), 'MarkerSize', 2.0, 'Marker', 'o', 'MarkerEdgeColor', 'Black', 'MarkerFaceColor', 'Black');
end
for i=1:n
    t = T_co_5(i);
    plot3(ax2, t.X(1,:), t.X(3,:), rad2deg(t.X(5,:)), 'Color', nicecolors.GreenEmerald * grad(1.25, 0.45, n, i) );
end

if strcmpi(type, 'LaneChange')
    view(ax2, [75, 28]);
    axis(ax2, [-5, 85, -5, 35, 0.0, 35]);
    ax2.Position = [0.18, 0.20, 0.78, 0.76];
    ax2.ZTick = [0, 10, 20, 30];
    xlabel(ax2, '$x^G_v$ (pos. north) [m]');
    ylabel(ax2, '$y^G_v$ (pos. west) [m]');
    zlabel(ax2, '$\psi_v$ (yaw angle) [$^\circ$]');
elseif strcmpi(type, 'LaneKeep')
    view(ax2, [55, 5]);
    axis(ax2, [-5, 85, -5, 60, 0.0, 80]);
    ax2.Position = [0.17, 0.22, 0.78, 0.72];
    ax2.ZTick = [0, 20, 40, 60, 80];
    xlabel(ax2, '$x^G_v$ (pos. north) [m]');
    ylabel(ax2, '$y^G_v$ (pos. west) [m]');
    zlabel(ax2, '$\psi_v$ (yaw angle) [$^\circ$]');
end

l = nicelegend({'Con', 'ConOpt'}, ax2, F);

% Adjust by hand
fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

% Save
name = sprintf('Traj_%s', type);
savefig(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');


