% Parameters
Dx =   1.0;
Bx =  15.0;
Cx =   1.45;
Ex =   0.4;
Dy =   0.9;
By =  13.0;
Cy =   1.85;
Ey =   0.5;
% Magic Formula
mf = @(D, C, B, E, s) ...
    D .* sin(C .* atan(B .* s - E .* (B .* s - atan(B .* s))));
% Slip
sx = (-1:0.01:1)';
sy = [-.03; +.03; -.10; +.10; -.20; +.20];
[Sx, Sy] = meshgrid(sx, sy);
% Forces with superposition
Sx2 = Sx.^2;
Sy2 = Sy.^2;
Sc  = sqrt(Sx2 + Sy2);
Fx = mf(Dx, Cx, Bx, Ex, Sc);
Fy = mf(Dy, Cy, By, Ey, Sc);
Fx2 = Fx.^2;
Fy2 = Fy.^2;
Fxc = sqrt(Fx2 .* Fy2 ./ (Fy2 + Sy2 ./ Sx2 .* Fx2)) .* sign(Sx);
Fyc = sqrt(Fx2 .* Fy2 ./ (Fx2 + Sx2 ./ Sy2 .* Fy2)) .* sign(Sy);
% Plot
W = 14;
H = 9;
F = 8;
colors = {1.1 * nicecolors.RedAlizarin, 0.7 * nicecolors.RedAlizarin, 1.1 * nicecolors.PurpleAmethyst, ...
    0.7 * nicecolors.PurpleAmethyst, 1.5 * nicecolors.BlueDeepKoamaru, 1.0 * nicecolors.BlueDeepKoamaru};
fig = nicefigure('mf', W, H);
ax = niceaxis(fig, F);
hold(ax, 'on');
for i = 1 : length(sy)
    plot(ax, sx, Fxc(i,:), 'Color', colors{i}, 'LineStyle', '-');
    plot(ax, sx, Fyc(i,:), 'Color', colors{i}, 'LineStyle', '-.');
end
axis(ax, 'equal');
xlim(ax, [-1;1]);
ylim(ax, [-1;1]);
xlabel(ax, '$s_x$ (longitudinal slip) [1]');
ylabel(ax, '$F^W_{[f/r]c,[x/y]}$ (unit tire forces) [N]');
l = nicelegend({'$f^W_{[f/r]c,x}$ at $s_y=+0.03$', '$f^W_{[f/r]c,y}$ at $s_y=+0.03$', '$f^W_{[f/r]c,x}$ at $s_y=-0.03$'...
    '$f^W_{[f/r]c,y}$ at $s_y=-0.03$', '$f^W_{[f/r]c,x}$ at $s_y=+0.10$', '$f^W_{[f/r]c,y}$ at $s_y=+0.10$',...
    '$f^W_{[f/r]c,x}$ at $s_y=-0.10$', '$f^W_{[f/r]c,y}$ at $s_y=-0.10$', '$f^W_{[f/r]c,x}$ at $s_y=+0.20$',...
    '$f^W_{[f/r]c,y}$ at $s_y=+0.20$', '$f^W_{[f/r]c,x}$ at $s_y=-0.20$', '$f^W_{[f/r]c,y}$ at $s_y=-0.20$'},...
    ax, F);
l.Location = 'NorthEastOutside';
annotation(fig, 'textbox', [0.7, 0.225, 0.25, 0.075], 'String',  '$\mu_{[f/r]} F^W_{[f/r],z} = 1$ N', ...
    'Interpreter', 'LaTeX', 'LineStyle', 'none', 'FontSize', F);
% Save
name = 'MagicFormula';
savefig(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');
