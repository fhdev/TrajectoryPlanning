%% WheelMf *****************************************************************************************
% [Summary]
%   This class represents a wheel model with Magic Formula tire.
%
% [Used in]
%   CtrlLatTrk
%   CtrlLatYr
%   VehQr
%   VehStOl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef WheelMf < WheelMfLon
    %% Instance properties =========================================================================
    properties    
        %% Properties set in constructor -----------------------------------------------------------
        % Magic formula parameters
        % Lateral maximal value (Dy) [1]
        MfMaxY
        % Lateral stiffness factors (By) [1]
        MfStiffnessY
        % Lateral shape factors (Cy) [1]
        MfShapeY
        % Lateral curvature factors (Ey) [1]
        MfCurvatureY
        
        % Transient slip model parameters
        % Relaxation length at zero lateral slip in the lateral direction [m]
        RelaxationLength0Y
        % Minimal lateral relaxation length [m]
        RelaxationLengthMinY
        % Standstill lateral slip damping [Ns/m]
        SlipDamping0Y
        % Lateral velocity at which slip damping switches off [m/s^2]
        VelSlowY
        
        % Slip superposition parameters
        % Minimal slip at which slip superposition is considered [1]
        MinSlip
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.MfMaxY(self, value)
            if ~isequal(self.MfMaxY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MfMaxY must be numeric scalar!')
                end
                self.MfMaxY = value;
            end
        end
        
        function set.MfStiffnessY(self, value)
            if ~isequal(self.MfStiffnessY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MfStiffnessY must be numeric scalar!')
                end
                self.MfStiffnessY = value;
            end
        end
        
        function set.MfShapeY(self, value)
            if ~isequal(self.MfShapeY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MfShapeY must be numeric scalar!')
                end
                self.MfShapeY = value;
            end
        end
        
        function set.MfCurvatureY(self, value)
            if ~isequal(self.MfCurvatureY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MfCurvatureY must be numeric scalar!')
                end
                self.MfCurvatureY = value;
            end
        end
        
        function set.RelaxationLength0Y(self, value)
            if ~isequal(self.RelaxationLength0Y, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: RelaxationLength0Y must be numeric scalar!')
                end
                self.RelaxationLength0Y = value;
            end
        end
        
        function set.RelaxationLengthMinY(self, value)
            if ~isequal(self.RelaxationLengthMinY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: RelaxationLengthMinY must be numeric scalar!')
                end
                self.RelaxationLengthMinY = value;
            end
        end
        
        function set.SlipDamping0Y(self, value)
            if ~isequal(self.SlipDamping0Y, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SlipDamping0Y must be numeric scalar!')
                end
                self.SlipDamping0Y = value;
            end
        end
        
        function set.VelSlowY(self, value)
            if ~isequal(self.VelSlowY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: VelSlowY must be numeric scalar!')
                end
                self.VelSlowY = value;
            end
        end
        
        function set.MinSlip(self, value)
            if ~isequal(self.MinSlip, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MinSlip must be numeric scalar!')
                end
                self.MinSlip = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = WheelMf(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 25
                superArgs = varargin(1:16);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@WheelMfLon(superArgs{:});
            if nargin == 1
                self.MfMaxY               = varargin{1}.MfMaxY;
                self.MfStiffnessY         = varargin{1}.MfStiffnessY;
                self.MfShapeY             = varargin{1}.MfShapeY;
                self.MfCurvatureY         = varargin{1}.MfCurvatureY;
                self.RelaxationLength0Y   = varargin{1}.RelaxationLength0Y;
                self.RelaxationLengthMinY = varargin{1}.RelaxationLengthMinY;
                self.SlipDamping0Y        = varargin{1}.SlipDamping0Y;
                self.VelSlowY             = varargin{1}.VelSlowY;
                self.MinSlip              = varargin{1}.MinSlip;
            elseif nargin == 25
                self.MfMaxY               = varargin{17};
                self.MfStiffnessY         = varargin{18};
                self.MfShapeY             = varargin{19};
                self.MfCurvatureY         = varargin{20};
                self.RelaxationLength0Y   = varargin{21};
                self.RelaxationLengthMinY = varargin{22};
                self.SlipDamping0Y        = varargin{23};
                self.VelSlowY             = varargin{24};
                self.MinSlip              = varargin{25};
            end
        end
        
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function contains the equations of motion of the longitudinal wheel model.
        %   References: [Pacejka]: Hans Pacejka - Tyre and Vehicle Dynamics (2nd edition) 2006.
        %               [Schramm]: Dieter Schramm - Vehicle Dynamics 2014
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => PitchY:    angle of the wheel [rad]
        %       X(2) => PitchVelY: angular velocity of the wheel [rad/s]
        %       X(3) => SlipX:     longitudinal slip [1]
        %       X(4) => SlipY:     lateral slip [1]
        %
        %   U - input vector
        %       U(1) => TorqueDrv: driving torque [Nm]
        %       U(2) => TorqueBrk: braking torque [Nm]
        %       U(3) => Friction:  friction coefficient [1]
        %       U(4) => ForceZW:   vertical force (tire load) in wheel-fixed cs. [N]
        %       U(5) => VelXW:     lon. velocity of the wheel center point in wheel-fixed cs. [m/s]
        %       U(6) => VelYW:     lat. velocity of the wheel center point in wheel-fixed cs. [m/s]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %       Y(1) => PitchAccY: angular acceleration [rad/s]
        %       Y(2) => DSlipX:    longitudinal slip derivative [1/s]
        %       Y(3) => ForceXW:   longitudinal force in wheel-fixed coordinate system [N]
        %       Y(4) => DSlipY:    lateral slip derivative [1/s]
        %       Y(5) => ForceYW:   lateral force in wheel-fixed coordinate system [N]
        % ------------------------------------------------------------------------------------------        
         function [dX, Y] = StateEquation(self, t, X, U) %#ok<INUSL> used for debugging
            %% Calculate necessary variables            
            % Calculation of maximal transmissable forces
            forceXWMax = self.MfMaxX * U(3) * U(4);
            forceYWMax = self.MfMaxY * U(3) * U(4);
            
            % Calculation of slip stiffness values ([Pacejka] Eq. 8.115 @ P. 405 & Eq. 1.6 @ P. 6)
            slipStiffnessX = self.MfStiffnessX * self.MfShapeX * forceXWMax;
            slipStiffnessY = self.MfStiffnessY * self.MfShapeY * forceYWMax;
            
            % Calculation of slip damping factor (Eq. 8.128 @ P. 407)
            if abs(U(5)) <= self.VelSlowX
                slipDampingX = self.SlipDamping0X * (0.5 * (1 + cos(pi * abs(U(5)) / self.VelSlowX)));
            else
                slipDampingX = 0;
            end
            
            % Calculation of damped slip values (Eq. 8.127 @ P. 407)
            velRollW = self.Radius * X(2);
            slipDampedX = X(3) + slipDampingX / slipStiffnessX * (velRollW - U(5));
            slipDampedY = X(4);
            
            if (abs(slipDampedX) > self.MinSlip) && (abs(slipDampedY) > self.MinSlip)
                % Calculation of tire forces in wheel-fixed coordinate system
                % ([Pacejka] Eq. 8.109 @ P. 404) ([Schramm] Eq. 7.49 @ P. 166)
                slipDampedX2 = slipDampedX^2;
                slipDampedY2 = slipDampedY^2;
                slipCombined = sqrt(slipDampedX2 + slipDampedY2);
                forceXW = forceXWMax * sin(self.MfShapeX * atan(self.MfStiffnessX * slipCombined - ...
                    self.MfCurvatureX * (self.MfStiffnessX * slipCombined - ...
                    atan(self.MfStiffnessX * slipCombined))));
                forceYW = forceYWMax * sin(self.MfShapeY * atan(self.MfStiffnessY * slipCombined - ...
                    self.MfCurvatureY * (self.MfStiffnessY * slipCombined - ...
                    atan(self.MfStiffnessY * slipCombined))));

                % Calculation of tire forces in case of slip superposition (own deduction)
                forceXW2 = forceXW^2;
                forceYW2 = forceYW^2;
                forceSupXW = sqrt(forceXW2 * forceYW2 / ...
                    (forceYW2 + slipDampedY2 / slipDampedX2 * forceXW2)) * sign(slipDampedX);
                forceSupYW = sqrt(forceXW2 * forceYW2 / ...
                    (forceXW2 + slipDampedX2 / slipDampedY2 * forceYW2)) * sign(slipDampedY);
            else
                % Calculation of tire forces in wheel-fixed coordinate system 
                % ([Pacejka] Eq. 8.109 @ P. 404)
                forceSupXW = forceXWMax * sin(self.MfShapeX * atan(self.MfStiffnessX * slipDampedX - ...
                    self.MfCurvatureX * (self.MfStiffnessX * slipDampedX - ...
                    atan(self.MfStiffnessX * slipDampedX))));
                forceSupYW = forceYWMax * sin(self.MfShapeY * atan(self.MfStiffnessY * slipDampedY - ...
                    self.MfCurvatureY * (self.MfStiffnessY * slipDampedY - ...
                    atan(self.MfStiffnessY * slipDampedY))));
            end
            
            % Calculation of rolling resistance torque
            % (https://www.mathworks.com/help/physmod/sdl/ref/rollingresistance.html)
            torqueRollRes = U(4) * self.Radius * (self.RollingResistanceConst + ...
                self.RollingResistanceLin * abs(velRollW) + self.RollingResistanceSqr * velRollW^2) * ...
                sign(velRollW);
            if abs(velRollW) < self.RollingResistanceVel
                torqueRollRes = torqueRollRes * 0.5 * (1 - cos(pi * abs(velRollW) / ...
                    self.RollingResistanceVel));
            end
            
            % Calculation of acting braking torque
            torqueBrakeAct = U(2) * sign(velRollW);
            brakingTorqueVel = self.BrakingTorqueVel + self.RollingResistanceVelCorr * abs(U(2));
            if abs(velRollW) < brakingTorqueVel
                torqueBrakeAct = torqueBrakeAct * (0.5 * (1 - cos(pi * abs(velRollW) / brakingTorqueVel)));
            end
            
            % Calculation of slip-dependent relaxation lengths
            % ([Pacejka] Eq. 8.121 @ P. 406 & Eq. 7.36 @ P. 349)
            relaxationLengthX = max(self.RelaxationLength0X * (1 - slipStiffnessX / ...
                (3 * forceXWMax) * abs(X(3))), self.RelaxationLengthMinX);
            relaxationLengthY = max(self.RelaxationLength0Y * (1 - slipStiffnessY / ...
                (3 * forceYWMax) * abs(X(4))), self.RelaxationLengthMinY);
            
            %% Calculation of state derivatives
            % Canculation of agular acceleration
            pitchAccY = (U(1) - torqueBrakeAct - self.Radius * forceSupXW - torqueRollRes) / ...
                self.MomentOfInertiaY;
            % Calculation of transient longitudinal slip (Eq. 8.119 @ P. 406)
            dSlipX = (velRollW - U(5) - abs(U(5)) * X(3)) / relaxationLengthX;
            % Calculation of transient lateral slip (Eq. 7.35 @ P. 349)
            dSlipY = (-U(6) - abs(U(5)) * X(4)) / relaxationLengthY;
            %% State derivative variables
            dX = [X(2);      % PitchVelY: angular velocity [rad/s]
                  pitchAccY; % PitchAccY: angular acceleration [rad/s^2]
                  dSlipX;    % DSlipX: longitudinal slip derivative [1/s]
                  dSlipY];   % DSlipY: lateral slip derivative [1/s]
            %% Output variables
            Y = [pitchAccY;  % PitchAccY: angular acceleration [rad/s]
                 dSlipX;     % DSlipX:    longitudinal slip derivative [1/s]
                 forceSupXW;    % ForceXW:   longitudinal force in wheel-fixed coordinate system [N]
                 dSlipY;     % DSlipY:    lateral slip derivative [1/s]
                 forceSupYW];   % ForceYW:   lateral force in wheel-fixed coordinate system [N]
         end
         
    end
end