%% PlannerConOut ************************************************************************************
% [Summary]
%   This class represents outputs of a constrained nonlinear solver based motion planner PlannerCon.
%
% [Used in]
%   PlannerCon
%
% [Subclasses]
%   PlannerOptOut
% **************************************************************************************************
classdef PlannerGoptOut < handle
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Calculation time of trajectors [s]
        CalcTime
        % Success flag
        Success
        % Final reference signal parameters (PlannerRefParam object)
        OptVar         % PlannerOptVarVelPath object
        % Output of geometric trajectory planning
        PlannerGeomOut % PlannerGeomOut object
        % Final value of deviations (constraints) (just as info, problem is unconstrained)
        ConGeom        % PlannerConFcnGeom object
        % Final value of objective function components
        CostGeom       % PlannerOptFcnGeom object
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------

        function set.CalcTime(self, value)
            if ~isequal(self.CalcTime, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: CalcTime must be a numeric scalar!')
                end
                self.CalcTime = value;
            end
        end
        
        function set.Success(self, value)
            if ~isequal(self.Success, value)
                if ~(islogical(value) && isscalar(value))
                    error('ERROR: Success must be a logical scalar!')
                end
                self.Success = value;
            end
        end
        function set.OptVar(self, value)
            if ~isequal(self.OptVar, value)
                if ~isa(value, 'PlannerOptVarVelPath') || ~isscalar(value)
                    error('ERROR: OptVar must be a PlannerOptVarVelPath object!')
                end
                self.OptVar = value;
            end
        end
        function set.PlannerGeomOut(self, value)
            if ~isequal(self.PlannerGeomOut, value)
                if ~isa(value, 'PlannerGeomOut') || ~isscalar(value)
                    error('ERROR: PlannerGeomOut must be a PlannerGeomOut object!')
                end
                self.PlannerGeomOut = value;
            end
        end        
        function set.ConGeom(self, value)
            if ~isequal(self.ConGeom, value)
                if ~isa(value, 'PlannerConFcnGeom') || ~isscalar(value)
                    error('ERROR: ConState must be a PlannerConFcnGeom object!')
                end
                self.ConGeom = value;
            end
        end
        function set.CostGeom(self, value)
            if ~isequal(self.CostGeom, value)
                if ~isa(value, 'PlannerOptFcnGeom') || ~isscalar(value)
                    error('ERROR: ConState must be a PlannerOptFcnGeom object!')
                end
                self.CostGeom = value;
            end
        end
        
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerGoptOut(calcTime, success, optVar, plannerGeomOut, conGeom, costGeom)
            %% Set properties
            self.CalcTime = calcTime;
            self.Success = success;
            self.OptVar = optVar;
            self.PlannerGeomOut = plannerGeomOut;
            self.ConGeom = conGeom;
            self.CostGeom = costGeom;
        end
    end
end
