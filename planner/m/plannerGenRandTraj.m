%% Parameters
% maximal allowed lateral acceleration
aymax = 4;
% minimal velocity
vmin = 10;
% maximal velocity
vmax = 30;
% arc length for initial and final straight segment
sstraight = 200;
% maximum multiplyier for curve radius
mRmax = 10;
% proportion of maximal arc length of normal segment to curve circumference
pCmax = 0.2;
% proportion of minimal arc length of normal segment to curve circumference
pCmin = 0.1;
% proportion of straight segments must be < 0.5
pS = 0.35;
% proportion of arc length of transition segments to normal segments
pT = 0.4;
% number of segments to generate
n = 500;

%% Generate segments
% Random velocities from the allowed range
v = floor(rand(n, 1) * (vmax - vmin) + vmin);
% Minimal allowed radius
Rmin = ceil(v.^2 ./ aymax);
% Random radii based on minimal allowed radius
R = ceil(Rmin .* (1 + rand(n, 1) * (mRmax - 1)));
% Random arc length segment values for main curves
C = 2 * R * pi;
l = floor(C .* (pCmin + (pCmax - pCmin) * rand(n, 1)));
% Curvature
iR = randperm(n);
k = 1 ./ R;
% Make half of the turns in the opposite direction
k(iR(1 : floor(0.5 * n))) = -1 * k(iR(1 : floor(0.5 * n)));
% Make cS proportion straight, not allowing the bounaries to be straight not allowing neighboring 
% straight segments
iS = 2 * randperm(n);
iS(iS > n - 1) = [];
k(iS(1 : floor(pS * n))) = 0;
% Add initial and final straight segment
k = [0; k; 0];
v = [v(1); v; v(end)];
l = [sstraight; l; sstraight];
% Multiply for transient segments
kp = reshape([k, k]', 2 * (n + 2), 1);
vp = reshape([v, v]', 2 * (n + 2), 1);
% Arc length segment values for transition curves
lt = floor(pT * (l(1:end-1) + l(2:end)) / 2);
lp = reshape([[0; lt], l]', 2 * (n + 2), 1);
sp = cumsum(lp);
% Generate trajectory
[t, s, x, y, r, v, dr, acp] = PlannerGeom.CurvatureTraj(sp, kp, vp);

%% Guess size of simulation output
nSignals = 17 + 4 + 32;
sizeMb = nSignals * 8 * 1000 * t(end) / 1024^2 * 1.2;
fprintf('Approximate simulation output size is %g MB.\n', sizeMb); 

%% Save raw trajectory
info = dir('trajectory*.mat');
save(sprintf('trajectory_%03d.mat', length(info) + 1), '-v7.3', 'kp', 'vp', 'sp', 't', 's', 'x', 'y', 'r', 'v', 'dr', 'acp');

%% Plot trajectory
% find indices where the knot ponints are located
iknot = find(sum(s == sp', 2));
% find indices where path is straight
istraight = find(dr == 0);
% find indices where path is curved
icurve = find(dr ~= 0);
% find switch indices between straight and curved segments
iswitch = [1; find(diff(istraight) > 1) + 1; length(istraight)];

f1 = nicefigure('Path', 8, 8); 
a11 = niceaxis(f1, 8);
plot(a11, x(icurve), y(icurve), 'b-');
for i = 1 : length(iswitch) - 1
    istart = istraight(iswitch(i));
    iend = istraight(iswitch(i + 1) - 1);
    plot(a11, x(istart:iend), y(istart:iend), 'm-');
end
plot(a11, x(iknot), y(iknot), 'ro', 'MarkerFaceColor', 'r', 'MarkerSize', 2);
axis(a11, 'equal');
xlabel(a11, '$x \mathrm{[m]}$');
ylabel(a11, '$y \mathrm{[m]}$');

f2 = nicefigure('Curvature', 8, 4);
a21 = niceaxis(f2, 8);
subplot(2, 1, 1, a21);
plot(a21, sp, kp);
xlabel(a21,'$t \mathrm{[s]}$');
ylabel(a21, '$\kappa \mathrm{[1/m]}$');
a22 = niceaxis(f2, 8);
subplot(2, 1, 2, a22);
plot(a22, t, acp);
xlabel(a22,'$t \mathrm{[s]}$');
ylabel(a22, '$a_{cp} \mathrm{[m/s^2]}$');

