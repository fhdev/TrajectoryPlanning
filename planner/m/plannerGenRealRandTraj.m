%% Clean up
clearvars;
close all;
DETAILED_PLOTS = false;
OUTPUT_DIR = fullfile('E:', 'VehModel_NN_2020');
%% Parameters
% number of segments (1 segment = 1 transition + 1 curve / straight
nSegments = 1000;
% minimal and maximal allowed velocity [m/s]
vxMin = 10;
vxMax = 30;
% minimal and maximal allowed lateral acceleration [m/s^2]
ayMin = 0.5;
ayMax = 6;
% minimal and maximal allowed longitudinal acceleration [m/s^2]
axMin = 0.5;
axMax = 5;
% minimal time for a segment [s]
tMin = 2;
% maximal time for constant velocity segment [s]
tConstMax = 3;
% multiplier for calculating maximal radius from minimal radius constraint [1]
cMaxRadius = 10;
% multiplier for calculating minimal time from maximal time constraint [1]
cMinTime = 0.3;
% multiplier for calculating minimal radius from given arc length [1]
% minimal radius is selected such that the given arc length is maximally cMinCirc portion of half
% circumference
cMinCirc = 2;
% proportion of straight segments [1]
pStraight = 0.33;
% proportion of constant velocity segments [1]
pConst = 0.33;
% proportion of acceleration segments [1]
pAcc = 0.33;
% proportion of length of transition subsegments [1]
pTransLength = 0.3;
% proportion of left turns [1]
pLeft = 0.5;

%% Generate random trajectory
rng('shuffle');

% Allocate storage
nKnots = 2 * nSegments + 1;
velXKnots = nan(nKnots, 1);
curvKnots = nan(nKnots, 1);
deltaTimeKnots = nan(nKnots, 1);
deltaLengthKnots = nan(nKnots, 1);
lengthKnots = nan(nKnots, 1);

% Set values for first point
velXKnots(1) = (vxMin + vxMax) / 2;
curvKnots(1) = 0;
deltaTimeKnots(1) = 0;
deltaLengthKnots(1) = 0;
lengthKnots(1) = 0;

% Generate segments -> segment = transition + curve / straight
for iKnot = 2 : 2 : nKnots - 1
    %% Get indices
    iPrev = iKnot - 1;
    iTransition = iKnot;
    iSegment = iKnot + 1;
    %% Choose next longitudinal maneuver
    % Choose maneuver: constant / accelerate / decelerate
    % The first and last segments will maintain velocity.
    % If we can't accelerate further for at least tMin time, we choose to decelerate or maintain velocity.
    % If we can't decelerate further for at least tMin time, we choose to accelerate or maintain velocity.
    % Otherwise, we choose randomly according to the given probabilities. 
    if (iKnot == 1) || (iKnot == 2 * nSegments)
        lonType = 'constant';
    elseif (vxMax - velXKnots(iPrev)) < (tMin * axMax)
        if rand() < 0.5
            lonType = 'constant';
        else
            lonType = 'decelerate';
        end
    elseif (velXKnots(iPrev) - vxMin) < (tMin * axMax)
        if rand() < 0.5
            lonType = 'constant';
        else
            lonType = 'accelerate';
        end
    else
        choice = rand();
        if choice < pConst
            lonType = 'constant';
        elseif choice < pConst + pAcc
            lonType = 'accelerate';
        else
            lonType = 'decelerate';
        end
    end
    % Calculate acceleration
    switch lonType
        case 'constant'
            % Maintain speed
            ax = 0;
            % Maximal time to maintain speed
            tMax = tConstMax;
        case 'accelerate'
            % Accelerate
            ax = + (axMin + rand() * (axMax - axMin));
            % Maximal time to maintain longitudinal acceleration
            tMax = (vxMax - velXKnots(iPrev)) / ax;
        case 'decelerate'
            % Brake
            ax = - (axMin + rand() * (axMax - axMin));
            % Maximal time to maintain longitudinal deceleration
            tMax = (vxMin - velXKnots(iPrev)) / ax;
    end
    % Calculate time on segment
    t = tMax * (cMinTime + rand() * (1 - cMinTime));
    % Distribute time on segment between transition and straight / curve
    deltaTimeKnots(iTransition) = pTransLength * t;
    deltaTimeKnots(iSegment) = (1 - pTransLength) * t;
    % Calculate longitudinal velocity
    velXKnots(iTransition) = velXKnots(iPrev) + ax * deltaTimeKnots(iTransition);
    velXKnots(iSegment) = velXKnots(iTransition) + ax * deltaTimeKnots(iSegment);
    % Calculate arc lengthes
    deltaLengthKnots(iTransition) = velXKnots(iPrev) * deltaTimeKnots(iTransition) + ax / 2 * deltaTimeKnots(iTransition)^2;
    deltaLengthKnots(iSegment) = velXKnots(iTransition) * deltaTimeKnots(iSegment) + ax / 2 * deltaTimeKnots(iSegment)^2;
    lengthKnots(iTransition) = lengthKnots(iPrev) + deltaLengthKnots(iTransition);
    lengthKnots(iSegment) = lengthKnots(iTransition) + deltaLengthKnots(iSegment);
    
    %% Choose next curve type
    % Choose maneuver: straight / left / right
    latChoice = rand();
    if (latChoice <= pStraight) || (iKnot == 1) || (iKnot == 2 * nSegments)
        latType = 'straight';
    else
        if curvKnots(iPrev) < 0
            latType = 'left';
        elseif curvKnots(iPrev) > 0
            latType = 'right';
        else
            dirChoice = rand();
            if dirChoice <= pLeft
                latType = 'left';
            else
                latType = 'right';
            end
        end
    end
    % Calculate curvature
    if strcmpi(latType, 'straight')
        % Straight
        curvKnots(iSegment) = 0;
    else
        % Curve
        % Minimal radius based on allowed maximal lateral acceleration
        rMinAcc = max(velXKnots(iSegment), velXKnots(iPrev))^2 / ayMax;
        % Minimal radius based on allowed maximal curve length
        rMinCirc = deltaLengthKnots(iSegment) / (pi * cMinCirc);
        % We choose the bigger minimal radius
        rMin = max(rMinAcc, rMinCirc);
        % Calculate actual radius
        r = rMin * (1 + rand() * (cMaxRadius - 1));
        % Calculate curvature based on direction
        if strcmpi(latType, 'left')
            % Left curve
            curvKnots(iSegment) = + 1 / r;
        else
            % Right curve
            curvKnots(iSegment) = - 1 / r;
        end

    end
    curvKnots(iTransition) = curvKnots(iSegment);
end

%% Generate
[t, s, x, y, r, v, dr, acp] = PlannerGeom.CurvatureTraj(lengthKnots, curvKnots, velXKnots);
trajectory.LengthKnots = lengthKnots;
trajectory.CurvKnots = curvKnots;
trajectory.VelXKnots = velXKnots;
trajectory.Time = t;
trajectory.Length = s;
trajectory.PosXG = x;
trajectory.PosYG = y;
trajectory.YawZ = r;
trajectory.VelXV = v;
trajectory.YawVelZ = dr;
trajectory.AccInrtYV = acp;

%% Guess size of simulation output
nSignals = 17 + 4 + 32;
sizeMb = nSignals * 8 * 1000 * t(end) / 1024^2 * 1.2;
fprintf('Approximate simulation output size is %g MB.\n', sizeMb);

%% Save
info = dir(fullfile(OUTPUT_DIR, 'trajectory*.mat'));
save(fullfile(OUTPUT_DIR, sprintf('trajectory_%03d.mat', length(info) + 1)), '-v7.3', 'trajectory');

%% Plot
if DETAILED_PLOTS
    % find indices where the knot ponints are located
    iknot = find(sum(diff(s <= lengthKnots'), 2));
    % find indices where path is straight
    istraight = find(dr == 0);
    % find indices where path is curved
    icurve = find(dr ~= 0);
    % find switch indices between straight and curved segments
    iswitch = [1; find(diff(istraight) > 1) + 1; length(istraight)];
    
    f1 = nicefigure('Path', 8, 8);
    a11 = niceaxis(f1, 8);
    plot(a11, x(icurve), y(icurve), 'b-');
    for i = 1 : length(iswitch) - 1
        istart = istraight(iswitch(i));
        iend = istraight(iswitch(i + 1) - 1);
        plot(a11, x(istart:iend), y(istart:iend), 'm-');
    end
    plot(a11, x(iknot), y(iknot), 'ro', 'MarkerFaceColor', 'r', 'MarkerSize', 2);
    axis(a11, 'equal');
    xlabel(a11, '$x \mathrm{[m]}$');
    ylabel(a11, '$y \mathrm{[m]}$');
    
    
    f2 = nicefigure('Curvature and velocity', 8, 8);
    a21 = niceaxis(f2, 8);
    subplot(2, 1, 1, a21);
    plot(a21, lengthKnots, curvKnots);
    xlabel(a21,'$s \mathrm{[m]}$');
    ylabel(a21, '$\kappa \mathrm{[1/m]}$');
    a22 = niceaxis(f2, 8);
    subplot(2, 1, 2, a22);
    plot(a22, t, v);
    xlabel(a22,'$t \mathrm{[s]}$');
    ylabel(a22, '$v_x \mathrm{[m/s]}$');
    
    f3 = nicefigure('Lateral acceleration', 8, 4);
    a31 = niceaxis(f3, 8);
    plot(a31, t, acp);
    xlabel(a31,'$t \mathrm{[s]}$');
    ylabel(a31, '$a_{cp} \mathrm{[m/s^2]}$');
else
    f1 = nicefigure('Path', 8, 8);
    a11 = niceaxis(f1, 8);
    plot(a11, x, y, 'b-');
    axis(a11, 'equal');
    xlabel(a11, '$x \mathrm{[m]}$');
    ylabel(a11, '$y \mathrm{[m]}$');
end


