channelList = canChannelList;

%% Switches
USE_VIRTUAL = 1;

%% Constants
CANCASE_RX_CH_NUM = 1;
CANCASE_TX_CH_NUM = 2;
VIRTUAL_RX_CH_NUM = 1;
VIRTUAL_TX_CH_NUM = 1;

%% Create CAN channels
virtualChannel1 = canChannel('Vector', 'Virtual 1', 1);
virtualChannel2 = canChannel('Vector', 'Virtual 1', 2);

if USE_VIRTUAL
    device = 'Virtual 1';
    % Receive channels
    channelRxImar  = canChannel('Vector', device, VIRTUAL_RX_CH_NUM);
    channelRxImu   = canChannel('Vector', device, VIRTUAL_RX_CH_NUM);
    channelRxVBox  = canChannel('Vector', device, VIRTUAL_RX_CH_NUM);
    % Transmit channels
    channelTxSmart = canChannel('Vector', device, VIRTUAL_TX_CH_NUM);
    channelTxImar  = canChannel('Vector', device, VIRTUAL_TX_CH_NUM);
else
    device = 'CANcaseXL 1';
    % Receive channels
    channelRxImar  = canChannel('Vector', device, CANCASE_RX_CH_NUM);
    channelRxImu   = canChannel('Vector', device, CANCASE_RX_CH_NUM);
    channelRxVBox  = canChannel('Vector', device, CANCASE_RX_CH_NUM);
    % Transmit channels
    channelTxSmart = canChannel('Vector', device, CANCASE_TX_CH_NUM);
end

%% Load dbc
dbcImar  = canDatabase('imar.dbc');
dbcImu   = canDatabase('MM5_10_Default.dbc');
dbcVbox  = canDatabase('VBOX3iSL_ADAS_VCI_VEHICO.dbc');
dbcSmart = canDatabase('smart_control.dbc');

%% Assign database to channels
% Receive channels
channelRxImar.Database = dbcImar;
channelRxImu.Database = dbcImu;
channelRxVBox.Database = dbcVbox;
% Transmit channels
channelTxSmart.Database = dbcSmart;
channelTxImar.Database = dbcImar;

%% Filter necessary data
filterAllowOnly(channelRxImar, {'Attitude', 'Body_Velocity', 'INS_Latitude', 'INS_Longitude'});
% Accelerations and angular rates
filterAllowOnly(channelRxImu, {'TX1', 'TX2', 'TX3'});
% Control signals
filterAllowOnly(channelTxSmart, 'Control_message');

%% Start channels
start(channelRxImar);
start(channelTxImar);

%% Build dummy messag

i = 1;
while true
    if channelRxImar.MessagesAvailable
        msgs = receive(channelRxImar, 5);
        fprintf('%g ', msgs(1).Signals.V_X);
        if mod(i, 10) == 0
            fprintf('\n');
        end
        i = i + 1;
    end
end
