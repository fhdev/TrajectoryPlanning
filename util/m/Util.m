%% Util ********************************************************************************************
% [Summary]
%   This class contains utlility functions that can be used by any modules.
%
% [Used in]
%   All classes.
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef Util
    %% Public static methods =======================================================================
    methods (Static = true)
        
        function repoRoot = GetRepoRoot()
            repoRoot = fileparts(which('tpinit'));
        end
        
        %% EvalTrapz -------------------------------------------------------------------------------
        % [Summary]
        %   This function evaluates the trapezoidal profile specified by parameter vector p in the
        %   points specified by x.
        %
        % [Input]
        %   x - Points in which the trapezoidal profile is to be evaluated.
        %   p - Parameter vector of the trapezoidal profile. p = [v0, a0, vt, af, vf]
        %
        % [Output]
        %   y - Values of trapezoidal profile in points x.
        % ------------------------------------------------------------------------------------------
        function y = EvalTrapz(x, p)
            % Check inputs
            np = length(p);
            nx = length(x);
            if ~isvector(x) || (nx < 2)
                error(['Evaluation points must be specified by a vector with a minimal length ', ...
                    'of 2.']);
            end
            if ~isvector(p) || (np ~= 5)
                error('Parameters must be specified by a vector with a length of 5.');
            end
            y0 = p(1);
            s0 = p(2);
            yt = p(3);
            sf = p(4);
            yf = p(5);
            if sign(yt - y0) ~= sign(s0)
                error(['Sign of initial slope does not match relationship of steady and ', ...
                    'initial value.']);
            end
            if sign(yf - yt) ~= sign(sf)
                error(['Sign of final slope does not match relationship of final and steady ', ...
                    'values.']);
            end
            if s0 == 0
                x1 = x(1);
            else
                x1 = x(1) + (yt - y0) / s0;
            end
            if sf == 0
                x2 = x(end);
            else
                x2 = x(end) - (yf - yt) / sf;
            end
            if x2 < x1
                error('Steady value cannot be reached.');
            end
            i1 = find(x >= x1, 1);
            i2 = find(x >= x2, 1);
            y = zeros(size(x));
            y(1 : (i1 - 1)) = y0 + s0 * (x(1 : (i1 - 1)) - x(1));
            y(i1 :(i2 - 1)) = yt;
            y(i2 : end) = yt + sf * (x(i2 : end) - x(i2));
        end
        
        %% LinInterp -------------------------------------------------------------------------------
        % [Summary]
        %   This function performs linear interpolation.
        %
        % [Input]
        %   x  - strictly monotonic vector of sample points
        %   y  - vector of sample values
        %   xi - query point
        %
        % [Output]
        %   yi - interpolated value
        % ------------------------------------------------------------------------------------------
        function yi = LinInterp(x, y, xi)
            %             %% Check arguments
            %             if ~(isnumeric(x) && isvector(x) && issorted(x, 'strictmonotonic'))
            %                 error('Sample points (x) must be provided as a strictly monotonic numeric vector.');
            %             elseif ~(isnumeric(y) && isvector(y) && isequal(size(x), size(y)))
            %                 error('Sample values (y) must be a provided as a numeric vector.');
            %             elseif ~(isnumeric(xi) && isscalar(xi))
            %                 error('Query point (xi) must be a numeric scalar.');
            %             end
            %% Interpolation without extrapolation
            if xi >= x(end)
                yi = y(end);
            elseif xi <= x(1)
                yi = y(1);
            else
                i = find(xi >= x, 1, 'last');
                yi = y(i) + (y(i+1) - y(i)) / (x(i+1) - x(i)) * (xi - x(i));
            end
        end
        
        %% LinInterpE ------------------------------------------------------------------------------
        % [Summary]
        %   This function performs linear interpolation with extrapolation if needed.
        %
        % [Input]
        %   x  - strictly monotonic vector of sample points
        %   y  - vector of sample values
        %   xi - query point
        %
        % [Output]
        %   yi - interpolated value
        % ------------------------------------------------------------------------------------------
        function yi = LinInterpE(x, y, xi)
            %% Check arguments
            %             if ~(isnumeric(x) && isvector(x) && issorted(x, 'strictmonotonic'))
            %                 error('Sample points (x) must be provided as a strictly monotonic numeric vector.');
            %             elseif ~(isnumeric(y) && isvector(y) && isequal(size(x), size(y)))
            %                 error('Sample values (y) must be a provided as a numeric vector.');
            %             elseif ~(isnumeric(xi) && isscalar(xi))
            %                 error('Query point (xi) must be a numeric scalar.');
            %             end
            %% Interpolation with extrapolation
            if xi >= x(end)
                i = length(x) - 1;
            elseif xi <= x(1)
                i = 1;
            else
                i = find(xi >= x, 1, 'last');
            end
            yi = y(i) + (y(i+1) - y(i)) / (x(i+1) - x(i)) * (xi - x(i));
        end
        
        function v = RandSpace(min, max, n)
            %% Check arguments
            if ~(isnumeric(min) && isscalar(min))
                error('ERROR: min must be a numeric scalar.');
            elseif ~(isnumeric(max) && isscalar(max) && (max > min))
                error('ERROR: max must be a numeric scalar bigger than min.');
            elseif ~(isnumeric(n) && isscalar(n) && (floor(n) == n) && (n > 0))
                error('ERROR: n must be an integer scalar bigger than 0.');
            end
            %% Calculate random points in linearly spaced ranges
            delta = (max - min) / n;
            v = (min : delta : (max - delta)) + rand(1, n) * delta;
        end
        
        function d = PointLineDistance(pt, v1, v2)
            a = v1 - v2;
            b = pt - v2;
            d = norm(cross(a,b)) / norm(a);
        end
        
        
        function TrajectoryTrackingAnimation(posXRef, posYRef, time, posX, ...
                posY, yawZ, accX, accY, steerAng, lengthFront, lengthRear, ...
                width, sampleRate, saveGif)
            WIDTH = 700;
            HEIGHT = 700;
            ILENFACT = 2;
            FILENAME = 'animation.gif';
            %% Default arguments
            % In case of offline models, there is no reference path
            if isempty(posXRef)
                posXRef = posX;
            end
            if isempty(posYRef)
                posYRef = posY;
            end
            % Set sample rate to moving animation if not specified
            if nargin < 13
                sampleRate = 1 / 25;
            end
            % Do not save .gif by default
            if nargin < 14
                saveGif = false;
            end
            subSampling = sampleRate / (time(2) - time(1));
            
            maxX = max(posX) + ILENFACT * (lengthFront + lengthRear + width);
            minX = min(posX) - ILENFACT * (lengthFront + lengthRear + width);
            maxY = max(posY) + ILENFACT * (lengthFront + lengthRear + width);
            minY = min(posY) - ILENFACT * (lengthFront + lengthRear + width);
            
            fig = figure('Name', 'TrajectoryTrackingAnination', 'Color', 'White', 'Unit', 'Points', ...
                'PaperUnit', 'Points', 'Position', [40, 40, WIDTH, HEIGHT], 'PaperPosition', [0, 0, WIDTH, HEIGHT]);
            removeToolbarExplorationButtons(fig);
            ax = axes(fig);
            % Disable interactivity
            axtoolbar(ax);
            disableDefaultInteractivity(ax);
            % Fill figure as much as possible
            inset = get(ax, 'TightInset');
            set(ax, 'Position', [inset(1:2), 1-inset(1)-inset(3), 1-inset(2)-inset(4)]);
            
            %% Create subsampling
            time = time(1:subSampling:end);
            posX = posX(1:subSampling:end);
            posY = posY(1:subSampling:end);
            yawZ = yawZ(1:subSampling:end);
            accX = accX(1:subSampling:end);
            accY = accY(1:subSampling:end);
            steerAng = steerAng(1:subSampling:end);
            
            %% Calculate necessary variables
            % Chassis rectangle vertices
            cosYawZ = cos(yawZ);
            sinYawZ = sin(yawZ);
            pFLV = [+ lengthFront, + width / 2];
            pFRV = [+ lengthFront, - width / 2];
            pRLV = [- lengthRear,  + width / 2];
            pRRV = [- lengthRear,  - width / 2];
            pFLG = [posX + cosYawZ * pFLV(1) - sinYawZ * pFLV(2), posY + sinYawZ * pFLV(1) + cosYawZ * pFLV(2)];
            pFRG = [posX + cosYawZ * pFRV(1) - sinYawZ * pFRV(2), posY + sinYawZ * pFRV(1) + cosYawZ * pFRV(2)];
            pRLG = [posX + cosYawZ * pRLV(1) - sinYawZ * pRLV(2), posY + sinYawZ * pRLV(1) + cosYawZ * pRLV(2)];
            pRRG = [posX + cosYawZ * pRRV(1) - sinYawZ * pRRV(2), posY + sinYawZ * pRRV(1) + cosYawZ * pRRV(2)];
            % Steering angle indicator
            cosYawZSteerAng = cos(yawZ + steerAng);
            sinYawZSteerAng = sin(yawZ + steerAng);
            lSteerAng = ILENFACT * (lengthFront + lengthRear);
            pSteerAngG = [posX + cosYawZSteerAng * lSteerAng, posY + sinYawZSteerAng * lSteerAng];
            % Longitudinal acceleration indicator
            lAccX = ILENFACT * accX / 9.8065 * (lengthFront + lengthRear);
            pAccXG = [posX + cosYawZ .* lAccX, posY + sinYawZ .* lAccX];
            % Lateral acceleration indicator
            lAccY = ILENFACT * accY / 9.8065 * (lengthFront + lengthRear);
            pAccYG = [posX - sinYawZ .* lAccY, posY + cosYawZ .* lAccY];
            
            %% Create animation
            for i = 1:length(time)                
                %% Create plot
                cla(ax);
                hold(ax, 'on');
                % Reference path
                plot(ax, -posYRef, posXRef, 'Color', [0 0 0], 'LineStyle', '-');
                % End points
                plot(ax, -posYRef([1 end]), posXRef([1 end]), 'Color', [0 0 0], 'LineStyle', 'none', 'Marker', 'o', ...
                    'MarkerFaceColor', [0 0 0], 'MarkerSize', 3);
                % Vehicle
                fill(ax, -[pFLG(i, 2), pRLG(i, 2), pRRG(i, 2), pFRG(i, 2)], [pFLG(i, 1), pRLG(i, 1), pRRG(i, 1), pFRG(i, 1)], ...
                    nicecolors.GreenEmerald, 'EdgeColor', [0 0 0]);
                % Longitudinal acceleration indicator
                plot(ax, -[posY(i), pAccXG(i, 2)], [posX(i), pAccXG(i, 1)], 'Color', nicecolors.GreenPureApple, ...
                    'LineStyle', '-', 'LineWidth', 3);
                % Lateral acceleration indicator
                plot(ax, -[posY(i), pAccYG(i, 2)], [posX(i), pAccYG(i, 1)], 'Color', nicecolors.GreenPureApple, ...
                    'LineStyle', '-', 'LineWidth', 3);
                % Steering angle indicator
                plot(ax, -[posY(i), pSteerAngG(i, 2)], [posX(i), pSteerAngG(i, 1)], 'Color', nicecolors.RedAlizarin, ...
                    'LineStyle', '-', 'LineWidth', 1);
                % Axis settings
                axis(ax, 'equal');
                xlim(ax, -[maxY, minY]);
                ylim(ax, [minX, maxX]);
                % Draw figure
                drawnow();
                %% Create gif
                if saveGif
                    filePath = fullfile(Util.GetRepoRoot, FILENAME);
                    imFrame = getframe(fig);
                    image = frame2im(imFrame);
                    [imageInd, colorMap] = rgb2ind(image, 256);
                    if i == 1
                        imwrite(imageInd, colorMap, filePath, 'gif', 'DelayTime', sampleRate, 'LoopCount', Inf);
                    else
                        imwrite(imageInd, colorMap, filePath, 'gif', 'DelayTime', sampleRate, 'WriteMode', 'append');
                    end
                end
            end
        end
        
    end
end