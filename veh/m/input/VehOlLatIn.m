%% VehOlLatIn **************************************************************************************
% [Summary]
%   This class represents open loop lateral dynamics inputs for vehicle models.
%
% [Used in]
%   none
%
% [Subclasses]
%   VehStOlIn
% **************************************************************************************************
classdef VehOlLatIn < ComTimeInOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % steering angle (wheel level) [rad]
        SteerWhlAng

    end
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.SteerWhlAng(self, value)
            if ~isequal(self.SteerWhlAng, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: SteerWhlAng must be a numeric vector!');
                end
                self.SteerWhlAng = value(:);
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehOlLatIn(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 2
                superArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@ComTimeInOut(superArgs{:});
            if nargin == 1
                self.SteerWhlAng = varargin{1}.SteerWhlAng;
            elseif nargin == 2
                self.SteerWhlAng = varargin{2};
            end
        end

    end
end