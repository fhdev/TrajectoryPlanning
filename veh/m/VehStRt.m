classdef VehStRt < VehStOl

    properties (Constant)
        LAT_REF = 46.895038; % [�]
        LON_REF = 16.843526; % [�]

        CANCASE_RX_CH_NUM = 1;
        CANCASE_TX_CH_NUM = 1;
        VIRTUAL_RX_CH_NUM = 1;
        VIRTUAL_TX_CH_NUM = 1;
    end

    properties (Access = protected)

        %% Vehicle
        % Temp variables from previous ode step for vehicle model
        torqueDrv
        torqueBrk
        velRef
        steerAng
        % Temp state and output storage
        tsim
        tstart
        X
        Y
        t

        %% CAN
        % Transmit channels
        channelTxImar
        channelTxImu
        channelTxVBox
        % Receive channels
        channelRxSmart
        % CAN databases
        dbcImar
        dbcImu
        dbcVbox
        dbcSmart
        % CAN messages
        msgAttitude
        msgBodyVelocity
        msgLat
        msgLon
        msgAccVBox
        msgTx1Imu
        msgTx2Imu
        msgTx3Imu

        %% Timer
        timer
        iRt
        counter
        started

    end

    properties

        CtrlLon

        SampleTimeRt

        MaxSimTime

        VirtualCan
    end

    methods

        function set.CtrlLon(self, value)
            if ~isequal(self.CtrlLon, value)
                if isstruct(value)
                    if isequal(value.Type, 'CtrlPidV')
                        value = CtrlPidV(self.Chassis, self.WheelFront, value);
                    elseif isequal(value.Type, 'CtrlLqsV')
                        value = CtrlLqsV(self.Chassis, self.WheelFront, value);
                    end
                end
                if ~(isa(value, 'CtrlPidV') || isa(value, 'CtrlLqsV'))
                    error('ERROR: CtrlLon must be a CtrlPidV or CtrlLqsV object!')
                end
                self.CtrlLon = value;
            end
        end

        function set.SampleTimeRt(self, value)
            if ~isequal(self.SampleTimeRt, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SampleTimeRt must be a numeric scalar!')
                end
                self.SampleTimeRt = value;
            end
        end

        function set.MaxSimTime(self, value)
            if ~isequal(self.MaxSimTime, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MaxSimTime must be a numeric scalar!')
                end
                self.MaxSimTime = value;
            end
        end

        function set.VirtualCan(self, value)
            if ~isequal(self.VirtualCan, value)
                if ~(islogical(value) && isscalar(value))
                    error('ERROR: VirtualCan must be a logical scalar!')
                end
                self.VirtualCan = value;
            end
        end

        function self = VehStRt(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 8
                superArgs = varargin(1:4);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@VehStOl(superArgs{:});
            if nargin == 1
                self.CtrlLon      = varargin{1}.CtrlLon;
                self.SampleTimeRt = varargin{1}.SampleTimeRt;
                self.MaxSimTime   = varargin{1}.MaxSimTime;
                self.VirtualCan   = varargin{1}.VirtualCan;
            elseif nargin == 8
                self.CtrlLon      = varargin{5};
                self.SampleTimeRt = varargin{6};
                self.MaxSimTime   = varargin{7};
                self.VirtualCan   = varargin{8};
            end
            assert(mod(self.MaxSimTime / self.SampleTimeRt, 1) == 0, 'Maximal simulation time must be a integer multiple of real-time step!');
            assert(mod(self.SampleTimeRt / self.SampleTime, 1) == 0, 'Real-time step must be a integer multiple of solution step!')

            self.torqueDrv = 0;
            self.torqueBrk = 0;
            self.velRef = 0;
            self.steerAng = 0;

            self.InitializeCAN();

            self.counter = 1;

            self.started = false;

            self.timer = timer('Name', 'VehicleTimer', ...
                'BusyMode', 'drop', ...
                'ExecutionMode', 'fixedRate', ...
                'Period', self.SampleTimeRt, ...
                'TasksToExecute', self.MaxSimTime / self.SampleTimeRt, ...
                'TimerFcn', @self.TimerFcn, ...
                'StopFcn', @self.TimerStopFcn);

        end

        function [dX, Y] = StateEquation(self, t, X, U)
            % The coordinate system of the vehicle is rotated around z-axis by the yaw angle
            % compared to the ground-fixed system! The coordinate systems of the wheels are rotated
            % by the steering angle compared to the vehicle-fixed system.
            % The vehicle-fixed coordinate system is: x points forward (driving direction); y points
            % left; z points upwards.

            %% States
            % Vehicle ==============================================================================
            % Chassis ------------------------------------------------------------------------------
            % X(1)  => PosXG:     longitudinal position in ground-fixed coordinate system [m]
            % X(2)  => VelXG:     longitudinal velocity in ground-fixed coordinate system [m/s]
            % X(3)  => PosYG:     lateral position in ground-fixed coordinate system [m]
            % X(4)  => VelYG:     lateral velocity in ground-fixed coordinate system [m/s]
            % X(5)  => YawZ:      yaw (heading) angle in ground-fixed coordinate system [rad]
            % X(6)  => YawVelZ:   yaw (heading) angular velocity in ground-fixed system [rad/s]
            % WheelFront ---------------------------------------------------------------------------
            % X(7)  => PitchY:    angle of the wheel [rad]
            % X(8)  => PitchVelY: angular velocity of the wheel [rad/s]
            % X(9)  => SlipX:     longitudinal slip [1]
            % X(10) => SlipY:     lateral slip [1]
            % WheelRear ----------------------------------------------------------------------------
            % X(11) => PitchY:    angle of the wheel [rad]
            % X(12) => PitchVelY: angular velocity of the wheel [rad/s]
            % X(13) => SlipX:     longitudinal slip [1]
            % X(14) => SlipY:     lateral slip [1]
            % Control ==============================================================================
            % CtrlLon ------------------------------------------------------------------------------
            % X(15) => integral of velocity tracking error in case of CtrlLqsV and CtrlPidV
            %% Inputs
            % U(1) => VelRef:        reference longitudinal velocity [m/s]
            % U(2) => SteerAng:      steering angle [rad]
            % U(3) => FrictionFront: friction coefficient front [1]
            % U(4) => FrictionRear:  friction coefficient rear [1]
            %% Outputs
            % Vehicle ==============================================================================
            % Chassis ------------------------------------------------------------------------------
            % Y(1)  => AccXG:     longitudinal acceleration in ground-fixed cs. [m/s^2]
            % Y(2)  => AccYG:     lateral acceleration in ground-fixed cs. [m/s^2]
            % Y(3)  => AccInrtXV: longitudinal inertial acceleration in vehicle-fixed cs. [m/s^2]
            % Y(4)  => AccInrtYV: lateral inertial acceleration in vehicle-fixed cs. [m/s^2]
            % Y(5)  => YawAccZ:   yaw (heading) angular acceleration in ground-fixed system [rad/s^2]
            % Y(6)  => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
            % Y(7)  => VelYV:     lateral velocity in vehicle-fixed cs. [m/s]
            % WheelFront ---------------------------------------------------------------------------
            % Y(8)  => PitchAccY: angular acceleration [rad/s]
            % Y(9)  => DSlipX:    longitudinal slip derivative [1/s]
            % Y(10) => ForceXW:   longitudinal force in wheel-fixed coordinate system [N]
            % Y(11) => DSlipY:    lateral slip derivative [1/s]
            % Y(12) => ForceYW:   lateral force in wheel-fixed coordinate system [N]
            % Y(13) => ForceZW:   vertical force in wheel-fixed coordinate system [N]
            % Y(14) => TorqueDrv: wheel level driving torque [Nm]
            % Y(15) => TorqueBrk: wheel level braking torque [Nm]
            % Y(16) => SteerAng:  wheel level steering angle [rad]
            % WheelRear ----------------------------------------------------------------------------
            % Y(17) => PitchAccY: angular acceleration [rad/s]
            % Y(18) => DSlipX:    longitudinal slip derivative [1/s]
            % Y(19) => ForceXW:   longitudinal force in wheel-fixed coordinate system [N]
            % Y(20) => DSlipY:    lateral slip derivative [1/s]
            % Y(21) => ForceYW:   lateral force in wheel-fixed coordinate system [N]
            % Y(22) => ForceZW:   vertical force in wheel-fixed coordinate system [N]
            % Y(23) => TorqueDrv: wheel level driving torque [Nm]
            % Y(24) => TorqueBrk: wheel level braking torque [Nm]
            % Y(25) => SteerAng:  wheel level steering angle [rad]
            % Control ==============================================================================
            % CtrlLon ------------------------------------------------------------------------------
            % Y(26) => TorqueDrv: driving torque output of controller [Nm]
            % Y(27) => TorqueBrk: braking torque output of controller [Nm]
            % CtrlLat ------------------------------------------------------------------------------
            % Y(28) => SteerAng:  steering angle output of controller [rad]

            %% Vehicle

            % Inputs
            % U(1) => TorqueDrv:     driving torque [Nm]
            % U(2) => TorqueBrk:     braking torque [Nm]
            % U(3) => FrictionFront: friction coefficient front [1]
            % U(4) => FrictionRear:  friction coefficient rear [1]
            % U(5) => SteerAng:      steering angle [rad]
            P = [self.Chassis.Mass;
                self.Chassis.MomentOfIntertiaZ;
                self.Chassis.DistanceFrontCOG;
                self.Chassis.DistanceRearCOG;
                self.Chassis.HeightCOG;
                self.Chassis.DragCoefficient;
                self.Chassis.FrontalArea;
                self.Chassis.DrivingTorqueSplit;
                self.WheelFront.Radius;
                self.WheelFront.MomentOfInertiaY;
                self.WheelFront.MfStiffnessX;
                self.WheelFront.MfShapeX;
                self.WheelFront.MfCurvatureX;
                self.WheelFront.RollingResistanceConst;
                self.WheelFront.RollingResistanceLin;
                self.WheelFront.RollingResistanceSqr;
                self.WheelFront.RollingResistanceVel;
                self.WheelFront.RollingResistanceVelCorr;
                self.WheelFront.BrakingTorqueVel;
                self.WheelFront.RelaxationLength0X;
                self.WheelFront.RelaxationLengthMinX;
                self.WheelFront.SlipDamping0X;
                self.WheelFront.VelSlowX;
                self.WheelFront.MfStiffnessY;
                self.WheelFront.MfShapeY;
                self.WheelFront.MfCurvatureY;
                self.WheelFront.RelaxationLength0Y;
                self.WheelFront.RelaxationLengthMinY;
                self.WheelFront.SlipDamping0Y;
                self.WheelFront.VelSlowY;
                self.WheelFront.MinSlip;
                self.WheelRear.Radius;
                self.WheelRear.MomentOfInertiaY;
                self.WheelRear.MfStiffnessX;
                self.WheelRear.MfShapeX;
                self.WheelRear.MfCurvatureX;
                self.WheelRear.RollingResistanceConst;
                self.WheelRear.RollingResistanceLin;
                self.WheelRear.RollingResistanceSqr;
                self.WheelRear.RollingResistanceVel;
                self.WheelRear.RollingResistanceVelCorr;
                self.WheelRear.BrakingTorqueVel;
                self.WheelRear.RelaxationLength0X;
                self.WheelRear.RelaxationLengthMinX;
                self.WheelRear.SlipDamping0X;
                self.WheelRear.VelSlowX;
                self.WheelRear.MfStiffnessY;
                self.WheelRear.MfShapeY;
                self.WheelRear.MfCurvatureY;
                self.WheelRear.RelaxationLength0Y;
                self.WheelRear.RelaxationLengthMinY;
                self.WheelRear.SlipDamping0Y;
                self.WheelRear.VelSlowY;
                self.WheelRear.MinSlip];

            [dXV, YV] = VehStStateEquationMex(P, t, X(1:14), [self.torqueDrv; self.torqueBrk; U([3, 4, 2])]);

            %% Control

            % Longitudinal
            cls = class(self.CtrlLon);
            if isequal(cls, 'CtrlPidV')
                % Inputs
                % U(1) => VelXV:          longitudinal velocity in vehicle-fixed cs. [m/s]
                % U(2) => VelRef - VelXV: tracking error of longitudinal velocity in vehicle-fixed cs. [m/s]
                [dXCLon, YCLon] = self.CtrlLon.StateEquation(t, X(15), [YV(6); U(1) - YV(6)]);
            elseif isequal(cls, 'CtrlLqsV')
                % Inputs
                % U(1) => VelXV:          longitudinal velocity in vehicle-fixed cs. [m/s]
                % U(2) => VelRef - VelXV: tracking error of longitudinal velocity in vehicle-fixed cs. [m/s]
                [dXCLon, YCLon] = self.CtrlLon.StateEquation(t, X(15), [YV(6); U(1) - YV(6)]);
            end
            self.torqueDrv = max(0, YCLon(1));
            self.torqueBrk = max(0, -YCLon(1));

            %% Set state derivatives
            dX = [dXV; dXCLon];

            %% Set outputs
            Y = [YV; self.torqueDrv; self.torqueBrk; U(2)];

        end

        function X0 = Initialize(self, initCon)
            %% Check arguments
            if ~isa(initCon, 'ChassisStBndCon')
                error('ERROR: initCon must be a ChassisStBndCon object!');
            end
            X0 = Initialize@VehStOl(self, initCon);
            ctrlLonType = class(self.CtrlLon);
            if isequal(ctrlLonType, 'CtrlPidV')
                XLon0 = 0;
            elseif isequal(ctrlLonType, 'CtrlLqsV')
                XLon0 = self.CtrlLon.Initialize(initCon.VelXV);
            end
            X0 = [X0; XLon0];
        end

        function InitializeCAN(self)

            %% Create CAN channels

            if self.VirtualCan
                device = 'Virtual 1';
                % Transmit channels
                self.channelTxImar  = canChannel('Vector', device, self.VIRTUAL_TX_CH_NUM);
                self.channelTxImu   = canChannel('Vector', device, self.VIRTUAL_TX_CH_NUM);
                self.channelTxVBox  = canChannel('Vector', device, self.VIRTUAL_TX_CH_NUM);
                % Receive channels
                self.channelRxSmart = canChannel('Vector', device, self.VIRTUAL_RX_CH_NUM);
            else
                device = 'CANcaseXL 1';
                % Transmit channels
                self.channelTxImar  = canChannel('Vector', device, self.CANCASE_TX_CH_NUM);
                self.channelTxImu   = canChannel('Vector', device, self.CANCASE_TX_CH_NUM);
                self.channelTxVBox  = canChannel('Vector', device, self.CANCASE_TX_CH_NUM);
                % Receive channels
                self.channelRxSmart = canChannel('Vector', device, self.CANCASE_RX_CH_NUM);
            end

            %% Load dbc
            self.dbcImar  = canDatabase('imar.dbc');
            self.dbcImu   = canDatabase('MM5_10_Default.dbc');
            self.dbcVbox  = canDatabase('VBOX3iSL_ADAS_VCI_VEHICO.dbc');
            self.dbcSmart = canDatabase('smart_control.dbc');

            %% Assign database to channels
            % Transmit channels
            self.channelTxImar.Database  = self.dbcImar;
            self.channelTxImu.Database   = self.dbcImu;
            self.channelTxVBox.Database  = self.dbcVbox;
            % Receive channels
            self.channelRxSmart.Database = self.dbcSmart;

            %% Filter necessary data
            filterBlockAll(self.channelTxImar, 'Standard');
            filterAllowOnly(self.channelTxImar, {'Attitude', 'Body_Velocity', 'INS_Latitude', 'INS_Longitude'});
            % Accelerations VBOX
            filterBlockAll(self.channelTxVBox, 'Extended');
            filterAllowOnly(self.channelTxVBox, 'VBOX_4');
            % Accelerations and angular rates
            filterAllowOnly(self.channelTxImu, {'TX1', 'TX2', 'TX3'});
            filterBlockAll(self.channelTxImu, 'Extended');
            % Control signals
            filterAllowOnly(self.channelRxSmart, 'Control_message');
            filterBlockAll(self.channelRxSmart, 'Extended');

            %% Create CAN messages
            self.msgAttitude     = canMessage(self.dbcImar, 'Attitude');
            self.msgBodyVelocity = canMessage(self.dbcImar, 'Body_Velocity');
            self.msgLat          = canMessage(self.dbcImar, 'INS_Latitude');
            self.msgLon          = canMessage(self.dbcImar, 'INS_Longitude');
            self.msgAccVBox      = canMessage(self.dbcVbox, 'VBOX_4');
            self.msgTx1Imu       = canMessage(self.dbcImu, 'TX1');
            self.msgTx2Imu       = canMessage(self.dbcImu, 'TX2');
            self.msgTx3Imu       = canMessage(self.dbcImu, 'TX3');

        end

        function StartSolver(self, X0)

            %% Check and prepare arguments
            if ~(isnumeric(X0) && isvector(X0))
                error('ERROR: Initial state (X0) must be a numeric vector!');
            end
            X0 = X0(:);
            nX = length(X0);

            %% Initial run and check of state equation
            try
                U0 = [0; 0; 1; 1];
                t0 = 0;
                [dX0, Y0] = self.StateEquation(t0, X0, U0);
            catch ex
                disp(ex.message);
                error('ERROR: Error when calling InputProvider and StateEquation.');
            end

            if ~(isnumeric(dX0) && isvector(dX0) && (length(dX0) == nX))
                error('ERROR: First return value of obj.StateEquation (dX) must be a numeric vector with length(X0) elements!');
            end

            if ~((isnumeric(Y0) && isvector(Y0)))
                error('ERROR: Second return value of obj.StateEquation (Y) must be a numeric vector or an empty scalar!');
            end
            Y0 = Y0(:);
            nY = length(Y0);

            %% Preallocate memory and set initial values

            % Reset real-time counter
            self.iRt = 1;

            % Maximal number of real-time steps
            nRtSteps = self.MaxSimTime / self.SampleTimeRt;

            % Pre-allocate memory for time and set initial value
            self.t = nan(1, nRtSteps + 1);
            self.t(1) = 0;

            % Pre-allocate memory for states and set initial value
            self.X = nan(nX, nRtSteps + 1);
            self.X(:, self.iRt) = X0;

            % Pre-allocate memory for outputs if there are any
            self.Y = nan(nY, nRtSteps + 1);
            self.Y(:, 1) = Y0;

        end

        function [Xminor, Yminor] = RunSolver(self, U)

            %% Solve state equation

%             % Ode solution sample time
%             h = self.SampleTime;
%             % Number of ode solution steps inside one real-time step
%             nSolSteps = self.SampleTimeRt / self.SampleTime;

            %% Previous time and state
            tminor = self.t(1, self.iRt);
            Xminor = self.X(:, self.iRt);

            %% Do minor steps
            K = nan(size(self.X, 1), 4);
            L = nan(size(self.Y, 1), 4);
            while tminor < self.tsim
                %% Choose minor step size
                % This is the complete time interval until simulation time
                dt = self.tsim - tminor;
                if dt > self.SampleTime
                    % if the interval is bigger than the maximal sample time, use sample time
                    h = self.SampleTime;
                else
                    % if interval is smaller than the maximal sample time, use the interval
                    h = dt;
                end
                 % 4th order Runge-Kutta coefficients
                [K(:, 1), L(:, 1)] = self.StateEquation(tminor,         Xminor,                   U);
                [K(:, 2), L(:, 2)] = self.StateEquation(tminor + h / 2, Xminor + h / 2 * K(:, 1), U);
                [K(:, 3), L(:, 3)] = self.StateEquation(tminor + h / 2, Xminor + h / 2 * K(:, 2), U);
                [K(:, 4), L(:, 4)] = self.StateEquation(tminor + h,     Xminor + h     * K(:, 3), U);
                % Calculate next state
                Xminor = Xminor + h / 6 * (K(:, 1)  + 2 * K(:, 2)  + 2 * K(:, 3)  + K(:, 4));
                % Increase time
                tminor = tminor + h;
            end
            % Calculate current output
            Yminor = 1 / 6 * (L(:, 1) + 2 * L(:, 2) + 2 * L(:, 3) + L(:, 4));


%             for iSolStep = 1 : nSolSteps
%                 % 4th order Runge-Kutta coefficients
%                 [K(:, 1), L(:, 1)] = self.StateEquation(self.tsim,         Xminor,                   U);
%                 [K(:, 2), L(:, 2)] = self.StateEquation(self.tsim + h / 2, Xminor + h / 2 * K(:, 1), U);
%                 [K(:, 3), L(:, 3)] = self.StateEquation(self.tsim + h / 2, Xminor + h / 2 * K(:, 2), U);
%                 [K(:, 4), L(:, 4)] = self.StateEquation(self.tsim + h,     Xminor + h     * K(:, 3), U);
%                 % Calculate next state
%                 Xminor = Xminor + h / 6 * (K(:, 1)  + 2 * K(:, 2)  + 2 * K(:, 3)  + K(:, 4));
%                 % Store current output as well if there are outputs
%                 if iSolStep == nSolSteps
%                     Yminor = 1 / 6 * (L(:, 1) + 2 * L(:, 2) + 2 * L(:, 3) + L(:, 4));
%                 end
%                 % Increase time
%                 self.tsim = self.tsim + h;
%             end

            %% Store results
            self.X(:, self.iRt + 1) = Xminor;
            self.Y(:, self.iRt + 1) = Yminor;
            self.t(1, self.iRt + 1) = tminor;

            %% Increase storate counter
            self.iRt = self.iRt + 1;

        end

        function [U, msgCount] = InputProvider(self)
            %% Inputs
            % U(1) => VelRef:        reference longitudinal velocity [m/s]
            % U(2) => SteerAng:      steering angle [rad]
            % U(3) => FrictionFront: friction coefficient front [1]
            % U(4) => FrictionRear:  friction coefficient rear [1]
            U = zeros(4, 1);
            msgAll = receive(self.channelRxSmart, Inf);
            msgCount = numel(msgAll);
            msgCtrl = extractRecent(msgAll, 'Control_message');
            if ~isempty(msgCtrl)
                % Speed demand
                U(1) = msgCtrl.Signals.Speed_demand / 3.6;
                % Steering angle at wheel level
                U(2) = deg2rad(msgCtrl.Signals.Steering_wheel_angle) / 25;
                self.velRef = U(1);
                self.steerAng = U(2);
            else
                U(1) = self.velRef;
                U(2) = self.steerAng;
            end
            U(3) = 1;
            U(4) = 1;
        end

        function OutputProvider(self, X, Y)
            % Body velocities
            self.msgBodyVelocity.Signals.V_X = Y(6) * 3.6; % [km/h]
            self.msgBodyVelocity.Signals.V_Y = Y(7) * 3.6; % [km/h]

            % Positions
            % lla = flat2lla(flatearth_pos, llo, psio, href) [lat; lon; alt]
            lla = flat2lla([X(1), X(3), 0], [self.LAT_REF, self.LON_REF], 0, 0);
            %lla = flat2lla([10, 10, 0], [self.LAT_REF, self.LON_REF], 0, 0);
            self.msgLat.Signals.Latitude = lla(1);  % [deg]
            self.msgLon.Signals.Longitude = lla(2); % [deg]

            % Attitude
            self.msgAttitude.Signals.Yaw = -mod(rad2deg(X(5)), 360) + 180; % [deg]

            % Accelerations
            self.msgAccVBox.Signals.Longitudinal_acceleration = Y(3);
            self.msgAccVBox.Signals.Lateral_acceleration = Y(4);
            self.msgTx1Imu.Signals.ACC_F_Y = Y(4) / 9.81;
            self.msgTx1Imu.Signals.OMEGA_F_Z = rad2deg(X(6));
            self.msgTx2Imu.Signals.ACC_F_X = Y(3) / 9.81;
            self.msgTx2Imu.Signals.OMEGA_F_X = 0;
            self.msgTx3Imu.Signals.ACC_F_Z   = 0;
            self.msgTx3Imu.Signals.OMEGA_F_Y = 0;

            % Transmit messages
            try
                transmit(self.channelTxImar, self.msgBodyVelocity);
                transmit(self.channelTxImar, self.msgLat);
                transmit(self.channelTxImar, self.msgLon);
                transmit(self.channelTxImar, self.msgAttitude);
                transmit(self.channelTxVBox, self.msgAccVBox);
                transmit(self.channelTxImu, self.msgTx1Imu);
                transmit(self.channelTxImu, self.msgTx2Imu);
                transmit(self.channelTxImu, self.msgTx3Imu);
            catch ex
                fprintf('Transmission error in OutputProvider!\n');
            end

        end

        function Start(self, initCon)
            %% Check arguments
            if ~isa(initCon, 'VehStBndCon')
                error('ERROR: initCon must be a VehStBndCon object!');
            end
            % Start solver
            X0 = self.Initialize(initCon);
            self.StartSolver(X0);

            % Reset model variables
            self.velRef = 0;
            self.steerAng = 0;

            % Reset timing related variables
            self.started = false;
            self.counter = 1;
            self.tsim = 0;

            % Start CAN channels
            if ~self.channelTxImar.Running
                start(self.channelTxImar);
            end
            if ~self.channelTxVBox.Running
                start(self.channelTxVBox);
            end
            if ~self.channelTxImu.Running
                start(self.channelTxImu);
            end
            if ~self.channelRxSmart.Running
                start(self.channelRxSmart);
            end

            % Receive everything from buffer
            receive(self.channelRxSmart, Inf);

            % Clear command window
            clc();

            % Start timer
            start(self.timer);
        end

        function output = Stop(self)
            % Stop timer
            stop(self.timer);

            % Receive everything from buffer
            receive(self.channelRxSmart, Inf);

            % Stop CAN channels
            if self.channelTxImar.Running
                stop(self.channelTxImar);
            end
            if self.channelTxVBox.Running
                stop(self.channelTxVBox);
            end
            if self.channelTxImu.Running
                stop(self.channelTxImu);
            end
            if self.channelRxSmart.Running
                stop(self.channelRxSmart);
            end

            % Provide output
            output = VehStClOut(self.t, ...
                ChassisStOut(self.t, self.X(1,:), self.X(2,:), self.Y(1,:), self.Y(6,:), self.Y(3,:),  self.X(3,:), self.X(4,:), self.Y(2,:), self.Y(7,:), self.Y(4,:), self.X(5,:), self.X(6,:), self.Y(5,:)), ...
                WheelMfExtOut(self.t, self.X(7,:), self.X(8,:), self.Y(8,:), self.X(9,:), self.Y(9,:), self.Y(10,:), self.Y(13,:), self.X(10,:), self.Y(11,:), self.Y(12,:), self.Y(14,:), self.Y(15,:), self.Y(16,:)), ...
                WheelMfExtOut(self.t, self.X(11,:), self.X(12,:), self.Y(17,:), self.X(13,:), self.Y(18,:), self.Y(19,:), self.Y(22,:), self.X(14,:), self.Y(20,:), self.Y(21,:), self.Y(23,:), self.Y(24,:), self.Y(25,:)), ...
                VehOlIn(self.t, self.Y(26,:), self.Y(27,:), self.Y(28,:)), ...
                CtrlLatOut(self.t, zeros(1, self.MaxSimTime / self.SampleTimeRt), zeros(1, self.MaxSimTime / self.SampleTimeRt)));

        end

        function TimerFcn(self, obj, event) %#ok<INUSD>
            %% Save start time
            if self.counter == 1
                self.tstart = tic;
            end

            %% Base task
            trun = tic;
            [Ua, msgCount] = self.InputProvider();
            self.tsim = toc(self.tstart);
            [Xa, Ya] = self.RunSolver(Ua);
            self.OutputProvider(Xa, Ya);
            trun = toc(trun);

            %% 5x task
            if mod(self.counter, 5) == 0

            end

            %% 10x task
            if mod(self.counter, 10) == 0
                fprintf('C = %8.4f [s] | t = %8.4f [s] | V = %8.4f [km/h] | Yr = %8.4f [deg/s] | X = %9.4f [m] | Y = %9.4f [m] | Hd = %9.4f [deg] | St= %8.4f [deg]\n', ...
                    self.counter * self.SampleTimeRt, self.tsim, Ya(6) * 3.6, rad2deg(Xa(6)), Xa(1), Xa(3), rad2deg(Xa(5)), rad2deg(Ya(28)));
%                 fprintf('trun = %g | msgCount = %d\n', trun, msgCount);
%                 fprintf('%s\n',mat2str(Ua));
            end

            %% 50x task
            if mod(self.counter, 50) == 0

            end

            %% Increase real time step counter
            self.counter = self.counter + 1;
        end

        function TimerStopFcn(self, obj, event) %#ok<INUSD>

            % Receive everything from buffer
            receive(self.channelRxSmart, Inf);

            % Stop CAN channels
            if self.channelTxImar.Running
                stop(self.channelTxImar);
            end
            if self.channelTxVBox.Running
                stop(self.channelTxVBox);
            end
            if self.channelTxImu.Running
                stop(self.channelTxImu);
            end
            if self.channelRxSmart.Running
                stop(self.channelRxSmart);
            end
        end

    end
end