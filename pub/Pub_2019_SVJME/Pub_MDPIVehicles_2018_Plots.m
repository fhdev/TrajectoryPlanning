%% Common

% Calculations
nInputs = length(input);
indexLinear = 1:nInputs;
maxAccelerationY = zeros(nInputs, 1);
for iInput = 1 : nInputs
    maxAccelerationY(iInput) = max(abs(outputOpt(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS));
end
[~, indexMaxAccelY] = sort(maxAccelerationY, 'ascend');

% Constants 
OPT_COLOR = 'Blue';
NN_COLOR = 'Red';
INIT_COLOR = 'Magenta';
FONT_SIZE = 8;
MARKER_SIZE = 15;
FORMAT = '-depsc';

%% Runtimes normalized to plain optimal solution

% Calculations
calcTimeAvgOpt = mean([outputOpt(:).CalcTime]);
calcTimeAvgNN = mean([outputNN(:).CalcTime]);
calcTimeAvgInit = mean([outputOptInit(:).CalcTime]);

calcTimeNormOpt = [outputOpt(:).CalcTime] ./ calcTimeAvgOpt;
calcTimeNormNN = [outputNN(:).CalcTime] ./ calcTimeAvgOpt;
calcTimeNormInit = [outputOptInit(:).CalcTime] ./ calcTimeAvgOpt;

% Constants
NAME = 'Runtimes';
HEIGHT = 6.5;
WIDTH = 8;

% Plots
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, calcTimeNormOpt(indexMaxAccelY), MARKER_SIZE, 'Marker', 'o', 'MarkerEdgeColor', OPT_COLOR);
scatter(indexLinear, calcTimeNormNN(indexMaxAccelY), MARKER_SIZE,  'Marker', 's', 'MarkerEdgeColor', NN_COLOR);
scatter(indexLinear, calcTimeNormInit(indexMaxAccelY), MARKER_SIZE, 'Marker', '+', 'MarkerEdgeColor', INIT_COLOR);
line([1, nInputs], [1, 1], 'Color', OPT_COLOR);
line([1, nInputs], repmat(calcTimeAvgNN / calcTimeAvgOpt, 2, 1), 'Color', NN_COLOR);
line([1, nInputs], repmat(calcTimeAvgInit / calcTimeAvgOpt, 2, 1), 'Color', INIT_COLOR);
xlim([1, nInputs]);
ylim([0, 1.5]);
grid on;
box on;
[~, ~, ~, lh] = plottexts('trajectory index [1]', '$k_t$ [1]', [], {'optimization planner', 'hybrid nn planner', ...
    'initialized optimization planner'}, [], FONT_SIZE);
set(lh, 'Location', 'NorthOutside');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

%% Deviation in reference signal parameters normalized to plain optimal solution

% Calculations
refParamDevNormNN = zeros(1, nInputs);
refParamDevNormInit = zeros(1, nInputs);
for iInput = 1 : nInputs
    refParamDevNormNN(iInput) = norm((outputOpt(iInput).RefParam.ToVector(false) - ...
        outputNN(iInput).RefParam.ToVector(false)) ./ outputOpt(iInput).RefParam.ToVector(false));
    refParamDevNormInit(iInput) = norm((outputOpt(iInput).RefParam.ToVector(false) - ...
        outputOptInit(iInput).RefParam.ToVector(false)) ./ ...
        outputOpt(iInput).RefParam.ToVector(false));
end
refParamDevNormAvgNN = mean(refParamDevNormNN, 2);
refParamDevNormAvgInit = mean(refParamDevNormInit, 2);

% Constants
NAME = 'RefParamDev_NN';
HEIGHT = 5;
WIDTH = 8;

% Plots
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, refParamDevNormNN(indexMaxAccelY), MARKER_SIZE, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR);
line([0, nInputs], repmat(refParamDevNormAvgNN, 1, 2), 'Color', NN_COLOR);
xlim([0, nInputs]);
box on;
grid on;
[~, ~, ~, lh] = plottexts('index of trajectory [1]', '$k_p$ [1]', [], 'hybrid nn planner', [], FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

% Constants
NAME = 'RefParamDev_Init';
HEIGHT = 5;
WIDTH = 8;

% Plots
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, refParamDevNormInit(indexMaxAccelY), MARKER_SIZE, 'Marker', '+', 'MarkerEdgeColor', INIT_COLOR);
line([0, nInputs], repmat(refParamDevNormAvgInit, 1, 2), 'Color', INIT_COLOR);
xlim([0, nInputs]);
box on;
grid on;
[~, ~, ~, lh] = plottexts('index of trajectory [1]', '$k_p$ [1]', [], 'initialized optimization planner', [], FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

%% Final value of state constraint

% Calculations
stateConstrOpt = zeros(1, nInputs);
stateConstrNN = zeros(1, nInputs);
stateConstrNNCalcTime = zeros(1, nInputs);
stateConstrNNCarMaker = zeros(1, nInputs);
stateConstrNNCarMakerCalcTime = zeros(1, nInputs);
stateConstrInit = zeros(1, nInputs);
for iInput = indexLinear
    % optimal
    [~, c] = outputOpt(iInput).StateConstraint.ToVector();
    stateConstrOpt(iInput) = norm(c);
    % nn
    [~, c] = outputNN(iInput).StateConstraint.ToVector();
    stateConstrNN(iInput) = norm(c);
    % nn after planning time
    i = find(outputNN(iInput).VehicleOutput.Chassis.Time > outputNN(iInput).CalcTime, 1, 'first');
    c = [outputNN(iInput).VehicleOutput.Chassis.PositionX_GCS(i)    - outputOpt(iInput).VehicleOutput.Chassis.PositionX_GCS(i);
         outputNN(iInput).VehicleOutput.Chassis.PositionY_GCS(i)    - outputOpt(iInput).VehicleOutput.Chassis.PositionY_GCS(i);
         outputNN(iInput).VehicleOutput.Chassis.AngleZ(i)           - outputOpt(iInput).VehicleOutput.Chassis.AngleZ(i);
         outputNN(iInput).VehicleOutput.Chassis.AngularVelocityZ(i) - outputOpt(iInput).VehicleOutput.Chassis.AngularVelocityZ(i)];
    stateConstrNNCalcTime(iInput) = norm(c);
    % nn with carmaker
    c = [outputCarMaker(iInput).PositionX_GCS(end)    - input(iInput).FinalConditions.PositionX_GCS;
         outputCarMaker(iInput).PositionY_GCS(end)    - input(iInput).FinalConditions.PositionY_GCS;
         outputCarMaker(iInput).AngleZ(end)           - input(iInput).FinalConditions.AngleZ;
         outputCarMaker(iInput).AngularVelocityZ(end) - input(iInput).FinalConditions.AngularVelocityZ];
    stateConstrNNCarMaker(iInput) = norm(c); 
    % nn with carmaker after planning time
    i = find(outputCarMaker(iInput).Time > outputNN(iInput).CalcTime, 1, 'first');
    io = find(outputOpt(iInput).VehicleOutput.Chassis.Time > outputNN(iInput).CalcTime, 1, 'first');
    c = [outputCarMaker(iInput).PositionX_GCS(i)    - outputOpt(iInput).VehicleOutput.Chassis.PositionX_GCS(io);
         outputCarMaker(iInput).PositionY_GCS(i)    - outputOpt(iInput).VehicleOutput.Chassis.PositionY_GCS(io);
         outputCarMaker(iInput).AngleZ(i)           - outputOpt(iInput).VehicleOutput.Chassis.AngleZ(io);
         outputCarMaker(iInput).AngularVelocityZ(i) - outputOpt(iInput).VehicleOutput.Chassis.AngularVelocityZ(io)];
    stateConstrNNCarMakerCalcTime(iInput) = norm(c);    
    % initialized optimal  
    [~, c] = outputOptInit(iInput).StateConstraint.ToVector();
    stateConstrInit(iInput) = norm(c);
end
stateConstrAvgOpt = mean(stateConstrOpt);
stateConstrAvgNN = mean(stateConstrNN);
stateConstrAvgNNCalcTime = mean(stateConstrNNCalcTime);
stateConstrAvgNNCarMaker = mean(stateConstrNNCarMaker);
stateConstrAvgNNCarMakerCalcTime = mean(stateConstrNNCarMakerCalcTime);
stateConstrAvgInit = mean(stateConstrInit);

% Constants
OPT_COLOR = 'Blue';
NN_COLOR = 'Red';
INIT_COLOR = 'Magenta';
HEIGHT = 5;
WIDTH = 8;
FONT_SIZE = 8;
MARKER_SIZE = 15;
FORMAT = '-depsc';

% Plots
NAME = 'StateConstr_Opt';
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, stateConstrOpt(indexMaxAccelY), MARKER_SIZE, 'Marker', 'o', 'MarkerEdgeColor', OPT_COLOR);
line([0, nInputs], repmat(stateConstrAvgOpt, 1, 2), 'Color', OPT_COLOR);
xlim([0, nInputs]);
box on;
grid on;
[~, ~, ~, lh] = plottexts('index of trajectory [1]', '$||C(X)||$ [1]', [], 'optimization planner', [], FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

NAME = 'StateConstr_NN';
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, stateConstrNN(indexMaxAccelY), MARKER_SIZE, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR);
line([0, nInputs], repmat(stateConstrAvgNN, 1, 2), 'Color', NN_COLOR);
xlim([0, nInputs]);
box on;
grid on;
[~, ~, ~, lh] = plottexts('index of trajectory [1]', '$||C(X)||$ [1]', [], 'hybrid nn planner', [], FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

NAME = 'StateConstr_NN_CalcTime';
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, stateConstrNNCalcTime(indexMaxAccelY), MARKER_SIZE, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR);
line([0, nInputs], repmat(stateConstrAvgNNCalcTime, 1, 2), 'Color', NN_COLOR);
xlim([0, nInputs]);
box on;
grid on;
[~, ~, ~, lh] = plottexts('index of trajectory [1]', '$||C_T(X)||$ [1]', [], 'hybrid nn planner', [], FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

NAME = 'StateConstr_NN_CarMaker';
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, stateConstrNNCarMaker(indexMaxAccelY), MARKER_SIZE, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR);
line([0, nInputs], repmat(stateConstrAvgNNCarMaker, 1, 2), 'Color', NN_COLOR);
xlim([0, nInputs]);
box on;
grid on;
[~, ~, ~, lh] = plottexts('index of trajectory [1]', '$||C(X)||$ [1]', [], 'hybrid nn planner', [], FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

NAME = 'StateConstr_NN_CarMaker_CalcTime';
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, stateConstrNNCarMakerCalcTime(indexMaxAccelY), MARKER_SIZE, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR);
line([0, nInputs], repmat(stateConstrAvgNNCarMakerCalcTime, 1, 2), 'Color', NN_COLOR);
xlim([0, nInputs]);
box on;
grid on;
[~, ~, ~, lh] = plottexts('index of trajectory [1]', '$||C_T(X)||$ [1]', [], 'hybrid nn planner', [], FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

NAME = 'StateConstr_Init';
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, stateConstrInit(indexMaxAccelY), MARKER_SIZE, 'Marker', '+', 'MarkerEdgeColor', INIT_COLOR);
line([0, nInputs], repmat(stateConstrAvgInit, 1, 2), 'Color', INIT_COLOR);
xlim([0, nInputs]);
box on;
grid on;
[~, ~, ~, lh] = plottexts('index of trajectory [1]', '$||C(X)||$ [1]', [], 'initialized optimization planner', [], ...
    FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

%% Maximal lateral acceleration normalized to the optimal solution
% Jerk is almost the same, therefore it is not visualized.

% Calculation

maxAccYNN = zeros(nInputs, 1);
maxAccYNN_CarMaker = zeros(nInputs, 1);
maxAccYInit = zeros(nInputs, 1);
for iInput = 1 : nInputs
    maxAccYNN(iInput) = max(abs(outputNN(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS)) / max(abs(outputOpt(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS));
    maxAccYNN_CarMaker(iInput) = max(abs(outputCarMaker(iInput).AccelerationYIntertial_VCS)) / max(abs(outputOpt(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS));
    maxAccYInit(iInput) = max(abs(outputOptInit(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS)) / max(abs(outputOpt(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS));
end

maxAccYAvgNN = mean(maxAccYNN);
maxAccYAvgNN_CarMaker = mean(maxAccYNN_CarMaker);
maxAccYAvgInit = mean(maxAccYInit);

% Constants

% Plots

NAME = 'MaxAccY_NN';
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, maxAccYNN(indexMaxAccelY), MARKER_SIZE, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR);
line([0, nInputs], repmat(maxAccYAvgNN, 1, 2), 'Color', NN_COLOR);
xlim([0, nInputs]);
box on;
grid on;
plottexts('index of trajectory [1]', '$k_{a_y}$ [1]', [], 'hybrid nn planner', [], ...
    FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

NAME = 'MaxAccY_NN_CarMaker';
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, maxAccYNN_CarMaker(indexMaxAccelY), MARKER_SIZE, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR);
line([0, nInputs], repmat(maxAccYAvgNN_CarMaker, 1, 2), 'Color', NN_COLOR);
xlim([0, nInputs]);
box on;
grid on;
plottexts('index of trajectory [1]', '$k_{a_y}$ [1]', [], 'hybrid nn planner', [], ...
    FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

NAME = 'MaxAccY_Init';
plotfigure(NAME, WIDTH, HEIGHT);
scatter(indexLinear, maxAccYInit(indexMaxAccelY), MARKER_SIZE, 'Marker', '+', 'MarkerEdgeColor', INIT_COLOR);
line([0, nInputs], repmat(maxAccYAvgInit, 1, 2), 'Color', INIT_COLOR);
xlim([0, nInputs]);
box on;
grid on;
plottexts('index of trajectory [1]', '$k_{a_y}$ [1]', [], 'initialized optimization planner', [], ...
    FONT_SIZE);
set(lh, 'Location', 'North');
print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

%% Final position deviation
% 
% % Calculations
% posDevOpt = zeros(1, nInputs);
% posDevNN = zeros(1, nInputs);
% posDevInit = zeros(1, nInputs);
% for iInput = indexLinear
%     posDevOpt(iInput) = sqrt(outputOpt(iInput).StateConstraint.Values.PositionX_GCS^2 + ...
%         outputOpt(iInput).StateConstraint.Values.PositionY_GCS^2);
%     posDevNN(iInput) = sqrt(outputNN(iInput).StateConstraint.Values.PositionX_GCS^2 + ...
%         outputNN(iInput).StateConstraint.Values.PositionY_GCS^2);
%     posDevInit(iInput) = sqrt(outputOptInit(iInput).StateConstraint.Values.PositionX_GCS^2 + ...
%         outputOptInit(iInput).StateConstraint.Values.PositionY_GCS^2);
% end
% posDevMeanOpt = mean(posDevOpt);
% posDevMeanNN = mean(posDevNN);
% posDevMeanInit = mean(posDevInit);
% 
% % Constants
% OPT_COLOR = 'Blue';
% NN_COLOR = 'Red';
% INIT_COLOR = 'Magenta';
% HEIGHT = 5;
% WIDTH = 8;
% FONT_SIZE = 8;
% MARKER_SIZE = 15;
% FORMAT = '-depsc';
% 
% % Plots
% NAME = 'PosDev_Opt';
% plotfigure(NAME, WIDTH, HEIGHT);
% scatter(indexLinear, posDevOpt(indexMaxAccelY), MARKER_SIZE, 'Marker', 'o', 'MarkerEdgeColor', OPT_COLOR);
% line([1, nInputs], repmat(posDevMeanOpt, 1, 2), 'Color', OPT_COLOR);
% xlim([1, nInputs]);
% grid on;
% box on;
% plottexts('trajectory index', 'final position deviation [m]', [], 'optimization planner', [], FONT_SIZE);
% print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');
% 
% NAME = 'PosDev_NN';
% plotfigure(NAME, WIDTH, HEIGHT);
% scatter(indexLinear, posDevNN(indexMaxAccelY), MARKER_SIZE, 'Marker', 'o', 'MarkerEdgeColor', NN_COLOR);
% line([1, nInputs], repmat(posDevMeanNN, 1, 2), 'Color', NN_COLOR);
% xlim([1, nInputs]);
% grid on;
% box on;
% plottexts('trajectory index', 'final position deviation [m]', [], 'hybrid nn planner', [], FONT_SIZE);
% print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT);
% 
% NAME = 'PosDev_Init';
% plotfigure(NAME, WIDTH, HEIGHT);
% scatter(indexLinear, posDevInit(indexMaxAccelY), MARKER_SIZE, 'Marker', 'o', 'MarkerEdgeColor', INIT_COLOR);
% line([1, nInputs], repmat(posDevMeanInit, 1, 2), 'Color', INIT_COLOR);
% xlim([1, nInputs]);
% grid on;
% box on;
% plottexts('trajectory index', 'final position deviation [m]', [], 'initialized optimization planner', [], ...
%     FONT_SIZE);
% print(fullfile(TP_ROOT_DIR, '_data',NAME), FORMAT, '-painters');

%% Optimal trajectories

% Constants
NAME_PATH = 'Pathes';
NAME_TRAJ = 'Trajectories';
CIRCULAR_COLOR = 'Blue';
SYMMETRIC_COLOR = 'Red';
HEIGHT = 5;
WIDTH = 8;
MARKER_SIZE = 5;
OFFSET = 5;
FONT_SIZE = 8;
FORMAT = '-depsc';

% Plots
figPath = plotfigure(NAME_PATH, WIDTH, HEIGHT);
figTraj = plotfigure(NAME_TRAJ, WIDTH, HEIGHT);
xl = zeros(1, 2);
yl = zeros(1, 2);
for iOutput = 1 : length(outputOpt)
    doPlot = false;
    % Circular trajectories - upper side of figure
    if mod(iOutput, 2) == 1
        color = CIRCULAR_COLOR;
        if outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS(end) >= 0
            doPlot = true;
        end
    % Symmetric trajectories - lower side of figure
    else
        color = SYMMETRIC_COLOR;
        if outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS(end) <= 0
            doPlot = true;
        end
    end
    if doPlot
        % Dynamic limits
        xl(1) = min(xl(1), min(outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS));
        xl(2) = max(xl(2), max(outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS));
        yl(1) = min(yl(1), min(outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS));
        yl(2) = max(yl(2), max(outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS));
        figure(figPath);
        plot(outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS, ...
            outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS, 'Color', color);
        scatter(outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS(end), ...
            outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS(end), MARKER_SIZE, ...
            'MarkerEdgeColor', color);
        figure(figTraj);
        plot3(outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS, ...
            outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS, ...
            outputOpt(iOutput).VehicleOutput.Chassis.AngleZ, ...
            'Color', color);
        scatter3(outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS(end), ...
            outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS(end), ...
            outputOpt(iOutput).VehicleOutput.Chassis.AngleZ(end), MARKER_SIZE, ...
            'MarkerEdgeColor', color);
        if iOutput > 60
            break
        end
    end
end
xl = round(xl + [-OFFSET, OFFSET]);
yl = round(yl + [-OFFSET, OFFSET]);

figure(figPath);
scatter(0, 0, 5, 'MarkerEdgeColor', 'Black');
line(xl, [0, 0], 'Color', 'Black');
xlim(xl);
ylim(yl);
grid on;
box on;
plottexts('$x$ [m]', '$y$ [m]', [], [], [], FONT_SIZE);
print(fullfile(TP_ROOT_DIR, '_data', NAME_PATH), FORMAT, '-painters');

figure(figTraj);
scatter3(0, 0, 0, 5, 'MarkerEdgeColor', 'Black');
line(xl, [yl(1), yl(1)], [0, 0], 'Color', 'Black');
line(xl, [yl(2), yl(2)], [0, 0], 'Color', 'Black');
line([xl(1), xl(1)], yl, [0, 0], 'Color', 'Black');
line([xl(2), xl(2)], yl, [0, 0], 'Color', 'Black');
xlim(xl);
ylim(yl);
grid on;
box on;
view([25, 15]);
plottexts('$x$ [m]', '$y$ [m]', '$\psi$ [rad]', [], [], FONT_SIZE);
print(fullfile(TP_ROOT_DIR, '_data', NAME_TRAJ), FORMAT, '-painters');