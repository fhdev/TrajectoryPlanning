% Parameters
W = 12;
H = 7;
F = 8;

type = 'LaneChange';

if strcmpi(type, 'LaneChange')
    lkConTrjs = {'fc3_lanechange.mat'};
    lkOptTrjs = {'co5_lanechange_1_1_1.mat',    {'$k_{p,a}$ at $w_p=$[1 1 1]',   '$k_{p,j}$ at $w_p=$[1 1 1]'},   'x'; ...
                 'co5_lanechange_10_1_1.mat',   {'$k_{p,a}$ at $w_p=$[10 1 1]',  '$k_{p,j}$ at $w_p=$[10 1 1]'},  'd'; ...
                 'co5_lanechange_0.1_1_1.mat',  {'$k_{p,a}$ at $w_p=$[0.1 1 1]', '$k_{p,j}$ at $w_p=$[0.1 1 1]'}, 's'; ...
                 'co5_lanechange_1_10_1.mat',   {'$k_{p,a}$ at $w_p=$[1 10 1]',  '$k_{p,j}$ at $w_p=$[1 10 1]'},  '*'; ...
                 'co5_lanechange_1_0.1_1.mat',  {'$k_{p,a}$ at $w_p=$[1 0.1 1]', '$k_{p,j}$ at $w_p=$[1 0.1 1]'}, '+'; ...
                 'co5_lanechange_1_1_10.mat',   {'$k_{p,a}$ at $w_p=$[1 1 10]',  '$k_{p,j}$ at $w_p=$[1 1 10]'},  'o'; ...
                 'co5_lanechange_1_1_0.1.mat',  {'$k_{p,a}$ at $w_p=$[1 1 0.1]', '$k_{p,j}$ at $w_p=$[1 1 0.1]'}, 'v'};
elseif strcmpi(type, 'LaneKeep')
    lkConTrjs = {'fc3_lanekeep.mat'};
    lkOptTrjs = {'co5_lanekeep_1_1_1.mat',    {'$k_{p,a}$ at $w_p=$[1 1 1]',   '$k_{p,j}$ at $w_p=$[1 1 1]'},   'x'; ...
                 'co5_lanekeep_10_1_1.mat',   {'$k_{p,a}$ at $w_p=$[10 1 1]',  '$k_{p,j}$ at $w_p=$[10 1 1]'},  'd'; ...
                 'co5_lanekeep_0.1_1_1.mat',  {'$k_{p,a}$ at $w_p=$[0.1 1 1]', '$k_{p,j}$ at $w_p=$[0.1 1 1]'}, 's'; ...
                 'co5_lanekeep_1_10_1.mat',   {'$k_{p,a}$ at $w_p=$[1 10 1]',  '$k_{p,j}$ at $w_p=$[1 10 1]'},  '*'; ...
                 'co5_lanekeep_1_0.1_1.mat',  {'$k_{p,a}$ at $w_p=$[1 0.1 1]', '$k_{p,j}$ at $w_p=$[1 0.1 1]'}, '+'; ...
                 'co5_lanekeep_1_1_10.mat',   {'$k_{p,a}$ at $w_p=$[1 1 10]',  '$k_{p,j}$ at $w_p=$[1 1 10]'},  'o'; ...
                 'co5_lanekeep_1_1_0.1.mat',  {'$k_{p,a}$ at $w_p=$[1 1 0.1]', '$k_{p,j}$ at $w_p=$[1 1 0.1]'}, 'v'};
else
    error('Non-existing type.');
end

% Plot
fig = nicefigure('WeightingEffects', W, H);
ax = niceaxis(fig, F);

load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', lkConTrjs{1}), 'T_fc_3');
for optTrjIndex=1:length(lkOptTrjs)
    clear('T_co_5');
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', lkOptTrjs{optTrjIndex, 1}), 'T_co_5');
    plotWeightingEffect(ax, T_co_5, T_fc_3, lkOptTrjs{optTrjIndex, 3}, lower(type));
end
line(ax, [1 10], [1 1], 'Color', 'Black', 'LineStyle', '-', 'LineWidth', 0.5);
line(ax, [1 10], [1.1 1.1], 'Color', 'Black', 'LineStyle', '-.', 'LineWidth', 0.5);

if strcmpi(type, 'LaneChange')
    ylim(ax, [0.7, 1.2]);
    ax.YTick = 0.7:0.05:1.2;
    ax.YTickLabel = {'0.70', '0.75', '0.80', '0.85', '0.90', '0.95', '1.00', '1.05', '1.10', '1.60', '2.10'};
elseif strcmpi(type, 'LaneKeep')
    ylim(ax, [0.5, 1.4]);
    ax.YTick = 0.5:0.1:1.4;
    ax.YTickLabel = {'0.50', '0.60', '0.70', '0.80', '0.90', '1.00', '1.10', '2.10', '3.10', '3.20'};
end
xlim(ax, [1, 10]);
ax.XTick = 1:10;
ax.Position = [0.1 0.15 0.89 0.80];

xlabel(ax, '$i$ (no. of trajectory) [1]');
ylabel(ax, '$k_{p,a}$, $k_{p,j}$ (max. rel. acc. / jerk) [1]', 'Position', [0.0, 0.95]);


l = nicelegend([lkOptTrjs{:,2}], ax, F);
l.Orientation = 'Vertical';
l.Location = 'NorthEastOutside';

name = sprintf('WeightingEffects_%s', type);
savefig(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');


