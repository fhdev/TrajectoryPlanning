%% PlannerOpt **************************************************************************************
% [Summary]
%   This class represents an unconstrained nonlinear optimization based motion planner.
%   The planner aims to minimize the trajectory tracking error and dynamical quantities which cause 
%   discomfort. 
%   This happens with the constrained optimization solver Matlab fminunc.
%
% [Used in]
%   user
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef PlannerOpt < PlannerCon
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % Cost function for tracking
        costTrack
        % Cost function for dynamics
        costState
        
    end
    
    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------

        function value = setSolverOptions(value)
            if ~isa(value, 'optim.options.Fminunc')
                error('ERROR: SolverOptions must be an optim.options.Fminunc object!')
            end
        end
        
    end
    
    %% Instance methods ============================================================================    
    methods
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerOpt(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 2
                superArgs = varargin(1:2);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@PlannerCon(superArgs{:});
        end
        
        %% optFcnStateAndTrack ---------------------------------------------------------------------
        % [Summary]
        %   This function calculates and provides tracking error and state related costs as it is 
        %   required by Matlab fminunc and fmincon.
        %   
        % [Input]
        %   input    - planner input as PlannerOptIn object
        %   optVarV  - optimization variable in a vector as provided by fsolve
        %   
        % [Output]
        %   cost     - scalar cost value for fminunc or fmincon
        % ------------------------------------------------------------------------------------------        
        function cost = optFcnStateAndTrack(self, input, optVarV)
            % Update optimized variable
            newOptVarFlag = self.optVar.Update(optVarV);
            % Run only at first time or if optimized variable changed
            if newOptVarFlag || (isempty(self.vehIn) && isempty(self.vehOut))
                % Perform new simulation
                self.predictMotion(input);
            end
            %% Return with state constraint vector
            self.costTrack.Update(self.vehOut.CtrlLat);
            self.costState.Update(self.vehOut.Chassis);
            cost = self.costTrack.ToScalar() + self.costState.ToScalar();
        end
        
        %% CalculateTrajectory ---------------------------------------------------------------------
        % [Summary]
        %   This function calculates a vehicle trajectory according to the provided input.
        %
        % [Input]
        %   input  - planner input signals as PlannerOptIn object
        %   
        % [Output]
        %   output - planner output signals as PlannerOptOut object
        % ------------------------------------------------------------------------------------------        
        function output = CalculateTrajectory(self, input)
            %% Check arguments
            % Serialize input
            input = input(:);
            if ~(isa(input, 'PlannerOptIn') && isscalar(input))
                error('ERROR: input must be a PlannerOptIn object!');
            end
            %% Plan trajectory
            % Message to Command Window
            fprintf([repmat('=', 1, 120), '\n']);
            fprintf('Planning an optimal trajectory to x = %.4g m, y = %.4g m, r = %.4g rad, dr = %.4g rad ...\n', ...
                input.FinalCon.PosXG(1), ...
                input.FinalCon.PosYG(1), ...
                input.FinalCon.YawZ(1), ...
                input.FinalCon.YawVelZ(1));
            % Initialize
            self.conState = PlannerConFcnVehState(input.FinalCon);
            self.optVar = PlannerOptVarVelPath(input.OptVarInit);
            self.costTrack = PlannerOptFcnTrack(input.CostTrackWeight);
            self.costState = PlannerOptFcnVehState(input.CostStateWeight);
            % Reset storage for vehicle input and output
            self.vehIn = [];
            self.vehOut = [];
            % Run solver
            optVarInitV = input.OptVarInit.ToVector();
            F = @(optVarV)self.optFcnStateAndTrack(input, optVarV);
            tic;
            [optVarOptV, optFcnOpt, exitFlag] = fminunc(F, optVarInitV, self.SolverOptions);
            self.optFcnStateAndTrack(input, optVarOptV);
            eqConOpt = self.conFcnState(input, optVarOptV);
            ellapsedTime = toc;
            % Print results
            if exitFlag > 0
                fprintf('Success.\n');
            else
                fprintf('Failure.\n');
            end
            fprintf('Planning time:          %.4g\n', ellapsedTime);
            fprintf('Equality constraint:    %s\n', mat2str(eqConOpt, 4));
            fprintf('Optimization variable:  %s\n', mat2str(optVarOptV, 4));
            fprintf('Optimization function:  %.4g\n', optFcnOpt);
            %% Set output
            output = PlannerOptOut(ellapsedTime, exitFlag > 0, self.optVar, ...
                self.vehIn, self.vehOut, self.conState, self.costTrack, self.costState);
        end
        
    end
    
end
