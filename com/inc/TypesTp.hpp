#pragma once
#ifndef TPTYPES_HPP
#define TPTYPES_HPP
// Common type definitions for Trajectory Planning project.
// Integral types
typedef char Int8_T;
typedef unsigned char UInt8_T;
typedef short Int16_T;
typedef unsigned short UInt16_T;
typedef int Int32_T;
typedef unsigned int UInt32_T;
typedef long long int Int64_T;
typedef unsigned long long int UInt64_T;
// Floating-point types
typedef float Float32_T;
typedef double Float64_T;
// Logical type
typedef enum { FALSE = 0, TRUE = 1 } Boolean_T;

#endif // !TPTYPES_HPP


