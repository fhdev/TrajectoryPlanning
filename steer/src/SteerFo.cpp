#include <cstdlib>
#include "SteerFo.hpp"
#include "MathTp.hpp"

using namespace Eigen;

// Parameters ======================================================================================
// mass [kg]
#define Ratio_P        (P(0))
// moment of inertia about vertical axis [kgm^2]
#define TimeConstant_P (P(1))
// number of parameters
#define NP 2

// Inputs ==========================================================================================
// steering wheel angle [rad]
#define SteerWhlAng_U (U(0))
// number of inputs
#define NU 1

// States and derivatives ==========================================================================
// wheel level steering angle [rad]
#define SteerAng_X     (X(0))
#define DSteerAng_dX   (dX(0))
// number of states
#define NX 1

// Outputs =========================================================================================
// derivative of wheel level steering angle [rad]
#define DSteerAng_Y     (Y(0))
// number of outputs
#define NY 1

namespace TrajectoryPlanning
{
	Float64_T SteerFo::Ratio()	const { return ratio; }

	Int64_T SteerFo::nP() const { return NP; }
	Int64_T SteerFo::nX() const { return NX; }
	Int64_T SteerFo::nU() const { return NU; }
	Int64_T SteerFo::nY() const { return NY; }

	SteerFo::SteerFo(const Ref<const VectorXd> P) :

		ratio(Ratio_P),
		timeConstant(TimeConstant_P)
	{
	}

	SteerFo::SteerFo(
		Float64_T ratio, 
		Float64_T timeConstant) :

		ratio(ratio),
		timeConstant(timeConstant)
	{
	}

	Int32_T SteerFo::Derivatives(Float64_T t,
		const Ref<const VectorXd> X,
		const Ref<const VectorXd> U,
		Ref<VectorXd> dX, 
		Ref<VectorXd> Y) const
	{
        // Check sizes
		CheckSizes(X, U, dX, Y);

		// Calculation of necessary variables

		// Calculation of state derivatives
		DSteerAng_dX = ratio / timeConstant * SteerWhlAng_U - 1 / timeConstant * SteerAng_X;

		// Calculation of additional output quantitites and storage of output quantities
		DSteerAng_Y = DSteerAng_dX;

		return 0;
	}
}
