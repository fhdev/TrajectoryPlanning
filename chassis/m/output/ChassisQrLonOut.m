%% ChassisQrLonOut *********************************************************************************
% [Summary]
%   This class represents outputs for longitudinal quarter vehicle chassis model.
%
% [Used in]
%   VehBaseOut
%   VehQrLonOut
%   VehQrLon
%
% [Subclasses]
%   ChassisQrOut
% **************************************************************************************************
classdef ChassisQrLonOut < ComTimeInOut
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % longitudinal position in ground-fixed cs. [m]
        PosXG
        % longitudinal velocity in ground-fixed cs. [m]
        VelXG
        % longitudinal acceleration in ground-fixed cs. [m]
        AccXG
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.PosXG(self, value)
            if ~isequal(self.PosXG, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PosXG must be a numeric vector!')
                end
                self.PosXG = value(:);
            end
        end
        
        function set.VelXG(self, value)
            if ~isequal(self.VelXG, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: VelXG must be a numeric vector!')
                end
                self.VelXG = value(:);
            end
        end
        
        function set.AccXG(self, value)
            if ~isequal(self.AccXG, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: AccXG must be a numeric vector!')
                end
                self.AccXG = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = ChassisQrLonOut(varargin)
            if nargin == 1
                comTimeInArgs = varargin(1);
            elseif nargin == 4
                comTimeInArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
           self@ComTimeInOut(comTimeInArgs{:});
            if nargin == 1
                self.PosXG = varargin{1}.PosXG;
                self.VelXG = varargin{1}.VelXG;
                self.AccXG = varargin{1}.AccXG;
            elseif nargin == 4
                self.PosXG = varargin{2};
                self.VelXG = varargin{3};
                self.AccXG = varargin{4};
            end
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the longitudinal quarter vehicle chassis
        %   model.
        %
        % [Input]
        %       name - name for the plots
        %
        % [Output]
        %       fig  - figure handle of plot
        % ------------------------------------------------------------------------------------------        
        function fig = DisplayPlots(self, name)
            %% Constants
            WIDTH = 15;
            HEIGHT = 15;
            FONTSIZE = 8;
            
            %% Default arguments
            if nargin < 2
                name = class(self);
            end
            
            %% Create plots
            fig = nicefigure(name, WIDTH, HEIGHT);
            
            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 1, ax);
            plot(ax, self.Time, self.PosXG, 'Color', Colors.Emerald);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$x^G \mathrm{[m]}$');
            title(ax, 'global lon. position');

            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 4, ax);  
            plot(ax, self.Time, self.VelXG, 'Color', Colors.Emerald);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$v_x^G \mathrm{[m/s]}$');
            title(ax, 'global lon. velocity');
            
            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 7, ax);  
            plot(ax, self.Time, self.AccXG, 'Color', Colors.Emerald);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$a_x^G \mathrm{[m/s^2]}$');
            title(ax, 'global lon. acceleration');
        end
    end
end