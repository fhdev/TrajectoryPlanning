% Parameters
%measName = 'MEAS_EVAL_20190528_174021';
measName = 'MEAS_EVAL_20190528_175234';

load(fullfile(pwd, 'data', 'Pub_2020_IFACWC2020', measName));

% Conversion
sinYaw0 = sin(data.Planner.Vehicle.CtrlLat.YawZRef(1));
cosYaw0 = cos(data.Planner.Vehicle.CtrlLat.YawZRef(1));
xG = + data.Planner.vehOut.Chassis.PosXG * cosYaw0 - data.Planner.vehOut.Chassis.PosYG * sinYaw0 + ...
    data.Planner.Vehicle.CtrlLat.PosXRef(1);
yG = + data.Planner.vehOut.Chassis.PosXG * sinYaw0 + data.Planner.vehOut.Chassis.PosYG * cosYaw0 + ...
    data.Planner.Vehicle.CtrlLat.PosYRef(2);

% Start and end indices
bInd = find(~isnan(data.S.steer) & data.S.steer ~= 0, 1, 'first');
if isempty(bInd)
    bInd = 1;
end
eInd = find(~isnan(data.S.steer) & data.S.steer ~= 0, 1, 'last');
if isempty(eInd)
    eInd = length(data.S.steer);
end

% Subsampling factor
subs = @(x) x(1:50:end);

% Create plot
W = 7;
H = 4;
F = 8;

% Figure 1 (left)
fig1 = nicefigure(measName, W, H);
ax1 = niceaxis(fig1, F);
plot(ax1, subs(data.plannerOut.VehOut.Input.Time + data.t(end, bInd)), subs(-18 * rad2deg(data.plannerOut.VehOut.Input.SteerAng)), ...
    'Color', nicecolors.RedAlizarin, 'LineStyle', '-');
plot(ax1, data.t(end, (bInd : eInd)), data.S.steer(bInd : eInd), 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-');
ylabel(ax1, '$\delta_{sw}$ (steering wheel angle) [$^\circ$]');
xlim(ax1, data.t(end, [bInd, eInd]));
xlabel(ax1, '$t_v$ (time) [s]');

l = nicelegend({'Simulation', 'Measurement'}, ax1, F);
l.Orientation = 'horizontal';
l.Location = 'northoutside';

% Figure 2 (right)
fig2 = nicefigure(measName, W, H + 1.1);

ax2 = niceaxis(fig2, F);

yyaxis(ax2, 'left');
plot(ax2, subs(data.plannerOut.VehOut.Ctrl.Time + data.t(end, bInd)), subs(rad2deg(data.plannerOut.VehOut.Chassis.YawVelZ)), ...
    'Color', nicecolors.RedAlizarin, ...
    'LineStyle', '-');
plot(ax2, data.t(end, (bInd : eInd)), -rad2deg(data.S.oz2(bInd : eInd)), 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-');
ylabel(ax2, '$\dot{\psi}_v$ (yaw rate) [$^\circ$/s]');
ax2.YAxis(1).Color = 'Black';

yyaxis(ax2, 'right');
plot(ax2, subs(data.plannerOut.VehOut.Ctrl.Time + data.t(end, bInd)), subs(data.plannerOut.VehOut.Chassis.AccInrtYV), ...
    'Color', nicecolors.RedAlizarin, ...
    'LineStyle', '-.',  'LineWidth', .75);
plot(ax2, data.t(end, (bInd : eInd)), data.S.ay2(bInd : eInd), 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-.',  'LineWidth', .75);
ylabel(ax2, '$\ddot{y}^V_{v,I}$ (lat. acc.) [m/s\textsuperscript{2}]');
ax2.YAxis(2).Color = 'Black';

xlim(ax2, data.t(end, [bInd, eInd]));
xlabel(ax2, '$t_v$ (time) [s]');

l = nicelegend({sprintf('Simulation yaw rate'), sprintf('Measurement yaw rate'), ...
    sprintf('Simulation lateral acc.'), sprintf('Measurement lateral acc.')}, ax2, F);
l.Location = 'northoutside';

% Save
name = [measName, '_LatDyn1'];
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');
name = [measName, '_LatDyn2'];
savefig(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');