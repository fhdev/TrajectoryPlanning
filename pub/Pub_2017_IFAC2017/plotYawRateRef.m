% Data
% Duration (travel time)
t_f = 5; % [s]
% Spline knot points 
wz = [1, 2, 2, -2, -2, 0]'; %[rad/s]
% Time sample points to plot
t = 0:0.01:t_f;
% Yaw rate curve to plot
psi = spline(0:1:t_f, wz, t);

% Parameters
W = 7;
H = 4;
F = 8;

% Create new figure
fig = nicefigure('YawRateRef', W, H);
ax = niceaxis(fig, F);
% Plot curve
plot(t, psi, 'LineStyle', '-', 'Color', nicecolors.GreenEmerald);
% Plot knot points
plot(0:1:t_f, wz, 'o', 'MarkerEdgeColor',  nicecolors.GreenEmerald, 'MarkerFaceColor',  nicecolors.GreenEmerald, 'MarkerSize', 3.0);
% Set axis limits
axis(ax, [0, 5, -3, 3]);
% Axis labels
xlabel(ax, '$t_v$ (time) [s]');
ylabel(ax, '$\dot{\psi}_{ref}$ (yaw rate ref.) [rad/s]');
% Text labels for knot points
text(0.1,1.8,'$\dot{\psi}^{s0}_{ref}$', 'FontSize', F, 'Interpreter', 'Latex');
text(1.1,1.6,'$\dot{\psi}^{s1}_{ref}$', 'FontSize', F, 'Interpreter', 'Latex');
text(2.1,2.3,'$\dot{\psi}^{s2}_{ref}$', 'FontSize', F, 'Interpreter', 'Latex');
text(3.1,-1.7,'$\dot{\psi}^{s3}_{ref}$', 'FontSize', F, 'Interpreter', 'Latex');
text(4.1,-2.3,'$\dot{\psi}^{s4}_{ref}$', 'FontSize', F, 'Interpreter', 'Latex');
text(4.5,0.6,'$\dot{\psi}^{s5}_{ref}$', 'FontSize', F, 'Interpreter', 'Latex');
% X tick labels
ax.XTick = [0 1 2 3 4 5];
ax.XTickLabel = {'$0$', '$\frac{1}{5}t_f$', '$\frac{2}{5}t_f$', '$\frac{3}{5}t_f$', '$\frac{4}{5}t_f$', '$t_f$'};
% Y tick labels
ax.YTick =[-3 -2 -1 0 1 2 3];
ay.YTickLabel = {'', '', '', '', '', '', ''};

fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

name = 'YawRateRef';
savefig(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');