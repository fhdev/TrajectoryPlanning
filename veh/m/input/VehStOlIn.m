%% VehQrLonIn **************************************************************************************
% [Summary]
%   This class represents inputs for single track open loop vehicle model.
%
% [Used in]
%   VehStOl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehStOlIn < VehOlIn & VehEnvIn2W
    %% Instance methods ============================================================================
    methods
        %% Constructor -----------------------------------------------------------------------------
        function self = VehStOlIn(varargin)
           if nargin == 1
               vehOlInArgs = varargin(1);
               vehEnvIn2WArgs = varargin(1);
           elseif nargin == 6
               vehOlInArgs = varargin(1:4);
               vehEnvIn2WArgs = varargin([1, 5, 6]);
           else
               error('ERROR: Inappropriate number of arguments!');
           end
           self@VehOlIn(vehOlInArgs{:});
           self@VehEnvIn2W(vehEnvIn2WArgs{:});
        end

    end
end