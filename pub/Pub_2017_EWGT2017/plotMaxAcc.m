% Parameters
W = 7;
H = 4;
F = 8;

type = 'LaneKeep';

% Data
if strcmpi(type, 'LaneChange')
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'fc3_lanechange.mat'), 'T_fc_3');
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'co5_lanechange_1_1_1.mat'), 'T_co_5');
elseif strcmpi(type, 'LaneKeep')
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'fc3_lanekeep.mat'), 'T_fc_3');
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'co5_lanekeep_10_1_1.mat'), 'T_co_5');
else
    error('Non-existing type.');
end

%% Path
fig1 = nicefigure('MaxAcc', W, H);
ax1 = niceaxis(fig1, F);

for i=1:length(T_fc_3)
    amaxCon(i) = sqrt(trapz(T_fc_3(i).t, T_fc_3(i).X(end,:) .^ 2) / T_fc_3(i).t(end)) ;
    amaxOpt(i) = sqrt(trapz(T_co_5(i).t, T_co_5(i).X(end,:) .^ 2) / T_co_5(i).t(end));
end

yyaxis(ax1, 'left');
plot(ax1, 1:9, amaxCon, 'LineStyle', '-', 'Color', nicecolors.BlueBelizeHole, 'MarkerSize', 3.0, 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.BlueBelizeHole, 'MarkerFaceColor', nicecolors.BlueBelizeHole);
plot(ax1, 1:9, amaxOpt, 'LineStyle', '-', 'Color', nicecolors.GreenEmerald, 'MarkerSize', 3.0, 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.GreenEmerald, 'MarkerFaceColor', nicecolors.GreenEmerald);
ax1.YAxis(1).Limits = [-1, 8];
ax1.YAxis(1).TickValues = 0:2:8;
ax1.YAxis(1).Color = nicecolors.BlueBelizeHole;
ylabel(ax1, '$\mathrm{M} ( | \ddot{y}^V_{v,i} | )$ (avg. lat. acc.) [m/s\textsuperscript{2}]', 'Position', [-0.5, 3]);

yyaxis(ax1, 'right');
plot(ax1, 1:9, amaxCon - amaxOpt, 'LineStyle', '-', 'Color', nicecolors.RedAlizarin, 'MarkerSize', 3.0, 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.RedAlizarin, 'MarkerFaceColor', nicecolors.RedAlizarin);
if strcmpi(type, 'LaneChange')
    ax1.YAxis(2).Limits = [-0.1, 0.8];
    ax1.YAxis(2).TickValues = 0:0.2:0.8;
elseif strcmpi(type, 'LaneKeep')
    ax1.YAxis(2).Limits = [-0.2, 0.4];
    ax1.YAxis(2).TickValues = -0.2:0.2:0.4;
end
ax1.YAxis(2).Color = nicecolors.RedAlizarin;
ylabel(ax1, 'difference [m/s\textsuperscript{2}]');

xlim(ax1, [0,10]);
ax1.XTick = 0:10;

xlabel(ax1, '$i$ (no. of trajectory) [1]');

l = nicelegend({'Con', 'ConOpt', 'Con - ConOpt'}, ax1, F);

% Adjust by hand
fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

% Save
name = sprintf('MaxAcc_%s', type);
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

