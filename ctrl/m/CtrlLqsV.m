%% CtrlLqsV ****************************************************************************************
% [Summary]
%   This class represents a SISO state feedback controller for longitudinal velocity tracking. 
%
% [Used in]
%   VehStCl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef CtrlLqsV < CtrlLon
    %% Instance properties =========================================================================    
    properties 
        %% Internal properties ---------------------------------------------------------------------
        % Controller parameters
        % State feedback gain for
        % longitudinal velocity
        GainVelX
        % velocity tracking error integral
        GainVelXTrackErrInt
        
        % Gain for calcultion of initial state value
        GainVelXInit
        
    end
    
    %% Instance methods ============================================================================    
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.GainVelX(self, value)
            if ~isequal(self.GainVelX, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: GainVelX must be a numeric scalar!')
                end
                self.GainVelX = value(:);
            end
        end
        
        function set.GainVelXTrackErrInt(self, value)
            if ~isequal(self.GainVelXTrackErrInt, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: GainVelXTrackErrInt must be a numeric scalar!')
                end
                self.GainVelXTrackErrInt = value(:);
            end
        end
        
        function set.GainVelXInit(self, value)
            if ~isequal(self.GainVelXInit, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: GainVelXInit must be a numeric scalar!')
                end
                self.GainVelXInit = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = CtrlLqsV(chassis, wheel, varargin)
            %% Assemble superclass arguments
            if nargin == 3
                superArgs = varargin(1);
            elseif nargin == 7
                superArgs = varargin(1:4);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            %% Call superclass constructor
            self@CtrlLon(superArgs{:});
            %% Set subclass properties
            if nargin == 3
                self.GainVelXInit = varargin{1}.GainVelXInit;
            elseif nargin == 7
                self.GainVelXInit = varargin{5};
            end            
            
            % mass [kg]
            m    = chassis.Mass;
            % Aerodynamic drag parameters
            % aerodynamic drag coefficient [1]
            cd   = chassis.DragCoefficient;
            % frontal area of the vehicle [m^2]
            Af   = chassis.FrontalArea;
            % wheel radius [m]
            r    = wheel.Radius;
            % Rolling resistance parameters
            % Constant model [1]
            frr  = 0.015;
            % velocity independent rolling resistance coefficient [1]
            Arr  = wheel.RollingResistanceConst;
            % linearly velocity dependent rolling resistance coefficient [s/m]
            Brr  = wheel.RollingResistanceLin;
            % squarely velocity depenfent rolling resistance coefficient [s^2/m^2]
            Crr  = wheel.RollingResistanceSqr;
            % gravitation [m/s^2]
            g = Com.Gravitation;
            % mass density of air [kg/m^3]
            rhoa = Com.AirDensity;
            
            %% Calculate linearized rolling resistance
            % Frr = Fz * (Arr + Brr.|v| + Crr.v^2)  => Fz * (Arrlin + Brrlin.|v|)
            % (calculate line which has same slope and value @ self.VelLin as the parabola)
            Brrlin = 2 * Crr * self.VelLin + Brr;
            Arrlin = Crr * self.VelLin^2 + Brr * self.VelLin + Arr - Brrlin * self.VelLin;
            
            %% Calculate linerized aerodynamic drag
            % (calculate line which has same value @ self.VelLin as the parabola, and value 0 at 0)
            Badlin = 0.5 * cd * Af * rhoa * self.VelLin;
            
            %% LTI model with state space representation
            % Inputs
            % U(1) => TorqueDrv: driving/braking wheel torque [Nm]
            % States
            % X(1) => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
            % Outputs
            % Y(1) => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
            A = -(Brrlin * g + Badlin / m);
            B = 1 / (m * r);
            C = 1;
            D = 0;
            
            %% LQR control design for extended state space representation
            % Inputs
            % U(1) => TorqueDrv:   driving/braking wheel torque [Nm]
            % States
            % X(1) => VelXV:       longitudinal velocity in vehicle-fixed cs. [m/s]
            % X(2) => TrackErrInt: integral of longitudinal velocity tracking error [m]
            % Outputs
            % Y(1) => VelXV:      longitudinal velocity in vehicle-fixed cs. [m/s]
            Ae =[A, 0;
                -C, 0];
            Be = [B;
                0];
            Ce = [C, 0];
            % Check controllability
            if rank(ctrb(Ae, Be)) ~= size(Ae, 1)
                error('System is not controllable.');
            end
            % Choose weights
            % velocity
            q11 = 1 / 50^2;
            % velocity tracking error
            q22 = 1 / 0.01^2;
            % torque
            r11 = 1 / 1000^2;
            % Weighting matrices
            Q0 = diag([q11, q22]);
            R0 = r11;
            Q1 = Q0;
            R1 = R0;
            % Design controller
            K = lqr(Ae, Be, Q1, R1);
            
            %% Assign computed property values
            self.GainVelX            = K(1);
            self.GainVelXTrackErrInt = K(2);
                      
        end
        
        %% Initialize -----------------------------------------------------------------------------
        % [Summary]
        %   This function calculates initial value of state vector.
        %       Let the output be Ki * U0r (self.GainVelXTrackErrInt * VelXV0) initially
        %           -Ke * X0 - K * U0r = Ki * U0r
        %            Ke * X0 = -(Ki + K) * U0r
        % [Input]
        %   U0r - initial input vector (= initial states of controlled system)
        %       U0r(1) => VelXV0: initial longitudinal velocity in vehicle-fixed cs. [m/s]
        %
        % [Output]
        %   X0 -  initial state vector
        %       X0(1) => VErrInt:  initial integral of longitudinal velocity tracking error [m]
        % ------------------------------------------------------------------------------------------         
        function X0 = Initialize(self, U0r)
            X0 = - self.GainVelXTrackErrInt \ ((self.GainVelXInit + self.GainVelX)' * U0r);
        end
        
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of the state feedback longitudinal velocity
        %   tracking controller.
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => VErrInt:  integral of longitudinal velocity tracking error [m]
        %
        %   U - input vector
        %       U(1) => VelXV:          longitudinal velocity in vehicle-fixed cs. [m/s]
        %       U(2) => VelRef:         target longitudinal velocity in vehicle-fixed cs. [m/s]
        %
        % [Output]
        %   dX - state derivative vector
        %       dX(1) => VErr:  longitudinal velocity tracking error [m]
        %
        %   Y  - output vector
        %       Y(1) => TorqueDrv: driving torque [Nm]
        %       Y(2) => TorqueBrk: braking torque [Nm]
        %       Y(3) => VErr:      longitudinal velocity tracking error [m]
        % ------------------------------------------------------------------------------------------        
        function [dX, Y] = StateEquation(self, t, X, U) %#ok<INUSL> t used for debugging           
            %% Calculate state feedback
            torque = - self.GainVelX * U(1) - self.GainVelXTrackErrInt * X(1);

            %% Saturation with anti-windup (clamping)
            [dX(1), torque] = self.AntiWindup(U, torque);
            
            %% Set output
            Y = [max(0, torque); max(0, -torque); U(2) - U(1)];            
        end
        
    end
end