%% WheelMfLon **************************************************************************************
% [Summary]
%   This class represents a longitudinal wheel model with Magic Formula tire.
%
% [Used in]
%   VehQrLon
%
% [Subclasses]
%   WheelMf
% **************************************************************************************************
classdef WheelMfLon < handle
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Wheel radius [m]
        Radius
        % Moment of inertia [kgm^2]
        MomentOfInertiaY
    
        % Magic formula parameters
        % Longitudinal maximal value (Dx) [1]
        MfMaxX
        % Longitudinal stiffness factor (B) [1]
        MfStiffnessX
        % Longitudinal shape factor (C) [1]
        MfShapeX
        % Longitudinal curvature factor (E) [1]
        MfCurvatureX
        
        % Rolling resistance coefficients
        % Velocity independent rolling resistance factor [1]
        RollingResistanceConst
        % Linearly velocity dependent rolling resistance factor [s/m]
        RollingResistanceLin
        % Squarely velocity dependent rollint resistance factor [s^2/m^2]
        RollingResistanceSqr
        % Longitudinal velocity at which rolling resistance reaches it's final value [m/s]
        RollingResistanceVel
        % Braking torque dependent correction in velocity at which rolling resistance reaches
        % it's final value [1/Ns]
        RollingResistanceVelCorr
        
        % Braking torque application parameters
        % Longitudinal velocity at which braking torque reaches it's final value [m/s]
        % braking torque independent
        BrakingTorqueVel
        
        % Transient slip model parameters
        % Longitudinal relaxation length at zero longitudinal slip [m]
        RelaxationLength0X
        % Minimal longitudinal relaxation length [m]
        RelaxationLengthMinX
        % Standstill longitudinal slip damping [Ns/m]
        SlipDamping0X
        % Longitudinal velocity at which slip damping switches off [m/s^2]
        VelSlowX
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.Radius(self, value)
            if ~isequal(self.Radius, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: Radius must be numeric scalar!')
                end
                self.Radius = value;
            end
        end
        
        function set.MomentOfInertiaY(self, value)
            if ~isequal(self.MomentOfInertiaY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MomentOfInertiaY must be numeric scalar!')
                end
                self.MomentOfInertiaY = value;
            end
        end
        
        function set.MfMaxX(self, value)
            if ~isequal(self.MfMaxX, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MfMaxX must be numeric scalar!')
                end
                self.MfMaxX = value;
            end
        end
        
        function set.MfStiffnessX(self, value)
            if ~isequal(self.MfStiffnessX, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MfStiffnessX must be numeric scalar!')
                end
                self.MfStiffnessX = value;
            end
        end
        
        function set.MfShapeX(self, value)
            if ~isequal(self.MfShapeX, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MfShapeX must be numeric scalar!')
                end
                self.MfShapeX = value;
            end
        end
        
        function set.MfCurvatureX(self, value)
            if ~isequal(self.MfCurvatureX, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MfCurvatureX must be numeric scalar!')
                end
                self.MfCurvatureX = value;
            end
        end
        
        function set.RollingResistanceConst(self, value)
            if ~isequal(self.RollingResistanceConst, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: RollingResistanceConst must be numeric scalar!')
                end
                self.RollingResistanceConst = value;
            end
        end
        
        function set.RollingResistanceLin(self, value)
            if ~isequal(self.RollingResistanceLin, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: RollingResistanceLin must be numeric scalar!')
                end
                self.RollingResistanceLin = value;
            end
        end
        
        function set.RollingResistanceSqr(self, value)
            if ~isequal(self.RollingResistanceSqr, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: RollingResistanceSqr must be numeric scalar!')
                end
                self.RollingResistanceSqr = value;
            end
        end
        
        function set.RollingResistanceVel(self, value)
            if ~isequal(self.RollingResistanceVel, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: RollingResistanceVel must be numeric scalar!')
                end
                self.RollingResistanceVel = value;
            end
        end
        
        function set.RollingResistanceVelCorr(self, value)
            if ~isequal(self.RollingResistanceVelCorr, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: RollingResistanceVelCorr must be numeric scalar!')
                end
                self.RollingResistanceVelCorr = value;
            end
        end
        
        function set.BrakingTorqueVel(self, value)
            if ~isequal(self.BrakingTorqueVel, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: BrakingTorqueVel must be numeric scalar!')
                end
                self.BrakingTorqueVel = value;
            end
        end
        
        function set.RelaxationLength0X(self, value)
            if ~isequal(self.RelaxationLength0X, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: RelaxationLength0X must be numeric scalar!')
                end
                self.RelaxationLength0X = value;
            end
        end
        
        function set.RelaxationLengthMinX(self, value)
            if ~isequal(self.RelaxationLengthMinX, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: RelaxationLengthMinX must be numeric scalar!')
                end
                self.RelaxationLengthMinX = value;
            end
        end
        
        function set.SlipDamping0X(self, value)
            if ~isequal(self.SlipDamping0X, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SlipDamping0X must be numeric scalar!')
                end
                self.SlipDamping0X = value;
            end
        end
        
        function set.VelSlowX(self, value)
            if ~isequal(self.VelSlowX, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: VelSlowX must be numeric scalar!')
                end
                self.VelSlowX = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = WheelMfLon(varargin)
            if nargin == 1
                self.Radius                   = varargin{1}.Radius;
                self.MomentOfInertiaY         = varargin{1}.MomentOfInertiaY;
                self.MfMaxX                   = varargin{1}.MfMaxX;
                self.MfStiffnessX             = varargin{1}.MfStiffnessX;
                self.MfShapeX                 = varargin{1}.MfShapeX;
                self.MfCurvatureX             = varargin{1}.MfCurvatureX;
                self.RollingResistanceConst   = varargin{1}.RollingResistanceConst;
                self.RollingResistanceLin     = varargin{1}.RollingResistanceLin;
                self.RollingResistanceSqr     = varargin{1}.RollingResistanceSqr;
                self.RollingResistanceVel     = varargin{1}.RollingResistanceVel;
                self.RollingResistanceVelCorr = varargin{1}.RollingResistanceVelCorr;
                self.BrakingTorqueVel         = varargin{1}.BrakingTorqueVel;
                self.RelaxationLength0X       = varargin{1}.RelaxationLength0X;
                self.RelaxationLengthMinX     = varargin{1}.RelaxationLengthMinX;
                self.SlipDamping0X            = varargin{1}.SlipDamping0X;
                self.VelSlowX                 = varargin{1}.VelSlowX;
            elseif nargin == 16
                self.Radius                   = varargin{1};
                self.MomentOfInertiaY         = varargin{2};
                self.MfMaxX                   = varargin{3};
                self.MfStiffnessX             = varargin{4};
                self.MfShapeX                 = varargin{5};
                self.MfCurvatureX             = varargin{6};
                self.RollingResistanceConst   = varargin{7};
                self.RollingResistanceLin     = varargin{8};
                self.RollingResistanceSqr     = varargin{9};
                self.RollingResistanceVel     = varargin{10};
                self.RollingResistanceVelCorr = varargin{11};
                self.BrakingTorqueVel         = varargin{12};
                self.RelaxationLength0X       = varargin{13};
                self.RelaxationLengthMinX     = varargin{14};
                self.SlipDamping0X            = varargin{15};
                self.VelSlowX                 = varargin{16};
            else
                 error('ERROR: Inappropriate number of arguments!');
            end
        end 
        
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function contains the equations of motion of the longitudinal wheel model.
        %   References are from Hans Pacejka - Tyre and Vehicle Dynamics (2nd edition) 2006.
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => PitchY:    angle of the wheel [rad]
        %       X(2) => PitchVelY: angular velocity of the wheel [rad/s]
        %       X(3) => SlipX:     longitudinal slip [1]
        %
        %   U - input vector
        %       U(1) => TorqueDrv: driving torque [Nm]
        %       U(2) => TorqueBrk: braking torque [Nm]
        %       U(3) => Friction:  friction coefficient [1]
        %       U(4) => ForceZW:   vertical force (tire load) in wheel-fixed cs. [N]
        %       U(5) => VelXW:     velocity of the wheel center point in wheel-fixed cs. [m/s]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %       Y(1) => PitchAccY: angular acceleration [rad/s]
        %       Y(2) => DSlipX:    longitudinal slip derivative [1/s]
        %       Y(3) => ForceXW:   longitudinal force in wheel-fixed coordinate system [N]
        % ------------------------------------------------------------------------------------------
        function [dX, Y] = StateEquation(self, ~, X, U)
            %% Calculation of necessary variables
            % Calculation of slip damping factor (Eq. 8.128 @ P. 407)
            if abs(U(5)) <= self.VelSlowX
                slipDampingX = 0.5 * self.SlipDamping0X * (1 + cos(pi * abs(U(5)) / self.VelSlowX));
            else
                slipDampingX = 0;
            end
            % Peak value of longitudinal force
            forceXWMax = self.MfMaxX * U(3) * U(4);
            % Calculation of slip stiffness (Eq. 8.115 @ P. 405)
            slipStiffness = self.MfStiffnessX * self.MfShapeX * forceXWMax;
            % Calculation of damped slip (Eq. 8.127 @ P. 407)
            velRollW = self.Radius * X(2);
            slipXDamped = X(3) + slipDampingX / slipStiffness * (velRollW - U(5));
            % Calculation of tire force (Eq. 8.109 @ P. 404)
            forceXW = forceXWMax * sin(self.MfShapeX * atan(self.MfStiffnessX * slipXDamped - ...
                self.MfCurvatureX * (self.MfStiffnessX * slipXDamped - ...
                atan(self.MfStiffnessX * slipXDamped))));
            % Calculation of slip-dependent relaxation length (Eq. 8.121 @ P. 406)
            relaxationLengthX = max(self.RelaxationLength0X * ...
                (1 - slipStiffness / (3 * forceXWMax) * abs(X(3))), self.RelaxationLengthMinX);
            % Calculation of rolling resistance torque
            % (https://www.mathworks.com/help/physmod/sdl/ref/rollingresistance.html)
            torqueRollRes = U(4) * self.Radius * (self.RollingResistanceConst + ...
                self.RollingResistanceLin * abs(velRollW) + self.RollingResistanceSqr * velRollW^2)  * ...
                sign(velRollW);
            if abs(velRollW) < self.RollingResistanceVel
                torqueRollRes = torqueRollRes * 0.5 * (1 - cos(pi * abs(velRollW) / ...
                    self.RollingResistanceVel));
            end
            % Calculation of acting braking torque
            torqueBrakeAct = U(2) * sign(velRollW);
            brakingTorqueVel = self.BrakingTorqueVel + self.RollingResistanceVelCorr * abs(U(2));
            if abs(velRollW) < brakingTorqueVel
                torqueBrakeAct = torqueBrakeAct * 0.5 * (1 - cos(pi * abs(velRollW) / brakingTorqueVel));
            end
            %% Calculation of state derivatives
            pitchAccY = (U(1) - torqueBrakeAct - self.Radius * forceXW - torqueRollRes) / ...
                self.MomentOfInertiaY;
            % Calculation of transient slip (Eq. 8.119 @ P. 406)
            dSlipX = (velRollW - U(5) - abs(U(5)) * X(3)) / relaxationLengthX;
            %% State derivative variables
            dX = [X(2);        % PitchVelY: angular velocity [rad/s]
                  pitchAccY;   % PitchAccY: angular acceleration [rad/s^2]
                  dSlipX];     % DSlipX:    longitudinal slip derivative [1/s]
            %% Output variables
            Y = [pitchAccY;    % PitchAccY: angular acceleration [rad/s]
                 dSlipX;       % DSlipX:    longitudinal slip derivative [1/s]
                 forceXW];     % ForceXW:   longitudinal force in wheel-fixed coordinate system [N]
        end
        
    end
end