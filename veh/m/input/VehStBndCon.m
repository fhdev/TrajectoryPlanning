%% VehStBndCon *************************************************************************************
% [Summary]
%   This class represents boundary conditions for single track open loop vehicle model.
%
% [Used in]
%   VehStOl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehStBndCon < ChassisStBndCon
    %% Instance methods ============================================================================
    methods
        %% Constructor -----------------------------------------------------------------------------
        function self = VehStBndCon(varargin)
            self@ChassisStBndCon(varargin{:});
        end

    end
end