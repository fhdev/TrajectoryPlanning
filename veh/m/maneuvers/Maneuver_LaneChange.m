function [input, initCon] = Maneuver_LaneChange()

%% Parameters that define the trajectory
% Open loop (constant driving torque and square form steering angle)
Md = 135;
delta = deg2rad(20);
% Closed loop (lane change in 200 m)
sp = [ 0; 30;    50;    80;   120;   150; 170; 200];
kp = [ 0;  0; -0.01; -0.01; +0.01; +0.01;   0;   0];
vp = [20; 20;    20;    20;    20;    20;  20;  20];
[t, ~, x, y, r, v, dr] = PlannerGeom.CurvatureTraj(sp, kp, vp);

%% Initial conditions
% Do not reset these
initCon.PosXG   = 0;
initCon.PosYG   = 0;
initCon.YawZ    = 0;
% Adjustable indeed
initCon.VelXV   = vp(1);
initCon.VelYV   = 0;
initCon.YawVelZ = kp(1) * vp(1);

%% Inputs
% Common
input.Time = t;
input.Friction = ones(size(input.Time));
input.FrictionFront = ones(size(input.Time));
input.FrictionRear = ones(size(input.Time));
% Open loop
input.TorqueDrv = Md * ones(size(input.Time)); 
input.TorqueBrk = zeros(size(input.Time));
input.SteerWhlAng = delta * (double(input.Time > 1) - ...
    2 * double(input.Time > (input.Time(end) - 2) / 2 + 1) + ...
    double(input.Time > input.Time(end) - 1));
input.SteerAng = input.SteerWhlAng / 18; 
% Closed loop with yaw rate tracking or path tracking
input.VelXVRef = v;
input.YawVelZRef = dr;
input.PosXGRef = x;
input.PosYGRef = y;
input.YawZRef = r;
end
