#include <cstdlib>
#include "VehStCl.hpp"
#include "MathTp.hpp"

using namespace Eigen;

// Parameters ======================================================================================
// number of parameters
#define NP 77

// Inputs ==========================================================================================
// reference longitudinal velocity [m/s]
#define VelRef_U        (U(0))
// reference yaw rate [rad/s] (used only with CtrlLqsYr)
#define YawRateRef_U    (U(1))
// friction coefficient front [1]
#define FrictionFront_U (U(2))
// friction coefficient rear [1]
#define FrictionRear_U  (U(3))
// number of inputs
#define NU 4

// Specified initial states ========================================================================
// Chassis -----------------------------------------------------------------------------------------
// longitudinal position in ground - fixed coordinate system [m]
#define PosXG_Xi	(Xi(0))
// longitudinal velocity in vehicle - fixed coordinate system [m/s]
#define VelXV_Xi	(Xi(1))
// lateral position in ground - fixed coordinate system [m]
#define PosYG_Xi	(Xi(2))
// lateral velocity in vehicle - fixed coordinate system [m/s]
#define VelYV_Xi	(Xi(3))
// yaw (heading) angle in ground - fixed coordinate system [rad]
#define YawZ_Xi	    (Xi(4))
// yaw (heading) angular velocity in ground - fixed system [rad/s]
#define YawVelZ_Xi  (Xi(5))
// number of specified initial states
#define NXi 6

// States and derivatives ==========================================================================
// Chassis -----------------------------------------------------------------------------------------
// longitudinal position in ground - fixed coordinate system [m]
#define PosXG_X     (X(0))
#define DPosXG_dX   (dX(0))
// longitudinal velocity in ground - fixed coordinate system [m/s]
#define VelXG_X     (X(1))
#define DVelXG_dX   (dX(1))
// lateral position in ground - fixed coordinate system [m]
#define PosYG_X     (X(2))
#define DPosYG_dX   (dX(2))
// lateral velocity in ground - fixed coordinate system [m/s]
#define VelYG_X     (X(3))
#define DVelYG_dX   (dX(3))
// yaw(heading) angle in ground - fixed coordinate system [rad]
#define YawZ_X      (X(4))
#define DYawZ_dX    (dX(4))
// yaw(heading) angular velocity in ground - fixed system [rad/s]
#define YawVelZ_X   (X(5))
#define DYawVelZ_dX (dX(5))
// WheelFront --------------------------------------------------------------------------------------
#define OFFSET_XWF 6
// angle of the wheel [rad]
#define PitchYFront_X      (X(6))
#define DPitchYFront_dX    (dX(6))
// angular velocity of the wheel [rad/s]
#define PitchVelYFront_X   (X(7))
#define DPitchVelYFront_dX (dX(7))
// longitudinal slip[1]
#define SlipXFront_X       (X(8))
#define DSlipXFront_dX     (dX(8))
// lateral slip[1]
#define SlipYFront_X       (X(9))
#define DSlipYFront_dX     (dX(9))
// WheelRear ---------------------------------------------------------------------------------------
#define OFFSET_XWR 10
// angle of the wheel [rad]
#define PitchYRear_X      (X(10))
#define DPitchYRear_dX    (dX(10))
// angular velocity of the wheel [rad/s]
#define PitchVelYRear_X   (X(11))
#define DPitchVelYRear_dX (dX(11))
// longitudinal slip[1]
#define SlipXRear_X       (X(12))
#define DSlipXRear_dX     (dX(12))
// lateral slip[1]
#define SlipYRear_X       (X(13))
#define DSlipYRear_dX     (dX(13))
// Steering ----------------------------------------------------------------------------------------
#define OFFSET_XS 14
// wheel level steering angle [rad]
#define SteerAng_X        (X(14))
#define DSteerAng_dX      (dX(14))
// Control -----------------------------------------------------------------------------------------
#define OFFSET_XCLon 15
// integral of velocity tracking error [m]
#define VelXVErrInt_X     (X(15))
#define DVelXVErrInt_dX   (dX(15))
#define OFFSET_XCLat 16
// integral of yaw rate tracking error [rad] (CtrlLqsYr)
// integral of longitudinal velocity multiplied by yaw rate tracking error [m.rad/s] (CtrlLatTrk)
#define YawVelZErrInt_X   (X(16))
#define DYawVelZErrInt_dX (dX(16))
// number of states
#define NX 17

// Outputs =========================================================================================
// Chassis -----------------------------------------------------------------------------------------
// longitudinal acceleration in ground - fixed cs. [m/s^2]
#define AccXG_Y     (Y(0))
// lateral acceleration in ground - fixed cs. [m/s^2]
#define AccYG_Y     (Y(1))
// longitudinal inertial acceleration in vehicle - fixed cs. [m/s^2]
#define AccInrtXV_Y (Y(2))
// lateral inertial acceleration in vehicle - fixed cs. [m/s^2]
#define AccInrtYV_Y (Y(3))
// yaw(heading) angular acceleration in ground - fixed system [rad/s^2]
#define YawAccZ_Y   (Y(4))
// longitudinal velocity in vehicle - fixed cs. [m/s]
#define VelXV_Y     (Y(5))
// lateral velocity in vehicle - fixed cs. [m/s]
#define VelYV_Y     (Y(6))
// WheelFront --------------------------------------------------------------------------------------
#define OFFSET_YWF 7
// angular acceleration [rad/s]
#define PitchAccYFront_Y (Y(7))
// longitudinal slip derivative [1/s]
#define DSlipXFront_Y    (Y(8))
// longitudinal force in wheel - fixed coordinate system [N]
#define ForceXWFront_Y   (Y(9))
// lateral slip derivative [1/s]
#define DSlipYFront_Y    (Y(10))
// lateral force in wheel - fixed coordinate system [N]
#define ForceYWFront_Y   (Y(11))
// vertical force in wheel - fixed coordinate system [N]
#define ForceZWFront_Y   (Y(12))
// wheel level driving torque [Nm]
#define TorqueDrvFront_Y (Y(13))
// wheel level braking torque [Nm]
#define TorqueBrkFront_Y (Y(14))
// wheel level steering angle [rad]
#define SteerAngFront_Y  (Y(15))
// WheelRear ---------------------------------------------------------------------------------------
#define OFFSET_YWR 16
// angular acceleration [rad/s]
#define PitchAccYRear_Y (Y(16))
// longitudinal slip derivative [1/s]
#define DSlipXRear_Y    (Y(17))
// longitudinal force in wheel - fixed coordinate system [N]
#define ForceXWRear_Y   (Y(18))
// lateral slip derivative [1/s]
#define DSlipYRear_Y    (Y(19))
// lateral force in wheel - fixed coordinate system [N]
#define ForceYWRear_Y   (Y(20))
// vertical force in wheel - fixed coordinate system [N]
#define ForceZWRear_Y   (Y(21))
// wheel level driving torque [Nm]
#define TorqueDrvRear_Y (Y(22))
// wheel level braking torque [Nm]
#define TorqueBrkRear_Y (Y(23))
// wheel level steering angle [rad]
#define SteerAngRear_Y  (Y(24))
// Steering ----------------------------------------------------------------------------------------
#define OFFSET_YS 25
// wheel level steering angle derivative [rad/s]
#define DSteerAng_Y     (Y(25))
// Control -----------------------------------------------------------------------------------------
#define OFFSET_YCLon 26
// total driving torque [Nm]
#define TorqueDrv_Y     (Y(26))
// total braking torque [Nm]
#define TorqueBrk_Y     (Y(27))
// velocity tracking error [m/s]
#define VErr_Y          (Y(28))
#define OFFSET_YCLat 29
// wheel level steering angle [rad]
#define SteerAng_Y		(Y(29))
// cross track error [m]
#define ErrCrossTrack_Y (Y(30))
// heading error [rad]
#define ErrHeading_Y    (Y(31))

// number of outputs
#define NY 32

namespace TrajectoryPlanning
{
	Int64_T VehStCl::nP() const { return NP; }
	Int64_T VehStCl::nX() const { return NX; }
	Int64_T VehStCl::nU() const { return NU; }
	Int64_T VehStCl::nY() const { return NY; }

	VehStCl::VehStCl(const ChassisSt &chassis, 
		const WheelMf &wheelFront,
		const WheelMf &wheelRear, 
		const SteerFo &steering,
		const CtrlLqsV &ctrlLon,
		const CtrlLatSty &ctrlLat,
		Float64_T timeInit) :

		VehStOl(chassis, wheelFront, wheelRear, steering),
		ctrlLon(ctrlLon), 
		ctrlLat(ctrlLat),
		timeInit(timeInit),
		torqueDrv(0.0),
		torqueBrk(0.0),
		steerWhlAng(0.0)
	{
	}

	Int32_T VehStCl::Derivatives(Float64_T t, 
		const Ref<const VectorXd> X,
		const Ref<const VectorXd> U,
		Ref<VectorXd> dX, 
		Ref<VectorXd> Y) const
	{
        // Check sizes
		CheckSizes(X, U, dX, Y);

		// Vehicle
		VectorXd UV(5);
		UV << torqueDrv, torqueBrk, FrictionFront_U, FrictionRear_U, steerWhlAng;
		VehStOl::Derivatives(t, X, UV, dX, Y);

		// Control 

		// Longitudinal
		VectorXd UCLon(2);
		UCLon << VelXV_Y, VelRef_U;
		ctrlLon.Derivatives(
			t, 
			X.segment(OFFSET_XCLon, OFFSET_XCLat - OFFSET_XCLon),
			UCLon, 
			dX.segment(OFFSET_XCLon, OFFSET_XCLat - OFFSET_XCLon), 
			Y.segment(OFFSET_YCLon, OFFSET_YCLat - OFFSET_YCLon));
		torqueDrv = TorqueDrv_Y;
		torqueBrk = TorqueBrk_Y;

		// Lateral
		VectorXd UCLat(4);
		UCLat << PosXG_X, PosYG_X, YawZ_X, VelXV_Y;
		ctrlLat.Derivatives(
			t,
			X.segment(OFFSET_XCLat, NX - OFFSET_XCLat),
			UCLat,
			dX.segment(OFFSET_XCLat, NX - OFFSET_XCLat),
			Y.segment(OFFSET_YCLat, NY - OFFSET_YCLat));
		steerWhlAng = SteerAng_Y * (1.0 / steering.Ratio());
		
		return 0;
	}

	Int32_T VehStCl::Initialize(const Ref<const VectorXd> Xi, Ref<VectorXd> X) const
	{
		// Check sizes
		if (Xi.size() != NXi)
		{
			throw std::invalid_argument("VehStCl.Initialize: Length of Xi is not appropriate!");
		}
		if (X.size() != NX)
		{
			throw std::invalid_argument("VehStCl.Initialize: Length of X is not appropriate!");
		}

		// Reset internal variables
		torqueDrv = 0.0;
		torqueBrk = 0.0;
		steerWhlAng = 0.0;

		// Fill initial condition vector
		// Vehicle
		VehStOl::Initialize(Xi, X.segment(0, OFFSET_XCLon));
		// Control
		// Longitudinal
		ctrlLon.Initialize(Xi.segment(1, 1), X.segment(OFFSET_XCLon, OFFSET_XCLat - OFFSET_XCLon));
		// Lateral (not used for CtrlLatSty)
		ctrlLat.Initialize(Xi, X.segment(OFFSET_XCLat, NX - OFFSET_XCLat));

		// Simulate switching transient
		if (timeInit > 0.0)
		{
			
		}

		return 0;
	}
}
