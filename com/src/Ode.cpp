#include "Ode.hpp"

using namespace Eigen;

namespace TrajectoryPlanning
{

	void DynamicSystem::CheckSizes(
		const Ref<const VectorXd> X,
		const Ref<const VectorXd> U,
		const Ref<const VectorXd> dX,
		const Ref<const VectorXd> Y) const
	{
		// Check sizes
		if (X.size() < nX())
		{
			throw std::invalid_argument("Length of X is not appropriate!");
		}
		if (U.size() < nU())
		{
			throw std::invalid_argument("Length of U is not appropriate!");
		}
		if (dX.size() < nX())
		{
			throw std::invalid_argument("Length of dX is not appropriate!");
		}
		if (Y.size() < nY())
		{
			throw std::invalid_argument("Length of Y is not appropriate!");
		}
	}

    Int32_T Rk4(DynamicSystem& sys,
        const Ref<const VectorXd> t,
        const Ref<const VectorXd> X0,
        const Ref<const MatrixXd> U,
        Ref<MatrixXd> X,
        Ref<MatrixXd> Y)
    {
        Int64_T nt = t.size();

        if (sys.nX() != X0.size())
        {
            throw std::invalid_argument("Rk4: Length of X0 is not appropriate according to sys!");
        }
        if (sys.nU() != U.rows())
        {
            throw std::invalid_argument("Rk4: Number of rows of U is not appropriate according to sys!");
        }
        if (nt != U.cols())
        {
            throw std::invalid_argument("Rk4: Number of columns of U and length of t is not equal!");
        }

        // Storage for Runge-Kutta coefficients
        MatrixXd K = MatrixXd::Zero(sys.nX(), 4);
        MatrixXd L = MatrixXd::Zero(sys.nY(), 4);

		// Try to run system
		try
		{
			sys.Derivatives(t(0), X0, U.col(0), K.col(0), L.col(0));
		}
		catch (const std::exception& e)
		{
			throw std::invalid_argument(std::string("Rk4: Derivatives function of sys throws error on initial condition: ") + e.what());
		}

        // Set initial condition and input
		X.col(0) = X0;
        Y.col(0) = L.col(0);

        // Calculate solution
        for (Int64_T i = 0; i < nt - 1; i++)
        {
            // Current step size
            Float64_T h = t(i + 1) - t(i);
            // Input at the middle
            VectorXd Um = U.col(i) + (U.col(i + 1) - U.col(i)) / 2.0;
            // 4th order Runge-Kutta coefficients
            sys.Derivatives(t(i),           X.col(i),                      U.col(i),     K.col(0), L.col(0));
            sys.Derivatives(t(i) + h / 2.0, X.col(i) + h / 2.0 * K.col(0), Um,           K.col(1), L.col(1));
            sys.Derivatives(t(i) + h / 2.0, X.col(i) + h / 2.0 * K.col(1), Um,           K.col(2), L.col(2));
            sys.Derivatives(t(i + 1),       X.col(i) + h * K.col(2),       U.col(i + 1), K.col(3), L.col(3));

            // Calculate next state
            X.col(i + 1) = X.col(i) + h / 6.0 * (K.col(0) + 2 * K.col(1) + 2 * K.col(2) + K.col(3));

            // Store current output
            Y.col(i + 1) = 1.0 / 6.0 * (L.col(0) + 2 * L.col(1) + 2 * L.col(2) + L.col(3));

        }        

        return 0;
    }
}