%% Run optimization based planner on samples
samples = 'PlannerFcIn_Samples_LaneKeep_LaneChange_20mps';
planner = PlannerFc();
inputStruct = eval(samples);
input = [];
for iInput = 1 : length(inputStruct)
    input = [input; PlannerFcIn(inputStruct(iInput))]; %#ok<AGROW>
end
% Run this to initialize Simulink and do not corrupt time measurement
planner.CalculateTrajectory(input(43));
% Run test set
outputOpt = planner.CalculateTrajectory(input);
%% Run NN planner in standalone mode
outputNN = [];
for iInput = 1 : length(input)
    % Input for NN planner
    inputVector = [input(iInput).InitialConditions.PositionX_GCS; ...
                   input(iInput).InitialConditions.PositionY_GCS; ...
                   input(iInput).InitialConditions.AngleZ; ...
                   input(iInput).InitialConditions.AngularVelocityZ; ...
                   input(iInput).InitialConditions.VelocityX_VCS; ...
                   input(iInput).FinalConditions.PositionX_GCS; ...
                   input(iInput).FinalConditions.PositionY_GCS; ...
                   input(iInput).FinalConditions.AngleZ; ...
                   input(iInput).FinalConditions.AngularVelocityZ; ...
                   input(iInput).InitialConditions.VelocityX_VCS];
    % Run NN planner
    tic;
    outputVector = nn_best_3rd(inputVector);
    % Assemble PlannerFcOut from NN planner results
    refParam = PlannerRefParam(input(iInput).InitialRefParam.Velocity, ...
                               outputVector(1:end-1), ...
                               outputVector(end));
    vehicleInput = VehStIn.FactoryForPlanner(input(iInput).EnvironmentInput, ...
                                             input(iInput).InitialConditions, ...
                                             refParam);
    % Run vehicle simulation
    vehicleOutput = planner.Vehicle.SimulateSl(vehicleInput);
    stateConstraint = PlannerConstrState(input(iInput).FinalConditions);
    stateConstraint.Update(vehicleOutput.Chassis, refParam.TravelTime);
    calcTime = toc; % calc time with plausibility check
    outputNN = [outputNN; PlannerFcOut(calcTime, refParam, vehicleInput, vehicleOutput, stateConstraint)]; %#ok<AGROW>
end
%% Run NN planner in initial value mode
outputOptInit = [];
for iInput = 1 : length(input)
    inputInit = PlannerFcIn(input(iInput).InitialConditions, ...
                            input(iInput).FinalConditions, ...
                            outputNN(iInput).RefParam, ...
                            input(iInput).EnvironmentInput);
    outputOptInit = [outputOptInit; planner.CalculateTrajectory(inputInit)]; %#ok<AGROW>
end
%% Save results
save(fullfile(TP_ROOT_DIR, '_data', [samples, '.mat']), 'input', 'outputOpt', 'outputOptInit', 'outputNN');