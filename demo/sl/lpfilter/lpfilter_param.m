A = 1;
T = 0.025; % 50 ms
Ts = 0.01; % 10 ms

sysc = tf(A, [T, 1])

sysd = c2d(sysc, Ts)
b0 = sysd.Numerator{1}(end);
a1 = sysd.Denominator{1}(end - 1);
a0 = sysd.Denominator{1}(end);