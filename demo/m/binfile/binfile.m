% Data
I = int64((1 : 9))';
D = (1 : 9)' * pi;

% Write (0 for padding and 'l' for little-endian)
f = fopen('test.bin', 'w');
fwrite(f, I, 'int64', 0, 'l');
fwrite(f, D, 'float64', 0, 'l');
fclose(f);

% Read (0 for padding and 'l' for little-endian)
f = fopen('test.bin', 'r');
I2 = fread(f, 9, 'int64=>int64', 0, 'l');
D2 = fread(f, 9, 'float64=>float64', 0, 'l');
fclose(f);

% Display
disp(I2);
disp(D2);