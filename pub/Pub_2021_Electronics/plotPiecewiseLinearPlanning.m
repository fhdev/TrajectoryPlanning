DATA_FOLDER = fullfile('E:', 'VehModel_NN_2020');
TRAJECTORY_DATA = 'trajectory_011';

% Drive data
if ~exist('output', 'var')
    load(fullfile(DATA_FOLDER, sprintf('%s.mat', TRAJECTORY_DATA)), 'trajectory');
end

rangeIn = 1:31;
%rangeIn = 101:131;
sMin = trajectory.LengthKnots(rangeIn(1));
sMax = trajectory.LengthKnots(rangeIn(end));
iStart = find(trajectory.Length >= sMin, 1, 'first');
iEnd = find(trajectory.Length <= sMax, 1, 'last');
rangeOut = iStart:iEnd;

isStraight = trajectory.YawVelZ(rangeOut) == 0;
iStartStraight = [1; find(diff(isStraight)==1)];
iEndStraight = [find(diff(isStraight)==-1); iEnd - iStart + 1];
rangeOutStraight = rangeOut(isStraight);
iOutStraight = [1, find(diff(rangeOutStraight) > 1)];
rangeOutCurve = rangeOut(~isStraight);
iOutCurve = [1, find(diff(rangeOutCurve) > 1)];

round100 = @(x) sign(x) .* round(round(abs(x) / 100) * 100);


H = 1.5 * 7; % cm
W = 7; % cm
F = 8; % pt

% Input plot
name = ['PiecewiseLinInput', TRAJECTORY_DATA(end-2:end)];
fig1 = nicefigure(name, W, H);

ax11 = niceaxis(fig1, F, true);
subplot(3, 1, 2, ax11)
plot(ax11, trajectory.LengthKnots(rangeIn), trajectory.CurvKnots(rangeIn), '-o', ...
    'Color', nicecolors.BlueDeepKoamaru, ...
    'MarkerSize', 2, 'MarkerFaceColor', nicecolors.BlueDeepKoamaru, ...
    'MarkerEdgeColor', nicecolors.BlueDeepKoamaru);
xlabel(ax11, '$\sigma^i_p$ (arc length) [m]');
ylabel(ax11, '$\kappa^i_p$ (curvature) [1/m]');
xlim(ax11, trajectory.LengthKnots(rangeIn([1, end])));

ax12 = niceaxis(fig1, F, true);
subplot(3, 1, 1, ax12)
plot(ax12, trajectory.LengthKnots(rangeIn), trajectory.VelXKnots(rangeIn) * 3.6, ':o', ...
    'LineWidth', 1, 'Color', nicecolors.BlueDeepKoamaru, ...
    'MarkerSize', 2, 'MarkerFaceColor', nicecolors.BlueDeepKoamaru, ...
    'MarkerEdgeColor', nicecolors.BlueDeepKoamaru);
xlabel(ax12, '$\sigma^i_p$ (arc length) [m]');
ylabel(ax12, '$\dot{x}^i_p$ (speed) [km/h]');
xlim(ax12, trajectory.LengthKnots(rangeIn([1, end])));

ax13 = niceaxis(fig1, F, true);
subplot(3, 1, 3, ax13)
nSeg = length(iStartStraight);
for iSeg = 1 : nSeg
    rangeOutStraight = [iStartStraight(iSeg) : 50 : iEndStraight(iSeg), iEndStraight(iSeg)];
    plot(ax13, trajectory.PosXG(rangeOutStraight), trajectory.PosYG(rangeOutStraight), '-', ...
        'Color', nicecolors.GreenSea, 'LineWidth', 1.5);
    if iSeg < nSeg
        rangeOutCurve = [iEndStraight(iSeg) : 50 : iStartStraight(iSeg + 1), iStartStraight(iSeg + 1)];
        plot(ax13, trajectory.PosXG(rangeOutCurve), trajectory.PosYG(rangeOutCurve), '-', ...
            'Color', nicecolors.GreenSea, 'LineWidth', 1.5);
    end
end
xlabel(ax13, '$x_p$ (pos. north) [m]');
ylabel(ax13, '$y_p$ (pos. west) [m]');
axis(ax13, 'equal');
xlim(sort(trajectory.PosXG(rangeOut([1, end]))) + [-100; 100]);
ylim(sort(trajectory.PosYG(rangeOut([1, end]))) + [-100; 100]);
view(ax13, [-90, 90]);

% Output plot
rangeOutSub = [rangeOut(1) : 50 : rangeOut(end), rangeOut(end)];
name = ['PiecewiseLinOutput', TRAJECTORY_DATA(end-2:end)];
fig2 = nicefigure(name, W, H);

ax21 = niceaxis(fig2, F, true);
subplot(3, 1, 1, ax21)
plot(ax21, trajectory.Time(rangeOutSub), trajectory.VelXV(rangeOutSub) * 3.6, 'Color', nicecolors.GreenSea);
xlabel(ax21, '$t$ (time) [s]');
ylabel(ax21, '$\dot{x}_p$ (lon. vel.) [km/h]');
xlim(ax21, trajectory.Time(rangeOut([1, end])));

ax22 = niceaxis(fig2, F, true);
subplot(3, 1, 2, ax22)
plot(ax22, trajectory.Time(rangeOutSub), rad2deg(trajectory.YawVelZ(rangeOutSub)), 'Color', nicecolors.GreenSea);
xlabel(ax22, '$t$ (time) [s]');
ylabel(ax22, '$\dot{\psi}_p$ (yaw rate ) [$^{\circ}$/s]');
xlim(ax22, trajectory.Time(rangeOut([1, end])));

ax23 = niceaxis(fig2, F, true);
subplot(3, 1, 3, ax23)
plot(ax23, trajectory.Time(rangeOutSub), trajectory.AccInrtYV(rangeOutSub), 'Color', nicecolors.GreenSea);
xlabel(ax23, '$t$ (time) [s]');
ylabel(ax23, '$\ddot{y}_p$ (centr. acc.) [m/s\textsuperscript{2}]');
xlim(ax23, trajectory.Time(rangeOut([1, end])));


% Save
fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

name = ['PiecewiseLinInput', TRAJECTORY_DATA(end-2:end)];
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

name = ['PiecewiseLinOutput', TRAJECTORY_DATA(end-2:end)];
savefig(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');