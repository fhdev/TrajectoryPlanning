#include <cstdlib>
#include <algorithm>
#include "CtrlLqsV.hpp"
#include "MathTp.hpp"

using namespace Eigen;
using namespace std;

// Parameters ======================================================================================

// number of parameters
#define NP 7

// Inputs ==========================================================================================
// longitudinal velocity in vehicle - fixed coordintate system [m/s]
#define VelXV_U    (U(0))
// longitudinal reference velocity [m/s]
#define VelXVRef_U (U(1))    
// number of inputs
#define NU 2

// Specified initial states ========================================================================
// Chassis -----------------------------------------------------------------------------------------
// longitudinal velocity in vehicle - fixed coordinate system [m/s]
#define VelXV_Xi	(Xi(0))
// number of specified initial states
#define NXi 1

// States and derivatives ==========================================================================
// integral of longitudinal velocity tracking error [m]
#define VelXVErrInt_X     (X(0))
#define DVelXVErrInt_dX   (dX(0))
// number of states
#define NX 1

// Outputs =========================================================================================
// driving torque [Nm]
#define TorqueDrv_Y     (Y(0))
// braking torque [Nm]
#define TorqueBrk_Y     (Y(1))
// velocity tracking error [m/s]
#define VelXVErr_Y      (Y(2))
// number of outputs
#define NY 3

namespace TrajectoryPlanning
{
	Int64_T CtrlLqsV::nP() const { return NP; }
	Int64_T CtrlLqsV::nX() const { return NX; }
	Int64_T CtrlLqsV::nU() const { return NU; }
	Int64_T CtrlLqsV::nY() const { return NY; }

	CtrlLqsV::CtrlLqsV(Float64_T velLin,
		const Ref<const VectorXd> satVel,
		const Ref<const VectorXd> satTorqueDrv,
		const Ref<const VectorXd> satTorqueBrk,
		Float64_T gainVelX,
		Float64_T gainVelXTrackErrInt,
		Float64_T gainVelXInit) :

		velLin(velLin),
		satVel(satVel),
		satTorqueDrv(satTorqueDrv),
		satTorqueBrk(satTorqueBrk),
		gainVelX(gainVelX),
		gainVelXTrackErrInt(gainVelXTrackErrInt),
		gainVelXInit(gainVelXInit)
	{
	}

	Int32_T CtrlLqsV::Initialize(const Ref<const VectorXd> Xi, Ref<VectorXd> X) const
	{
		VelXVErrInt_X = -((gainVelXInit + gainVelX) * VelXV_Xi) / gainVelXTrackErrInt;
		return 0;
	}

	Int32_T CtrlLqsV::Derivatives(
		Float64_T t,
		const Ref<const VectorXd> X,
		const Ref<const VectorXd> U,
		Ref<VectorXd> dX, 
		Ref<VectorXd> Y) const
	{
        // Check sizes
		CheckSizes(X, U, dX, Y);

		// State feedback equation
		Float64_T torqueDrvBrk = -gainVelX * VelXV_U - gainVelXTrackErrInt * VelXVErrInt_X;

		// Integration with anti-windup (clamping)
		Float64_T torqueDrvMax = interpolate(satVel, satTorqueDrv, abs(VelXV_U));
		Float64_T torqueBrkMax = interpolate(satVel, satTorqueBrk, abs(VelXV_U));

		VelXVErr_Y = VelXVRef_U - VelXV_U;
		if (torqueDrvBrk > torqueDrvMax)
		{
			TorqueDrv_Y = torqueDrvMax;
			TorqueBrk_Y = 0.0;
			DVelXVErrInt_dX = 0.0;
		}
		else if (torqueDrvBrk < -torqueBrkMax)
		{
			TorqueDrv_Y = 0.0;
			TorqueBrk_Y = torqueBrkMax;
			DVelXVErrInt_dX = 0.0;
		}
		else
		{
			TorqueDrv_Y = max(0.0, torqueDrvBrk);
			TorqueBrk_Y = max(0.0, -torqueDrvBrk);
			DVelXVErrInt_dX = VelXVErr_Y;
		}

		return 0;
	}
}
