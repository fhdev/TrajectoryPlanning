
fig = nicefigure('tracking', 8.8, 8.8);

ax = niceaxis(fig, 8, true);

subplot(2, 1, 1, ax);
hold on;
plot(ax, input.XRef, input.YRef, 'Color', [241,204,20]/255, 'LineStyle', '-', 'LineWidth', 5);
plot(ax, outsty.Chassis.PosXG, outsty.Chassis.PosYG, 'Color', [230, 76, 60]/ 255, 'LineStyle', ':', 'LineWidth', 3);
plot(ax, outtrk.Chassis.PosXG, outtrk.Chassis.PosYG, 'Color', [10, 100, 200]/ 255, 'LineStyle', '-', 'LineWidth', 1.5);
l = nicelegend({'reference', 'stanley', 'lqr'}, ax, 8);
set(l, 'Location', 'EastOutside');
xlabel(ax, '$x[\mathrm{m}]$');
ylabel(ax, '$y[\mathrm{m}]$');

ax = niceaxis(fig, 8, true);

subplot(2, 1, 2, ax);
hold on;
plot(ax, outsty.Ctrl.Time, outsty.Ctrl.CrossTrackErr, 'Color', [230, 76, 60]/ 255, 'LineStyle', '-');
plot(ax, outsty.Ctrl.Time, rad2deg(outsty.Ctrl.HeadingErr), 'Color', [230, 76, 60]/ 255, 'LineStyle', '-.');

plot(ax, outtrk.Ctrl.Time, outtrk.Ctrl.CrossTrackErr, 'Color', [10, 100, 200]/ 255, 'LineStyle', '-');
plot(ax, outtrk.Ctrl.Time, rad2deg(outtrk.Ctrl.HeadingErr), 'Color', [10, 100, 200]/ 255, 'LineStyle', '-.');

l = nicelegend({'stanley $e_{lat}$', 'stanley $e_{\psi}$', 'lqr $e_{lat}$', 'lqr $e_{\psi}$'}, ax, 8);
set(l, 'NumColumns', 2);

xlabel(ax, '$t[\mathrm{s}]$');
ylabel(ax, '$e_{lat}[\mathrm{m}]$, $e_{\psi}[\mathrm{^\circ}]$');

print('tracking.pdf', '-dpdf', '-painters');