function initCon = InitCon_Smart_Straight()

initCon.PosXG   = 200;
initCon.VelXV   = 0; % [m/s]
initCon.PosYG   = 100;
initCon.VelYV   = 0;
initCon.YawZ    = deg2rad(270);
initCon.YawVelZ = 0;

end