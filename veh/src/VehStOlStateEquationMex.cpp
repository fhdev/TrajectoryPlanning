#include <math.h>
#include "mex.h"
#include <Eigen/Dense>
#include "TypesTp.hpp"
#include "MathTp.hpp"
#include "VehStOl.hpp"
#include "ChassisSt.hpp"
#include "WheelMf.hpp"

// number of parameters
#define NP 56
// number of inputs
#define NU 5
// number of provided initial states
#define NXi 6
// number of states
#define NX 15
// number of outputs
#define NY 26

#define P_  (prhs[0])
#define t_  (prhs[1])
#define X0_ (prhs[2])
#define U_  (prhs[3])

#define X_ (plhs[0])
#define Y_ (plhs[1])

using namespace Eigen;
using namespace TrajectoryPlanning;

// [X, Y] = VehStMex(P, t, X0, U)
void mexFunction(int nlhs, mxArray *plhs[],	int nrhs, const mxArray *prhs[])
{
	// Check number of input arguments
	if (nrhs != 4)
    {
		mexErrMsgIdAndTxt("VehStMex:nrhs", 
            "Argument list must be [dX, Y] = VehStStateEquationMex(P, t, X, U). Number of input parameters is not 4.");
	}
    // Check number of output arguments
	if (nlhs != 2) 
    {
        mexErrMsgIdAndTxt("VehStMex:nlhs", 
            "Argument list must be [dX, Y] = VehStStateEquationMex(P, t, X, U). Number of output parameters is not 2.");
	}
	// Check P (parameter array)
    Int64_T rowsP = mxGetM(P_);
    Int64_T colsP = mxGetN(P_);
	if (!mxIsDouble(P_) ||	mxIsComplex(P_) || (rowsP != NP) || (colsP != 1))
    {
		mexErrMsgIdAndTxt("VehStMex:invalidArgument", 
            "P must be a column vector of doubles with 54 elements.");
	}
    // Check t (time sites)
    Int64_T rowst = mxGetM(t_);
    Int64_T colst = mxGetN(t_);
    if (!mxIsDouble(t_) || mxIsComplex(t_) || (rowst != 1) || (colst != 1))
    {
        mexErrMsgIdAndTxt("VehStMex:invalidArgument",
            "t must be a double scalar.");
    }
    // Check X0 (time sites)
    Int64_T rowsX0 = mxGetM(X0_);
    Int64_T colsX0 = mxGetN(X0_);
    if (!mxIsDouble(X0_) || mxIsComplex(X0_) || (rowsX0 != NX) || (colsX0 != 1))
    {
        mexErrMsgIdAndTxt("VehStMex:invalidArgument",
            "Xi must be a column vector of doubles with 14 elements.");
    }
    // Check U (input data)
    Int64_T rowsU = mxGetM(U_);
    Int64_T colsU = mxGetN(U_);
    if (!mxIsDouble(U_) || mxIsComplex(U_) || (rowsU != NU) || (colsU != 1))
    {
        mexErrMsgIdAndTxt("VehStMex:invalidArgument",
            "U must be a column vector of doubles with 5 elements.");
    }

    // Get input data pointers
#if MX_HAS_INTERLEAVED_COMPLEX
    Float64_T *Pptr = mxGetDoubles(P_);
    Float64_T *tptr = mxGetDoubles(t_);
    Float64_T *Xptr = mxGetDoubles(X0_);
    Float64_T *Uptr = mxGetDoubles(U_);
#else
    Float64_T *Pptr = mxGetPr(P_);
    Float64_T *tptr = mxGetPr(t_);
    Float64_T *Xptr = mxGetPr(X0_);
    Float64_T *Uptr = mxGetPr(U_);
#endif

    // Allocate output
    X_ = mxCreateDoubleMatrix(NX, colst, mxREAL);
    Y_ = mxCreateDoubleMatrix(NY, colst, mxREAL);

    // Get output data pointers
#if MX_HAS_INTERLEAVED_COMPLEX
    Float64_T *dXprt = mxGetDoubles(X_);
    Float64_T *Yprt = mxGetDoubles(Y_);
#else
    Float64_T *dXprt = mxGetPr(X_);
    Float64_T *Yprt = mxGetPr(Y_);
#endif

    // Create mappings for Eigen usage
    Map<VectorXd> P(Pptr, NP);
	Map<VectorXd> X(Xptr, NX);
    Map<VectorXd> U(Uptr, NU);
    Map<VectorXd> dX(dXprt, NX);
    Map<VectorXd> Y(Yprt, NY);

	Float64_T t = *tptr;

    // Create vehicle
	VehStOl v = VehStOl(P);

    // Compute motion
	v.Derivatives(t, X, U, dX, Y);

}
