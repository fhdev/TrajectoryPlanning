% Run vehTestNnFitFeedback first! (with MotionDeltasV00, n256l4v1_relu_00050, iSection=6)

H = 2.5 * 7; % cm
W = 7; % cm
F = 8; % pt 
PLOT_PER_FIG = 5;

pldata = {'$x^G_v$ (pos. north) ',          '[m]',  1,  1; ...
          '$y^G_v$ (pos. west) ',           '[m]',  2,  2; ...
          '$\psi_v$ (yaw angle) ',          '[rad]',  3,  3; ...
          '$\dot{x}^V_v$ (lon. vel.) ',     '[m/s]',  4,  4; ...
          '$\dot{y}^V_v$ (lat. vel.) ',     '[m/s]',  5,  5; ...
          '$\dot{\psi}_v$ (yaw rate) ',     '[rad/s]',  6,  6; ...
          '$\ddot{x}^V_{v,I}$ (lon. acc.) ','[m/s\textsuperscript{2}]', 14,  7; ...
          '$\ddot{y}^V_{v,I}$ (lat. acc.) ','[m/s\textsuperscript{2}]', 15,  8; ...
          '$\ddot{\psi}_v$ (yaw acc.) ',    '[rad/s\textsuperscript{2}]', 16,  9; ...
          '$\delta_f$ (steering angle) ',   '[rad]', 13, 10};
time = startTime : SAMPLE_RATE_NN_S : endTime;

fig(1) = nicefigure('nn', W, H);
fig(2) = nicefigure('nn', W, H);

for iOut = 1 : size(pldata, 1)
    iFig = floor((pldata{iOut, 4} - 1) / PLOT_PER_FIG) + 1;
    iPlot = floor(mod(pldata{iOut, 4} - 1, PLOT_PER_FIG)) + 1;
    ax(iFig, iPlot) = niceaxis(fig(iFig), F, true);
    subplot(PLOT_PER_FIG, 1, iPlot, ax(iFig, iPlot));
    yyaxis(ax(iFig, iPlot), 'left');
    plot(ax(iFig, iPlot), time, vmOutMdl(:, pldata{iOut, 3}),  'Color', nicecolors.BlueDeepKoamaru);
    plot(ax(iFig, iPlot), time, vmOutNn(:, pldata{iOut, 3}), 'Color', nicecolors.PurpleAmethyst, 'LineWidth', 2,  'LineStyle', ':');
    ax(iFig, iPlot).YAxis(1).Color = nicecolors.BlueDeepKoamaru;
    ax(iFig, iPlot).YAxis(1).Label.String = [pldata{iOut, 1:2}];
    yyaxis(ax(iFig, iPlot), 'right');
    plot(ax(iFig, iPlot), time, vmOutMdl(:, pldata{iOut, 3}) - vmOutNn(:, pldata{iOut, 3}), 'Color', nicecolors.GreenSea);
    ax(iFig, iPlot).YAxis(2).Color = nicecolors.GreenSea;
    ax(iFig, iPlot).YAxis(2).Label.String = ['pred. error ', pldata{iOut, 2}];

    xlabel(ax(iFig, iPlot), '$t$ (time) [s]');
end

for iFig = 1 : 2
ax(iFig, 1).Position(1) = 0.17;
ax(iFig, 1).Position(2) = 0.81;
ax(iFig, 1).Position(3) = 0.65;

ax(iFig, 2).Position(1) = 0.17;
ax(iFig, 2).Position(2) = 0.62;
ax(iFig, 2).Position(3) = 0.65;

ax(iFig, 3).Position(1) = 0.17;
ax(iFig, 3).Position(2) = 0.43;
ax(iFig, 3).Position(3) = 0.65;

ax(iFig, 4).Position(1) = 0.17;
ax(iFig, 4).Position(2) = 0.24;
ax(iFig, 4).Position(3) = 0.65;

ax(iFig, 5).Position(1) = 0.17;
ax(iFig, 5).Position(2) = 0.05;
ax(iFig, 5).Position(3) = 0.65;

leg = nicelegend({'actual', 'predicted'}, ax(iFig, end), 8);
leg.Orientation = 'horizontal';
end

fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

%% Save plot
name = [NAME, '_', TAG, '_FitFeedbackNice_1'];
savefig(fig(1), fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig(1), fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig(1), fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

name = [NAME, '_', TAG, '_FitFeedbackNice_2'];
savefig(fig(2), fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig(2), fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig(2), fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');
