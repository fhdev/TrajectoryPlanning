import numpy
# Data
I = numpy.arange(1, 10, dtype="int64")
D = numpy.arange(1, 10, dtype="float64") * numpy.pi

# Write
f = open("test.bin", "wb")
I.astype("<q").tofile(f)
D.astype("<d").tofile(f)
f.close()

# Read (< for little endian, q for int64, d for double)
f = open("test.bin", "r")
I2 = numpy.fromfile(f, "<q", 9)
D2 = numpy.fromfile(f, "<d", 9)
f.close()

# Display
print(I2)
print(D2)