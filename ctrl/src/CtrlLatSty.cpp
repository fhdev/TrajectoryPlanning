#include <cstdlib>
#include <algorithm>
#include "CtrlLatSty.hpp"
#include "MathTp.hpp"

using namespace Eigen;
using namespace std;

// Parameters ======================================================================================

// number of parameters
#define NP 5

// Inputs ==========================================================================================
// longitudinal position in ground - fixed coordinate system [m]
#define PosXG_U (U(0))
// lateral position in ground - fixed coordinate system [m]
#define PosYG_U (U(1))
// yaw (heading) angle in ground - fixed coordinate system [rad]
#define YawZ_U  (U(2))
// longitudinal velocity in vehicle - fixed coordintate system [m/s]
#define VelXV_U (U(3))    
// number of inputs
#define NU 4

// States and derivatives ==========================================================================
// number of states
#define NX 0

// Outputs =========================================================================================
// steering angle [rad]
#define SteerAng_Y      (Y(0))
#define ErrCrossTrack_Y (Y(1))
#define ErrHeading_Y    (Y(2))
// number of outputs
#define NY 3

namespace TrajectoryPlanning
{

	CtrlLatSty::CtrlLatSty(Float64_T velLin,
		const Ref<const VectorXd> satVel,
		const Ref<const VectorXd> satSteerAng,
		Float64_T gainSty,
		Float64_T distanceFrontCOG,
		Float64_T timeConstant,
		const Ref<const VectorXd> posXRef,
		const Ref<const VectorXd> posYRef,
		const Ref<const VectorXd> yawZRef) :

		velLin(velLin),
		satVel(satVel),
		satSteerAng(satSteerAng),
		gainSty(gainSty),
		distanceFrontCOG(distanceFrontCOG),
		timeConstant(timeConstant),
		posXRef(posXRef),
		posYRef(posYRef),
		yawZRef(yawZRef),
		iClosestPrev(-1)
	{
	}

	tuple<Float64_T, Float64_T>  CtrlLatSty::getReferenceValues(
		const Ref<const VectorXd> xGRefPts,
		const Ref<const VectorXd> yGRefPts,
		const Ref<const VectorXd> yawZRefPts,
		Float64_T xGAct,
		Float64_T yGAct,
		Float64_T yawZAct) const
	{
        // Reference coordinate pointers (for faster access)
        const Float64_T *xGRefPtr = xGRefPts.data();
        const Float64_T *yGRefPtr = yGRefPts.data();

		// Sine and cosine of yaw angle
		Float64_T sinYawZ = sin(yawZAct);
		Float64_T cosYawZ = cos(yawZAct);

		// Find the point in the trajectory that is closest to the vehicle's front axis.

        // Find optimal length for subsampling: number of checks is N(s) = n / x + 2x
        // N'(x) = -n/x^2 + 2 = 0   -> x = sqrt(n/2)
		Int64_T nRef = xGRefPts.size();
		Int64_T nSub = (Int64_T)round((Float64_T)sqrt(nRef / 2));

		// Variables for minimum search
		Float64_T xGRel, xGRelNext, yGRel, yGRelNext, d2Act, d2Min;

		// Step 1: find closest point in subsampled trajectory if no previously found is present
		Int64_T iClosestStep1;
		if ((iClosestPrev < 0) || (iClosestPrev > nRef - 1))
		{	
			iClosestStep1 = 0;
			d2Min = INFINITY;
			for (Int64_T iRef = 0; iRef < nRef; iRef += nSub)
			{
				xGRel = xGRefPtr[iRef] - xGAct;
				yGRel = yGRefPtr[iRef] - yGAct;
				d2Act = SQR(xGRel) + SQR(yGRel);
				if (d2Act < d2Min)
				{
					iClosestStep1 = iRef;
					d2Min = d2Act;
				}
			}
		}
		else
		{
			iClosestStep1 = iClosestPrev;
		}
		Int64_T iLeftStep1 = max(0LL, iClosestStep1 - nSub);
		Int64_T iRightStep1 = min(iClosestStep1 + nSub, nRef - 1);
		// End Step 1 ==============================================================================

		// Step 2: find closest point in the closest region of trajectory ==========================
		Int64_T iClosestStep2 = iClosestStep1;
		d2Min = INFINITY;
		for (Int64_T iRef = iLeftStep1; iRef <= iRightStep1; iRef++)
		{
			xGRel = xGRefPtr[iRef] - xGAct;
			yGRel = yGRefPtr[iRef] - yGAct;
			d2Act = SQR(xGRel) + SQR(yGRel);
			if (d2Act < d2Min)
			{
				iClosestStep2 = iRef;
				d2Min = d2Act;
			}
		}
		iClosestPrev = iClosestStep2;

		// Find next closest point
		Int64_T iNextClosestStep2;
		if (iClosestStep2 == 0)
		{
			iNextClosestStep2 = iClosestStep2 + 1;
		}
		else if(iClosestStep2 == nRef - 1)
		{
			iNextClosestStep2 = iClosestStep2 - 1;
		}
		else
		{
			xGRel = xGRefPtr[iClosestStep2 - 1] - xGAct;
			yGRel = yGRefPtr[iClosestStep2 - 1] - yGAct;
			xGRelNext = xGRefPtr[iClosestStep2 + 1] - xGAct;
			yGRelNext = yGRefPtr[iClosestStep2 + 1] - yGAct;
			if ((SQR(xGRel) + SQR(yGRel)) < (SQR(xGRelNext) + SQR(yGRelNext)))
			{
				iNextClosestStep2 = iClosestStep2 - 1;
			}
			else
			{
				iNextClosestStep2 = iClosestStep2 + 1;
			}
		}
		// End Step 2 ==============================================================================
		// Calculate tracking errors
		// Lets approximate linearly between points: y(x) = a.x + b
		// Squared distance function: x ^ 2 + y ^ 2 = (a ^ 2 + 1).x ^ 2 + 2ab.x + b ^ 2
		// Distance is minimal where distance function's derivative is zero: x = (-2ab) / (2(a^2 +1))
		Float64_T a, b;

		// Lateral error
		xGRel = xGRefPtr[iClosestStep2] - xGAct;
		yGRel = yGRefPtr[iClosestStep2] - yGAct;
		xGRelNext = xGRefPtr[iNextClosestStep2] - xGAct;
		yGRelNext = yGRefPtr[iNextClosestStep2] - yGAct;
		a = (yGRel == yGRelNext) ? (0.0) : ((yGRel - yGRelNext) / (xGRel - xGRelNext));
		b = yGRel - a * xGRel;
		Float64_T xGRelOpt = -2.0 * a * b / (2.0 * (SQR(a) + 1));
		Float64_T yGRelOpt = a * xGRelOpt + b;
		Float64_T yVRelOpt = -sinYawZ * xGRelOpt + cosYawZ * yGRelOpt;
		Float64_T eLat = sign(yVRelOpt) * sqrt(SQR(xGRelOpt) + SQR(yGRelOpt));

		// Yaw (heading) error
		a = (yawZRefPts[iClosestStep2] == yawZRefPts[iNextClosestStep2]) ? (0.0) : 
			((yawZRefPts[iClosestStep2] - yawZRefPts[iNextClosestStep2]) / (xGRel - xGRelNext));
		b = yawZRefPts[iClosestStep2] - a * xGRel;
		Float64_T yawZRefOpt = a * xGRelOpt + b;
		Float64_T eYawZ = yawZRefOpt - yawZAct;

		return make_tuple(eLat, eYawZ);
	}

	Int64_T CtrlLatSty::nP() const { return NP; }
	Int64_T CtrlLatSty::nX() const { return NX; }
	Int64_T CtrlLatSty::nU() const { return NU; }
	Int64_T CtrlLatSty::nY() const { return NY; }

	Int32_T CtrlLatSty::Derivatives(Float64_T t,
		const Ref<const VectorXd> X,
		const Ref<const VectorXd> U,
		Ref<VectorXd> dX,
		Ref<VectorXd> Y) const
	{
        // Check sizes
		CheckSizes(X, U, dX, Y);


		Float64_T D_POSX = PosXG_U;
		Float64_T D_POSY = PosYG_U;
		Float64_T D_YAW = YawZ_U;


		// Find reference values in trajectory
		// Reference point for traditional Stanley controller is the front axis centerpoint. It
    	// is however beneficial to define a lookahead considering the settling time of steering
    	// actuator.
		Float64_T lookAhead = lookAheadFactor * VelXV_U * timeConstant;
		Float64_T xGActFront = PosXG_U + cos(YawZ_U) * lookAhead;
		Float64_T yGActFront = PosYG_U + sin(YawZ_U) * lookAhead;

		Float64_T eLat;
		Float64_T eYawZ;
		tie(eLat, eYawZ) = getReferenceValues(posXRef, posYRef, yawZRef, xGActFront, yGActFront, YawZ_U);
        ErrCrossTrack_Y = eLat;
		ErrHeading_Y = eYawZ;

		// Stanley control law
		Float64_T velXVAbs = abs(VelXV_U);
		SteerAng_Y = ErrHeading_Y;
		if (velXVAbs > 0.0)
		{
		  	SteerAng_Y += atan(gainSty * ErrCrossTrack_Y / velXVAbs);
		}

		// Saturation
		Float64_T maxSteerAng = interpolate(satVel, satSteerAng, velXVAbs);
		if (SteerAng_Y > maxSteerAng)
		{
			SteerAng_Y = maxSteerAng;
		}
		else if (SteerAng_Y < -maxSteerAng)
		{
			SteerAng_Y = -maxSteerAng;
		}

		// Set output tracking errors calculated in cog position
		tie(eLat, eYawZ) = getReferenceValues(posXRef, posYRef, yawZRef, PosXG_U, PosYG_U, YawZ_U);
        ErrCrossTrack_Y = eLat;
		ErrHeading_Y = eYawZ;

		return 0;
	}

	Int32_T CtrlLatSty::Initialize(
		const Eigen::Ref<const Eigen::VectorXd> Xi,
		Eigen::Ref<Eigen::VectorXd> X) const
	{
		// Reset closest point index
		iClosestPrev = -1;
		// Stanley controller has no state
		X(0) = 0;

		return 0;
	}
}
