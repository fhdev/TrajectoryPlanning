%% VehQrLonIn **************************************************************************************
% [Summary]
%   This class represents inputs for longitudinal quarter vehicle model.
%
% [Used in]
%   VehQrLon
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehQrLonIn < VehOlLonIn & VehEnvIn1W
    %% Instance methods ============================================================================
    methods
        %% Constructor -----------------------------------------------------------------------------
        function self = VehQrLonIn(varargin)
           if nargin == 1
               vehOlLonInArgs = varargin(1);
               vehEnvIn1WArgs = varargin(1);
           elseif nargin == 4
               vehOlLonInArgs = varargin(1:3);
               vehEnvIn1WArgs = varargin([1, 4]);
           else
               error('ERROR: Inappropriate number of arguments!');
           end
           self@VehOlLonIn(vehOlLonInArgs{:});
           self@VehEnvIn1W(vehEnvIn1WArgs{:});
        end

    end
end