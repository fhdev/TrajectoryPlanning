%% ChassisQrLonOut *********************************************************************************
% [Summary]
%   This class represents outputs for longitudinal quarter vehicle chassis model.
%
% [Used in]
%   VehBaseOut
%   VehQrLonOut
%   VehQrLon
%
% [Subclasses]
%   ChassisQrOut
% **************************************************************************************************
classdef PlannerGeomOut < ComTimeInOut
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        %xf, yf, rf, vi, vf, dri, drf
        % arc length coordintate [m]
        ArcLenS
        % longitudinal position in ground-fixed cs. [m]
        PosXG
        % lateral position in ground-fixed cs. [m]
        PosYG
        % yaw angle in ground-fixed cs. [rad]
        YawZ
        % longitudinal velocity in vehicle-fixed cs. [m]
        VelXV
        % yaw rate in ground-fixed cs. [rad/s]
        YawVelZ
        % lateral intertial acceleration in vehicle-fixed cs. [m/s^2]
        AccInrtYV
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.ArcLenS(self, value)
            if ~isequal(self.ArcLenS, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: ArcLenS must be a numeric vector!')
                end
                self.ArcLenS = value(:);
            end
        end
        
        function set.PosXG(self, value)
            if ~isequal(self.PosXG, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PosXG must be a numeric vector!')
                end
                self.PosXG = value(:);
            end
        end
        
        function set.PosYG(self, value)
            if ~isequal(self.PosYG, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PosYG must be a numeric vector!')
                end
                self.PosYG = value(:);
            end
        end
        
        function set.YawZ(self, value)
            if ~isequal(self.YawZ, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawZ must be a numeric vector!')
                end
                self.YawZ = value(:);
            end
        end
        
        function set.VelXV(self, value)
            if ~isequal(self.VelXV, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: VelXV must be a numeric vector!')
                end
                self.VelXV = value(:);
            end
        end
        
        function set.YawVelZ(self, value)
            if ~isequal(self.YawVelZ, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawVelZ must be a numeric vector!')
                end
                self.YawVelZ = value(:);
            end
        end
        
        function set.AccInrtYV(self, value)
            if ~isequal(self.AccInrtYV, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: AccInrtYV must be a numeric vector!')
                end
                self.AccInrtYV = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = PlannerGeomOut(varargin)
            if nargin == 1
                comTimeInArgs = varargin(1);
            elseif nargin == 8
                comTimeInArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
           self@ComTimeInOut(comTimeInArgs{:});
            if nargin == 1
                self.ArcLenS   = varargin{1}.ArcLenS;
                self.PosXG     = varargin{1}.PosXG;
                self.PosYG     = varargin{1}.PosYG;
                self.YawZ      = varargin{1}.YawZ;
                self.VelXV     = varargin{1}.VelXV;
                self.YawVelZ   = varargin{1}.YawVelZ;
                self.AccInrtYV = varargin{1}.AccInrtYV;
            elseif nargin == 8
                self.ArcLenS   = varargin{2};
                self.PosXG     = varargin{3};
                self.PosYG     = varargin{4};
                self.YawZ      = varargin{5};
                self.VelXV     = varargin{6};
                self.YawVelZ   = varargin{7};
                self.AccInrtYV = varargin{8};
            end
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the longitudinal quarter vehicle chassis
        %   model.
        %
        % [Input]
        %       name - name for the plots
        %
        % [Output]
        %       fig  - figure handle of plot
        % ------------------------------------------------------------------------------------------        
        function figPath = DisplayPlots(self, name)
            %% Constants
            FONTSIZE = 8;
            WIDTH = 10;
            HEIGHT = 10;
            WIDTH_PATH = 15;
            HEIGHT_PATH = 10;
            
            %% Default arguments
            if nargin < 2
                name = class(self);
            end
            
            %% Create plots
            % --------------------------------------------------------------------------------------
            figPath = nicefigure([name, '/Path'], WIDTH_PATH, HEIGHT_PATH);
            ax = niceaxis(figPath, FONTSIZE);
            plot(ax, self.PosXG, self.PosYG, 'Color', Colors.Pomegrante);
            axis(ax, 'equal');
            xlabel(ax, '$x^G \mathrm{[m]}$');
            ylabel(ax, '$y^G \mathrm{[m]}$');
            title(ax, 'path');
        end
    end
end