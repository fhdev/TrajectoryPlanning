DATA_FOLDER = fullfile('E:', 'VehModel_NN_2020');
TRAJECTORY_DATA = 'trajectory_011';

% Subsampling
subs = @(x) [x(1:1000:end); x(end)];

% Drive data
if ~exist('output', 'var')
    load(fullfile(DATA_FOLDER, sprintf('%s.mat', TRAJECTORY_DATA)), 'trajectory');
end

% Trajectory plot
H = 6.5; % cm
W = 7; % cm
F = 8; % pt

name = ['RandomLearningTrajectory', TRAJECTORY_DATA(end-2:end)];
fig1 = nicefigure(name, W, H);
ax11 = niceaxis(fig1, F, true);
plot(ax11, subs(trajectory.PosXG), subs(trajectory.PosYG), 'Color', nicecolors.BlueDeepKoamaru);
plot(ax11, trajectory.PosXG(1), trajectory.PosYG(1), 'o', 'Color', 'g', 'MarkerFaceColor', 'g', ...
    'MarkerSize', 3);
plot(ax11, trajectory.PosXG(end), trajectory.PosYG(end), 'o', 'Color', 'r', 'MarkerFaceColor', 'r', ...
    'MarkerSize', 3);
switch TRAJECTORY_DATA
    case 'trajectory_009'
        annotation(fig1, 'textbox', [0.6, 0.5, 0.28, 0.08], 'String', sprintf('L=%gm', round(trajectory.Length(end))), ...
            'FitBoxToText', 'off', 'FontSize', F, 'Interpreter', 'Latex');
    case 'trajectory_011'
        annotation(fig1, 'textbox', [0.57, 0.75, 0.28, 0.08], 'String', sprintf('L=%gm', round(trajectory.Length(end))), ...
            'FitBoxToText', 'off', 'FontSize', F, 'Interpreter', 'Latex');
    case 'trajectory_014'
        annotation(fig1, 'textbox', [0.57, 0.2, 0.28, 0.08], 'String', sprintf('L=%gm', round(trajectory.Length(end))), ...
            'FitBoxToText', 'off', 'FontSize', F, 'Interpreter', 'Latex');
end
xlabel(ax11, '$x^G$ (pos. north) [m]');
ylabel(ax11, '$y^G$ (pos. west) [m]');
axis(ax11, 'equal');
view(ax11, [-90, 90]);

savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');