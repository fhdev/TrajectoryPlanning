%% VehOlLonIn **************************************************************************************
% [Summary]
%   This class represents open loop longitudinal dynamics inputs for vehicle models.
%
% [Used in]
%   none
%
% [Subclasses]
%   VehQrLonIn
%   VehOlIn
% **************************************************************************************************
classdef VehOlLonIn < ComTimeInOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % driving torque [Nm]
        TorqueDrv
        % braking torque [Nm]
        TorqueBrk

    end
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.TorqueDrv(self, value)
            if ~isequal(self.TorqueDrv, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: TorqueDrv must be a numeric vector!');
                end
                self.TorqueDrv = value(:);
            end
        end

        function set.TorqueBrk(self, value)
            if ~isequal(self.TorqueBrk, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: TorqueBrk must be a numeric vector!');
                end
                self.TorqueBrk = value(:);
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehOlLonIn(varargin)
            if nargin == 1
                comTimeInArgs = varargin(1);
            elseif nargin == 3
                comTimeInArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
           self@ComTimeInOut(comTimeInArgs{:});
            if nargin == 1
                self.TorqueDrv = varargin{1}.TorqueDrv;
                self.TorqueBrk = varargin{1}.TorqueBrk;
            elseif nargin == 3
                self.TorqueDrv = varargin{2};
                self.TorqueBrk = varargin{3};
            end
        end

    end
end