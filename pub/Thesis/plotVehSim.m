% To get the data, run closed loop model.
%
% vehSim = obj2struct(VehTest.TestManeuver('VehStCl_Default', 'Maneuver_SCurve_VaryingSpeed', false, false));

% Subsampling factor
subs = @(x) x(1:50:end);

% Signals
pldata = {'$x^G_v$ (pos. north) ',          '[m]',                        1,  1; ...
          '$y^G_v$ (pos. west) ',           '[m]',                        1,  1; ...
          '$\psi_v$ (yaw angle) ',          '[rad]',                      1,  3; ...
          '$\dot{x}^V_v$ (lon. vel.) ',     '[m/s]',                      1,  4; ...
          '$\dot{y}^V_v$ (lat. vel.) ',     '[m/s]',                      1,  5; ...
          '$\dot{\psi}_v$ (yaw rate) ',     '[rad/s]',                    2,  6; ...
          '$\ddot{x}^V_{v,I}$ (lon. acc.) ','[m/s\textsuperscript{2}]',   2,  7; ...
          '$\ddot{y}^V_{v,I}$ (lat. acc.) ','[m/s\textsuperscript{2}]',   2,  8; ...
          '$\ddot{\psi}_v$ (yaw acc.) ',    '[rad/s\textsuperscript{2}]', 2,  9; ...
          '$\delta_f$ (steering angle) ',   '[rad]',                      2, 10};

% Create plot 1
W = 7;
H = 15;
F = 8;
fig1 = nicefigure('VehSim1', W, H);

ax11 = niceaxis(fig1, F);
subplot(5, 1, [1, 2], ax11);
hold(ax11, 'on');
plot(ax11, subs(vehSim.out.Chassis.PosXG), subs(vehSim.out.Chassis.PosYG), 'Color', nicecolors.BlueBelizeHole, ...
    'LineStyle', '-', 'LineWidth', 1);
plot(ax11, nan, nan, 'LineStyle', '-', 'Color', 'Black');
plot(ax11, nan, nan, 'LineStyle', '-.', 'Color', 'Black');
xlabel(ax11, '$x^G_v$ (pos. north) [m]');
ylabel(ax11, '$y^G_v$ (pos. west) [m]');
axis(ax11, 'equal');
xlim(ax11, [-210; 170]);
ylim(ax11, [-10; 335]); 
ax11.Position = [0.16, 0.66, 0.77, 0.33];

ax13 = niceaxis(fig1, F);
subplot(5, 1, 3, ax13);
hold(ax13, 'on');
plot(ax13, subs(vehSim.out.Chassis.Time), subs(rad2deg(vehSim.out.Chassis.YawZ)), ...
    'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
xlabel(ax13, '$t_v$ (time) [s]');
ylabel(ax13, '$\psi_v$ (yaw angle) [$^\circ$]');
xlim(ax13, [0, 55]);
ylim(ax13, [-10, 200]);
ax13.Position = [0.16, 0.45, 0.77, 0.14];

ax14 = niceaxis(fig1, F);
subplot(5, 1, 4, ax14);
hold(ax14, 'on');
plot(ax14, subs(vehSim.out.Chassis.Time), subs(vehSim.out.Chassis.VelXV * 3.6), ...
    'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
xlabel(ax14, '$t_v$ (time) [s]');
ylabel(ax14, '$\dot{x}^V_v$ (lon. vel.) [km/h]');
xlim(ax14, [0, 55]);
ylim(ax14, [30, 120]);
ax14.Position = [0.16, 0.25, 0.77, 0.14];
ax14.YTick = 40:40:120;

ax15 = niceaxis(fig1, F);
subplot(5, 1, 5, ax15);
hold(ax15, 'on');
plot(ax15, subs(vehSim.out.Chassis.Time), subs(vehSim.out.Chassis.VelYV * 3.6), ...
    'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
xlabel(ax15, '$t_v$ (time) [s]');
ylabel(ax15, '$\dot{y}^V_v$ (lat. vel.) [km/h]');
xlim(ax15, [0, 55]);
ylim(ax15, [-1, 1]);
ax15.Position = [0.16, 0.05, 0.77, 0.14];

% Create plot 2
W = 7;
H = 15;
F = 8;
fig2 = nicefigure('VehSim2', W, H);

ax21 = niceaxis(fig2, F);
subplot(5, 1, 1, ax21);
hold(ax21, 'on');
plot(ax21, subs(vehSim.out.Chassis.Time), subs(rad2deg(vehSim.out.Chassis.YawVelZ)), ...
    'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
xlabel(ax21, '$t_v$ (time) [s]');
ylabel(ax21, '$\dot{\psi}_v$ (yaw rate) [$^\circ$/s]');
xlim(ax21, [0, 55]);
%ylim(ax21, [-10, 200]);
ax21.Position = [0.17, 0.85, 0.77, 0.14];

ax22 = niceaxis(fig2, F);
subplot(5, 1, 2, ax22);
hold(ax22, 'on');
plot(ax22, subs(vehSim.out.Chassis.Time), subs(vehSim.out.Chassis.AccInrtXV), ...
    'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
xlabel(ax22, '$t_v$ (time) [s]');
ylabel(ax22, '$\ddot{x}^V_{v,I}$ (lon. acc.) [m/s\textsuperscript{2}]');
xlim(ax22, [0, 55]);
%ylim(ax22, [-10, 200]);
ax22.Position = [0.16, 0.65, 0.77, 0.14];

ax23 = niceaxis(fig2, F);
subplot(5, 1, 3, ax23);
hold(ax23, 'on');
plot(ax23, subs(vehSim.out.Chassis.Time), subs(vehSim.out.Chassis.AccInrtYV), ...
    'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
xlabel(ax23, '$t_v$ (time) [s]');
ylabel(ax23, '$\ddot{y}^V_{v,I}$ (lat. acc.) [m/s\textsuperscript{2}]');
xlim(ax23, [0, 55]);
%ylim(ax23, [-10, 200]);
ax23.Position = [0.16, 0.45, 0.77, 0.14];

ax24 = niceaxis(fig2, F);
subplot(5, 1, 4, ax24);
hold(ax24, 'on');
plot(ax24, subs(vehSim.out.Chassis.Time), subs(rad2deg(vehSim.out.Chassis.YawAccZ)), ...
    'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
xlabel(ax24, '$t_v$ (time) [s]');
ylabel(ax24, '$\ddot{\psi}_v$ (yaw acc.) [$^\circ$/s\textsuperscript{2}]');
xlim(ax24, [0, 55]);
%ylim(ax24, [30, 120]);
ax24.Position = [0.16, 0.25, 0.77, 0.14];

ax25 = niceaxis(fig2, F);
subplot(5, 1, 5, ax25);
hold(ax25, 'on');
plot(ax25, subs(vehSim.out.Chassis.Time), subs(rad2deg(vehSim.out.Steering.SteerAng)), ...
    'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
xlabel(ax25, '$t_v$ (time) [s]');
ylabel(ax25, '$\delta_f$ (steering angle) [$^\circ$]');
xlim(ax25, [0, 55]);
%ylim(ax25, [-1, 1]);
ax25.Position = [0.16, 0.05, 0.77, 0.14];

% Save
name = 'VehSim1';
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

name = 'VehSim2';
savefig(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');