%% PlannerOptVarVelYawRate *************************************************************************
% [Summary]
%   This class represents the parameters of reference signal profiles used for motion planning.
%   For the longitudinal velocity reference signal, a trapezoidal profile is used, meanwhile the
%   yaw rate reference profile is a polynomial spline.
%
% [Used in]
%   PlannerConIn
%   PlannerCon
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef PlannerOptVarVelYawRate < handle
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % end velocity of linear speed reference (scalar) (start velocity is in initial condition)
        VelRef
        % knot points of cubic spline yaw rate reference (3 element vector)
        YawRateRef      % optimized
        % total tavel time (scalar)
        TravelTime      % optimized
        % sample time for input generation (scalar)
        SampleTime
        
    end
    
    %% Public instance methods =====================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.VelRef(self, value)
            if ~isequal(self.VelRef, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: VelRef must be a numeric scalar!')
                end
                self.VelRef = value(:);
            end
        end
        
        function set.YawRateRef(self, value)
            if ~isequal(self.YawRateRef, value)
                if ~(isnumeric(value) && isvector(value) && (length(value) == 3))
                    error('ERROR: YawRateRef must be a numeric vector with 3 elements!')
                end
                self.YawRateRef = value(:);
            end
        end

        function set.TravelTime(self, value)
            if ~isequal(self.TravelTime, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: TravelTime must be a numeric scalar!')
                end
                self.TravelTime = value;
            end
        end
        
        function set.SampleTime(self, value)
            if ~isequal(self.SampleTime, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SampleTime must be a numeric scalar!')
                end
                self.SampleTime = value;
            end
        end
        %% Constructor -----------------------------------------------------------------------------
        function self = PlannerOptVarVelYawRate(varargin)
            if nargin == 1
                self.VelRef     = varargin{1}.VelRef;
                self.YawRateRef = varargin{1}.YawRateRef;
                self.TravelTime = varargin{1}.TravelTime;
                self.SampleTime = varargin{1}.SampleTime;
            elseif nargin == 4
                self.VelRef     = varargin{1};
                self.YawRateRef = varargin{2};
                self.TravelTime = varargin{3};
                self.SampleTime = varargin{4};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
 
        end
        
        %% Update ----------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------
        function updated = Update(self, vector)
            updated = false;
            if ~isequal(self.ToVector(), vector)
                self.YawRateRef = vector(1:end-1);
                self.TravelTime = vector(end);
                updated = true;
            end
        end
        
        %% ToVector --------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------
        function vector = ToVector(self)
            vector = [self.YawRateRef; self.TravelTime];
        end
        
        %% ToVehIn ---------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------
        function input = ToVehIn(self, input)
            % True sample time must be <= self.SampleTime, and t(end) must be self.TravelTime
            nt = ceil(self.TravelTime / self.SampleTime) + 1;
            t = linspace(0, self.TravelTime, nt)';
            dr = interpspline3(linspace(0, self.TravelTime, length(self.YawRateRef) + 1), ...
                [input.InitCon.YawVelZ; self.YawRateRef], t);
            v = linspace(input.InitCon.VelXV, self.VelRef, nt)';
            
            % Calculate path and heading points
            ds = v(1:end-1) .* diff(t);
            r = cumsum([input.InitCon.YawZ; dr(1:end-1) .* diff(t)]);
            x = cumsum([input.InitCon.PosXG; ds .* cos(r(1:end-1))]);
            y = cumsum([input.InitCon.PosYG; ds .* sin(r(1:end-1))]);
            
            muf = input.EnvIn.FrictionFront * ones(size(t));
            mur = input.EnvIn.FrictionRear * ones(size(t));            
            input = VehStClIn(t, x, y, r, v, dr, muf, mur); 
        end
    end
end
