% Parameters
%measName = 'MEAS_EVAL_20190528_174021';
measName = 'MEAS_EVAL_20190528_175234';

load(fullfile(pwd, 'data', 'Pub_2020_IFACWC2020', measName));

% Conversion
sinYaw0 = sin(data.Planner.Vehicle.CtrlLat.YawZRef(1));
cosYaw0 = cos(data.Planner.Vehicle.CtrlLat.YawZRef(1));
xG = + data.Planner.vehOut.Chassis.PosXG * cosYaw0 - data.Planner.vehOut.Chassis.PosYG * sinYaw0 + ...
    data.Planner.Vehicle.CtrlLat.PosXRef(1);
yG = + data.Planner.vehOut.Chassis.PosXG * sinYaw0 + data.Planner.vehOut.Chassis.PosYG * cosYaw0 + ...
    data.Planner.Vehicle.CtrlLat.PosYRef(2);

% Start and end indices
bInd = find(~isnan(data.S.steer) & data.S.steer ~= 0, 1, 'first');
if isempty(bInd)
    bInd = 1;
end
eInd = find(~isnan(data.S.steer) & data.S.steer ~= 0, 1, 'last');
if isempty(eInd)
    eInd = length(data.S.steer);
end

% Subsampling factor
subs = @(x) x(1:50:end);

% Create plot
W = 10;
H = 8;
F = 8;
fig = nicefigure(measName, W, H);

ax1 = niceaxis(fig, F);
subplot(2, 1, 1, ax1);
hold(ax1, 'on');
plot(ax1, data.Planner.Vehicle.CtrlLat.PosXRef, data.Planner.Vehicle.CtrlLat.PosYRef, 'Color', nicecolors.YellowSunFlower, ...
    'LineStyle', '-', 'LineWidth', 3);
plot(ax1, xG, yG, 'Color', nicecolors.RedAlizarin, ...
    'LineStyle', '-');
plot(ax1, data.S.x(bInd : eInd), data.S.y(bInd : eInd), 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-');
xlabel(ax1, '$x^G_v$ (pos. north) [m]');
ylabel(ax1, '$y^G_v$ (pos. west) [m]');
axis(ax1, 'equal');
% xlim(ax1, [-10,330]);
% ylim(ax1, [-20, 130]);
l = nicelegend({'Reference', 'Simulation', 'Measurement'}, ax1, F);
l.Orientation = 'vertical';
l.Location = 'northeastoutside';
ax1.Position = [0.11, 0.64, 0.56, 0.32];

ax2 = niceaxis(fig, F);
subplot(2, 1, 2, ax2);
hold(ax2, 'on');
yyaxis(ax2, 'left');
plot(ax2, subs(data.plannerOut.VehOut.Ctrl.Time + data.t(end, bInd)), subs(data.plannerOut.VehOut.Ctrl.CrossTrackErr * 100), ...
    'Color', nicecolors.RedAlizarin, ...
    'LineStyle', '-');
plot(ax2, data.t(end, (bInd : eInd)), data.S.offsetError(bInd : eInd) * 100, 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-');
%ylim(ax2, [-100, 100]);
xlabel(ax2, '$t_v$ (time) [s]');
ylabel(ax2, '$y_e$ (cross track err.) [cm]');
ax2.YAxis(1).Color = [0, 0, 0];
yyaxis(ax2, 'right');
plot(ax2, subs(data.plannerOut.VehOut.Ctrl.Time + data.t(end, bInd)), subs(rad2deg(data.plannerOut.VehOut.Ctrl.HeadingErr)), ...
    'Color', nicecolors.RedAlizarin, 'LineStyle', '-.', 'LineWidth', .75);
plot(ax2, data.t(end, (bInd : eInd)), rad2deg(data.S.angleError(bInd : eInd)), 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-.', 'LineWidth', .75);
%ylim(ax2, [-6, 6]);
xlabel(ax2, '$t_v$ (time) [s]');
ylabel(ax2, '$\psi_e$ (yaw angle err.) [$^\circ$]');
ax2.YAxis(2).Color = [0, 0, 0];
xlim(ax2, data.t(end, [bInd, eInd]));
l = nicelegend({sprintf('Simulation\ncross track'), sprintf('Measurement\ncross track'), sprintf('Simulation\nheading'), sprintf('Measurement\nheading')}, ax2, F);
l.Orientation = 'vertical';
l.Location = 'northeastoutside';
l.Position = [0.715, 0.10, 0.267, 0.39];
ax2.Position = [0.11, 0.10, 0.495, 0.39];

% Save
name = [measName, '_LatTracking'];
savefig(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');