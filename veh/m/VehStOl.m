%% VehStOl *******************************************************************************************
% [Summary]
%   This class represents a planar nonlinear single track vehicle model.
%
% [Used in]
%   user
%
% [Subclasses]
%   VehStCl
% **************************************************************************************************
classdef VehStOl < handle
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % longitudinal front and rear tire forces in vehicle-fixed cs. [N] (from previous ode step)
        forceFrontXV
        forceRearXV

        %% Properties set in constructor -----------------------------------------------------------
        % step size for ode solution
        SampleTime
        % chassis model (ChassisSt object)
        Chassis
        % wheel model (WheelMf object)
        WheelFront
        % wheel model (WheelMf object)
        WheelRear
        % steering model (SteerFo object)
        Steering

    end

    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------
        function value = setChassis(value)
            if isstruct(value)
                value = ChassisSt(value);
            end
            if ~isa(value, 'ChassisSt')
                error('ERROR: Chassis must be a ChassisSt object!')
            end
        end

        function value = setWheel(value)
            if isstruct(value)
                value = WheelMf(value);
            end
            if ~isa(value, 'WheelMf')
                error('ERROR: Wheel must be a struct or a WheelMf object!')
            end
        end
        
        function value = setSteering(value)
            if isstruct(value)
                value = SteerFo(value);
            end
            if ~isa(value, 'SteerFo')
                error('ERROR: Steering must be a struct or a SteerFo object!')
            end
        end
        
        %% CreateAnimation -------------------------------------------------------------------------
        % [Summary]
        %   This function creates a video about the vehicle's movement.
        %
        % [Input]
        %   in  - vehicle inputs as VehStOlIn object
        %   out - vehicle outputs as VehStOlOut object
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------
        function CreateAnimation(in, out)
            if ~isa(in, 'VehStOlIn')
                error('ERROR: in must be a VehStOlIn object!');
            end
            if ~isa(out, 'VehStOlOut')
                error('ERROR: out must be a VehStOlOut object!');
            end
            
        end

    end

    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.SampleTime(self, value)
            if ~isequal(self.SampleTime, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SampleTime must be a numeric scalar!')
                end
                self.SampleTime = value;
            end
        end

        function set.Chassis(self, value)
            if ~isequal(self.Chassis, value)
                self.Chassis = self.setChassis(value);
            end
        end

        function set.WheelFront(self, value)
            if ~isequal(self.WheelFront, value)
                self.WheelFront = self.setWheel(value);
            end
        end

        function set.WheelRear(self, value)
            if ~isequal(self.WheelRear, value)
                self.WheelRear = self.setWheel(value);
            end
        end
        
        function set.Steering(self, value)
            if ~isequal(self.Steering, value)
                self.Steering = self.setSteering(value);
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehStOl(varargin)
            if nargin == 1
                self.SampleTime = varargin{1}.SampleTime;
                self.Chassis    = varargin{1}.Chassis;
                self.WheelFront = varargin{1}.WheelFront;
                self.WheelRear  = varargin{1}.WheelRear;
                self.Steering   = varargin{1}.Steering;
            elseif nargin == 4
                self.SampleTime = varargin{1};
                self.Chassis    = varargin{2};
                self.WheelFront = varargin{3};
                self.WheelRear  = varargin{4};
                self.Steering   = varargin{5};
            else
                 error('ERROR: Inappropriate number of arguments!');
            end
            self.forceFrontXV = 0;
            self.forceRearXV = 0;
        end

        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of the planar nonlinear single track vehicle
        %   model.
        %
        %   The coordinate system of the vehicle is rotated around z-axis by the yaw angle
        %   compared to the ground-fixed system! The coordinate systems of the wheels are rotated
        %   by the steering angle compared to the vehicle-fixed system.
        %   The vehicle-fixed coordinate system is: x points forward (driving direction); y points
        %   left; z points upwards.
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %   Chassis ------------------------------------------------------------------------------
        %       X(1)  => PosXG:     longitudinal position in ground-fixed cs. [m]
        %       X(2)  => VelXG:     longitudinal velocity in ground-fixed cs. [m/s]
        %       X(3)  => PosYG:     lateral position in ground-fixed cs. [m]
        %       X(4)  => VelYG:     lateral velocity in ground-fixed cs. [m/s]
        %       X(5)  => YawZ:      yaw (heading) angle in ground-fixed cs. [rad]
        %       X(6)  => YawVelZ:   yaw (heading) angular velocity in ground-fixed cs. [rad/s]
        %   WheelFront ---------------------------------------------------------------------------
        %       X(7)  => PitchY:    angle of the wheel [rad]
        %       X(8)  => PitchVelY: angular velocity of the wheel [rad/s]
        %       X(9)  => SlipX:     longitudinal slip [1]
        %       X(10) => SlipY:     lateral slip [1]
        %   WheelRear ----------------------------------------------------------------------------
        %       X(11) => PitchY:    angle of the wheel [rad]
        %       X(12) => PitchVelY: angular velocity of the wheel [rad/s]
        %       X(13) => SlipX:     longitudinal slip [1]
        %       X(14) => SlipY:     lateral slip [1]
        %   Steering -----------------------------------------------------------------------------
        %       X(15) => SteerAng:  wheel level steering angle [rad]
        %
        %   U - input vector
        %       U(1) => TorqueDrv:     driving torque [Nm]
        %       U(2) => TorqueBrk:     braking torque [Nm]
        %       U(3) => FrictionFront: friction coefficient front [1]
        %       U(4) => FrictionRear:  friction coefficient rear [1]
        %       U(5) => SteerWhlAng:   steering wheel angle [rad]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %   Chassis ------------------------------------------------------------------------------
        %       Y(1)  => AccXG:     longitudinal acceleration in ground-fixed cs. [m/s^2]
        %       Y(2)  => AccYG:     lateral acceleration in ground-fixed cs. [m/s^2]
        %       Y(3)  => AccInrtXV: longitudinal inertial acceleration in vehicle-fixed cs. [m/s^2]
        %       Y(4)  => AccInrtYV: lateral inertial acceleration in vehicle-fixed cs. [m/s^2]
        %       Y(5)  => YawAccZ:   yaw (heading) angular acceleration in ground-fixed cs. [rad/s^2]
        %       Y(6)  => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
        %       Y(7)  => VelYV:     lateral velocity in vehicle-fixed cs. [m/s]
        %   WheelFront ---------------------------------------------------------------------------
        %       Y(8)  => PitchAccY: angular acceleration [rad/s]
        %       Y(9)  => DSlipX:    longitudinal slip derivative [1/s]
        %       Y(10) => ForceXW:   longitudinal force in wheel-fixed cs. [N]
        %       Y(11) => DSlipY:    lateral slip derivative [1/s]
        %       Y(12) => ForceYW:   lateral force in wheel-fixed cs. [N]
        %       Y(13) => ForceZW:   vertical force in wheel-fixed cs. [N]
        %       Y(14) => TorqueDrv: wheel level driving torque [Nm]
        %       Y(15) => TorqueBrk: wheel level braking torque [Nm]
        %       Y(16) => SteerAng:  wheel level steering angle [rad]
        %   WheelRear ----------------------------------------------------------------------------
        %       Y(17) => PitchAccY: angular acceleration [rad/s]
        %       Y(18) => DSlipX:    longitudinal slip derivative [1/s]
        %       Y(19) => ForceXW:   longitudinal force in wheel-fixed cs. [N]
        %       Y(20) => DSlipY:    lateral slip derivative [1/s]
        %       Y(21) => ForceYW:   lateral force in wheel-fixed cs. [N]
        %       Y(22) => ForceZW:   vertical force in wheel-fixed cs. [N]
        %       Y(23) => TorqueDrv: wheel level driving torque [Nm]
        %       Y(24) => TorqueBrk: wheel level braking torque [Nm]
        %       Y(25) => SteerAng:  wheel level steering angle [rad]
        %   Steering -----------------------------------------------------------------------------
        %       Y(26) => DSteerAng: wheel level steering angle derivative [rad/s]
        % ------------------------------------------------------------------------------------------
        function [dX, Y] = StateEquation(self, t, X, U)
            %% Common

            % Sine and cosine of steering angle
            cosSteerAng = cos(X(15));
            sinSteerAng = sin(X(15));

            % Sine and cosine of yaw angle
            cosYawV = cos(X(5));
            sinYawV = sin(X(5));

            %% Wheels

            % Chassis velocities in vehicle-fixed cs.
            velXV = + cosYawV * X(2) + sinYawV * X(4);
            velYV = - sinYawV * X(2) + cosYawV * X(4);

            % Wheel velocities in vehicle-fixed cs.
            velFrontXV = velXV;
            velFrontYV = velYV + self.Chassis.DistanceFrontCOG * X(6);
            velRearXV = velXV;
            velRearYV = velYV - self.Chassis.DistanceRearCOG * X(6);

            % Wheel velocities in wheel-fixed cs.
            velFrontXW = + cosSteerAng * velFrontXV + sinSteerAng * velFrontYV;
            velFrontYW = - sinSteerAng * velFrontXV + cosSteerAng * velFrontYV;

            velRearXW = velRearXV;
            velRearYW = velRearYV;

            % Tire loads (vertical forces)
            forceFrontZW = (self.Chassis.Mass * Com.Gravitation * self.Chassis.DistanceRearCOG ...
                - self.Chassis.HeightCOG * (self.forceFrontXV + self.forceRearXV)) / ...
                (self.Chassis.DistanceFrontCOG + self.Chassis.DistanceRearCOG);
            forceRearZW = (self.Chassis.Mass * Com.Gravitation * self.Chassis.DistanceFrontCOG ...
                + self.Chassis.HeightCOG * (self.forceFrontXV + self.forceRearXV)) / ...
                (self.Chassis.DistanceFrontCOG + self.Chassis.DistanceRearCOG);

            % Driving torque distribution
            coeffFront = self.WheelFront.Radius * forceFrontZW;
            coeffRear = self.WheelRear.Radius * forceRearZW;
            torqueFrontDrv = coeffFront / (coeffFront + coeffRear) * U(1);
            torqueRearDrv = coeffRear / (coeffFront + coeffRear) * U(1);

            % Braking torque distribution
            torqueFrontBrk = coeffFront / (coeffFront + coeffRear) * U(2);
            torqueRearBrk = coeffRear / (coeffFront + coeffRear) * U(2);

            % State equation

            % Inputs
            % U(1) => TorqueDrv: driving torque [Nm]
            % U(2) => TorqueBrk: braking torque [Nm]
            % U(3) => Friction:  friction coefficient [1]
            % U(4) => ForceZW:   vertical force (tire load) in wheel-fixed cs. [N]
            % U(5) => VelXW:     lon. velocity of the wheel center point in wheel-fixed cs. [m/s]
            % U(6) => VelYW:     lat. velocity of the wheel center point in wheel-fixed cs. [m/s]

            [dXWF, YWF] = self.WheelFront.StateEquation(t, X(7:10), ...
               [torqueFrontDrv; torqueFrontBrk; U(3); forceFrontZW; velFrontXW; velFrontYW]);

            [dXWR, YWR] = self.WheelRear.StateEquation(t, X(11:14), ...
                [torqueRearDrv; torqueRearBrk; U(4); forceRearZW; velRearXW; velRearYW]);

            % Outputs
            % Y(1) => PitchAccY: angular acceleration [rad/s]
            % Y(2) => DSlipX:    longitudinal slip derivative [1/s]
            % Y(3) => ForceXW:   longitudinal force in wheel-fixed cs. [N]
            % Y(4) => DSlipY:    lateral slip derivative [1/s]
            % Y(5) => ForceYW:   lateral force in wheel-fixed cs. [N]

            %% Chassis

            % Forces in vehicle-fixed cs.
            self.forceFrontXV = + cosSteerAng * YWF(3) - sinSteerAng * YWF(5);
            forceFrontYV = + sinSteerAng * YWF(3) + cosSteerAng * YWF(5);

            self.forceRearXV = YWR(3);
            forceRearYV = YWR(5);

            % Inputs
            % U(1) => ForceFrontXV: longitudinal front tire force in vehicle-fixed cs. [N]
            % U(2) => ForceFrontYV: lateral front tire force in vehicle-fixed cs. [N]
            % U(3) => ForceRearXV:  longitudinal rear tire force in vehicle-fixed cs. [N]
            % U(4) => ForceRearYV:  lateral rear tire force in vehicle-fixed cs. [N]
            % U(5) => VelXV:        longitudinal velocity in vehicle-fixed coordintate system [m/s]
            % U(6) => VelYV:        lateral velocity in vehicle-fixed coordintate system [m/s]
            % U(7) => CosYawV:      cosine of yaw (heading) angle in ground-fixed cs. [1]
            % U(8) => SinYawV:      sine of yaw (heading) angle in ground-fixed cs. [1]

            [dXC, YC] = self.Chassis.StateEquation(t, X(1:6), ...
                [self.forceFrontXV; forceFrontYV; self.forceRearXV; forceRearYV; velXV; velYV; cosYawV; sinYawV]);

            % Outputs
            % Y(1)  => AccXG:     longitudinal acceleration in ground-fixed cs. [m/s^2]
            % Y(2)  => AccYG:     lateral acceleration in ground-fixed cs. [m/s^2]
            % Y(3)  => AccInrtXV: longitudinal inertial acceleration in vehicle-fixed cs. [m/s^2]
            % Y(4)  => AccInrtYV: lateral inertial acceleration in vehicle-fixed cs. [m/s^2]
            % Y(5)  => YawAccZ:   yaw (heading) angular acceleration in ground-fixed system [rad/s^2]
            
            %% Steering
            
            % Inputs
            % U(1) => teerAng: steering wheel angle [rad]
            
            [dXS, YS] = self.Steering.StateEquation(t, X(15), U(5));
            
            % Outputs
            % Y(1) => DSteerAng: derivative of wheel level steering angle [rad/s]

            %% Set state derivatives
            dX = [dXC; dXWF; dXWR; dXS];

            %% Set outputs
            Y = [YC; velXV; velYV; ...
                YWF; forceFrontZW; torqueFrontDrv; torqueFrontBrk; X(15); ...
                YWR; forceRearZW; torqueRearDrv; torqueRearBrk; 0; ...
                YS];
        end

        %% Initialize -----------------------------------------------------------------------------
        % [Summary]
        %   This function provides the initial state vector of the planar nonlinear single track
        %   vehicle model based on the provided initial conditios.
        %
        % [Input]
        %   initCon - initial conditions as ChassisStBndCon object.
        %
        % [Output]
        %   X0      - initial state vector of vehicle model
        % ------------------------------------------------------------------------------------------
        function X0 = Initialize(self, initCon)
            %% Check arguments
            if ~isa(initCon, 'ChassisStBndCon')
                error('ERROR: input must be a ChassisStBndCon object!');
            end
            
            %% Reset internal variables
            self.forceFrontXV = 0;
            self.forceRearXV = 0;
            
            %% Initial state vector

            % Sine and cosine of yaw angle
            cosYawZ0 = cos(initCon.YawZ);
            sinYawZ0 = sin(initCon.YawZ);

            % Velocities in ground-fixed cs.
            velXG0 = + cosYawZ0 * initCon.VelXV - sinYawZ0 * initCon.VelYV;
            velYG0 = + sinYawZ0 * initCon.VelXV + cosYawZ0 * initCon.VelYV;

            % Initial value of state vector
            X0 = [initCon.PosXG;
                  velXG0;
                  initCon.PosYG;
                  velYG0;
                  initCon.YawZ;
                  initCon.YawVelZ;
                  0;
                  initCon.VelXV / self.WheelFront.Radius;
                  0;
                  0;
                  0;
                  initCon.VelXV / self.WheelRear.Radius;
                  0;
                  0;
                  0];
        end

        %% Simulate --------------------------------------------------------------------------------
        % [Summary]
        %   This function simulates the planar nonlinear single track vehicle model.
        %
        % [Input]
        %   initCon  - initial conditions for the vehicle as VehStBndCon object
        %   input    - input as VehStOlIn object
        %
        % [Output]
        %   output   - output as VehStOlOut object
        % ------------------------------------------------------------------------------------------
        function output = Simulate(self, initCon, input)
            %% Check arguments
            if ~isa(initCon, 'VehStBndCon')
                error('ERROR: initCon must be a VehStBndCon object!');
            end
            if ~isa(input, 'VehStOlIn')
                error('ERROR: input must be a VehStOlIn object!');
            end

            %% Input

            t = (input.Time(1) : self.SampleTime : input.Time(end))';
            if t(end) ~= input.Time(end)
                t(end + 1) = input.Time(end);
            end
            U = interp1q(input.Time, [input.TorqueDrv,  input.TorqueBrk, input.FrictionFront, ...
                input.FrictionRear, input.SteerWhlAng], t)';

            %% Initial conditions

            X0 = self.Initialize(initCon);

            %% Calculation of the motion
            [X, Y] = ode4(self, t, X0, U);

            output = VehStOlOut(t, ...
                ChassisStOut(t, X(1,:), X(2,:), Y(1,:), Y(6,:), Y(3,:),  X(3,:), X(4,:), ...
                    Y(2,:), Y(7,:), Y(4,:), X(5,:), X(6,:), Y(5,:)), ...
                WheelMfExtOut(t, X(7,:), X(8,:), Y(8,:), X(9,:), Y(9,:), Y(10,:), Y(13,:), ...
                    X(10,:), Y(11,:), Y(12,:), Y(14,:), Y(15,:), Y(16,:)), ...
                WheelMfExtOut(t, X(11,:), X(12,:), Y(17,:), X(13,:), Y(18,:), Y(19,:), Y(22,:), ...
                    X(14,:), Y(20,:), Y(21,:), Y(23,:), Y(24,:), Y(25,:)), ...
                SteerFoOut(t, X(15, :), Y(26, :)) ...
            );
        end

        %% SimulateMeX -----------------------------------------------------------------------------
        % [Summary]
        %   This function simulates the planar nonlinear single track vehicle model with C++
        %   implementation.
        %
        % [Input]
        %   initCon  - initial conditions for the vehicle as VehStBndCon object
        %   input    - input as VehStOlIn object
        %
        % [Output]
        %   output   - output as VehStOlOut object
        % ------------------------------------------------------------------------------------------
        function output = SimulateMex(self, initCon, input)
            %% Check arguments
            if ~isa(initCon, 'VehStBndCon')
                error('ERROR: initCon must be a VehStBndCon object!');
            end
            if ~isa(input, 'VehStOlIn')
                error('ERROR: input must be a VehStOlIn object!');
            end

            %% Parameters

            P = [self.Chassis.Mass;
                self.Chassis.MomentOfIntertiaZ;
                self.Chassis.DistanceFrontCOG;
                self.Chassis.DistanceRearCOG;
                self.Chassis.HeightCOG;
                self.Chassis.DragCoefficient;
                self.Chassis.FrontalArea;
                self.Chassis.DrivingTorqueSplit;
                self.WheelFront.Radius;
                self.WheelFront.MomentOfInertiaY;
                self.WheelFront.MfMaxX;
                self.WheelFront.MfStiffnessX;
                self.WheelFront.MfShapeX;
                self.WheelFront.MfCurvatureX;
                self.WheelFront.RollingResistanceConst;
                self.WheelFront.RollingResistanceLin;
                self.WheelFront.RollingResistanceSqr;
                self.WheelFront.RollingResistanceVel;
                self.WheelFront.RollingResistanceVelCorr;
                self.WheelFront.BrakingTorqueVel;
                self.WheelFront.RelaxationLength0X;
                self.WheelFront.RelaxationLengthMinX;
                self.WheelFront.SlipDamping0X;
                self.WheelFront.VelSlowX;
                self.WheelFront.MfMaxY;
                self.WheelFront.MfStiffnessY;
                self.WheelFront.MfShapeY;
                self.WheelFront.MfCurvatureY;
                self.WheelFront.RelaxationLength0Y;
                self.WheelFront.RelaxationLengthMinY;
                self.WheelFront.SlipDamping0Y;
                self.WheelFront.VelSlowY;
                self.WheelFront.MinSlip;
                self.WheelRear.Radius;
                self.WheelRear.MomentOfInertiaY;
                self.WheelRear.MfMaxX;
                self.WheelRear.MfStiffnessX;
                self.WheelRear.MfShapeX;
                self.WheelRear.MfCurvatureX;
                self.WheelRear.RollingResistanceConst;
                self.WheelRear.RollingResistanceLin;
                self.WheelRear.RollingResistanceSqr;
                self.WheelRear.RollingResistanceVel;
                self.WheelRear.RollingResistanceVelCorr;
                self.WheelRear.BrakingTorqueVel;
                self.WheelRear.RelaxationLength0X;
                self.WheelRear.RelaxationLengthMinX;
                self.WheelRear.SlipDamping0X;
                self.WheelRear.VelSlowX;
                self.WheelRear.MfMaxY;
                self.WheelRear.MfStiffnessY;
                self.WheelRear.MfShapeY;
                self.WheelRear.MfCurvatureY;
                self.WheelRear.RelaxationLength0Y;
                self.WheelRear.RelaxationLengthMinY;
                self.WheelRear.SlipDamping0Y;
                self.WheelRear.VelSlowY;
                self.WheelRear.MinSlip;
                self.Steering.Ratio;
                self.Steering.TimeConstant];

            %% Input

            t = (input.Time(1) : self.SampleTime : input.Time(end))';
            if t(end) ~= input.Time(end)
                t(end + 1) = input.Time(end);
            end
            U = interp1q(input.Time, [input.TorqueDrv,  input.TorqueBrk, input.FrictionFront, input.FrictionRear, input.SteerWhlAng], t)';

            %% Initial conditions

            X0 = [initCon.PosXG; initCon.VelXV; initCon.PosYG; initCon.VelYV; initCon.YawZ; initCon.YawVelZ];

            %% Calculation of the motion
            [X, Y] = VehStOlMex(P, t', X0, U);

            output = VehStOlOut(t, ...
                ChassisStOut(t, X(1,:), X(2,:), Y(1,:), Y(6,:), Y(3,:),  X(3,:), X(4,:), ...
                    Y(2,:), Y(7,:), Y(4,:), X(5,:), X(6,:), Y(5,:)), ...
                WheelMfExtOut(t, X(7,:), X(8,:), Y(8,:), X(9,:), Y(9,:), Y(10,:), Y(13,:), ...
                    X(10,:), Y(11,:), Y(12,:), Y(14,:), Y(15,:), Y(16,:)), ...
                WheelMfExtOut(t, X(11,:), X(12,:), Y(17,:), X(13,:), Y(18,:), Y(19,:), Y(22,:), ...
                    X(14,:), Y(20,:), Y(21,:), Y(23,:), Y(24,:), Y(25,:)), ...
                SteerFoOut(t, X(15, :), Y(26, :)) ...
            );
        end

    end
end