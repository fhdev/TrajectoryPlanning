#include <math.h>
#include "mex.h"
#include "../../ext//eigen-3.3.7/Eigen/Dense"
#include "../../com/inc/TypesTp.hpp"
#include "../../com/inc/MathTp.hpp"
#include "../../com/inc/Ode.hpp"
#include "../inc/VehStCl.hpp"

#if MX_HAS_INTERLEAVED_COMPLEX
#define mxGetFloat64(cell) mxGetDoubles((cell))
#else
#define mxGetFloat64(cell) mxGetPr((cell))
#endif

// number of parameters
#define NP 77
// number of inputs
#define NU 4
// number of provided initial states
#define NXi 6
// number of states
#define NX 17
// number of outputs
#define NY 32

#define P_  (prhs[0])
#define t_  (prhs[1])
#define X0_ (prhs[2])
#define U_  (prhs[3])

#define X_ (plhs[0])
#define Y_ (plhs[1])

using namespace Eigen;
using namespace TrajectoryPlanning;

// [X, Y] = VehStMex(P, t, X0, U)
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
	// Check number of input arguments
	if (nrhs != 4)
	{
		mexErrMsgIdAndTxt("VehStClMex:nrhs",
			"Argument list must be [X, Y] = VehStMex(P, t, X0, U). Number of input parameters is not 4.");
	}
	// Check number of output arguments
	if (nlhs != 2)
	{
		mexErrMsgIdAndTxt("VehStClMex:nlhs",
			"Argument list must be [X, Y] = VehStMex(P, t, X0, U). Number of output parameters is not 2.");
	}
	// Check P (parameter cell array)
	Int64_T rowsP = mxGetM(P_);
	Int64_T colsP = mxGetN(P_);
	if (!mxIsCell(P_) || (rowsP != NP) || (colsP != 1))
	{
		mexErrMsgIdAndTxt("VehStClMex:invalidArgument",
			"P must be a column cell array with %d elements.", NP);
	}
	else
	{
		for (Int64_T i = 0; i < NP; i++)
		{
			mxArray *cell = mxGetCell(P_, i);
			Int64_T rowscell = mxGetM(cell);
			Int64_T colscell = mxGetN(cell);
			if (!mxIsDouble(cell) || mxIsComplex(cell) || (rowscell < 1) || (colscell != 1))
			{
				mexErrMsgIdAndTxt("VehStClMex:invalidArgument",
					"P must be a column cell array with column vector of double elements.");
			}
		}
	}
	// Check t (time sites)
	Int64_T rowst = mxGetM(t_);
	Int64_T colst = mxGetN(t_);
	if (!mxIsDouble(t_) || mxIsComplex(t_) || (rowst != 1))
	{
		mexErrMsgIdAndTxt("VehStClMex:invalidArgument",
			"t must be a row vector of doubles.");
	}
	// Check X0 (time sites)
	Int64_T rowsX0 = mxGetM(X0_);
	Int64_T colsX0 = mxGetN(X0_);
	if (!mxIsDouble(X0_) || mxIsComplex(X0_) || (rowsX0 != NXi) || (colsX0 != 1))
	{
		mexErrMsgIdAndTxt("VehStClMex:invalidArgument",
			"X0 must be a column vector of doubles with %d elements.", NXi);
	}
	// Check U (input data)
	Int64_T rowsU = mxGetM(U_);
	Int64_T colsU = mxGetN(U_);
	if (!mxIsDouble(U_) || mxIsComplex(U_) || (rowsU != NU) || (colsU != colst))
	{
		mexErrMsgIdAndTxt("VehStClMex:invalidArgument",
			"U must be a matrix of doubles with %d rows and same number of column as t.", NU);
	}

	// Get input data pointers
	Float64_T *tptr = mxGetFloat64(t_);
	Float64_T *Xiptr = mxGetFloat64(X0_);
	Float64_T *Uptr = mxGetFloat64(U_);

	// Allocate output
	X_ = mxCreateDoubleMatrix(NX, colst, mxREAL);
	Y_ = mxCreateDoubleMatrix(NY, colst, mxREAL);

	// Get output data pointers
	Float64_T *Xptr = mxGetFloat64(X_);
	Float64_T *Yptr = mxGetFloat64(Y_);

	// Create mappings for Eigen usage
	Map<VectorXd> t(tptr, colst);
	Map<VectorXd> Xi(Xiptr, NXi);
	Map<MatrixXd, ColMajor> U(Uptr, NU, colst);
	Map<MatrixXd, ColMajor> X(Xptr, NX, colst);
	Map<MatrixXd, ColMajor> Y(Yptr, NY, colst);

	// Get parameters
	mxArray *arg61 = mxGetCell(P_, 61);
	mxArray *arg62 = mxGetCell(P_, 62);
	mxArray *arg63 = mxGetCell(P_, 63);
	mxArray *arg68 = mxGetCell(P_, 68);
	mxArray *arg69 = mxGetCell(P_, 69);
	mxArray *arg73 = mxGetCell(P_, 73);
	mxArray *arg74 = mxGetCell(P_, 74);
	mxArray *arg75 = mxGetCell(P_, 75);

	// Create vehicle
	VehStCl v = VehStCl(
		ChassisSt(
			mxGetFloat64(mxGetCell(P_, 0))[0],
			mxGetFloat64(mxGetCell(P_, 1))[0],
			mxGetFloat64(mxGetCell(P_, 2))[0],
			mxGetFloat64(mxGetCell(P_, 3))[0],
			mxGetFloat64(mxGetCell(P_, 4))[0],
			mxGetFloat64(mxGetCell(P_, 5))[0],
			mxGetFloat64(mxGetCell(P_, 6))[0],
			mxGetFloat64(mxGetCell(P_, 7))[0]),
		WheelMf(
			mxGetFloat64(mxGetCell(P_, 8))[0],
			mxGetFloat64(mxGetCell(P_, 9))[0],
			mxGetFloat64(mxGetCell(P_, 10))[0],
			mxGetFloat64(mxGetCell(P_, 11))[0],
			mxGetFloat64(mxGetCell(P_, 12))[0],
			mxGetFloat64(mxGetCell(P_, 13))[0],
			mxGetFloat64(mxGetCell(P_, 14))[0],
			mxGetFloat64(mxGetCell(P_, 15))[0],
			mxGetFloat64(mxGetCell(P_, 16))[0],
			mxGetFloat64(mxGetCell(P_, 17))[0],
			mxGetFloat64(mxGetCell(P_, 18))[0],
			mxGetFloat64(mxGetCell(P_, 19))[0],
			mxGetFloat64(mxGetCell(P_, 20))[0],
			mxGetFloat64(mxGetCell(P_, 21))[0],
			mxGetFloat64(mxGetCell(P_, 22))[0],
			mxGetFloat64(mxGetCell(P_, 23))[0],
			mxGetFloat64(mxGetCell(P_, 24))[0],
			mxGetFloat64(mxGetCell(P_, 25))[0],
			mxGetFloat64(mxGetCell(P_, 26))[0],
			mxGetFloat64(mxGetCell(P_, 27))[0],
			mxGetFloat64(mxGetCell(P_, 28))[0],
			mxGetFloat64(mxGetCell(P_, 29))[0],
			mxGetFloat64(mxGetCell(P_, 30))[0],
			mxGetFloat64(mxGetCell(P_, 31))[0],
			mxGetFloat64(mxGetCell(P_, 32))[0]),
		WheelMf(
			mxGetFloat64(mxGetCell(P_, 33))[0],
			mxGetFloat64(mxGetCell(P_, 34))[0],
			mxGetFloat64(mxGetCell(P_, 35))[0],
			mxGetFloat64(mxGetCell(P_, 36))[0],
			mxGetFloat64(mxGetCell(P_, 37))[0],
			mxGetFloat64(mxGetCell(P_, 38))[0],
			mxGetFloat64(mxGetCell(P_, 39))[0],
			mxGetFloat64(mxGetCell(P_, 40))[0],
			mxGetFloat64(mxGetCell(P_, 41))[0],
			mxGetFloat64(mxGetCell(P_, 42))[0],
			mxGetFloat64(mxGetCell(P_, 43))[0],
			mxGetFloat64(mxGetCell(P_, 44))[0],
			mxGetFloat64(mxGetCell(P_, 45))[0],
			mxGetFloat64(mxGetCell(P_, 46))[0],
			mxGetFloat64(mxGetCell(P_, 47))[0],
			mxGetFloat64(mxGetCell(P_, 48))[0],
			mxGetFloat64(mxGetCell(P_, 49))[0],
			mxGetFloat64(mxGetCell(P_, 50))[0],
			mxGetFloat64(mxGetCell(P_, 51))[0],
			mxGetFloat64(mxGetCell(P_, 52))[0],
			mxGetFloat64(mxGetCell(P_, 53))[0],
			mxGetFloat64(mxGetCell(P_, 54))[0],
			mxGetFloat64(mxGetCell(P_, 55))[0],
			mxGetFloat64(mxGetCell(P_, 56))[0],
			mxGetFloat64(mxGetCell(P_, 57))[0]),
		SteerFo(
			mxGetFloat64(mxGetCell(P_, 58))[0],
			mxGetFloat64(mxGetCell(P_, 59))[0]),
		CtrlLqsV(
			mxGetFloat64(mxGetCell(P_, 60))[0],
			Map<VectorXd>(mxGetFloat64(arg61), mxGetM(arg61)),
			Map<VectorXd>(mxGetFloat64(arg62), mxGetM(arg62)),
			Map<VectorXd>(mxGetFloat64(arg63), mxGetM(arg63)),
			mxGetFloat64(mxGetCell(P_, 64))[0],
			mxGetFloat64(mxGetCell(P_, 65))[0],
			mxGetFloat64(mxGetCell(P_, 66))[0]),
		CtrlLatSty(
			mxGetFloat64(mxGetCell(P_, 67))[0],
			Map<VectorXd>(mxGetFloat64(arg68), mxGetM(arg68)),
			Map<VectorXd>(mxGetFloat64(arg69), mxGetM(arg69)),
			mxGetFloat64(mxGetCell(P_, 70))[0],
			mxGetFloat64(mxGetCell(P_, 71))[0],
			mxGetFloat64(mxGetCell(P_, 72))[0],
			Map<VectorXd>(mxGetFloat64(arg73), mxGetM(arg73)),
			Map<VectorXd>(mxGetFloat64(arg74), mxGetM(arg74)),
			Map<VectorXd>(mxGetFloat64(arg75), mxGetM(arg75))),
		mxGetFloat64(mxGetCell(P_, 76))[0]);

	// Calculate initial state
	VectorXd X0(NX);
	v.Initialize(Xi, X0);

	// Compute motion
	Rk4(v, t, X0, U, X, Y);

}
