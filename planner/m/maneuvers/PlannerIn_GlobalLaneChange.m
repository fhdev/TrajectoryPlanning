function input = PlannerIn_GlobalLaneChange()
%% Calculate global goal
% Local goal (so that we know this is a lange change shape)
xf = 50;
yf = 10;
rf = 0;
drf = 0;
% global init point
Xi = 100;
Yi = 50;
Ri = deg2rad(45);
dRi = 0;
% gloal goal
sin_ri = sin(Ri);
cos_ri = cos(Ri);
Xf = Xi + cos_ri * xf - sin_ri * yf;
Yf = Yi + sin_ri * xf + cos_ri * yf;
Rf = Ri + rf;
dRf = drf;
% velocity
v = 20;
%% Initial and final conditions (common for all planners)
% Environment input
input.EnvIn.Time          = 1; % currntly not used
input.EnvIn.FrictionFront = 1;
input.EnvIn.FrictionRear  = 1;
% Initial conditions
input.InitCon.PosXG   = Xi;
input.InitCon.PosYG   = Yi;
input.InitCon.YawZ    = Ri;
input.InitCon.VelXV   = v;
input.InitCon.VelYV   = 0;
input.InitCon.YawVelZ = dRi;
% Final conditions
input.FinalCon.Time      = nan;
input.FinalCon.PosXG     = Xf;
input.FinalCon.VelXG     = nan;
input.FinalCon.VelXV     = nan;
input.FinalCon.AccXG     = nan;
input.FinalCon.AccInrtXV = nan;
input.FinalCon.PosYG     = Yf;
input.FinalCon.VelYG     = nan;
input.FinalCon.VelYV     = nan;
input.FinalCon.AccYG     = nan;
input.FinalCon.AccInrtYV = nan;
input.FinalCon.YawZ      = Rf;
input.FinalCon.YawVelZ   = dRf;
input.FinalCon.YawAccZ   = nan;

%% Initial reference parameters

% Common 
input.OptVarInit.VelRef     = v;

% PlannerCon
input.OptVarInit.YawRateRef = [0; 0; 0];
input.OptVarInit.TravelTime = xf / v;
input.OptVarInit.SampleTime = 0.1 / v;

% PlannerOpt
input.OptVarInit.PosYRef        = [0; 0];
input.OptVarInit.SampleDistance = 1e-2;

input.CostTrackWeight.Time          = nan;
input.CostTrackWeight.SteerWhlAng   = nan;
input.CostTrackWeight.CrossTrackErr = 1;
input.CostTrackWeight.HeadingErr    = rad2deg(0.2);

input.CostStateWeight.Time      = nan;
input.CostStateWeight.PosXG     = nan;
input.CostStateWeight.VelXG     = nan;
input.CostStateWeight.VelXV     = nan;
input.CostStateWeight.AccXG     = nan;
input.CostStateWeight.AccInrtXV = nan;
input.CostStateWeight.PosYG     = nan;
input.CostStateWeight.VelYG     = nan;
input.CostStateWeight.VelYV     = nan;
input.CostStateWeight.AccYG     = nan;
input.CostStateWeight.AccInrtYV = 0.05;
input.CostStateWeight.YawZ      = nan;
input.CostStateWeight.YawVelZ   = nan;
input.CostStateWeight.YawAccZ   = nan;

% PlannerCopt
input.OptVarInit.LookAhead = xf;
input.OptVarInit.Heading   = rf;

end
