%% VehQrLonIn **************************************************************************************
% [Summary]
%   This class represents boundary conditions for quarter vehicle model.
%
% [Used in]
%   VehQr
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehQrBndCon < ChassisQrBndCon
    %% Instance methods ============================================================================
    methods(Static)
        %% Constructor -----------------------------------------------------------------------------
        function self = VehQrBndCon(varargin)
            self@ChassisQrBndCon(varargin{:});
        end

    end
end