%% WheelMfExtOut ***********************************************************************************
% [Summary]
%   This class represents outputs for wheel model with Magic Formula tire. Contains also wheel level
%   inputs.
%
% [Used in]
%   VehStOlOut
%   VehStOl
%   VehStCl
%   VehStRt
%
% [Subclasses]
%   WheelMfExtOut
% **************************************************************************************************
classdef WheelMfExtOut < WheelMfOut & VehOlLonIn
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % steering angle (wheel level) [rad]
        SteerAng
        
    end
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.SteerAng(self, value)
            if ~isequal(self.SteerAng, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: SteerAng must be a numeric vector!');
                end
                self.SteerAng = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------        
        function self = WheelMfExtOut(varargin)
            if nargin == 1
                wheelMfOutArgs = varargin(1);
                vehOlInArgs = varargin(1);
            elseif nargin == 14
                wheelMfOutArgs = varargin(1:11);
                vehOlInArgs = varargin([1, 12, 13]);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@WheelMfOut(wheelMfOutArgs{:});
            self@VehOlLonIn(vehOlInArgs{:});
            if nargin == 1
                self.SteerAng = varargin{1}.SteerAng;
            elseif nargin == 14
                self.SteerAng = varargin{14};
            end
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the wheel model.
        %
        % [Input]
        %       name - name for the plots
        %
        % [Output]
        %       figDyn  - figure handle of plot containing wheel dynamics quantitites
        %       figIn   - figure handle of plot containing wheel level inputs
        % ------------------------------------------------------------------------------------------        
        function [figDyn, figIn] = DisplayPlots(self, name)
            %% Constants
            WIDTH = 5;
            HEIGHT = 15;
            FONTSIZE = 8;
            
            %% Default arguments
            if nargin < 2
                name = class(self);
            end
            
            %% Create plots
            
            % Plot superclass properties
            figDyn = DisplayPlots@WheelMfOut(self, [name, '/Dynamics']);
            
            % Plot own properties
            figIn = nicefigure([name, '/Inputs'], WIDTH, HEIGHT);
            
            ax = niceaxis(figIn, FONTSIZE);
            subplot(3, 1, 1, ax);
            plot(ax, self.Time, self.TorqueDrv, 'Color', Colors.Nephritis);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$M_d \mathrm{[Nm]}$');
            title(ax, 'driving torque');
            
            ax = niceaxis(figIn, FONTSIZE);
            subplot(3, 1, 2, ax);
            plot(ax, self.Time, self.TorqueBrk, 'Color', Colors.PeterRiver);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$M_b \mathrm{[Nm]}$');
            title(ax, 'braking torque');
            
            ax = niceaxis(figIn, FONTSIZE);
            subplot(3, 1, 3, ax);
            plot(ax, self.Time, rad2deg(self.SteerAng), 'Color', Colors.Wisteria);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$\delta \mathrm{[^{\circ}]}$');
            title(ax, 'steering angle');
            
        end
    end
end