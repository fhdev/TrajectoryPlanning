%% ChassisQrLonOut *********************************************************************************
% [Summary]
%   This class represents outputs for longitudinal quarter vehicle chassis model.
%
% [Used in]
%   VehBaseOut
%   VehQrLonOut
%   VehQrLon
%
% [Subclasses]
%   ChassisQrOut
% **************************************************************************************************
classdef PlannerGeomBndCon < handle
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % longitudinal velocity in vehicle-fixed cs. [m]
        VelXV
        % yaw rate in ground-fixed cs. [rad/s]
        YawVelZ
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------        
        function set.VelXV(self, value)
            if ~isequal(self.VelXV, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: VelXV must be a numeric vector!')
                end
                self.VelXV = value(:);
            end
        end
        
        function set.YawVelZ(self, value)
            if ~isequal(self.YawVelZ, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawVelZ must be a numeric vector!')
                end
                self.YawVelZ = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = PlannerGeomBndCon(varargin)
            if nargin == 1
                self.VelXV     = varargin{1}.VelXV;
                self.YawVelZ   = varargin{1}.YawVelZ;
            elseif nargin == 2
                self.VelXV     = varargin{1};
                self.YawVelZ   = varargin{2};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end
    end
end