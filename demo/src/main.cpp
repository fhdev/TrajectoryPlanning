#include <iostream>
#include <string>
#include <cstdlib>
#include <algorithm>
#include <../../ext/eigen/Dense>

using namespace Eigen;
using namespace std;

class Base
{
public:
	int Value;

	Base(int value) : Value(value) {}

	std::string Test()
	{
		return "Base";
	}

	void Print()
	{
		std::cout << Test() << " " << Value << std::endl;
	}
};

class Derived : public Base
{
public:

	Derived() : Base(2) {}

	std::string Test()
	{
		return "Derived";
	}

	void Call()
	{
		Base::Print();
	}

};

int main(int argc, char* argv[])
{
	VectorXd v = VectorXd::LinSpaced(20, 1, 20);
	cout << "Input:" << endl << v.transpose() << endl;
	Map<VectorXd, 0, InnerStride<3> > v2(v.data(), (v.size() / 3) + 1);
	cout << "Even:" << v2.transpose() << endl;
}

