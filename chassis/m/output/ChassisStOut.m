%% ChassisStOut ************************************************************************************
% [Summary]
%   This class represents outputs for single track vehicle chassis model.
%
% [Used in]
%   VehStOlOut
%   VehStOl
%   VehStCl
%   PlannerConIn
%   PlannerOptIn
%   PlannerConFcnVehState
%   PlannerOptFcnVehState
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef ChassisStOut < ChassisQrOut
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % yaw angle in ground-fixed cs. [rad]
        YawZ
        % yaw rate in ground-fixed cs. [rad/s]
        YawVelZ
        % yaw acceleration in ground-fixed cs. [rad/s^2]
        YawAccZ

    end
    
    %% Instance methods ============================================================================    
    methods
        %% Property getter and setter methods ------------------------------------------------------        
        function set.YawZ(self, value)
            if ~isequal(self.YawZ, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawZ must be a numeric vector!')
                end
                self.YawZ = value(:);
            end
        end
        
        function set.YawVelZ(self, value)
            if ~isequal(self.YawVelZ, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawVelZ must be a numeric vector!')
                end
                self.YawVelZ = value(:);
            end
        end
        
        function set.YawAccZ(self, value)
            if ~isequal(self.YawAccZ, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawAccZ must be a numeric vector!')
                end
                self.YawAccZ = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------        
        function self = ChassisStOut(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 14
                superArgs = varargin(1:11);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@ChassisQrOut(superArgs{:});
            if nargin == 1
                self.YawZ    = varargin{1}.YawZ;
                self.YawVelZ = varargin{1}.YawVelZ;
                self.YawAccZ = varargin{1}.YawAccZ;
            elseif nargin == 14
                self.YawZ    = varargin{12};
                self.YawVelZ = varargin{13};
                self.YawAccZ = varargin{14};
            end
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the single track vehicle chassis model.
        %
        % [Input]
        %       name - name for the plots
        %
        % [Output]
        %       figGlob  - figure handle of plot containing global cs. quantities
        %       figLoc   - figure handle of plot containing vehicle cs. quantities
        %       figPath  - figure handle of plot containing vehicle path
        % ------------------------------------------------------------------------------------------        
        function [figGlob, figLoc, figPath] = DisplayPlots(self, name)
            %% Constants
            FONTSIZE = 8;
            
            %% Default arguments
            if nargin < 2
                name = class(self);
            end
            
            %% Create plots
            
            % Plot superclass properties
            [figGlob, figLoc, figPath] = DisplayPlots@ChassisQrOut(self, name);
            
            % Plot own properties
            % --------------------------------------------------------------------------------------
            
            ax = niceaxis(figGlob, FONTSIZE);
            subplot(3, 3, 3, ax);
            plot(ax, self.Time, rad2deg(self.YawZ), 'Color', Colors.Pumpkin);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$\phi_z \mathrm{[^{\circ}]}$');
            title(ax, 'yaw angle');
            
            ax = niceaxis(figGlob, FONTSIZE);
            subplot(3, 3, 6, ax);
            plot(ax, self.Time, rad2deg(self.YawVelZ), 'Color', Colors.Pumpkin);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$\dot{\phi}_z \mathrm{[^{\circ}/s]}$');
            title(ax, 'yaw rate');
            
            ax = niceaxis(figGlob, FONTSIZE);
            subplot(3, 3, 9, ax);
            plot(ax, self.Time, rad2deg(self.YawAccZ), 'Color', Colors.Pumpkin);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$\ddot{\phi}_z  \mathrm{[^{\circ}/s^2]}$');
            title(ax, 'yaw acceleration');
            
        end
    end
end
