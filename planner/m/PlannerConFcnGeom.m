%% PlannerConFcnGeom *******************************************************************************
% [Summary]
%   This class represents state contraints for PlannerCon motion planner. State constraint means the
%   deviation between the required final state and the final state reached with current reference
%   signal parameters.
%
% [Used in]
%   PlannerCon
%   PlannerOpt
%   PlannerCopt
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef PlannerConFcnGeom < handle
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % property names of PlannerGeomOut
        propNames
        
        %% Properties set in constructor -----------------------------------------------------------
        % required final states
        FinalCon
        % actual state constraint values
        Value
    end
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        
        function set.FinalCon(self, value)
            if ~isequal(self.FinalCon, value)
                if isstruct(value)
                    value = PlannerGeomOut(value);
                end
                if ~isa(value, 'PlannerGeomOut')
                    error('ERROR: FinalCon must be a PlannerGeomOut object!');
                end
                self.FinalCon = value;
            end
        end
        
        function set.Value(self, value)
            if ~isequal(self.Value, value)
                if isstruct(value)
                    value = PlannerGeomOut(value);
                end
                if ~isa(value, 'PlannerGeomOut')
                    error('ERROR: Value must be a PlannerGeomOut object!');
                end
                self.Value = value;
            end
        end
                
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerConFcnGeom(finalCon)
            %% Check arguments
            if ~(isa(finalCon, 'PlannerGeomOut'))
                error('ERROR: finalCon must be a PlannerGeomOut object!');
            end
            %% Initialize
            self.FinalCon = PlannerGeomOut(finalCon);
            nulls = repmat({0}, 1, 8);
            self.Value = PlannerGeomOut(nulls{:});
            self.propNames = properties('PlannerGeomOut');
        end
        
        %% Update ----------------------------------------------------------------------------------
        % [Summary]
        %   This function updates state constaint values according to given vehicle simulation
        %   output.
        %
        % [Input]
        %   chassisOut - output of current vehicle simulation as PlannerGeomOut object
        %
        % [Output]
        %   none
        % ------------------------------------------------------------------------------------------
        function Update(self, chassisOut)
            %% Check arguments
            if ~(isa(chassisOut, 'PlannerGeomOut'))
                error('ERROR: chassisOut most be a PlannerGeomOut object!');
            end
            %% Calculate state constraints
            for iProp = 1 : length(self.propNames)
                % There is constraint specified
                if ~isnan(self.FinalCon.(self.propNames{iProp}))
                    % Calculate constraint values
                    self.Value.(self.propNames{iProp}) = self.FinalCon.(self.propNames{iProp})(1) - ...
                        chassisOut.(self.propNames{iProp})(end);
                else
                    self.Value.(self.propNames{iProp}) = nan;
                end
            end
        end
        %% ToVector --------------------------------------------------------------------------------
        % [Summary]
        %   This method provides nonlinear equality and inequality constraint vectors as required by
        %   fsolve and fmincon.
        %
        % [Input]
        %   none
        %
        % [Output]
        %   neqCon - nonlinear inequality constraints
        %   eqCon  - nonlinear equality constraints
        % ------------------------------------------------------------------------------------------        
        function [neqCon, eqCon] = ToVector(self)
            eqCon = [];
            neqCon = [];
            for iProp = 1 : length(self.propNames)
                if ~isnan(self.FinalCon.(self.propNames{iProp}))
                    eqCon = [eqCon; self.Value.(self.propNames{iProp})]; %#ok<AGROW>
                end
            end
        end
    end
end
