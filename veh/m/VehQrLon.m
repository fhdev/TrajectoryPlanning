%% VehQrLon ****************************************************************************************
% [Summary]
%   This class represents a longitudinal quarter vehicle model that consists of a wheel and a
%   coupled mass.
%
% [Used in]
%   user
%
% [Subclasses]
%   VehQr
% **************************************************************************************************
classdef VehQrLon < handle

    properties
        %% Properties set in constructor -----------------------------------------------------------
        % step size for ode solution
        SampleTime
        % chassis model (ChassisQrLon object)
        Chassis
        % wheel model (WheelMfLon object)
        Wheel

    end
    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------
        function value = setChassis(value)
            if isstruct(value)
                value = ChassisQrLon(value);
            end
            if ~isa(value, 'ChassisQrLon')
                error('ERROR: Chassis must be a ChassisQrLon object!')
            end
        end

        function value = setWheel(value)
            if isstruct(value)
                value = WheelMfLon(value);
            end
            if ~isa(value, 'WheelMfLon')
                error('ERROR: Wheel must be a struct or a WheelMfLon object!')
            end
        end

    end
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.SampleTime(self, value)
            if ~isequal(self.SampleTime, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SampleTime must be a numeric scalar!')
                end
                self.SampleTime = value;
            end
        end

        function set.Chassis(self, value)
            if ~isequal(self.Chassis, value)
                self.Chassis = self.setChassis(value);
            end
        end

        function set.Wheel(self, value)
            if ~isequal(self.Wheel, value)
                self.Wheel = self.setWheel(value);
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehQrLon(varargin)
            if nargin == 1
                self.SampleTime = varargin{1}.SampleTime;
                self.Chassis    = varargin{1}.Chassis;
                self.Wheel      = varargin{1}.Wheel;
            elseif nargin == 3
                self.SampleTime = varargin{1};
                self.Chassis    = varargin{2};
                self.Wheel      = varargin{3};
            else
                 error('ERROR: Inappropriate number of arguments!');
            end
        end

        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of the longitudinal quarter vehicle model.
        %
        %   The coordinate systems of the wheel, vehicle, and ground are coinciding!
        %   The vehicle-fixed coordinate system is: x points forward (driving direction); y points
        %   left; z points upwards.
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %   Chassis ------------------------------------------------------------------------------
        %       X(1) => PosXG:     longitudinal position in ground-fixed cs. [m]
        %       X(2) => VelXG:     longitudinal velocity in ground-fixed cs. [m/s]
        %   Wheel --------------------------------------------------------------------------------
        %       X(3) => PitchY:    angle of the wheel [rad]
        %       X(4) => PitchVelY: angular velocity of the wheel [rad/s]
        %       X(5) => SlipX:     longitudinal slip [1]
        %
        %   U - input vector
        %       U(1) => TorqueDrv: driving torque [Nm]
        %       U(2) => TorqueBrk: braking torque [Nm]
        %       U(3) => Friction:  friction coefficient [1]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %   Chassis ------------------------------------------------------------------------------
        %       Y(1)  => AccXG:     longitudinal acceleration in ground-fixed cs. [m/s^2]
        %       Y(2)  => PitchAccY: angular acceleration [rad/s]
        %   Wheel --------------------------------------------------------------------------------
        %       Y(3)  => DSlipX:    longitudinal slip derivative [1/s]
        %       Y(4)  => ForceXW:   longitudinal force in wheel-fixed cs. [N]
        %       Y(5)  => ForceZW:   vertical force in wheel-fixed cs. [N]
        % ------------------------------------------------------------------------------------------
        function [dX, Y] = StateEquation(self, t, X, U)
            %% Common

            % Vertical force
            Fz = self.Chassis.Mass * Com.Gravitation;

            %% Wheel

            % Inputs
            % U(1) => TorqueDrv: driving torque [Nm]
            % U(2) => TorqueBrk: braking torque [Nm]
            % U(3) => Friction:  friction coefficient [1]
            % U(4) => ForceZW:   vertical force (tire load) in wheel-fixed cs. [N]
            % U(5) => VelXW:     velocity of the wheel center point in wheel-fixed cs. [m/s]

            [dXW, YW] = self.Wheel.StateEquation(t, X(3:5), [U; Fz; X(2)]);

            % Outputs
            % Y(1) => PitchAccY: angular acceleration [rad/s]
            % Y(2) => DSlipX:    longitudinal slip derivative [1/s]
            % Y(3) => ForceXW:   longitudinal force in wheel-fixed cs. [N]

            %% Chassis

            % Inputs
            % U(1) => ForceXG:  longitudinal tire force in ground-fixed cs. [N];

            [dXC, YC] = self.Chassis.StateEquation(t, X(1:2), YW(3));

            % Outputs
            % Y(1)  => AccXG:     longitudinal acceleration in ground-fixed cs. [m/s^2]

            %% Set state derivatives
            dX = [dXC; dXW];

            %% Set outputs
            Y = [YC; YW; Fz];

        end

        %% Simulate --------------------------------------------------------------------------------
        % [Summary]
        %   This function simulates the longitudinal quarter vehicle model.
        %
        % [Input]
        %   initCon  - initial conditions for the vehicle as VehQrLonBndCon object
        %   input    - input as VehQrLonIn object
        %
        % [Output]
        %   output   - output as VehQrLonOut object
        % ------------------------------------------------------------------------------------------
        function output = Simulate(self, initCon, input)
            %% Check arguments
            if ~isa(initCon, 'VehQrLonBndCon')
                error('ERROR: initCon must be a VehQrLonBndCon object!');
            end
            if ~isa(input, 'VehQrLonIn')
                error('ERROR: input must be a VehQrLonIn object!');
            end
            %% Input data for simulation
            % Initial value of state vector
            X0 = [initCon.PosXG;
                  initCon.VelXV;
                  0;
                  initCon.VelXV / self.Wheel.Radius;
                  0];
            % Input
            t = (input.Time(1) : self.SampleTime : input.Time(end))';
            if t(end) ~= input.Time(end)
                t(end + 1) = input.Time(end);
            end
            U = interp1q(input.Time, [input.TorqueDrv, input.TorqueBrk, input.Friction], t)';
            %% Calculation of the motion
            [X, Y] = ode4(self, t, X0, U);
            %% Get results
            output = VehQrLonOut(t, ...
                ChassisQrLonOut(t, X(1,:), X(2,:), Y(1,:)), ...
                WheelMfLonOut(t, X(3,:), X(4,:), Y(2, :), X(5, :), Y(3, :), Y(4, :), Y(5, :)));
        end

    end
end