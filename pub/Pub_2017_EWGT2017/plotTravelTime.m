% Parameters
W = 7;
H = 4;
F = 8;

type = 'LaneKeep';

% Data
if strcmpi(type, 'LaneChange')
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'fc3_lanechange.mat'), 'T_fc_3');
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'co5_lanechange_1_1_1.mat'), 'T_co_5');
elseif strcmpi(type, 'LaneKeep')
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'fc3_lanekeep.mat'), 'T_fc_3');
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', 'co5_lanekeep_10_1_1.mat'), 'T_co_5');
else
    error('Non-existing type.');
end

%% Path
fig1 = nicefigure('TravelTime', W, H);
ax1 = niceaxis(fig1, F);

for i=1:length(T_fc_3)
    tmaxCon(i) = T_fc_3(i).t(end);
    tmaxOpt(i) = T_co_5(i).t(end);
end

yyaxis(ax1, 'left');
plot(ax1, 1:9, tmaxCon, 'LineStyle', '-', 'Color', nicecolors.BlueBelizeHole, 'MarkerSize', 3.0, 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.BlueBelizeHole, 'MarkerFaceColor', nicecolors.BlueBelizeHole);
plot(ax1, 1:9, tmaxOpt, 'LineStyle', '-', 'Color', nicecolors.GreenEmerald, 'MarkerSize', 3.0, 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.GreenEmerald, 'MarkerFaceColor', nicecolors.GreenEmerald);
if strcmpi(type, 'LaneChange')
    ax1.YAxis(1).Limits = [3.2, 3.4];
    ax1.YAxis(1).TickValues = 3.2:0.1:3.4;
elseif strcmpi(type, 'LaneKeep')
    ax1.YAxis(1).Limits = [3, 4.5];
    ax1.YAxis(1).TickValues = 3:0.5:4.5;
end
ax1.YAxis(1).Color = nicecolors.BlueBelizeHole;
ylabel(ax1, '$t_f$ (travel time) [s]');

yyaxis(ax1, 'right');
plot(ax1, 1:9, tmaxCon - tmaxOpt, 'LineStyle', '-', 'Color', nicecolors.RedAlizarin, 'MarkerSize', 3.0, 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.RedAlizarin, 'MarkerFaceColor', nicecolors.RedAlizarin);
if strcmpi(type, 'LaneChange')
    ax1.YAxis(2).Limits = [0, 0.02];
    ax1.YAxis(2).TickValues = 0:0.01:0.02;
elseif strcmpi(type, 'LaneKeep')
    ax1.YAxis(2).Limits = [0, 0.2];
    ax1.YAxis(2).TickValues = 0:0.1:0.2;
end
ax1.YAxis(2).Color = nicecolors.RedAlizarin;
ylabel(ax1, 'difference [s]');

xlim(ax1, [0,10]);
ax1.XTick = 0:10;

xlabel(ax1, '$i$ (no. of trajectory) [1]');

l = nicelegend({'Con', 'ConOpt', 'Con - ConOpt'}, ax1, F);

fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

% Save
name = sprintf('TravelTime_%s', type);
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

