%% PlannerConstrState ******************************************************************************
% [Summary]
%   This class represents state contraints for PlannerCon motion planner. State constraint means the
%   deviation between the required final state and the final state reached with current reference
%   signal parameters.
%
% [Used in]
%   PlannerCon
%   PlannerOpt
%   PlannerCopt
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef PlannerConFcnVehState < handle
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % property names of ChassisStOut
        propNames
        
        %% Properties set in constructor -----------------------------------------------------------
        % required final states
        FinalCon
        % actual state constraint values
        Value
    end

    %% Static methods ==============================================================================
    methods (Static)
        
        %% calculateInequalityConstraint -----------------------------------------------------------
        % [Summary]
        %   This function calculates nonlinear inequality (<=0) state constraints.
        %
        % [Input]
        %   endRegion - vector of 2 elements, the endValue should be between the 2 elements
        %   endValue  - final state value
        % [Output]
        %   nlcon     - value of nonlinear inequality constraint
        % ------------------------------------------------------------------------------------------
        function nlcon = calculateInequalityConstraint(endRegion, endValue)
            nlcon = (endValue - endRegion(1)) .* (endValue - endRegion(2));
        end
        
        %% CircConstr ------------------------------------------------------------------------------
        % [Summary]
        %   This function calculates the final yaw angle, yaw rate, and centripetal acceelration to
        %   the given initial and final conditions for circular path.
        %
        % [Input]
        %   x0  - initial longitudinal position
        %   y0  - initial lateral position
        %   x1  - final longitudinal position
        %   y1  - final lateral position
        %   dx  - longitudinal velocity along path
        %
        % [Output]
        %   rz  - final yaw angle for circular path
        %   drz - final yaw rate for circular path
        %   acp - final centripetal acceleration for circular path
        % ------------------------------------------------------------------------------------------
        function [rz, drz, acp] = CircConstr(x0, y0, x1, y1, dx)
            x = x1 - x0;
            y = y1 - y0;
            % final heading angle
            rz = 2 .* atan(y ./ x);
            % radius of circle
            R = x ./ sin(rz);
            % final yaw rate
            drz = dx ./ R;
            % centripetal acceleration
            acp = dx.^2 ./ R;
        end
        
        %% AccCpEq ---------------------------------------------------------------------------------
        % [Summary]
        %   This function calculates the final lateral position to the given initial and final
        %   conditions for a circular path which has a target centripetal acceleration.
        %
        % [Input]
        %   x0   - initial longitudinal position
        %   y0   - initial lateral position
        %   x1   - final longitudinal position
        %   dx   - longitudinal velocity along path
        %   acpt - target centripetal acceleration
        %
        % [Output]
        %   y1  - final lateral position of circular path with centripetal acceleration a_cpt
        %   acp - final centripetal acceleration for circular path (e.g.: 3.0769)
        % ------------------------------------------------------------------------------------------        
        function [y1, rz, drz, acp] = AccCpEqConstr(x0, y0, x1, dx, acpt)
            opts = optimoptions('fsolve', 'Display', 'None');
            y_i = y0 + 1;
            [y1, acp] = fsolve(@(y_1)acpfun(x0, y0, x1, y_1, dx) - acpt, y_i, opts);
            acp = acpt + acp;
            [rz, drz] = PlannerConstrState.CircConstr(x0, y0, x1, y1, dx);
            function a_cp = acpfun(x_0, y_0, x_1, y_1, dx_1)
                [~, ~, a_cp] = ...
                    PlannerConstrState.CircConstr(x_0, y_0, x_1, y_1, dx_1);
            end
        end
        
        %% GlobalFromLocal -------------------------------------------------------------------------
        % [Summary]
        %   Based on the final condition provided in vehicle-based global coordinate system, this
        %   function calculates the values of the final condition in ground-based global coodinate
        %   system.
        %   
        % [Input]
        %   finalConLocal - final condition in vehicle-based global coordinate system
        %   Xi            - lon. coordinate of origin of vehicle-based gcs. in ground-based gcs.
        %   Yi            - lat. coordinate of origin of vehicle-based gcs. in ground-based gcs.
        %   Ri            - rotation angle of vehicle-based gcs. compared to ground-based gcs.
        %   
        % [Output]
        %   finalConGlobal - final condition in ground-based global coordinate system
        % ------------------------------------------------------------------------------------------        
        function finalConGlobal = GlobalFromLocal(finalConLocal, Xi, Yi, Ri)
            finalConGlobal = ChassisStOut(finalConLocal);
            sin_Ri = sin(Ri);
            cos_Ri = cos(Ri);
            Xf = Xi + cos_Ri * finalConLocal.PosXG - sin_Ri * finalConLocal.PosYG;
            Yf = Yi + sin_Ri * finalConLocal.PosXG + cos_Ri * finalConLocal.PosYG;
            Rf = Ri + finalConLocal.YawZ;
            finalConGlobal.PosXG   = Xf;
            finalConGlobal.PosYG   = Yf;
            finalConGlobal.YawZ    = Rf;
        end
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        
        function set.FinalCon(self, value)
            if ~isequal(self.FinalCon, value)
                if isstruct(value)
                    value = ChassisStOut(value);
                end
                if ~isa(value, 'ChassisStOut')
                    error('ERROR: FinalCon must be a ChassisStOut object!');
                end
                self.FinalCon = value;
            end
        end
        
        function set.Value(self, value)
            if ~isequal(self.Value, value)
                if isstruct(value)
                    value = ChassisStOut(value);
                end
                if ~isa(value, 'ChassisStOut')
                    error('ERROR: Value must be a ChassisStOut object!');
                end
                self.Value = value;
            end
        end
                
        %% Constructor  ----------------------------------------------------------------------------
        
        function self = PlannerConFcnVehState(finalCon)
            %% Check arguments
            if ~(isa(finalCon, 'ChassisStOut'))
                error('ERROR: finalCon must be a ChassisStOut object!');
            end
            %% Initialize
            self.FinalCon = ChassisStOut(finalCon);
            nulls = repmat({0}, 1, 14);
            self.Value = ChassisStOut(nulls{:});
            self.propNames = properties('ChassisStOut');
        end
        
        %% Update ----------------------------------------------------------------------------------
        % [Summary]
        %   This function updates state constaint values according to given vehicle simulation
        %   output.
        %
        % [Input]
        %   chassisOut - output of current vehicle simulation as ChassisStOut object
        %
        % [Output]
        %   none
        % ------------------------------------------------------------------------------------------
        function Update(self, chassisOut, allowInequality)
            %% Default arguments
            if nargin < 3
                allowInequality = false;
            end
            %% Check arguments
            if ~(isa(chassisOut, 'ChassisStOut'))
                error('ERROR: chassisOut most be a ChassisStOut object!');
            end
            %% Calculate state constraints
            for iProp = 1 : length(self.propNames)
                % There is constraint specified
                if ~isnan(self.FinalCon.(self.propNames{iProp}))
                    % Calculate constraint values
                    if (length(self.FinalCon.(self.propNames{iProp})) == 2) && allowInequality
                        % State value must lie inside a specified region
                        self.Value.(self.propNames{iProp}) = self.calculateInequalityConstraint(...
                            self.FinalCon.(self.propNames{iProp}), chassisOut.(self.propNames{iProp})(end));
                    else
                        % Single state value has to be reached.
                        self.Value.(self.propNames{iProp}) = ...
                            self.FinalCon.(self.propNames{iProp})(1) - chassisOut.(self.propNames{iProp})(end);
                    end
                else
                    self.Value.(self.propNames{iProp}) = nan;
                end
            end
        end
        %% ToVector --------------------------------------------------------------------------------
        % [Summary]
        %   This method provides nonlinear equality and inequality constraint vectors as required by
        %   fsolve and fmincon.
        %
        % [Input]
        %   none
        %
        % [Output]
        %   neqCon - nonlinear inequality constraints
        %   eqCon  - nonlinear equality constraints
        % ------------------------------------------------------------------------------------------        
        function [neqCon, eqCon] = ToVector(self, allowInequality)
            %% Default arguments
            if nargin < 2
                allowInequality = false;
            end
            %% Convert to vector
            eqCon = [];
            neqCon = [];
            for iProp = 1 : length(self.propNames)
                if ~isnan(self.FinalCon.(self.propNames{iProp}))
                    if (length(self.FinalCon.(self.propNames{iProp})) == 2) && allowInequality
                        neqCon = [neqCon; self.Value.(self.propNames{iProp})]; %#ok<AGROW>
                    else
                        eqCon = [eqCon; self.Value.(self.propNames{iProp})]; %#ok<AGROW>
                    end
                end
            end
        end
    end
end
