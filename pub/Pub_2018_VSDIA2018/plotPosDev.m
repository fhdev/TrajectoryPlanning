% Load one of VSDIA2018 mat files and run analyzeData for plotting data

% Sizes
W = 7;
H = 4;
F = 8;

fig1 = nicefigure('MaxSlips', W, H);
ax1 = niceaxis(fig1, F);
% Dummy for legend
plot(ax1, nan, nan, 'Color', 'Black', 'LineStyle', '-.');
plot(ax1, nan, nan, 'Color', 'Black', 'LineStyle', '-');
plot(ax1, nan, nan, 'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
plot(ax1, nan, nan, 'Color', nicecolors.GreenEmerald, 'LineStyle', '-');
% Lane change
plot(ax1, accMaxTraj(1:2:end), posDevMaxLin(1:2:end), 'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-.');
plot(ax1, accMaxTraj(1:2:end), posDevMax(1:2:end), 'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
% Lane keep
plot(ax1, accMaxTraj(2:2:end), posDevMaxLin(2:2:end), 'Color', nicecolors.GreenEmerald, 'LineStyle', '-.');
plot(ax1, accMaxTraj(2:2:end), posDevMax(2:2:end), 'Color', nicecolors.GreenEmerald, 'LineStyle', '-');
xlim(ax1, [0, max(accMaxTraj)]);
xlabel(ax1, '$\max(\ddot{y}_{ref})$ (max lat. acc. ref.) [m/s\textsuperscript{2}]');
ylabel(ax1, '$\max(\ddot{y}^V_{v,I})$ (max lat. a.) [m/s]\textsuperscript{2}]');
l = nicelegend({'linear', 'nonlinear', 'lane change', 'lane keep'}, ax1, F);

fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

name = 'MaxPosDev';
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');
