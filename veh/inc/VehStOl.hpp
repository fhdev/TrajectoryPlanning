#pragma once
#ifndef VEHST_HPP
#define VEHST_HPP

#include "Eigen/Dense"
#include "TypesTp.hpp"
#include "ChassisSt.hpp"
#include "WheelMf.hpp"
#include "SteerFo.hpp"
#include "Ode.hpp"

namespace TrajectoryPlanning
{
	class VehStOl : public DynamicSystem
	{
	protected:
		ChassisSt chassis;
		WheelMf wheelFront;
		WheelMf wheelRear;
		SteerFo steering;
		
		mutable Float64_T forceXVFront;
		mutable Float64_T forceXVRear;

	public:

		Int64_T nP() const override;
		Int64_T nX() const override;
		Int64_T nU() const override;
		Int64_T nY() const override;

		VehStOl(const Eigen::Ref<const Eigen::VectorXd> P);

		VehStOl(const ChassisSt &chassis, const WheelMf &wheelFront, const WheelMf &wheelRear, const SteerFo &steering);

		Int32_T	Derivatives(
			Float64_T t, 
			const Eigen::Ref<const Eigen::VectorXd> X,
			const Eigen::Ref<const Eigen::VectorXd> U,
			Eigen::Ref<Eigen::VectorXd> dX, 
			Eigen::Ref<Eigen::VectorXd> Y) const override;

		Int32_T Initialize(
			const Eigen::Ref<const Eigen::VectorXd> Xi, 
			Eigen::Ref<Eigen::VectorXd> X) const;

		Int32_T InitConL(
			const Eigen::Ref<const Eigen::VectorXd> Xi,
			Eigen::Ref<Eigen::VectorXd> X) const;
	};
}

#endif // !VEHST_HPP