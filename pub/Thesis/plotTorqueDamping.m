% To get the data, run model with default parameters and run it with 
%   vehicle.Wheel.RollingResistanceVelCorr = 0.0;
%   vehicle.Wheel.BrakingTorqueVel = 0.0;
%
% withDamping = VehTest.TestManeuver('VehQrLon_Default', 'Maneuver_BrakeStill', false);
% withoutDamping = VehTest.TestManeuver('VehQrLon_Default', 'Maneuver_BrakeStill', false);

% Subsampling factor
subs = @(x) x(1:20:end);

% Create plot
W = 10;
H = 8;
F = 8;
fig = nicefigure('TorqueDamping', W, H);

ax1 = niceaxis(fig, F);
subplot(2, 1, 1, ax1);
hold(ax1, 'on');
plot(ax1, subs(withoutDamping.out.Chassis.Time), subs(withoutDamping.out.Wheel.PitchVelY * withoutDamping.veh.Wheel.Radius * 3.6), ...
    'Color', nicecolors.GreenPureApple, 'LineStyle', '-');
plot(ax1, subs(withDamping.out.Chassis.Time), subs(withDamping.out.Wheel.PitchVelY * withDamping.veh.Wheel.Radius * 3.6), ...
    'Color', nicecolors.RedAlizarin, 'LineStyle', '-');
plot(ax1, subs(withDamping.out.Chassis.Time), subs(withDamping.out.Chassis.VelXG * 3.6), 'Color', nicecolors.BlueBelizeHole, ...
    'LineStyle', '-');
xlabel(ax1, '$t_v$ (time) [s]');
ylabel(ax1, '$\dot{x}^V_v$, $v_{[f/r],r}$ (vel.) [km/h]');
l = nicelegend({'undamped', 'damped', 'vehicle'}, ax1, F);
l.Orientation = 'vertical';
l.Location = 'northeastoutside';

ax2 = niceaxis(fig, F);
subplot(2, 1, 2, ax2);
hold(ax2, 'on');
yyaxis(ax2, 'left');
plot(ax2, subs(withoutDamping.out.Chassis.Time), subs(withoutDamping.out.Wheel.PitchAccY), ...
    'Color', nicecolors.GreenPureApple, 'LineStyle', '-');
plot(ax2, subs(withDamping.out.Chassis.Time), subs(withDamping.out.Wheel.PitchAccY), ...
    'Color', nicecolors.RedAlizarin, 'LineStyle', '-');
ax2.YAxis(1).Color = nicecolors.RedAlizarin;
ax2.YAxis(1).Label.String = '$\ddot{\rho}_{[f/r]}$ (ang. acc.) [rad/s\textsuperscript{2}]';
yyaxis(ax2, 'right');
plot(ax2, subs(withDamping.out.Chassis.Time), subs(withDamping.out.Chassis.AccXG), 'Color', nicecolors.BlueBelizeHole, ...
    'LineStyle', '-');
ax2.YAxis(2).Color = nicecolors.BlueBelizeHole;
ax2.YAxis(2).Label.String = '$\ddot{x}^V_{v,I}$ (lon. acc.) [m/s\textsuperscript{2}]';
xlabel(ax2, '$t_v$ (time) [s]');

ax2.Position(3) = ax1.Position(3) * 0.6;

% Save
name = 'TorqueDamping';
savefig(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');
