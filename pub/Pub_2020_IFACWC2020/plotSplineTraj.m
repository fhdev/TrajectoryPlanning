% Parameters
W = 7;
H = 4;
F = 8;

fig1 = nicefigure('SplinePathRef1', W, H);
ax1 = niceaxis(fig1, F, true);

fig2 = nicefigure('SplinePathRef2', W, H);
ax2 = niceaxis(fig2, F, true);

% Calculation parameters
xb = [0, 80];
yb = [0, 30];
rb1 = [0, 0];
rb2 = [0, deg2rad(30)];

yp1 = [5, 10, 15];

yp2 = [0.5, 3, 5.5;
       8, 12, 16];

vb = [10, 10];

xp1 = linspace(xb(1), xb(2), 2 + size(yp1, 1));
xp1 = xp1(2:end-1);

xp2 = linspace(xb(1), xb(2), 2 + size(yp2, 1));
xp2 = xp2(2:end-1);

n = size(yp1, 2);
c = cell(n, 7);

l = 20;
plot(ax1, [xb(1), xb(1) + l * cos(rb1(1))], [yb(1), yb(1) + l * sin(rb1(1))], 'Color', 'Black', 'LineWidth', 1);
plot(ax1, [xb(2), xb(2) - l * cos(rb1(1))], [yb(2), yb(2) - l * sin(rb1(2))], 'Color', 'Black', 'LineWidth', 1);

plot(ax2, [xb(1), xb(1) + l * cos(rb2(1))], [yb(1), yb(1) + l * sin(rb2(1))], 'Color', 'Black', 'LineWidth', 1);
plot(ax2, [xb(2), xb(2) - l * cos(rb2(1))], [yb(2), yb(2) - l * sin(rb2(2))], 'Color', 'Black', 'LineWidth', 1);

for i = 1 : n
    [ts, s, xs, ys, rs, vs, drs]  = SplineTrajLinVel2(xb, yb, rb1, vb, yp1(:, i));
    plot(ax1, xs, ys, 'Color', nicecolors.PurpleAmethyst);
    plot(ax1, xb, yb, 'MarkerEdgeColor', 'Black', 'MarkerFaceColor', 'Black', 'MarkerSize', 3, 'LineStyle', 'none', 'Marker', 'o');
    plot(ax1, xp1, yp1(:, i), 'MarkerEdgeColor', 'Black', 'MarkerFaceColor', 'Black', 'MarkerSize', 3, 'LineStyle', 'none', 'Marker', 'o');
    
    [ts, s, xs, ys, rs, vs, drs]  = SplineTrajLinVel2(xb, yb, rb2, vb, yp2(:, i));
    plot(ax2, xs, ys, 'Color', nicecolors.PurpleAmethyst);
    plot(ax2, xb, yb, 'MarkerEdgeColor', 'Black', 'MarkerFaceColor', 'Black', 'MarkerSize', 3, 'LineStyle', 'none', 'Marker', 'o');
    plot(ax2, xp2, yp2(:, i), 'MarkerEdgeColor', 'Black', 'MarkerFaceColor', 'Black', 'MarkerSize', 3, 'LineStyle', 'none', 'Marker', 'o'); 
end

axis(ax1, 'equal');
xlim(ax1, xb);
ylim(ax1, yb);
set(ax1, 'XTick', [xb(1), xp1, xb(2)]);
set(ax1, 'XTickLabel', {'$x^G_{v,i}$', '$x^{G,s1}_{ref}$', '$x^G_{v,f}$'});
set(ax1, 'YTick', yb);
set(ax1, 'YTickLabel', {'$y^G_{v,i}$', '$y^G_{v,f}$'});
text(ax1, xp1(1)+2, max(yp1) + 9, '$y^{G,s1}_{ref}$', 'FontSize', 8);
text(ax1, xb(1)+2, yb(1) + 5, '$\psi_{v,i}=0$', 'FontSize', 8);
text(ax1, xb(2)-15, yb(2) + 5, '$\psi_{v,f}=0$', 'FontSize', 8);
xlabel(ax1, '$x^G_{ref}$ [m]');
ylabel(ax1, '$y^G_{ref}$ [m]');

axis(ax2, 'equal');
xlim(ax2, xb);
ylim(ax2, yb);
set(ax2, 'XTick', [xb(1), xp2, xb(2)]);
set(ax2, 'XTickLabel', {'$x^G_{v,i}$', '$x^{G,s1}_{ref}$', '$x^{G,s2}_{ref}$', '$x^G_{v,f}$'});
set(ax2, 'YTick', yb);
set(ax2, 'YTickLabel', {'$y^G_{v,i}$', '$y^G_{v,f}$'});
text(ax2, xp2(1)+2, max(yp2(1, :)) + 8, '$y^{G,s1}_{ref}$', 'FontSize', 8);
text(ax2, xp2(2)+2, max(yp2(2, :)) + 9, '$y^{G,s2}_{ref}$', 'FontSize', 8);
text(ax2, xb(1)+2, yb(1) + 5, '$\psi_{v,i}=0$', 'FontSize', 8);
text(ax2, xb(2)-15, yb(2) + 5, '$\psi_{v,f}\neq0$', 'FontSize', 8);
xlabel(ax2, '$x^G_{ref}$ [m]');
ylabel(ax2, '$y^G_{ref}$ [m]');

% Save
fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

name = 'SplinePathRef1';
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

name = 'SplinePathRef2';
savefig(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

%% Spline interpolation
function [ts, s, xs, ys, rs, vs, drs]  = SplineTrajLinVel2(xb, yb, rb, vb, yp, dx, ds)
%% Default arguments
if nargin < 7
    ds = 0.1;
    if nargin < 6
        dx = 0.01;
    end
end

%% Check arguments
if ~(isnumeric(xb) && isvector(xb) && (length(xb) == 2))
    error('Boundary condition for x (xb) must be a numeric vector with 2 elements.');
end
if ~(isnumeric(yb) && isvector(yb) && (length(yb) == 2))
    error('Boundary condition for y (yb) must be a numeric vector with 2 elements.');
end
if ~(isnumeric(rb) && isvector(rb) && (length(rb) == 2))
    error('Boundary condition for r (rb) must be a numeric vector with 2 elements.');
end
if ~(isnumeric(vb) && isvector(vb) && (length(vb) == 2))
    error('Boundary condition for v (vb) must be a numeric vector with 2 elements.');
end
if ~(isnumeric(yp) && isvector(yp))
    error('Normalized values for y (ypn) must be a numeric vector.');
end
yp = yp(:);
if ~(isnumeric(dx) && isscalar(dx))
    error('Resolution for x (xres) must be a numeric scalar.');
end
if ~(isnumeric(dx) && isscalar(dx))
    error('Resolution for arc length (sres) must be a numeric scalar.');
end

%% Calculate final coordinates in local coordinate system
cos_rb1 = cos(rb(1));
sin_rb1 = sin(rb(1));
xfl = + cos_rb1 * diff(xb) + sin_rb1 * diff(yb);
yfl = - sin_rb1 * diff(xb) + cos_rb1 * diff(yb);

%% Get equidistant samples in local coordinates
nyp = length(yp);
xp = linspace(0, xfl, nyp + 2)';
yp = [0; yp; yfl];

%% Evaluate path and curvature as function of longitudinal coordinate [ y(x), k(x) ]
% longitudinal coordinate domain x
nx = ceil(xfl / dx) + 1;
dxa = xfl / (nx - 1);
x = linspace(0, xfl, nx)';

% path y(x), path derivative y'(x), and path second derivative y''(x) functions
[yx, dyx_dx, ddyx_dx] = interpspline3(xp, yp, x, [0; tan(diff(rb))]);

% curvature function
kx = ddyx_dx ./ ((1 + dyx_dx.^2).^(3 / 2));

%% Calculate position and heading as function of arc length [x(s), y(s), r(s) ]

% calculate arc length as function of lon. position s(x)
sx = cumsum([0; sqrt(dxa^2 + diff(yx).^2)]);

% arc length domain s
ns = ceil(sx(end) / ds) + 1;
dsa = sx(end) / (ns - 1);
s = linspace(0, sx(end), ns)';

% calculate lon. and lat. positions as function of arc length x(s) and y(s)
xsl = interp1q(sx, x, s);
ysl = interp1q(sx, yx, s);
xs = + cos_rb1 * xsl - sin_rb1 * ysl + xb(1);
ys = + sin_rb1 * xsl + cos_rb1 * ysl + yb(1);

% calculate curvature as function of arc length k(s)
ks = interp1q(sx, kx, s);

% calculate heading as function of arc length r(s)
rs = [0; atan(diff(ysl) ./ diff(xsl))] + rb(1);

%% Calculate time, velocity and yaw rate as function of arc length [ t(s), v(s), dr(s) ]
% calculate time as function of arc length t(s)
tf = s(end) / mean(vb);
a = diff(vb) / tf;
if a ~= 0
    % (a/2).t^2 + v0.t - s = 0 -> t
    ts = (-vb(1) + sqrt(vb(1)^2 + 2 * a * s)) / a;
else
    ts = s / vb(1);
end

% calculate velocity and yaw rate as function of arc length dr(s), v(s)
vs = vb(1) + a * ts;
drs = vs .* ks;

end