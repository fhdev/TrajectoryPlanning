classdef PlannerGopt < handle
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % Geometric planner in- and outputs
        plannerGeomOut  % PlannerGeomOut object
        % Constraint function for states
        conGeom        % PlannerConFcnGeom object
        % Cost function for dynamics
        costGeom       % PlannerOptFcnGeom object
        % Optimization variable (parameters of reference signal)
        optVar          % PlannerOptVarVel Path object
        
        
        %% Properties set in constructor -----------------------------------------------------------
        % Solver options
        SolverOptions               % optim.options.Fminunc object

    end
    
    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------        
        function value = setSolverOptions(value)
            if ~isa(value, 'optim.options.Fminunc')
                error('ERROR: SolverOptions must be an optim.options.Fminunc object!')
            end
        end      
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------        
        function set.SolverOptions(self, value)
            if ~isequal(self.SolverOptions, value)
                self.SolverOptions = self.setSolverOptions(value);
            end
        end
        
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerGopt(varargin)
            if nargin == 1 && (isfield(varargin{1}, 'SolverOptions') || isprop(varargin{1}, 'SolverOptions'))
                self.SolverOptions = varargin{1}.SolverOptions;
            elseif nargin == 1
                self.SolverOptions = varargin{1};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end
        
        %% conFcnGeom ------------------------------------------------------------------------------
        % [Summary]
        %   This function calculates and provides state constraint values in format required by 
        %   fsolve.
        %   
        % [Input]
        %   input    - planner input as PlannerConIn object
        %   optVarV  - optimization variable in a vector as provided by fsolve
        %   
        % [Output]
        %   eqCon    - nonlinear equality constraint vector as required by fsolve
        % ------------------------------------------------------------------------------------------
        function eqCon = conFcnGeom(self, input, optVarV)
            % Update optimized variable
            newOptVarFlag = self.optVar.Update(optVarV);
            % Run only at first time or if optimized variable changed
            if newOptVarFlag || isempty(self.plannerGeomOut)
                % Perform new geometric planning
                self.plannerGeomOut = self.optVar.ToPlannerGeomOut(input);
            end
            %% Return with updated state constraint vector
            self.conGeom.Update(self.plannerGeomOut);
            [~, eqCon] = self.conGeom.ToVector();
        end
        
        %% optFcnState -----------------------------------------------------------------------------
        % [Summary]
        %   This function calculates and provides state related costs as it is required by Matlab
        %   fminunc and fmincon.
        %   
        % [Input]
        %   input    - planner input as PlannerOptIn object
        %   optVarV  - optimization variable in a vector as provided by fsolve
        %   
        % [Output]
        %   cost     - scalar cost value for fminunc or fmincon
        % ------------------------------------------------------------------------------------------        
        function cost = optFcnGeom(self, input, optVarV)
            % Update optimized variable
            newOptVarFlag = self.optVar.Update(optVarV);
            % Run only at first time or if optimized variable changed
            if newOptVarFlag || isempty(self.plannerGeomOut)
                % Perform new geometric planning
                self.plannerGeomOut = self.optVar.ToPlannerGeomOut(input);
            end
            %% Return with state constraint vector
            self.costGeom.Update(self.plannerGeomOut);
            cost = self.costGeom.ToScalar();
        end
        
        %% CalculateTrajectory ---------------------------------------------------------------------
        % [Summary]
        %   This function calculates a vehicle trajectory according to the provided input.
        %
        % [Input]
        %   input  - planner input signals as PlannerConIn object
        %   
        % [Output]
        %   output - planner output signals as PlannerConOut object
        % ------------------------------------------------------------------------------------------
        function output = CalculateTrajectory(self, input)
            %% Check arguments
            % Serialize input 
            input = input(:);
            if ~(isa(input, 'PlannerGoptIn') && isscalar(input))
                error('ERROR: input must be a PlannerGoptIn object!');
            end
            %% Plan trajectory
            % Message to Command Window
            fprintf([repmat('=', 1, 120), '\n']);
            fprintf('Planning a constrained trajectory to x = %.4g m, y = %.4g m, r = %.4g rad, dr = %.4g rad ...\n', ...
                    input.FinalCon.PosXG(1), ...
                    input.FinalCon.PosYG(1), ...
                    input.FinalCon.YawZ(1), ...
                    input.FinalCon.YawVelZ(1));
            % Initialize
            self.conGeom = PlannerConFcnGeom(input.FinalCon);
            self.costGeom = PlannerOptFcnGeom(input.CostGeomWeight);
            self.optVar = PlannerOptVarVelPath(input.OptVarInit);
            % Reset storage for geometric planner output
            self.plannerGeomOut = [];
            % Run solver
            optVarInitV = input.OptVarInit.ToVector();
            F = @(optVarV)self.optFcnGeom(input, optVarV);
            tic;
            [optVarOptV, optFcnOpt, exitFlag] = fminunc(F, optVarInitV, self.SolverOptions);
            self.optFcnGeom(input, optVarOptV);
            eqConOpt = self.conFcnGeom(input, optVarOptV);
            ellapsedTime = toc;
            if exitFlag > 0
                fprintf('Success.\n');
            else
                fprintf('Failure.\n');
            end
            fprintf('Planning time:          %.4g\n', ellapsedTime);
            fprintf('Equality constraint:    %s\n', mat2str(eqConOpt, 4));
            fprintf('Optimization variable:  %s\n', mat2str(optVarOptV, 4));
            fprintf('Optimization function:  %.4g\n', optFcnOpt);
            %% Set output
            output = PlannerGoptOut(ellapsedTime, exitFlag > 0, self.optVar, ...
                self.plannerGeomOut, self.conGeom, self.costGeom);
        end    

    end
end
