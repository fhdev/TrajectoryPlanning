classdef PlannerTest
    methods (Static)
        
        function TestAll()
            plOut = PlannerTest.TestManeuver('PlannerCon_Default', 'PlannerIn_LaneChange', true);
            plOut = PlannerTest.TestManeuver('PlannerCon_Default', 'PlannerIn_LaneKeep', true);
            plOut = PlannerTest.TestManeuver('PlannerOpt_Default', 'PlannerIn_LaneChange', true);
            plOut = PlannerTest.TestManeuver('PlannerOpt_Default', 'PlannerIn_LaneKeep', true);
            plOut = PlannerTest.TestManeuver('PlannerCopt_Default', 'PlannerIn_LaneChange', true);
            plOut = PlannerTest.TestManeuver('PlannerCopt_Default', 'PlannerIn_LaneKeep', true);
            plOut = PlannerTest.TestManeuver('PlannerGopt_Default', 'PlannerIn_LaneChange', true);
            plOut = PlannerTest.TestManeuver('PlannerGopt_Default', 'PlannerIn_LaneKeep', true);
        end
        
        function test = TestManeuver(paramSet, inputName, doPlot)
            if (nargin < 3) || isempty(doPlot)
                doPlot = true;
            end
            if (nargin < 2) || isempty(paramSet)
                paramSet = 'PlannerCon_Default';
            end
            if (nargin < 1) || isempty(inputName)
                inputName = 'PlannerIn_LaneChange';
            end
            

            planner = PlannerTest.CreatePlanner(paramSet);
            in = PlannerTest.CreatePlannerInput(paramSet, inputName);
            out = planner.CalculateTrajectory(in);
            
            if(doPlot)
                switch paramSet
                    case {'PlannerCon_Default', 'PlannerOpt_Default', 'PlannerCopt_Default'}
                        out.DisplayPlots();
                        planner.Vehicle.DisplayPlots(out.VehIn, out.VehOut);
                    case 'PlannerGopt_Default'
                        out.PlannerGeomOut.DisplayPlots();
                    otherwise
                        error('Unknown param set %s.', paramSet);
                end
            end
            
            test.in = in;
            test.planner = planner;
            test.out = out;
        end
        
        function planner = CreatePlanner(paramSet)
            switch paramSet
                case 'PlannerCon_Default'
                    planner = PlannerCon(PlannerCon_Default());
                case 'PlannerOpt_Default'
                    planner = PlannerOpt(PlannerOpt_Default());
                case 'PlannerCopt_Default'
                    planner = PlannerCopt(PlannerCopt_Default());
                case 'PlannerGopt_Default'
                    planner = PlannerGopt(PlannerGopt_Default());
                otherwise
                    error('Unknown param set %s.', paramSet);
            end
        end
        
        function in = CreatePlannerInput(paramSet, inputName)
            switch inputName
                case 'PlannerIn_LaneChange'
                    in = PlannerIn_LaneChange();
                case 'PlannerIn_LaneKeep'
                    in = PlannerIn_LaneKeep();
                otherwise
                    error('Unknown input name %s.', inputName);
            end
            switch paramSet
                case 'PlannerCon_Default'
                    in = PlannerConIn(in);
                case 'PlannerOpt_Default'
                    in = PlannerOptIn(in);
                case 'PlannerCopt_Default'
                    in = PlannerCoptIn(in);
                case 'PlannerGopt_Default'
                    in = PlannerGoptIn(in);
                otherwise
                    error('Unknown param set %s.', paramSet);
            end
        end
        
    end
end