import ctypes as c
import numpy as np

lib = c.CDLL('../bin/VehStOlPyL/VehStOlPyL.dll')
lib.dummy.argtypes = [c.c_double, c.POINTER(c.c_double), c.POINTER(c.c_double)]
lib.dummy.restype = c.c_double

scalarIn = 1
vectIn = np.array([1, 2]);
vectInC = (c.c_double * 2)(*vectIn.tolist())
vectOutLen = 2;
vectOutC = (c.c_double * vectOutLen)(*([0] * vectOutLen))
scalarOut = lib.dummy(scalarIn, vectInC, vectOutC)
vectOut = np.ctypeslib.as_array(vectOutC)

print("scalar input: ")
print(scalarIn)
print("vector input: ")
print(vectIn)
print("scalar output: ")
print(scalarOut)
print("vector output: ")
print(vectOut)


