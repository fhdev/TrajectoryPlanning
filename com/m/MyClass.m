%% MyClass *****************************************************************************************
% [Summary]
%   This class 
%
% [Used in]
%   
%
% [Subclasses]
%   
%
% **************************************************************************************************
classdef MyClass < handle
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        myInternalProp
        
        %% Properties set in constructor -----------------------------------------------------------
        MyPublicProp

    end
    
    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------
        function value = setMyPublicProp(value)

        end    
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.MyPublicProp(self, value)
            
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = MyClass(varargin)
            
        end
        
        %% MyFcn -----------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------
        function y = MyFcn(u)
            y = u;
        end
        
    end
end