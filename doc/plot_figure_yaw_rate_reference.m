% Clear and close all
clear all;
close all;
% Set default interpreter to LaTeX
set(0,'DefaultTextInterpreter','latex');

% Degree of polynomial
n = 5;
% Duration (travel time)
t_f = 5; % [s]
% Spline knot points 
p = [1, 2, 2, -2, -2, 0]'; %[rad/s]
% Step size (duration between knot points)
t_s = t_f / n; % [s]
% Transformation matrix from polynomial coefficients to knot points
T = [1,           0,                0,                0,                0,                0;              
     1,    1 .* t_s,    1 .* t_s .^ 2,    1 .* t_s .^ 3,    1 .* t_s .^ 4,    1 .* t_s .^ 5;
     1,    2 .* t_s,    4 .* t_s .^ 2,    8 .* t_s .^ 3,   16 .* t_s .^ 4,   32 .* t_s .^ 5;
     1,    3 .* t_s,    9 .* t_s .^ 2,   27 .* t_s .^ 3,   81 .* t_s .^ 4,  243 .* t_s .^ 5;
     1,    4 .* t_s,   16 .* t_s .^ 2,   64 .* t_s .^ 3,  256 .* t_s .^ 4, 1024 .* t_s .^ 5;
     1,    5 .* t_s,   25 .* t_s .^ 2,  125 .* t_s .^ 3,  625 .* t_s .^ 4, 3125 .* t_s .^ 5];
% Transform knot point parameters to polynomial coefficients c = T^-1 * p
c = T \ p;
% Time sample points to plot
t = 0:0.01:5;
% Yaw rate curve to plot
psi = c(6).*t.^5 + c(5).*t.^4 + c(4).*t.^3 + c(3).*t.^2 + c(2).*t + c(1);

% Create new figure
figure('Name', 'Parametrized yaw-rate input', 'NumberTitle', 'off', 'Color', 'White');
% Set plot size (9.25 cm witdh to fit two column article page)
set(gcf,'Units','Centimeters','Position',[10, 10, 8, 5], 'PaperUnits','Centimeters','PaperPosition',[10, 10, 8, 5]);
hold on;
% Plot curve
plot(t, psi, 'LineStyle', '-', 'LineWidth', 1.0, 'Color', 'Blue');
% Plot knot points
plot(0, p(1), 'o', 'MarkerEdgeColor', [0 0 1], 'MarkerFaceColor', [0 0 1], 'MarkerSize', 2.0);
plot(1, p(2), 'o', 'MarkerEdgeColor', [0 0 1], 'MarkerFaceColor', [0 0 1], 'MarkerSize', 2.0);
plot(2, p(3), 'o', 'MarkerEdgeColor', [0 0 1], 'MarkerFaceColor', [0 0 1], 'MarkerSize', 2.0);
plot(3, p(4), 'o', 'MarkerEdgeColor', [0 0 1], 'MarkerFaceColor', [0 0 1], 'MarkerSize', 2.0);
plot(4, p(5), 'o', 'MarkerEdgeColor', [0 0 1], 'MarkerFaceColor', [0 0 1], 'MarkerSize', 2.0);
plot(5, p(6), 'o', 'MarkerEdgeColor', [0 0 1], 'MarkerFaceColor', [0 0 1], 'MarkerSize', 2.0);
% Set axis limits
axis([0, 5, -3, 3]);
% Grid and box on
grid on;
box on;
% Set latex interpreter
set(gca, 'DefaultTextInterpreter', 'LaTex', 'FontSize', 10, 'LabelFontSizeMultiplier', 1, 'TitleFontSizeMultiplier', 1);
% Axis labels
xlabel('Time','FontSize',10);
ylabel('Yaw rate','FontSize',10);
% Text labels for knot points
text(0.1,1.4,'$\omega_0$','FontSize',10);
text(1.1,1.6,'$\omega_1$','FontSize',10);
text(2.1,2.3,'$\omega_2$','FontSize',10);
text(3.1,-1.7,'$\omega_3$','FontSize',10);
text(4.1,-2.3,'$\omega_4$','FontSize',10);
text(4.7,-0.4,'$\omega_5$','FontSize',10);
% X tick labels
xdata = [0 1 2 3 4 5];
xlabels = {'$0$', '$\frac{1}{5}t_f$', '$\frac{2}{5}t_f$', '$\frac{3}{5}t_f$', '$\frac{4}{5}t_f$', '$t_f$'};
% Y tick labels
ydata =[-3 -2 -1 0 1 2 3];
ylabels = {'', '', '', '', '', '', ''};
% Set tick labels
set(gca, 'Xtick', xdata, 'XtickLabel', xlabels, 'TickLabelInterpreter', 'latex');
set(gca, 'Ytick', ydata, 'YtickLabel', ylabels, 'TickLabelInterpreter', 'latex');
print('yaw_rate_reference.eps', '-depsc');