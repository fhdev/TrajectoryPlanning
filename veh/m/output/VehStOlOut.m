%% VehStOlOut **************************************************************************************
% [Summary]
%   This class represents output for open-loop single track vehicle model. 
%
% [Used in]
%   VehStOl
%
% [Subclasses]
%   VehStClOut
% **************************************************************************************************
classdef VehStOlOut < VehBaseOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Front wheel related output (WheelMfExtOut object)
        WheelFront
        % Rear wheel related output (WheelMfExtOut object)
        WheelRear
        % Steering related output (SteerFoOut object)
        Steering

    end
    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------
        function value = setChassis(value)
            if isstruct(value)
                value = ChassisStOut(value);
            end
            if ~isa(value, 'ChassisStOut')
                error('ERROR: Chassis must be a ChassisStOut object!')
            end
        end

        function value = setWheel(value)
            if isstruct(value)
                value = WheelMfExtOut(value);
            end
            if ~isa(value, 'WheelMfExtOut')
                error('ERROR: Wheel must be a struct or a WheelMfExtOut object!')
            end
        end
        
        function value = setSteering(value)
            if isstruct(value)
                value = SteerFoOut(value);
            end
            if ~isa(value, 'SteerFoOut')
                error('ERROR: Steering must be a struct or a SteerFoOut object!')
            end
        end

    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.WheelFront(self, value)
            if ~isequal(self.WheelFront, value)
                self.WheelFront = self.setWheel(value);
            end
        end

        function set.WheelRear(self, value)
            if ~isequal(self.WheelRear, value)
                self.WheelRear = self.setWheel(value);
            end
        end
        
        function set.Steering(self, value)
            if ~isequal(self.Steering, value)
                self.Steering = self.setSteering(value);
            end
        end
        %% Constructor -----------------------------------------------------------------------------
        function self = VehStOlOut(varargin)
            if nargin == 1
                vehBaseOutAgs = varargin(1);
            elseif nargin == 5
                vehBaseOutAgs = varargin(1:2);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@VehBaseOut(vehBaseOutAgs{:});
            if nargin == 1
                self.WheelFront = varargin{1}.WheelFront;
                self.WheelRear  = varargin{1}.WheelRear;
                self.Steering   = varargin{1}.Steering;
            elseif nargin == 5
                self.WheelFront = varargin{3};
                self.WheelRear  = varargin{4};
                self.Steering   = varargin{5};
            end
        end
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the quarter vehicle model.
        %
        % [Input]
        %       name   - name for the plots
        % [Output]
        %       figWheelDynF - figure handle of plot containing wheel dynamics related quantities
        %       figWheelInF  - figure handle of plot containing wheel input related quantities
        %       figWheelDynR - figure handle of plot containing wheel dynamics related quantities
        %       figWheelInR  - figure handle of plot containing wheel input related quantities
        %       figGlob      - figure handle of plot containing chassis related quantities in global
        %       figLoc       - figure handle of plot containing chassis related quantities in local
        %       figPath      - figure handle of vehicle path plot
        % ------------------------------------------------------------------------------------------
        function [figWheelDynF, figWheelInF, figWheelDynR, figWheelInR, figGlob, figLoc, figPath] = ...
                DisplayPlots(self, name)
            %% Default arguments
            if nargin < 2
                name = class(self);
            end

            %% Create plots
            [figWheelDynF, figWheelInF] = self.WheelFront.DisplayPlots([name, '/WheelFront']);
            [figWheelDynR, figWheelInR] = self.WheelRear.DisplayPlots([name, '/WheelRear']);
            [figGlob, figLoc, figPath] = self.Chassis.DisplayPlots([name, '/Chassis']);
            % Activate last plot
            figure(figPath);
        end
    end
end