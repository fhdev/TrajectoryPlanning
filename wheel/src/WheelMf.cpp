#include <cstdlib>
#include <algorithm>
#include "../inc/WheelMf.hpp"
#include "../../com/inc/MathTp.hpp"

using namespace Eigen;
using namespace std;

// Parameters ======================================================================================
// wheel radius [m]
#define Radius_P                   (P(0))
// moment of inertia [kgm^2]
#define MomentOfInertiaY_P         (P(1))
// longitudinal max value (Dx) [1]
#define MfMaxX_P				   (P(2))
// longitudinal stiffness factor (Bx) [1]
#define MfStiffnessX_P             (P(3))
// longitudinal shape factor (Cx) [1]
#define MfShapeX_P                 (P(4))
// longitudinal curvature factor (Ex [1]
#define MfCurvatureX_P             (P(5))
// velocity independent rolling resistance factor [1]
#define RollingResistanceConst_P   (P(6))
// linearly velocity dependent rolling resistance factor [s/m]
#define RollingResistanceLin_P     (P(7))
// squarely velocity dependent rollint resistance factor [s^2/m^2]
#define RollingResistanceSqr_P     (P(8))
// longitudinal velocity at which rolling resistance reaches it's final value [m/s]
#define RollingResistanceVel_P     (P(9))
// braking torque dependent correction in velocity at which rolling resistance reaches
// it's final value [1/Ns]
#define RollingResistanceVelCorr_P (P(10))
// longitudinal velocity at which braking torque reaches it's final value [m/s]
#define BrakingTorqueVel_P         (P(11))
// longitudinal relaxation length at zero longitudinal slip [m]
#define RelaxationLength0X_P       (P(12))
// minimal longitudinal relaxation length [m]
#define RelaxationLengthMinX_P     (P(13))
// standstill longitudinal slip damping [Ns/m]
#define SlipDamping0X_P            (P(14))
// longitudinal velocity at which slip damping switches off [m/s^2]
#define VelSlowX_P                 (P(15))
// lateral max value (Dy) [1]
#define MfMaxY_P				   (P(16))
// lateral stiffness factors (By) [1]
#define MfStiffnessY_P             (P(17))
// lateral shape factors (Cy) [1]
#define MfShapeY_P                 (P(18))
// lateral curvature factors (Ey) [1]
#define MfCurvatureY_P             (P(19))
// relaxation length at zero lateral slip in the lateral direction [m]
#define RelaxationLength0Y_P       (P(20))
// minimal lateral relaxation length [m]
#define RelaxationLengthMinY_P     (P(21))
// standstill lateral slip damping [Ns/m]
#define SlipDamping0Y_P            (P(22))
// lateral velocity at which slip damping switches off [m/s^2]
#define VelSlowY_P                 (P(23))
// minimal slip at which slip superposition is considered [1]
#define MinSlip_P                  (P(24))
// number of parameters
#define NP 25

// Inputs ==========================================================================================
// driving torque [Nm]
#define TorqueDrv_U    (U(0))
// braking torque [Nm]
#define TorqueBrk_U    (U(1))
// friction coefficient [1]
#define Friction_U     (U(2))
// vertical force (tire load) in wheel-fixed coordinate system [N]
#define ForceZW_U      (U(3))
// lon. velocity of the wheel center point in wheel-fixed coordinate system [m/s] 
#define VelXW_U        (U(4))
// lat. velocity of the wheel center point in wheel-fixed coordinate system [m/s] 
#define VelYW_U        (U(5))
// number of inputs
#define NU 6

// States and derivatives ==========================================================================
// angle of the wheel [rad]
#define PitchY_X      (X(0))
#define DPitchY_dX    (dX(0))
// angular velocity of the wheel [rad/s]
#define PitchVelY_X   (X(1))
#define DPitchVelY_dX (dX(1))
// longitudinal slip[1]
#define SlipX_X       (X(2))
#define DSlipX_dX     (dX(2))
// lateral slip[1]
#define SlipY_X       (X(3))
#define DSlipY_dX     (dX(3))
// number of states
#define NX 4

// Outputs =========================================================================================
// angular acceleration [rad/s]
#define PitchAccY_Y (Y(0))
// longitudinal slip derivative [1/s]
#define DSlipX_Y    (Y(1))
// longitudinal force in wheel - fixed coordinate system [N]
#define ForceXW_Y   (Y(2))
// lateral slip derivative [1/s]
#define DSlipY_Y    (Y(3))
// lateral force in wheel - fixed coordinate system [N]
#define ForceYW_Y   (Y(4))
// vertical force in wheel - fixed coordinate system [N]
#define ForceZW_Y   (Y(5))
// wheel level driving torque [Nm]
#define TorqueDrv_Y (Y(6))
// wheel level braking torque [Nm]
#define TorqueBrk_Y (Y(7))
// wheel level steering angle [rad]
#define SteerAng_Y  (Y(8))
// number of outputs
#define NY 9

namespace TrajectoryPlanning
{
	Float64_T WheelMf::Radius() const { return radius; }

	Int64_T WheelMf::nP() const { return NP; }
	Int64_T WheelMf::nX() const { return NX; }
	Int64_T WheelMf::nU() const { return NU; }
	Int64_T WheelMf::nY() const { return NY; }

	WheelMf::WheelMf(const Eigen::Ref<const Eigen::VectorXd> P) :

		radius(Radius_P), 
		momentOfInertiaY(MomentOfInertiaY_P),
		mfMaxX(MfMaxX_P),
		mfStiffnessX(MfStiffnessX_P),
		mfShapeX(MfShapeX_P),
		mfCurvatureX(MfCurvatureX_P),
		rollingResistanceConst(RollingResistanceConst_P),
		rollingResistanceLin(RollingResistanceLin_P),
		rollingResistanceSqr(RollingResistanceSqr_P),
		rollingResistanceVel(RollingResistanceVel_P),
		rollingResistanceVelCorr(RollingResistanceVelCorr_P),
		brakingTorqueVel(BrakingTorqueVel_P),
		relaxationLength0X(RelaxationLength0X_P),
		relaxationLengthMinX(RelaxationLengthMinX_P),
		slipDamping0X(SlipDamping0X_P),
		velSlowX(VelSlowX_P),
		mfMaxY(MfMaxY_P),
		mfStiffnessY(MfStiffnessY_P),
		mfShapeY(MfShapeY_P),
		mfCurvatureY(MfCurvatureY_P),
		relaxationLength0Y(RelaxationLength0Y_P),
		relaxationLengthMinY(RelaxationLengthMinY_P),
		slipDamping0Y(SlipDamping0Y_P),
		velSlowY(VelSlowY_P),
		minSlip(MinSlip_P)
	{
	}

	WheelMf::WheelMf(
		Float64_T radius,
		Float64_T momentOfInertiaY,
		Float64_T mfMaxX,
		Float64_T mfStiffnessX,
		Float64_T mfShapeX,
		Float64_T mfCurvatureX,
		Float64_T rollingResistanceConst,
		Float64_T rollingResistanceLin,
		Float64_T rollingResistanceSqr,
		Float64_T rollingResistanceVel,
		Float64_T rollingResistanceVelCorr,
		Float64_T brakingTorqueVel,
		Float64_T relaxationLength0X,
		Float64_T relaxationLengthMinX,
		Float64_T slipDamping0X,
		Float64_T velSlowX,
		Float64_T mfMaxY,
		Float64_T mfStiffnessY,
		Float64_T mfShapeY,
		Float64_T mfCurvatureY,
		Float64_T relaxationLength0Y,
		Float64_T relaxationLengthMinY,
		Float64_T slipDamping0Y,
		Float64_T velSlowY,
		Float64_T minSlip) :

		radius(radius),
		momentOfInertiaY(momentOfInertiaY),
		mfMaxX(mfMaxX),
		mfStiffnessX(mfStiffnessX),
		mfShapeX(mfShapeX),
		mfCurvatureX(mfCurvatureX),
		rollingResistanceConst(rollingResistanceConst),
		rollingResistanceLin(rollingResistanceLin),
		rollingResistanceSqr(rollingResistanceSqr),
		rollingResistanceVel(rollingResistanceVel),
		rollingResistanceVelCorr(rollingResistanceVelCorr),
		brakingTorqueVel(brakingTorqueVel),
		relaxationLength0X(relaxationLength0X),
		relaxationLengthMinX(relaxationLengthMinX),
		slipDamping0X(slipDamping0X),
		velSlowX(velSlowX),
		mfMaxY(mfMaxY),
		mfStiffnessY(mfStiffnessY),
		mfShapeY(mfShapeY),
		mfCurvatureY(mfCurvatureY),
		relaxationLength0Y(relaxationLength0Y),
		relaxationLengthMinY(relaxationLengthMinY),
		slipDamping0Y(slipDamping0Y),
		velSlowY(velSlowY),
		minSlip(minSlip)
	{
	}

	Int32_T WheelMf::Derivatives(
		Float64_T t, 
		const Ref<const VectorXd> X,
		const Ref<const VectorXd> U,
		Ref<VectorXd> dX, 
		Ref<VectorXd> Y) const
	{
		// References: [Pacejka]: Hans Pacejka - Tyre and Vehicle Dynamics (2nd edition) 2006.
		//             [Schramm]: Dieter Schramm - Vehicle Dynamics 2014

        // Check sizes
		CheckSizes(X, U, dX, Y);

		// Calculation of necessary variables 

		// Calculation of maximal transmissable forces
		Float64_T forceXWMax = mfMaxX * Friction_U * ForceZW_U;
		Float64_T forceYWMax = mfMaxY * Friction_U * ForceZW_U;

		// Calculation of slip stiffness values ([Pacejka] Eq. 8.115 @ P. 405 & Eq. 1.6 @ P. 6)
		Float64_T slipStiffnessX = mfStiffnessX * mfShapeX * forceXWMax;
		Float64_T slipStiffnessY = mfStiffnessY * mfShapeY * forceYWMax;

		// Calculation of slip damping factor([Pacejka] Eq. 8.128 @ P. 407)
		Float64_T velXWAbs = abs(VelXW_U);
		Float64_T slipDampingX = (velXWAbs <= velSlowX) ? (0.5 * slipDamping0X * (1 + cos(PI * velXWAbs / velSlowX))) : (0.0);

		// Calculation of damped slip values([Pacejka] Eq. 8.127 @ P. 407)
		Float64_T velRollW = radius * PitchVelY_X;
		Float64_T velRollWAbs = abs(velRollW);
		Float64_T velRollWSign = sign(velRollW);
		Float64_T slipDampedX = SlipX_X + slipDampingX / slipStiffnessX * (velRollW - VelXW_U);
		Float64_T slipDampedY = SlipY_X;

		if ((abs(slipDampedX) > minSlip) && (abs(slipDampedY) > minSlip))
		{
			// Calculation of tire forces in wheel - fixed coordinate system
			// ([Pacejka] Eq. 8.109 @ P. 404) ([Schramm] Eq. 7.49 @ P. 166)
			Float64_T slipDampedX2 = SQR(slipDampedX);
			Float64_T slipDampedY2 = SQR(slipDampedY);
			Float64_T slipCombined = sqrt(slipDampedX2 + slipDampedY2);
			ForceXW_Y = forceXWMax * sin(mfShapeX * atan(mfStiffnessX * slipCombined -
				mfCurvatureX * (mfStiffnessX * slipCombined - atan(mfStiffnessX * slipCombined))));
			ForceYW_Y = forceYWMax * sin(mfShapeY * atan(mfStiffnessY * slipCombined -
				mfCurvatureY * (mfStiffnessY * slipCombined - atan(mfStiffnessY * slipCombined))));

			// Calculation of slip superposition (own deduction)
			Float64_T forceXW2 = SQR(ForceXW_Y);
			Float64_T forceYW2 = SQR(ForceYW_Y);
			ForceXW_Y = sqrt(forceXW2 * forceYW2 / (forceYW2 + slipDampedY2 / slipDampedX2 * forceXW2)) *
				sign(slipDampedX);
			ForceYW_Y = sqrt(forceXW2 * forceYW2 / (forceXW2 + slipDampedX2 / slipDampedY2 * forceYW2)) *
				sign(slipDampedY);
		}
		else
		{
			// Calculation of tire forces in wheel - fixed coordinate system
			// ([Pacejka] Eq. 8.109 @ P. 404)
			ForceXW_Y = forceXWMax * sin(mfShapeX * atan(mfStiffnessX * slipDampedX -
				mfCurvatureX * (mfStiffnessX * slipDampedX - atan(mfStiffnessX * slipDampedX))));
			ForceYW_Y = forceYWMax * sin(mfShapeY * atan(mfStiffnessY * slipDampedY -
				mfCurvatureY * (mfStiffnessY * slipDampedY - atan(mfStiffnessY * slipDampedY))));
		}
		
		// Calculation of rolling resistance torque
		// (https://www.mathworks.com/help/physmod/sdl/ref/rollingresistance.html)
		Float64_T torqueRollRes = velRollWSign * ForceZW_U * radius * (rollingResistanceConst +
			rollingResistanceLin * velRollWAbs + rollingResistanceSqr * SQR(velRollW));
		if (velRollWAbs < rollingResistanceVel)
		{
			torqueRollRes = torqueRollRes * 0.5 * (1 - cos(PI * velRollWAbs / rollingResistanceVel));
		}

		// Calculation of acting braking torque
		Float64_T torqueBrakeAct = TorqueBrk_U * velRollWSign;
		Float64_T brakingTorqueVelAct = brakingTorqueVel + rollingResistanceVelCorr * abs(TorqueBrk_U);
		if (velRollWAbs < brakingTorqueVelAct)
		{
			torqueBrakeAct = torqueBrakeAct * 0.5 * (1 - cos(PI * velRollWAbs / brakingTorqueVelAct));
		}

		// Calculation of slip - dependent relaxation lengths (Eq. 8.121 @ P. 406 & Eq. 7.36 @ P. 349)
		Float64_T relaxationLengthX = max(relaxationLength0X * (1 - slipStiffnessX / (3 * forceXWMax) *
			abs(SlipX_X)), relaxationLengthMinX);
		Float64_T relaxationLengthY = max(relaxationLength0Y * (1 - slipStiffnessY / (3 * forceYWMax) *
			abs(SlipY_X)), relaxationLengthMinY);

		// Calculation of state derivatives
		// Canculation of agular velocity
		DPitchY_dX = PitchVelY_X;
		// Canculation of agular acceleration
		DPitchVelY_dX = (TorqueDrv_U - torqueBrakeAct - radius * ForceXW_Y - torqueRollRes) / momentOfInertiaY;
		// Calculation of transient longitudinal slip (Eq. 8.119 @ P. 406)
		DSlipX_dX = (velRollW - VelXW_U - velXWAbs * SlipX_X) / relaxationLengthX;
		// Calculation of transient lateral slip (Eq. 7.35 @ P. 349)
		DSlipY_dX = (-VelYW_U - velXWAbs * SlipY_X) / relaxationLengthY;

		// Storage of output variables
		PitchAccY_Y = DPitchVelY_dX;
		DSlipX_Y = DSlipX_dX;
		// ForceXW_YW assigned previously
		DSlipY_Y = DSlipY_dX;
		// ForceXW_YW assigned previously

		return 0;
	}
}
