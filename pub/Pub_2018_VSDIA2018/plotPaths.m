% Load one of VSDIA2018 mat files and run analyzeData for plotting data

% Subsampling factor
subs = @(x) [x(1:5:end); x(end)];

W = 7;
H = 4;
F = 8;

fig1 = nicefigure('PolyTrajsLaneChange', W, H);
fig2 = nicefigure('PolyTrajsLaneKeep', W, H);
ax1 = niceaxis(fig1, F);
ax2 = niceaxis(fig2, F);
for i = 1:length(t)
    if(mod(i,2)==1)
        % Lane change
        plot(ax1, subs(t(i).PositionX_GCS), subs(t(i).PositionY_GCS), 'Color', nicecolors.BlueBelizeHole);
    else
        % Lane keep
        plot(ax2, subs(t(i).PositionX_GCS), subs(-t(i).PositionY_GCS), 'Color', nicecolors.GreenEmerald);
    end
end
axis(ax1, 'equal');
axis(ax2, 'equal');
xlim(ax1, [0, 50]);
ylim(ax1, [0, 35]);
xlim(ax2, [0, 50]);
ylim(ax2, [-35, 0]);
ax1.Position = [0.16, 0.18, 0.74, 0.79];
ax2.Position = [0.16, 0.18, 0.74, 0.79];
xlabel(ax1, '$x^G_v$ (pos. north) [m]');
ylabel(ax1, '$y^G_v$ (pos. west) [m]');
xlabel(ax2, '$x^G_v$ (pos. north) [m]');
ylabel(ax2, '$y^G_v$ (pos. west) [m]');

name = 'PolyTrajsLaneChange';
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

name = 'PolyTrajsLaneKeep';
savefig(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');