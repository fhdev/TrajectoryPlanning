
% Clean up
clearvars();

%% Parameters
DATA_FOLDER = fullfile('E:', 'VehModel_NN_2020');
NAME = 'MotionDeltasV00';
TAG = 'n256l4v1_relu_00050';
TYPE = 'validation';
START_INDEX = 1000;
SAMPLE_SIZE = 10000;

% Dependent
trainingFolder = fullfile(DATA_FOLDER, NAME);
modelFolder = fullfile(trainingFolder, ['training_', TAG]);

%% Load input and output
% Metadata
metadataPath = fullfile(trainingFolder, 'metadata.json');
metadata = jsondecode(fileread(metadataPath));
    
% Input
if ~exist('matIn', 'var')
    matIn = nan(metadata.NumSamplesValidation, metadata.NumInputs);
    numSamplesRead = 0;
    inputDatasetPath = fullfile(trainingFolder, sprintf('input_%s_*.bin', TYPE));
    inputDatasets = dir(inputDatasetPath);
    for iDataset = 1 : length(inputDatasets)
        fileIn = fopen(fullfile(inputDatasets(iDataset).folder, inputDatasets(iDataset).name), 'r');
        matIn1 = fread(fileIn, metadata.BufferSize * metadata.BatchSize * metadata.NumInputs, 'float64=>float64', 0, 'l');
        fclose(fileIn);
        numSamples1 = floor(numel(matIn1) / metadata.NumInputs);
        matIn(numSamplesRead + 1 : numSamplesRead + numSamples1, :) = reshape(matIn1, metadata.NumInputs, [])';
        numSamplesRead = numSamplesRead + numSamples1;
    end
    clear matIn1;
    
    inputScalePath = fullfile(trainingFolder, 'input_scale.bin');
    fileIn = fopen(inputScalePath, 'r');
    scaleIn = fread(fileIn, metadata.NumInputs, 'float64=>float64', 0, 'l');
    fclose(fileIn);
    scaleIn = reshape(scaleIn, metadata.NumInputs, 1)';
    
    matIn = matIn ./ scaleIn;
end

% Output
if ~exist('matOut', 'var')
    matOut = nan(metadata.NumSamplesValidation, metadata.NumOutputs);
    numSamplesRead = 0;
    outputDatasetPath = fullfile(trainingFolder, sprintf('output_%s_*.bin', TYPE));
    outputDatasets = dir(outputDatasetPath);
    for iDataset = 1 : length(outputDatasets)
        fileOut = fopen(fullfile(outputDatasets(iDataset).folder, outputDatasets(iDataset).name), 'r');
        matOut1 = fread(fileOut, metadata.BufferSize * metadata.BatchSize * metadata.NumOutputs, 'float64=>float64', 0, 'l');
        fclose(fileOut);
        numSamples1 = floor(numel(matOut1) / metadata.NumOutputs);
        matOut(numSamplesRead + 1 : numSamplesRead + numSamples1, :) = reshape(matOut1, metadata.NumOutputs, [])';
        numSamplesRead = numSamplesRead + numSamples1;
    end
    clear matOut1;
    
    outputScalePath = fullfile(trainingFolder, 'output_scale.bin');
    fileOut = fopen(outputScalePath, 'r');
    scaleOut = fread(fileOut, metadata.NumOutputs, 'float64=>float64', 0, 'l');
    fclose(fileOut);
    scaleOut = reshape(scaleOut, metadata.NumOutputs, 1)';
    
    matOut = matOut ./ scaleOut;
end

%% Load model
if ~exist('model', 'var')
    modelPath = fullfile(modelFolder, ['model_', TAG, '.json']);
    weightPath = fullfile(modelFolder, ['weights_', TAG, '.h5']);
    % Import Keras layers
    layers = importKerasLayers(modelPath, 'WeightFile', weightPath, 'ImportWeights', true);
    % Replace input layer from default imageInputLayer (MATLAB converts Keras Input to this) to
    % sequenceInputLayer and add final regressionLayer as MATLAB needs an output layer.
    layers = [sequenceInputLayer(max(layers(1).InputSize), 'Name', 'input_1'); layers(2:end); regressionLayer('Name', 'regression_1')];
    %layers = [layers; regressionLayer('Name', 'regression_1')];
    lGraph = layerGraph(layers);
    model = assembleNetwork(lGraph);
end

%% Predict with model
nnOut = nan(metadata.NumSamplesValidation, metadata.NumOutputs);
for iBatch = 1 : ceil(metadata.NumSamplesValidation / metadata.BatchSize)
    iStart = (iBatch - 1) * metadata.BatchSize + 1;
    iEnd = min(iBatch * metadata.BatchSize, metadata.NumSamplesValidation);
    nnOut(iStart : iEnd, :) = double(model.predict(matIn(iStart:iEnd, :)', 'ExecutionEnvironment', 'CPU', 'Acceleration', 'None')');
    nnOutTest = predictModel(model, matIn(iStart:iEnd, :)')';
    assert(isequal(nnOut(iStart : iEnd, :), nnOutTest));
end

%% Calculate errors
% Aboslute error
err = matOut - nnOut;
rmse = sqrt(mean(err.^2));
emax = max(abs(err));
mae = mean(abs(err));

%% Print results
% Absoulute error
fileId = fopen(fullfile(pwd, 'data', 'PlotTemp', [NAME, '_', TAG, '_Fit.txt']), 'w');
fprintf(fileId, 'RMSE    = ');
fprintf(fileId, '%.3e  ', rmse);
fprintf(fileId, '\n');
fprintf(fileId, 'EMAX    = ');
fprintf(fileId, '%.3e  ', emax);
fprintf(fileId, '\n');
fprintf(fileId, 'MAE     = ');
fprintf(fileId, '%.3e  ', mae);
fprintf(fileId, '\n');
fclose(fileId);

%% Plot results
indices = START_INDEX : START_INDEX + SAMPLE_SIZE;
%close('all');
fig = nicefigure('nn', 45, 20);
subPlotCols = 5;
subPlotRows = ceil(metadata.NumOutputs / subPlotCols);
for iPlot = 1 : metadata.NumOutputs
    ax = niceaxis(fig, 8, true);
    subplot(subPlotRows, subPlotCols, iPlot, ax);
    yyaxis(ax, 'left');
    plot(ax, matOut(indices, iPlot),  'Color', nicecolors.RedAlizarin);
    plot(ax, nnOut(indices, iPlot), 'Color', nicecolors.PurpleWisteria, 'LineWidth', 2,  'LineStyle', ':');
    xlabel('epoch');
    ax.YAxis(1).Color = nicecolors.RedAlizarin;
    ax.YAxis(1).Label.String = metadata.OutputNames{iPlot};
    yyaxis(ax, 'right');
    plot(ax, matOut(indices, iPlot) - nnOut(indices, iPlot), 'Color', nicecolors.GreenEmerald);
    ax.YAxis(2).Color = nicecolors.GreenEmerald;
    ax.YAxis(2).Label.String = 'error';
    title(ax, sprintf('rmse=%.4e\nemax=%.4e', rmse(iPlot), emax(iPlot)));
    nicelegend({'act', 'pred'}, ax, 8);
end
sgtitle(fig, sprintf('%s\n%s', strrep(NAME, '_', '\_'), strrep(TAG, '_', '\_')), 'Interpreter', 'Latex', 'FontSize', 8);
print(fig, fullfile(pwd, 'data', 'PlotTemp', [NAME, '_', TAG, '_Fit.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [NAME, '_', TAG, '_Fit.png']), '-dpng', '-opengl');