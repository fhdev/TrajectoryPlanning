# Dependencies
import numpy
import pandas
import tensorflow
import os
import sklearn.preprocessing
import sklearn.model_selection
import sklearn.metrics
import datetime
import json
import shutil
import gc

# Custom training data generator
class TrainDataGenerator(tensorflow.keras.utils.Sequence) :
    # Constructor
    def __init__(self, dataFolder, fileType, numInputs, numOutputs, sampleSize, bufferSize, batchSize) :
        # Folder that contains data files
        self.dataFolder = dataFolder
        # Type of file to read: train / validation
        self.fileType = fileType
        # Number of input / outputs
        self.numInputs = numInputs
        self.numOutputs = numOutputs
        # Number of total samples
        self.sampleSize = sampleSize
        # How many batches are contained in one file
        self.bufferSize = bufferSize
        # What is the batch size (number of samples per batch)
        self.batchSize = batchSize
        # Number of last file that was loaded
        self.lastFileNumber = 0

        # Load norms
        inputScalePath = os.path.join(self.dataFolder, "input_scale.bin")
        with open(inputScalePath, "r") as inputScaleFile:
            self.scaleIn = numpy.fromfile(inputScaleFile, "<d", self.numInputs)
        self.scaleIn = self.scaleIn.reshape((1, self.numInputs), order="C")
        outputScalePath = os.path.join(self.dataFolder, "output_scale.bin")
        with open(outputScalePath, "r") as outputScaleFile:
            self.scaleOut = numpy.fromfile(outputScaleFile, "<d", self.numOutputs)
        self.scaleOut = self.scaleOut.reshape((1, self.numOutputs), order="C")

        # Generate random indices for choosing samples from this buffer
        self.indices = numpy.random.permutation(self.bufferSize * self.batchSize)
        self.uindices = self.indices
    
    # Provide number of batches
    def __len__(self) :
        return int(numpy.ceil(self.sampleSize / self.batchSize))
    
    # Get the actual batch
    def __getitem__(self, iBatch) :
        # Check index
        assert iBatch < numpy.ceil(self.sampleSize / self.batchSize)
        # Load new dataset if necessary
        fileNumber = int(numpy.floor(iBatch / self.bufferSize)) + 1
        if self.lastFileNumber != fileNumber :
            # Read new input buffer
            inputDataPath = os.path.join(self.dataFolder, f"input_{self.fileType}_{fileNumber:03d}.bin")
            with open(inputDataPath, "r") as inputDataFile:
                self.matIn = numpy.fromfile(inputDataFile, "<d", self.bufferSize * self.batchSize * self.numInputs)
            self.matIn = self.matIn.reshape((-1, self.numInputs), order="C")
            self.matIn /= self.scaleIn
            # Read new output buffer
            outputDataPath = os.path.join(self.dataFolder, f"output_{self.fileType}_{fileNumber:03d}.bin")
            with open(outputDataPath, "r") as outputDataFile:
                self.matOut = numpy.fromfile(outputDataFile, "<d", self.bufferSize * self.batchSize * self.numOutputs)
            self.matOut = self.matOut.reshape((-1, self.numOutputs), order="C")
            self.matOut /= self.scaleOut
            # Check sizes
            assert self.matIn.shape[0] == self.matOut.shape[0]
            # Remove sample indices that are too big (if this is the last batch)
            self.uindices = self.indices[self.indices < self.matOut.shape[0]]

            # Set last file number
            self.lastFileNumber = fileNumber

        # Calculate start and end of current batch inside current buffer
        start = int((iBatch % self.bufferSize) * self.batchSize)
        end = int(numpy.minimum(((iBatch % self.bufferSize) + 1) * self.batchSize, len(self.uindices)))

        # Return batch
        return self.matIn[self.uindices[start : end], :], self.matOut[self.uindices[start : end], :]

# Custom loss function
class WeightedMseLoss:
    def __init__(self, weights):
        self.weights = weights

    def calc(self, yTrue, yPred):
        sqr = tensorflow.keras.backend.square(yTrue - yPred)
        sqrWeighted = sqr * self.weights
        loss = tensorflow.keras.backend.mean(sqrWeighted, axis=1)
        return loss

# Main function
def main():
    # Constants
    DATAFOLDER = os.path.join("C:", "VehModel_NN_2020", "MotionDeltasV02W")
    EPOCHS = 100
    LEARNINGRATE = 0.001 # Default learning rate is 0.001 for Adam optimizer
    SEED = 12345

    # Parameters to loop over
    PARAMETERS = {
        "Layers": [
            # ["n192l4v2", [32, 64, 64, 32]],
            # ["n256l3v1", [64, 128, 64]],
            # ["n256l3v2", [32, 192, 32]],
            # ["n256l4v1", [64, 64, 64, 64]],
            ["n256l4v2", [32, 96, 96, 32]],
            # ["n384l4v2", [64, 128, 128, 64]]
        ],
        "Activation": ["relu"],
        "LearningRateMultiplier": [1.0]
    }

    # Tensorflow settings
    devices = tensorflow.config.list_physical_devices('GPU')
    tensorflow.config.experimental.set_memory_growth(devices[0], True)
    config = [tensorflow.config.experimental.VirtualDeviceConfiguration(memory_limit=3072)]
    tensorflow.config.experimental.set_virtual_device_configuration(devices[0], config)

    # Initialize random generation of Numpy
    numpy.random.seed(SEED)

    # Read in metadata
    metadataPath = os.path.join(DATAFOLDER, "metadata.json")
    with open(metadataPath, "r") as metadataFile:
        metadata = json.load(metadataFile)

    # Create training data generator
    trainGenerator = TrainDataGenerator(DATAFOLDER, "train", metadata["NumInputs"], metadata["NumOutputs"], 
        metadata["NumSamplesTrain"], metadata["BufferSize"], metadata["BatchSize"])
    validationGenerator = TrainDataGenerator(DATAFOLDER, "validation", metadata["NumInputs"], metadata["NumOutputs"], 
        metadata["NumSamplesValidation"], metadata["BufferSize"], metadata["BatchSize"])

    for layers in PARAMETERS["Layers"]:
        for activation in PARAMETERS["Activation"]:
            for learningRateMultiplier in PARAMETERS["LearningRateMultiplier"]:
                # Save tag and create result folder
                tag = f"{layers[0]}_{activation}_{int(round(100 * learningRateMultiplier)):05d}"
                trainingFolder = os.path.join(DATAFOLDER, f"training_{tag}")
                if os.path.isdir(trainingFolder) :
                    shutil.rmtree(trainingFolder)
                os.mkdir(trainingFolder)

                # Save metadata of training
                trainMetadata = {
                    "Layers": layers,
                    "Activation": activation, 
                    "LearningRateMultiplier" : learningRateMultiplier
                }
                trainMetadataPath = os.path.join(trainingFolder, f"metadata_{tag}.json")
                with open(trainMetadataPath, "w") as trainMetadataFile:
                    trainMetadataFile.write(json.dumps(trainMetadata, indent=4, sort_keys=True))

                # Initialize random generation of Tensorflow
                tensorflow.random.set_seed(SEED + 1)

                # Build neural network (fully connected sequation)
                model = tensorflow.keras.models.Sequential()
                # Input layer + first hidden layer with 64 neurons and 13 inputs
                model.add(tensorflow.keras.layers.Dense(layers[1][0], input_dim=metadata["NumInputs"], activation=activation))
                for iLayer in range(1, len(layers[1])):
                    model.add(tensorflow.keras.layers.Dense(layers[1][iLayer], activation=activation))
                # Output layer with 10 output neurons as we have 10 output
                # Activation functions (see in Keras Docs):
                #   softmax:     converts a real vector to a vector of categorical probabilities (for classification problems)
                #   relu:        rectified linear unit [max(x, 0)]
                model.add(tensorflow.keras.layers.Dense(metadata["NumOutputs"], activation="linear"))

                # Specify loss function and optimizer
                # Losses [minimization objective function] (see in Keras Docs): 
                #   categorical_crossentropy: crossentropy loss between the labels and predictions (for classification problems)
                #   mean_squared_error:       mean of squares of errors between the labels and predictions (for regression problems)
                # Optimizers (see in Keras Docs):
                #   sgd:     gradient descent
                #   adam:    stochastic gradient descent with adaptive estimation of first and second order moments
                #   Specify learning rates in optimizer constructor.
                # Metrics [to judge the performance of the model] (see in Keras Docs): 
                #   accuracy:           calculates how often predictions equals labels (for classification problems)
                #   mean_squared_error: as before
                #   any loss function can be used as metrics as well
                learningRate = LEARNINGRATE * learningRateMultiplier
                optim = tensorflow.keras.optimizers.Adam(learning_rate=learningRate)
                lossFcn = WeightedMseLoss(metadata["OutputWeights"])
                metricFcn = tensorflow.keras.metrics.RootMeanSquaredError()
                model.compile(loss = lossFcn.calc, optimizer=optim, metrics=[metricFcn])

                # Train
                # Specify validation data to calculate the metrics in every epoch also to the test data set.
                # This way you can avoid overfitting.
                logPath = os.path.join(trainingFolder, f"log_{tag}.csv")
                csvLogger = tensorflow.keras.callbacks.CSVLogger(logPath)
                tensorboardLogger = tensorflow.keras.callbacks.TensorBoard(log_dir=trainingFolder)
                history = model.fit(x=trainGenerator, validation_data=validationGenerator, epochs=EPOCHS, 
                    callbacks=[csvLogger, tensorboardLogger], shuffle=False)

                # Save model
                modelPath = os.path.join(trainingFolder, f"model_{tag}.json")
                with open(modelPath, "w") as modelFile:
                    modelFile.write(model.to_json(indent=4, sort_keys=True))
                # Save weights
                weightPath = os.path.join(trainingFolder, f"weights_{tag}.h5")
                model.save_weights(weightPath)

                print("Model saved successfully.")

                # Clean up
                tensorflow.keras.backend.clear_session()
                del history, tensorboardLogger, csvLogger, optim, lossFcn, metricFcn, model
                gc.collect()

# Run main function
if __name__ == "__main__":
    main()


