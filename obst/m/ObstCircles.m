%% ObstacleCircles *********************************************************************************
% [Summary]
%   This class 
%   Origin of coordinate system is bottom left corner of safety rectangle.
% [Used in]
%   
%
% [Subclasses]
%   
%
% **************************************************************************************************
classdef ObstCircles < handle
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % vertices of safety rectangle
        xRect
        yRect
        % positions (from bottom left corner) and radii of safety circles
        rCirc
        xCirc
        yCirc
        % positions of cog
        xCog
        yCog
        yawZCog
        
        %% Properties set in constructor -----------------------------------------------------------
        % height of safety rectangle (longitudinal direction)
        HeightX
        % width of safety rectangle (lateral direction)
        WidthY
        % distance of cog from the bottom (x position)
        DistCogBottomX
        % distance of cog from left (y position)
        DistCogLeftY
        % number of circles
        NCirles
        
        
        
        
        

    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.HeightX(self, value)
            if ~isequal(self.HeightX, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: HeightX must be a numeric vector!')
                end
                self.HeightX = value(:);
            end
        end
        
        function set.WidthY(self, value)
            if ~isequal(self.WidthY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: WidthY must be a numeric vector!')
                end
                self.WidthY = value(:);
            end
        end
        
        function set.DistCogBottomX(self, value)
            if ~isequal(self.DistCogBottomX, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DistCogBottomX must be a numeric vector!')
                end
                self.DistCogBottomX = value(:);
            end
        end
        
        function set.DistCogLeftY(self, value)
            if ~isequal(self.DistCogLeftY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DistCogLeftY must be a numeric vector!')
                end
                self.DistCogLeftY = value(:);
            end
        end
        
        function set.NCirles(self, value)
            if ~isequal(self.NCirles, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: NCirles must be a numeric vector!')
                end
                self.NCirles = value(:);
            end
        end        
        %% Constructor -----------------------------------------------------------------------------
        function self = ObstacleCircles(varargin)
            if nargin == 2
                self.HeightX        = varargin{1}.HeightX;
                self.WidthY         = varargin{1}.WidthY;
                self.DistCogBottomX = varargin{1}.DistCogBottomX;
                self.DistCogLeftY   = varargin{1}.DistCogLeftY;
                self.NCirles        = varargin{1}.NCirles;
                posXG = varargin{2}.PosXG;
                posYG = varargin{2}.PosYG;
                yawZ = varargin{2}.YawZ;
            elseif nargin == 8
                self.HeightX        = varargin{1};
				self.WidthY         = varargin{2};
				self.DistCogBottomX = varargin{3};
				self.DistCogLeftY   = varargin{4};
				self.NCirles        = varargin{5};
                posXG = varargin{6};
                posYG = varargin{7};
                yawZ = varargin{8};
            else
                error('ERROR: Inappropriate number of argiments!');
            end
            
            % coordinates of rectangle vertices (x points forward, y points left)
            cosYawZ = cos(yawZ);
            sinYawZ = sin(yawZ);
            
            xRect = [-self.DistCogBottomX; -self.DistCogBottomX; ...
                self.HeightX - self.DistCogBottomX; self.HeightX - self.DistCogBottomX];
            yRect = [self.DistCogLeftY; self.DistCogLeftY - self.WidthY; self.DistCogLeftY - self.WidthY; self.DistCogLeftY];
            self.xRect = cosYawZ * xRect' - sinYawZ * yRect' + posXG;
            self.yRect = sinYawZ * xRect' + cosYawZ * yRect' + posYG;
            
            % coordinates of circles
            dCenter = self.HeightX / self.NCirles;
            xCirc = (dCenter / 2 : dCenter : self.HeightX - dCenter / 2)' - self.DistCogBottomX;
            yCirc = repmat(self.DistCogLeftY - self.WidthY / 2, self.NCirles, 1);
            self.xCirc = cosYawZ * xCirc' - sinYawZ * yCirc' + posXG;
            self.yCirc = sinYawZ * xCirc' + cosYawZ * yCirc' + posYG;
            self.rCirc = sqrt((self.WidthY / 2)^2 + (dCenter / 2)^2);
        end
        
        %% MyFcn -----------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------
        function DisplayPlots(self, name)
            if nargin < 2
                name = class(self);
            end
            fig = figure('Color', 'white', 'Name', name);
            ax = axes(fig);
            hold(ax, 'on');
            plot(ax, posXG, posYG, 'ro', 'MarkerFaceColor', 'r');
            plot(ax, [self.xRect, self.xRect(1)], [self.yRect,  self.yRect(1)], 'r-');
            circle(ax, self.xCirc, self.yCirc, self.rCirc, 'b-');
            axis(ax, 'equal');
            view(ax, -90, 90);
            xlabel(ax, 'x');
            ylabel(ax, 'y');
            box(ax, 'on');
            grid(ax, 'on');
        end
        
    end
end