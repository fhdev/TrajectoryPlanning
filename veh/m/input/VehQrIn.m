%% VehQrLonIn **************************************************************************************
% [Summary]
%   This class represents inputs for quarter vehicle model.
%
% [Used in]
%   VehQr
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehQrIn < VehOlIn & VehEnvIn1W
    %% Instance methods ============================================================================
    methods
        %% Constructor -----------------------------------------------------------------------------
        function self = VehQrIn(varargin)
           if nargin == 1
               vehOlInArgs = varargin(1);
               vehEnvIn1WArgs = varargin(1);
           elseif nargin == 5
               vehOlInArgs = varargin(1:4);
               vehEnvIn1WArgs = varargin([1, 5]);
           else
               error('ERROR: Inappropriate number of arguments!');
           end
           self@VehOlIn(vehOlInArgs{:});
           self@VehEnvIn1W(vehEnvIn1WArgs{:});
        end

    end
end