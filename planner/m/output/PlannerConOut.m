%% PlannerConOut ************************************************************************************
% [Summary]
%   This class represents outputs of a constrained nonlinear solver based motion planner PlannerCon.
%
% [Used in]
%   PlannerCon
%
% [Subclasses]
%   PlannerOptOut
% **************************************************************************************************
classdef PlannerConOut < handle
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Calculation time of trajectors [s]
        CalcTime
        % Success flag
        Success
        % Final reference signal parameters (PlannerRefParam object)
        OptVar
        % Final vehicle inputs according to reference signal parameters (VehStOlIn object)
        VehIn
        % Final vehicle outputs according to reference signal parameters (VehPlOut object)
        VehOut
        % Final state constraints (PlannerConstrState object)
        ConState
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.CalcTime(self, value)
            if ~isequal(self.CalcTime, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: CalcTime must be a numeric scalar!')
                end
                self.CalcTime = value;
            end
        end
        
        function set.Success(self, value)
            if ~isequal(self.Success, value)
                if ~(islogical(value) && isscalar(value))
                    error('ERROR: Success must be a logical scalar!')
                end
                self.Success = value;
            end
        end
        function set.OptVar(self, value)
            if ~isequal(self.OptVar, value)
                if ~((isa(value, 'PlannerOptVarVelYawRate') || isa(value, 'PlannerOptVarVelPath')) && isscalar(value))
                    error('ERROR: OptVar must be a PlannerOptVarVelYawRate or PlannerOptVarVelPath object!')
                end
                self.OptVar = value;
            end
        end
        function set.VehIn(self, value)
            if ~isequal(self.VehIn, value)
                if ~(isa(value, 'VehStClIn') && isscalar(value))
                    error('ERROR: VehIn must be a VehStClIn object!')
                end
                self.VehIn = value;
            end
        end
        function set.VehOut(self, value)
            if ~isequal(self.VehOut, value)
                if ~(isa(value, 'VehStClOut') && isscalar(value))
                    error('ERROR: VehOut must be a VehStClOut object!')
                end
                self.VehOut = value;
            end
        end
        
        function set.ConState(self, value)
            if ~isequal(self.ConState, value)
                if ~(isa(value, 'PlannerConFcnVehState') && isscalar(value))
                    error('ERROR: ConState must be a PlannerConFcnVehState object!')
                end
                self.ConState = value;
            end
        end
        
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerConOut(calcTime, success, optVar, vehIn, vehOut, conState)
            %% Set properties
            self.CalcTime = calcTime;
            self.Success = success;
            self.OptVar = optVar;
            self.VehIn = vehIn;
            self.VehOut = vehOut;
            self.ConState = conState;
        end
        
        function  [figWheelDynF, figWheelInF, figWheelDynR, figWheelInR, figGlob, figLoc, ...
                figPath, figLonCtrl, figLatCtrl] = DisplayPlots(self, name)
            %% Constants
            WIDTH = 5;
            HEIGHT3 = 15;
            FONTSIZE = 8;

            %% Default arguments
            if nargin < 2
                name = class(self);
            end
            
            %% Create vehicle output plots
            [figWheelDynF, figWheelInF, figWheelDynR, figWheelInR, figGlob, figLoc, ...
                figPath, figLonCtrl, figLatCtrl] = self.VehOut.DisplayPlots([name, '/VehOut']);
            
            %% Plot planning info
            % Constraint markers
            markerSettings = { 'LineStyle', 'none', 'Marker', 'square', ...
                'MarkerEdgeColor', 'black', 'MarkerFaceColor', 'black', 'MarkerSize', 5 };
            
            % Global dynamics
            [tEnd, xCon] = meshgrid(self.VehOut.Chassis.Time(end), self.ConState.FinalCon.PosXG);
            plot(figGlob.Children(9), tEnd, xCon, markerSettings{:});
            [tEnd, yCon] = meshgrid(self.VehOut.Chassis.Time(end), self.ConState.FinalCon.PosYG);
            plot(figGlob.Children(6), tEnd, yCon, markerSettings{:});
            [tEnd, rCon] = meshgrid(self.VehOut.Chassis.Time(end), self.ConState.FinalCon.YawZ);
            plot(figGlob.Children(3), tEnd, rad2deg(rCon), markerSettings{:});
            [tEnd, drCon] = meshgrid(self.VehOut.Chassis.Time(end), self.ConState.FinalCon.YawVelZ);
            plot(figGlob.Children(2), tEnd, rad2deg(drCon), markerSettings{:});
            
            % Path
            [xCon, yCon] = meshgrid(self.ConState.FinalCon.PosXG, self.ConState.FinalCon.PosYG);
            plot(figPath.Children(1), xCon, yCon, markerSettings{:});
        end
    end
end
