#include "MathTp.hpp"

// Function definitions
using namespace Eigen;

namespace TrajectoryPlanning
{
	// This function determines the maximum of its parameters, x, y, and z.
	Float64_T max3(Float64_T x, Float64_T y, Float64_T z)
	{
		Float64_T max = x;
		if (y > max)
		{
			max = y;
		}
		if (z > max)
		{
			max = z;
		}
		return max;
	}

	// This function determines the sign of its parameter, x. 
	// Return value:    1, if x > 0
	//                  0, if x = 0
	//                 -1, if x < 0
	Float64_T sign(Float64_T x)
	{
		return (Float64_T)((x > 0.0) - (x < 0.0));
	}

	Float64_T interpolate(const Ref<const VectorXd> x, const Ref<const VectorXd> y, Float64_T xq)
	{
		Float64_T yq;
		Int64_T n = x.size();
		if (xq >= x(n - 1))
		{
			yq = y(n - 1);
		}
		else if (xq <= x(0))
		{
			yq = y(0);
		}
		else
		{
			Int64_T i;
			for (i = n - 1; i >= 0; i--)
			{
				if (x(i) <= xq)
				{
					break;
				}
			}
			yq = y(i) + (y(i + 1) - y(i)) / (x(i + 1) - x(i)) * (xq - x(i));
		}

		return yq;
	}

	Int32_T interp1(const Ref<const VectorXd> x,
					const Ref<const VectorXd> y,
					const Ref<const VectorXd> xq,
					Ref<VectorXd> yq)
	{
		// Check input
		if (!((diff(x).array() > 0).all()))
			throw std::invalid_argument("Vector x must be monotonic increasing.");
		if(y.size() != x.size())
			throw std::invalid_argument("Vector y must have same size as vector x.");
		if(!((diff(xq).array() > 0).all()) || (xq(0) < x(0)) || (xq.tail(1)(0) > x.tail(1)(0)))
			throw std::invalid_argument("Vector xq must be monotonic increasing in the range of vector x.");
		
		const Float64_T *xptr = x.data();
		const Float64_T *yptr = y.data();
		const Float64_T *xqptr = xq.data();
		Float64_T *yqptr = yq.data();
		
		Int64_T xl_1 = x.size() - 1;
		Int64_T xql = xq.size();
		Int64_T i = 0;
		Int64_T j = 0;
		for(i = 0; i < xql; i++)
		{
			while((xptr[j] <= xqptr[i]) && (j < xl_1))
				j++;
			yqptr[i] = yptr[j - 1] + (yptr[j] - yptr[j - 1]) /
				(xptr[j] - xptr[j - 1]) * (xqptr[i] - xptr[j - 1]);
		}
		return 0;
	}

	Int32_T interp4spline5(const Ref<const VectorXd> x,
						   const Ref<const VectorXd> y,
						   const Ref<const VectorXd> dyb,
						   const Ref<const VectorXd> ddyb,
						   const Ref<const VectorXd> xq,
						   Ref<VectorXd> yq,
						   Ref<VectorXd> dyq,
						   Ref<VectorXd> ddyq)
	{
		// Constants
		const Int32_T N = 6;
		const Int32_T N_1 = 5;
		const Int32_T M = 4;

		// Check input
		if ((x.size() != M)  || !((diff(x).array() > 0).all()))
			throw std::invalid_argument("Vector x must have 4 elements and must be monotonic increasing.");
		if(y.size() != M)
			throw std::invalid_argument("Vector y must have 4 elements.");
		if(dyb.size() != 2)
			throw std::invalid_argument("Vector dyb must have 2 elements.");
		if (ddyb.size() != 2)
			throw std::invalid_argument("Vector ddyb must have 2 elements.");
		if(!((diff(xq).array() > 0).all()) || (xq(0) < x(0)) || (xq.tail(1)(0) > x.tail(1)(0)))
			throw std::invalid_argument("Vector xq must be monotonic increasing in the range of vector x.");

		// Assemble linear regression problem
		VectorXd e5 = VectorXd::LinSpaced(N, N_1, 0);
		ArrayWrapper<VectorXd> e5v = e5.array();

		VectorXd e4 = VectorXd::Zero(N);
		e4.head(N_1) = e5.tail(N_1);
		ArrayWrapper<VectorXd> e4v = e4.array();

		VectorXd e3 = VectorXd::Zero(N);
		e3.head(N_1) << e4.tail(N_1);
		ArrayWrapper<VectorXd> e3v = e3.array();

		VectorXd e2 = VectorXd::Zero(N);
		e2.head(N_1) = e3.tail(N_1);
		ArrayWrapper<VectorXd> e2v = e2.array();

		Float64_T xmax = (x.tail(1))(0);
		VectorXd xs = x / xmax;
		VectorXd xqs = xq / xmax;

		MatrixXd xv = MatrixXd::Zero(M, N);
		MatrixXd dxv = MatrixXd::Zero(M, N);
		MatrixXd ddxv = MatrixXd::Zero(M, N);
		MatrixXd dddxv = MatrixXd::Zero(M, N);
		for (Int64_T i = 0; i < M; i++)
		{
			xv.row(i) = pow(xs(i), e5v);
			dxv.row(i).head(N - 1) = xv.row(i).tail(N - 1).array() * e5v.head(N - 1).transpose();
			ddxv.row(i).head(N - 2) = xv.row(i).tail(N - 2).array() * e5v.head(N - 2).transpose() * e4v.head(N - 2).transpose();
			dddxv.row(i).head(N - 3) = xv.row(i).tail(N - 3).array() * e5v.head(N - 3).transpose() * e4v.head(N - 3).transpose() * e3v.head(N - 3).transpose();
		}

		RowVectorXd z = RowVectorXd::Zero(N);

		VectorXd Y = VectorXd::Zero(3 * N);
		Y << y(0), y(1), y(1), y(2), y(2), y(3), dyb * xmax, 0, 0, ddyb * xmax * xmax, z.transpose();

		MatrixXd X = MatrixXd::Zero(3 * N, 3 * N);
		X <<   (RowVectorXd)xv.row(0),                          z,                          z,      // p1(x1) = y1
			   (RowVectorXd)xv.row(1),                          z,                          z,      // p1(x2) = y2
			   						z,     (RowVectorXd)xv.row(1),                          z,      // p2(x2) = y2
									z,     (RowVectorXd)xv.row(2),                          z,      // p2(x3) = y3
									z,                          z,     (RowVectorXd)xv.row(2),      // p3(x3) = y3
									z,                          z,     (RowVectorXd)xv.row(3),      // p3(x4) = y4
			  (RowVectorXd)dxv.row(0),                          z,                          z,      // p1'(x1) = y1'
									z,                          z,    (RowVectorXd)dxv.row(3),      // p3'(x4) = y4'
			  (RowVectorXd)dxv.row(1),   -(RowVectorXd)dxv.row(1),                          z,      // p1'(x2) - p2'(x2) = 0
									z,    (RowVectorXd)dxv.row(2),   -(RowVectorXd)dxv.row(2),      // p2'(x3) - p3'(x3) = 0
			 (RowVectorXd)ddxv.row(0),                          z,                          z,      // p1''(x1) = y1''
									z,                          z,   (RowVectorXd)ddxv.row(3),      // p3''(x4) = y4''
			 (RowVectorXd)ddxv.row(1),  -(RowVectorXd)ddxv.row(1),                          z,      // p1''(x2) - p2''(x2) = 0
									z,   (RowVectorXd)ddxv.row(2),  -(RowVectorXd)ddxv.row(2),      // p2''(x3) - p3''(x3) = 0
			(RowVectorXd)dddxv.row(0),                          z,                          z,      // p1'''(x1) = 0
									z,                          z,  (RowVectorXd)dddxv.row(3),      // p3'''(x4) = 0
			(RowVectorXd)dddxv.row(1), -(RowVectorXd)dddxv.row(1),                          z,      // p1'''(x2) - p2'''(x2) = 0
									z,  (RowVectorXd)dddxv.row(2), -(RowVectorXd)dddxv.row(2);      // p2'''(x3) - p3'''(x3) = 0




		// Solve regression  
		VectorXd P = X.householderQr().solve(Y);

		// Calculate interpolated values
		Int64_T xql = xq.size();
		Int64_T i = 0;
		Int64_T j = 0;
		Int64_T k = 0;
		for (i = 0; i < xql; i++)
		{
			while((xs(j) <= xqs(i)) && (j < M - 1))
				j++;
			k = N * (j - 1);
			VectorXd yqp = pow(xqs(i), e5v);
			yq(i)   = (yqp.array() *                                                 P.segment(k, N).array()).sum();
			dyq(i)  = (yqp.tail(N - 1).array() * e5v.head(N - 1) *                   P.segment(k, N - 1).array()).sum() / xmax;
			ddyq(i) = (yqp.tail(N - 2).array()*  e5v.head(N - 2) * e4v.head(N - 2) * P.segment(k, N - 2).array()).sum() / xmax / xmax;
		}

		return 0;
	}

	VectorXd diff(const VectorXd & v)
	{
		Int64_T l = v.size();
		VectorXd dv = v.tail(l - 1) - v.head(l - 1);
		return dv;
	}

	VectorXd cumsum(const VectorXd & v)
	{
		Int64_T l = v.size();
		VectorXd iv = VectorXd::Zero(l);
		iv(0) = v(0);
		for (Int64_T i = 1; i < l; i++)
			iv(i) = iv(i - 1) + v(i);
		return iv;
	}

	void printm(MatrixXd m)
	{
		for (Int64_T i = 0; i < m.rows(); i++)
		{
			for(Int64_T j = 0; j < m.cols(); j++)
			{
				printf("%12.6g ", m(i, j));
			}
			printf("\n");
		}
	}
}