%% Cleanup
clear;
%% Create geometric planner and vehicle
planner = PlannerGopt(PlannerGopt_Default());
veh = VehStCl(Vehicle_Default);

%% Generate ranges for trajectories
xRange = linspace(50, 100, 1);
yMultRange = sort([0, linspace(-0.2, 0.2, 10)]);
vRange = linspace(10, 30, 10);
rMultRange = sort([0, linspace(-0.1, 1.2, 10)]);
drMultRange = sort([0, linspace(-0.1, 1.2, 10)]);
nSamplesInFile = 1000;

nSamples = length(xRange) * length(yMultRange) * length(vRange) * length(rMultRange) * length(drMultRange);
samples = repmat(struct('VehIn', [], 'VehOut', []), nSamplesInFile, 1);

%% Generate samples
iSample = 1;
for ix = 1 : length(xRange)
    xf = xRange(ix);
    for iy = 1 : length(yMultRange)
        yf = yMultRange(iy) * xf;
        for iv = 1 : length(vRange)
            vi = vRange(iv);
            vf = vi;
            [rCirc, drCirc] = PlannerConFcnVehState.CircConstr(0, 0, xf, yf, vf);
            for ir = 1 : length(rMultRange)
                rf = rMultRange(ir) * rCirc;
                for idr = 1 : length(drMultRange)
                    dri = 0;
                    drf = drMultRange(idr) * drCirc;
                    
                    %% Create trajectory plan
                    % initial condition
                    plannerInput.InitCon.VelXV         = vi;
                    plannerInput.InitCon.YawVelZ       = dri;
                    % final condition
                    plannerInput.FinalCon.ArcLenS      = nan;
                    plannerInput.FinalCon.Time         = nan;
                    plannerInput.FinalCon.PosXG        = xf;
                    plannerInput.FinalCon.VelXV        = vf;
                    plannerInput.FinalCon.PosYG        = yf;
                    plannerInput.FinalCon.AccInrtYV    = nan;
                    plannerInput.FinalCon.YawZ         = rf;
                    plannerInput.FinalCon.YawVelZ      = drf;
                    % optimization variable
                    plannerInput.OptVarInit.VelXVRef             = vi;
                    plannerInput.OptVarInit.PosPosYGRef            = [0; 0];
                    plannerInput.OptVarInit.SampleDistance     = 1e-2;
                    % cost function
                    plannerInput.CostGeomWeight.Time           = nan;
                    plannerInput.CostGeomWeight.PosXG          = nan;
                    plannerInput.CostGeomWeight.VelXV          = nan;
                    plannerInput.CostGeomWeight.PosYG          = nan;
                    plannerInput.CostGeomWeight.AccInrtYV      = 1;
                    plannerInput.CostGeomWeight.YawZ           = nan;
                    plannerInput.CostGeomWeight.YawVelZ        = nan;
                    plannerInput.CostGeomWeight.ArcLenS        = nan;
                    
                    % Calculate geometric trajectory
                    plannerIn = PlannerGoptIn(plannerInput);
                    plannerOut = planner.CalculateTrajectory(plannerIn);
                    
                    %% Calculate vehicle response
                    % initial conditions
                    vehInitCondition.PosXG   = 0;
                    vehInitCondition.PosYG   = 0;
                    vehInitCondition.YawZ    = 0;
                    vehInitCondition.VelXV   = vi;
                    vehInitCondition.VelYV   = 0;
                    vehInitCondition.YawVelZ = dri;
                    % input
                    vehInput.Time = plannerOut.PlannerGeomOut.Time;
                    vehInput.FrictionFront = ones(size(vehInput.Time));
                    vehInput.FrictionRear = ones(size(vehInput.Time));
                    vehInput.VelXVRef = plannerOut.PlannerGeomOut.VelXV;
                    vehInput.YawVelZRef = plannerOut.PlannerGeomOut.YawVelZ;
                    vehInput.PosXGRef = plannerOut.PlannerGeomOut.PosXG;
                    vehInput.PosYGRef = plannerOut.PlannerGeomOut.PosYG;
                    vehInput.YawZRef = plannerOut.PlannerGeomOut.YawZ;
                    
                    % Calculate vehicle response
                    vehInitCon = VehStBndCon(vehInitCondition);
                    vehIn = VehStClIn(vehInput);
                    vehOut = veh.SimulateMex(vehInitCon, vehIn);
                    
                    %% Save results
                    iSampleInFile = mod(iSample - 1, nSamplesInFile) + 1;
                    samples(iSampleInFile).VehIn = obj2struct(vehIn);
                    samples(iSampleInFile).VehOut = obj2struct(vehOut);
                    % Save to file as well
                    if (iSampleInFile == nSamplesInFile) || (iSample == nSamples)
                        fileName = sprintf('trajectories_%06d_%06d.mat', iSample - nSamplesInFile + 1, iSample);
                        filePath = fullfile('D:\Letoltesek\Chrome\VehModel_NN_2020', fileName);
                        save(filePath, 'samples', '-v7.3');
                        samples = repmat(struct('VehIn', [], 'VehOut', []), nSamplesInFile, 1);
                    end
                    fprintf(1, 'Ready %06d/%06d ...\n', nSamples, iSample);
                    
                    %% Increase counter
                    iSample = iSample + 1;
                    
                end
            end
        end
    end
end
