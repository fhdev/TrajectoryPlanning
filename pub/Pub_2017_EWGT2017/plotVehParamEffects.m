% Parameters
W = 12;
H = 6;
F = 8;

type = 'LaneKeep';

if strcmpi(type, 'LaneChange')
    lkConTrjs = {'fc3_lanechange.mat'};
    lkOptTrjs = {'co5_lanechange_1_1_1.mat',           'direct nominal',      'x';...
                 'co5_lanechange_1_1_1_indirect.mat',  'indirect nominal',    '^'; ...
                 'co5_lanechange_1_1_1_param75.mat',   'parameters 75\%',     'd'; ...
                 'co5_lanechange_1_1_1_param85.mat',   'parameters 85\%',     's'; ...
                 'co5_lanechange_1_1_1_param95.mat',   'parameters 95\%',     '*'; ...
                 'co5_lanechange_1_1_1_param105.mat',  'parameters 105\%',    '+'; ...
                 'co5_lanechange_1_1_1_param115.mat',  'parameters 115\%',    'o'; ...
                 'co5_lanechange_1_1_1_param125.mat',  'parameters 125\%',    'v'};
elseif strcmpi(type, 'LaneKeep')
    lkConTrjs = {'fc3_lanekeep.mat'};
    lkOptTrjs = {'co5_lanekeep_1_1_1.mat',           'direct nominal',      'x';...
                 'co5_lanekeep_1_1_1_indirect.mat',  'indirect nominal',    '^'; ...
                 'co5_lanekeep_1_1_1_param75.mat',   'parameters 75\%',     'd'; ...
                 'co5_lanekeep_1_1_1_param85.mat',   'parameters 85\%',     's'; ...
                 'co5_lanekeep_1_1_1_param95.mat',   'parameters 95\%',     '*'; ...
                 'co5_lanekeep_1_1_1_param105.mat',  'parameters 105\%',    '+'; ...
                 'co5_lanekeep_1_1_1_param115.mat',  'parameters 115\%',    'o'; ...
                 'co5_lanekeep_1_1_1_param125.mat',  'parameters 125\%',    'v'};  
else
    error('Non-existing type.');
end

fig = nicefigure('ParamEffects', W, H);
ax = niceaxis(fig, F);
load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', lkConTrjs{1}), 'T_fc_3');
plotVehParamEffect(ax, T_fc_3, nicecolors.BlueBelizeHole, 'p');
load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', lkOptTrjs{1,1}), 'T_co_5');
plotVehParamEffect(ax, T_co_5, nicecolors.PurpleAmethyst, lkOptTrjs{1,3});
load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', lkOptTrjs{2,1}), 'T_co_5_i');
plotVehParamEffect(ax, T_co_5_i, nicecolors.RedAlizarin, lkOptTrjs{2,3});
for optTrjIndex=3:length(lkOptTrjs)
    clear('T_co_5');
    load(fullfile(pwd, 'data', 'Pub_2017_EWGT2017', lkOptTrjs{optTrjIndex, 1}), 'T_co_5_changed');
    plotVehParamEffect(ax, T_co_5_changed,nicecolors.GreenEmerald,lkOptTrjs{optTrjIndex,3}, ['errorparam', lower(type)]);
end

if strcmpi(type, 'LaneChange')
    line([0, 10], [0.6, 0.6], 'Color', 'Black', 'LineStyle', '-.', 'LineWidth', 0.5);
    ylim(ax, [0, 0.8]);
    ax.YTick = 0:0.1:0.8;
    ax.YTickLabel = {'0.0', '0.1', '0.2', '0.3', '0.4', '0.5', '0.6', '1.6', '2.6'};
elseif strcmpi(type, 'LaneKeep')
    line([0, 10], [5, 5], 'Color', 'Black', 'LineStyle', '-.', 'LineWidth', 0.5);
    ylim(ax, [0, 7]);
    ax.YTick = 0:1:7;
    ax.YTickLabel = {'0', '1', '2', '3', '4', '5', '15', '25'};
end

xlim(ax, [0, 10]);
ax.XTick = 0:10;

xlabel(ax, '$i$ (no. of trajectory) [1]');
ylabel('$||C_p||$ (norm of constraint violation) [1]');

l = nicelegend({'constrained', lkOptTrjs{:,2}}, ax, F);
l.Orientation = 'Vertical';
l.Location = 'NorthEastOutside';

name = sprintf('ParamEffects_%s', type);
savefig(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

