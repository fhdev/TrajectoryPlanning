%% VehStCl *****************************************************************************************
% [Summary]
%   This class represents a planar nonlinear single track vehicle model with trajectory tracking
%   controllers.
%
% [Used in]
%   PlannerCon
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehStCl < VehStOl
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % driving torque [Nm] (from previous ode step)
        torqueDrv
        % braking torque [Nm] (from previous ode step)
        torqueBrk
        % steering angle on front wheel [rad] (from previous ode step)
        steerWhlAng

        %% Properties set in constructor -----------------------------------------------------------
        % lateral controller model (CtrlLatTrk / CtrlLqsYr / CtrlLatSty object)
        CtrlLat
        % longitudinal controller model (CtrlPidV / CtrlLqsV object)
        CtrlLon
        % initialization time for switching transient
        TimeInit

    end
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.CtrlLat(self, value)
            if ~isequal(self.CtrlLat, value)
                if isstruct(value)
                    switch value.Type
                        case 'CtrlLatTrk'
                            value = CtrlLatTrk(self.Chassis, self.WheelFront, value);
                        case 'CtrlLqsYr'
                            value = CtrlLqsYr(self.Chassis, self.WheelFront, value);
                        case 'CtrlLatSty'
                            value = CtrlLatSty(self.Chassis, self.Steering, value);
                    end
                end
                if ~(isa(value, 'CtrlLqsYr') || isa(value, 'CtrlLatTrk') || isa(value, 'CtrlLatSty'))
                    error('ERROR: CtrlLat must be a CtrlLqsYr or CtrlLatTrk or CtrlLatSty object!')
                end
                self.CtrlLat = value;
            end
        end

        function set.CtrlLon(self, value)
            if ~isequal(self.CtrlLon, value)
                if isstruct(value)
                    switch value.Type
                        case 'CtrlPidV'
                            value = CtrlPidV(self.Chassis, self.WheelFront, value);
                        case 'CtrlLqsV'
                            value = CtrlLqsV(self.Chassis, self.WheelFront, value);
                    end
                end
                if ~(isa(value, 'CtrlPidV') || isa(value, 'CtrlLqsV'))
                    error('ERROR: CtrlLon must be a CtrlPidV or CtrlLqsV object!')
                end
                self.CtrlLon = value;
            end
        end
        
        function set.TimeInit(self, value)
            if ~isequal(self.TimeInit, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: TimeInit must be a numeric scalar!')
                end
                self.TimeInit = value;
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehStCl(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 7
                superArgs = varargin(1:4);
            else
                 error('ERROR: Inappropriate number of arguments!');
            end
            self@VehStOl(superArgs{:});
            if nargin == 1
                self.CtrlLat = varargin{1}.CtrlLat;
                self.CtrlLon = varargin{1}.CtrlLon;
                self.TimeInit = varargin{1}.TimeInit;
            elseif nargin == 7
                self.CtrlLat  = varargin{5};
                self.CtrlLon  = varargin{6};
                self.TimeInit = varargin{7};
            end
            self.torqueDrv = 0;
            self.torqueBrk = 0;
            self.steerWhlAng = 0;
        end
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of the planar nonlinear single track vehicle
        %   model with trajectory tracking controllers.
        %
        %   The coordinate system of the vehicle is rotated around z-axis by the yaw angle
        %   compared to the ground-fixed system! The coordinate systems of the wheels are rotated
        %   by the steering angle compared to the vehicle-fixed system.
        %   The vehicle-fixed coordinate system is: x points forward (driving direction); y points
        %   left; z points upwards.
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %   Vehicle ==============================================================================
        %   Chassis ------------------------------------------------------------------------------
        %       X(1)  => PosXG:     longitudinal position in ground-fixed cs. [m]
        %       X(2)  => VelXG:     longitudinal velocity in ground-fixed cs. [m/s]
        %       X(3)  => PosYG:     lateral position in ground-fixed cs. [m]
        %       X(4)  => VelYG:     lateral velocity in ground-fixed cs. [m/s]
        %       X(5)  => YawZ:      yaw (heading) angle in ground-fixed cs. [rad]
        %       X(6)  => YawVelZ:   yaw (heading) angular velocity in ground-fixed system [rad/s]
        %   WheelFront ---------------------------------------------------------------------------
        %       X(7)  => PitchY:    angle of the wheel [rad]
        %       X(8)  => PitchVelY: angular velocity of the wheel [rad/s]
        %       X(9)  => SlipX:     longitudinal slip [1]
        %       X(10) => SlipY:     lateral slip [1]
        %   WheelRear ----------------------------------------------------------------------------
        %       X(11) => PitchY:    angle of the wheel [rad]
        %       X(12) => PitchVelY: angular velocity of the wheel [rad/s]
        %       X(13) => SlipX:     longitudinal slip [1]
        %       X(14) => SlipY:     lateral slip [1]
        %   Steering -----------------------------------------------------------------------------
        %       X(15) => SteerAng:  wheel level steering angle [rad]
        %   Control ==============================================================================
        %   CtrlLon ------------------------------------------------------------------------------
        %       X(16) => integral of velocity tracking error in case of CtrlLqsV and CtrlPidV
        %   CtrlLat ------------------------------------------------------------------------------
        %       X(17) => integral of yaw rate tracking error in case of CtrlLqsYr
        %             => integral of longitudinal velocity multiplied by yaw rate tracking error in
        %                case of CtrlLatTrk
        %
        %   U - input vector
        %       U(1) => VelXVRef:        reference longitudinal velocity [m/s]
        %       U(2) => YawVelZRef:    reference yaw rate [rad/s] (used only with CtrlLqsYr)
        %       U(3) => FrictionFront: friction coefficient front [1]
        %       U(4) => FrictionRear:  friction coefficient rear [1]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %   Vehicle ==============================================================================
        %   Chassis ------------------------------------------------------------------------------
        %       Y(1)  => AccXG:     longitudinal acceleration in ground-fixed cs. [m/s^2]
        %       Y(2)  => AccYG:     lateral acceleration in ground-fixed cs. [m/s^2]
        %       Y(3)  => AccInrtXV: longitudinal inertial acceleration in vehicle-fixed cs. [m/s^2]
        %       Y(4)  => AccInrtYV: lateral inertial acceleration in vehicle-fixed cs. [m/s^2]
        %       Y(5)  => YawAccZ:   yaw (heading) angular acceleration in ground-fixed cs. [rad/s^2]
        %       Y(6)  => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
        %       Y(7)  => VelYV:     lateral velocity in vehicle-fixed cs. [m/s]
        %   WheelFront ---------------------------------------------------------------------------
        %       Y(8)  => PitchAccY: angular acceleration [rad/s]
        %       Y(9)  => DSlipX:    longitudinal slip derivative [1/s]
        %       Y(10) => ForceXW:   longitudinal force in wheel-fixed cs. [N]
        %       Y(11) => DSlipY:    lateral slip derivative [1/s]
        %       Y(12) => ForceYW:   lateral force in wheel-fixed cs. [N]
        %       Y(13) => ForceZW:   vertical force in wheel-fixed cs. [N]
        %       Y(14) => TorqueDrv: wheel level driving torque [Nm]
        %       Y(15) => TorqueBrk: wheel level braking torque [Nm]
        %       Y(16) => SteerAng:  wheel level steering angle [rad]
        %   WheelRear ----------------------------------------------------------------------------
        %       Y(17) => PitchAccY: angular acceleration [rad/s]
        %       Y(18) => DSlipX:    longitudinal slip derivative [1/s]
        %       Y(19) => ForceXW:   longitudinal force in wheel-fixed cs. [N]
        %       Y(20) => DSlipY:    lateral slip derivative [1/s]
        %       Y(21) => ForceYW:   lateral force in wheel-fixed cs. [N]
        %       Y(22) => ForceZW:   vertical force in wheel-fixed cs. [N]
        %       Y(23) => TorqueDrv: wheel level driving torque [Nm]
        %       Y(24) => TorqueBrk: wheel level braking torque [Nm]
        %       Y(25) => SteerAng:  wheel level steering angle [rad]
        %   Steering -----------------------------------------------------------------------------
        %       Y(26) => DSteerAng: wheel level steering angle derivative [rad/s]
        %   Control ==============================================================================
        %   CtrlLon ------------------------------------------------------------------------------
        %       Y(27) => TorqueDrv: driving torque output of controller [Nm]
        %       Y(28) => TorqueBrk: braking torque output of controller [Nm]
        %       Y(29) => VErr:      longitudinal velocity tracking error [m]
        %   CtrlLat ------------------------------------------------------------------------------
        %       Y(30) => SteerAng:      steering angle output of controller [rad]
        %       Y(31) => ErrCrossTrack: cross track error [m] in case of CtrlLatSty and CtrlLatTrk
        %       Y(32) => ErrHeading:    heading error [rad] in case of CtrlLatSty and CtrlLatTrk
        % ------------------------------------------------------------------------------------------
        function [dX, Y] = StateEquation(self, t, X, U)
            %% Vehicle

            % Inputs
            % U(1) => TorqueDrv:     driving torque [Nm]
            % U(2) => TorqueBrk:     braking torque [Nm]
            % U(3) => FrictionFront: friction coefficient front [1]
            % U(4) => FrictionRear:  friction coefficient rear [1]
            % U(5) => SteerWhlAng:   steering wheel angle [rad]
            [dXV, YV] = StateEquation@VehStOl(self, t, X(1:15), [self.torqueDrv; self.torqueBrk; U(3:4); self.steerWhlAng]);

            %% Control

            % Longitudinal
            switch class(self.CtrlLon)
                case 'CtrlPidV'
                    % Inputs
                    % U(1) => VelXV:          longitudinal velocity in vehicle-fixed cs. [m/s]
                    % U(2) => VelXVRef:         target longitudinal velocity in vehicle-fixed cs. [m/s]
                    [dXCLon, YCLon] = self.CtrlLon.StateEquation(t, X(16), [YV(6); U(1)]);
                case 'CtrlLqsV'
                    % Inputs
                    % U(1) => VelXV:          longitudinal velocity in vehicle-fixed cs. [m/s]
                    % U(2) => VelXVRef:         target longitudinal velocity in vehicle-fixed cs. [m/s]
                    [dXCLon, YCLon] = self.CtrlLon.StateEquation(t, X(16), [YV(6); U(1)]);
            end
            self.torqueDrv = YCLon(1);
            self.torqueBrk = YCLon(2);

            % Lateral
            switch class(self.CtrlLat)
                case 'CtrlLqsYr'
                    % Inputs
                    % U(1) => VelXV:    longitudinal velocity in vehicle-fixed cs. [m/s]
                    % U(2) => VelYV:    lateral velocity in vehicle-fixed cs. [m/s]
                    % U(3) => YawVelZ:  yaw (heading) angular velocity in ground-fixed system [rad/s]
                    % U(4) => YrErr:    tracking error of yaw angular velocity in ground-fixed system [rad/s]
                    [dXCLat, YCLat] = self.CtrlLat.StateEquation(t, X(17), [YV(6); YV(7); X(6); U(2) - X(6)]);
                    % there is no cross track and heading error calculation
                    YCLat = [YCLat; 0; 0];
                case  'CtrlLatTrk'
                    % U(1) => PosXG:    longitudinal position in ground-fixed cs. [m]
                    % U(2) => PosYG:    lateral position in ground-fixed cs. [m]
                    % U(3) => YawZ:     yaw (heading) angle in ground-fixed cs. [rad]
                    % U(4) => YawVelZ:  yaw (heading) angular velocity in ground-fixed system [rad/s]
                    % U(5) => VelXV:    longitudinal velocity in vehicle-fixed cs. [m/s]
                    % U(6) => VelYV:    lateral velocity in vehicle-fixed cs. [m/s]
                    [dXCLat, YCLat] = self.CtrlLat.StateEquation(t, X(17), [X(1); X(3); X(5:6); YV(6:7)]);
                case 'CtrlLatSty'
                    % U(1) => PosXG:    longitudinal position in ground-fixed cs. [m]
                    % U(2) => PosYG:    lateral position in ground-fixed cs. [m]
                    % U(3) => YawZ:     yaw (heading) angle in ground-fixed cs. [rad]
                    % U(4) => VelXV:    longitudinal velocity in vehicle-fixed cs. [m/s]
                    [dXCLat, YCLat] = self.CtrlLat.StateEquation(t, X(17), [X(1); X(3); X(5); YV(6)]);
            end
            self.steerWhlAng = YCLat(1) * (1 / self.Steering.Ratio);

            %% Set state derivatives
            dX = [dXV; dXCLon; dXCLat];

            %% Set outputs
            Y = [YV; YCLon; YCLat];

        end

        %% Initialize -----------------------------------------------------------------------------
        % [Summary]
        %   This function provides the initial state vector of the planar nonlinear single track
        %   vehicle model with trajectory tracking controllers based on the provided initial
        %   conditios.
        %
        % [Input]
        %   initCon - initial conditions as ChassisStBndCon object.
        %
        % [Output]
        %   X0      - initial state vector of vehicle model
        % ------------------------------------------------------------------------------------------
        function X0 = Initialize(self, initCon, input)
            %% Check arguments
            if ~isa(initCon, 'ChassisStBndCon')
                error('ERROR: initCon must be a ChassisStBndCon object!');
            end
            
            %% Reset internal variables
            self.torqueDrv   = 0;
            self.torqueBrk   = 0;
            self.steerWhlAng = 0;
            
            %% Assemble initial condition vector
            X0 = Initialize@VehStOl(self, initCon);
            switch class(self.CtrlLon)
                case 'CtrlPidV'
                    XLon0 = self.CtrlLon.Initialize();
                case 'CtrlLqsV'
                    XLon0 = self.CtrlLon.Initialize(initCon.VelXV);
            end
            switch class(self.CtrlLat)
                case  'CtrlLqsYr'
                    XLat0 = self.CtrlLat.Initialize([initCon.VelYV; initCon.YawVelZ]);
                case {'CtrlLatTrk', 'CtrlLatSty'}
                    XLat0 = self.CtrlLat.Initialize();
            end
            X0 = [X0; XLon0; XLat0];
            
            %% Simulate switching transient
            if self.TimeInit > 0
                % Plan circular trajectory with yaw rate matching initial yaw rate            
                sp = [0; 0.3 * self.TimeInit * initCon.VelXV; self.TimeInit * initCon.VelXV];
                kp = [0; initCon.YawVelZ / initCon.VelXV; initCon.YawVelZ / initCon.VelXV];
                vp = [initCon.VelXV; initCon.VelXV; initCon.VelXV];            
                [t, ~, x, y, r, v, dr] = PlannerGeom.CurvatureTraj(sp, kp, vp);
                % Set reference for controller
                switch class(self.CtrlLat)
                    case 'CtrlLatTrk'
                        self.CtrlLat.PosXGRef = x;
                        self.CtrlLat.PosYGRef = y;
                        self.CtrlLat.YawZRef = r;
                        self.CtrlLat.YawVelZRef = dr;
                    case 'CtrlLatSty'
                        self.CtrlLat.PosXGRef = x;
                        self.CtrlLat.PosYGRef = y;
                        self.CtrlLat.YawZRef = r;
                end
                % Create input matrix for time-variant input signals
                muf = input.FrictionFront(1) * ones(size(t));
                mur = input.FrictionRear(1) * ones(size(t));
                ts = (t(1) : self.SampleTime : t(end))';
                U = interp1q(t, [v, dr, muf, mur], ts)';
                % Simulate transient
                X0(6) = 0;
                [X, Y] = ode4(self, ts, X0, U);

    %             output = VehStClOut(ts, ...
    %                 ChassisStOut(ts, X(1,:), X(2,:), Y(1,:), Y(6,:), Y(3,:),  X(3,:), X(4,:), ...
    %                     Y(2,:), Y(7,:), Y(4,:), X(5,:), X(6,:), Y(5,:)), ...
    %                 WheelMfExtOut(ts, X(7,:), X(8,:), Y(8,:), X(9,:), Y(9,:), Y(10,:), Y(13,:), ...
    %                     X(10,:), Y(11,:), Y(12,:), Y(14,:), Y(15,:), Y(16,:)), ...
    %                 WheelMfExtOut(ts, X(11,:), X(12,:), Y(17,:), X(13,:), Y(18,:), Y(19,:), Y(22,:), ...
    %                     X(14,:), Y(20,:), Y(21,:), Y(23,:), Y(24,:), Y(25,:)), ...
    %                 SteerFoOut(ts, X(15, :), Y(26, :)), ...
    %                 CtrlLonOut(ts, Y(27, :), Y(28, :), Y(29, :)), ...
    %                 CtrlLatOut(ts, Y(30,:), Y(31,:), Y(32,:)) ...
    %             );

                % Transform input trajectory relative to the vehicle so, that it begins at the reference
                % point of last time step, from vehicle point of view. We have to maintain the tracking
                %  errors to get a continuous motion.
                deltaXV = 0;
                deltaYV = Y(31, end);
                deltaYawZ = Y(32, end);
                sinDeltaYawZ = sin(deltaYawZ);
                cosDeltaYawZ = cos(deltaYawZ);
                % Rotate by heading error and shift with lateral offset
                xRef = + cosDeltaYawZ * input.PosXGRef - sinDeltaYawZ * input.PosYGRef + deltaXV;
                yRef = + sinDeltaYawZ * input.PosXGRef + cosDeltaYawZ * input.PosYGRef + deltaYV;
                yawRef = input.YawZRef  + deltaYawZ;
                % Set reference for controller
                switch class(self.CtrlLat)
                    case  'CtrlLatTrk'
                        self.CtrlLat.PosXGRef = xRef;
                        self.CtrlLat.PosYGRef = yRef;
                        self.CtrlLat.YawZRef = yawRef;
                        self.CtrlLat.YawVelZRef = input.YawVelZRef;
                    case 'CtrlLatSty'
                        self.CtrlLat.PosXGRef = xRef;
                        self.CtrlLat.PosYGRef = yRef;
                        self.CtrlLat.YawZRef = yawRef;
                end
                % Reset closest point index
                self.CtrlLat.iClosestPrev = 0;
                % Assemble initial state vector
                X0 = [0; Y(6, end); 0; Y(7, end); 0; X(6, end); 0; X(8:10, end); 0; X(12:end, end)];
            else
                % Set reference for controller
                switch class(self.CtrlLat)
                    case  'CtrlLatTrk'
                        self.CtrlLat.PosXGRef = input.PosXGRef;
                        self.CtrlLat.PosYGRef = input.PosYGRef;
                        self.CtrlLat.YawZRef = input.YawZRef;
                        self.CtrlLat.YawVelZRef = input.YawVelZRef;
                    case 'CtrlLatSty'
                        self.CtrlLat.PosXGRef = input.PosXGRef;
                        self.CtrlLat.PosYGRef = input.PosYGRef;
                        self.CtrlLat.YawZRef = input.YawZRef;
                end
            end
            
        end

        %% Simulate --------------------------------------------------------------------------------
        % [Summary]
        %   This function simulates the planar nonlinear single track vehicle model with trajectory
        %   tracking controllers.
        %
        % [Input]
        %   initCon  - initial conditions for the vehicle as VehStBndCon object
        %   input    - input as VehStClIn object
        %
        % [Output]
        %   output   - output as VehStClOut object
        % ------------------------------------------------------------------------------------------
        function output = Simulate(self, initCon, input)

            %% Check arguments
            if ~isa(initCon, 'ChassisStBndCon')
                error('ERROR: initCon must be a ChassisStBndCon object!');
            end
            if ~isa(input, 'VehStClIn')
                error('ERROR: input must be a VehStClIn object!');
            end
            
            %% Initial conditions
            X0 = self.Initialize(initCon, input);

            %% Input
            t = (input.Time(1) : self.SampleTime : input.Time(end))';
            if t(end) ~= input.Time(end)
                t(end + 1) = input.Time(end);
            end
            U = interp1q(input.Time, [input.VelXVRef, input.YawVelZRef, input.FrictionFront, input.FrictionRear], t)';

            %% Calculation of the motion
            [X, Y] = ode4(self, t, X0, U);
            
            output = VehStClOut(t, ...
                ChassisStOut(t, X(1,:), X(2,:), Y(1,:), Y(6,:), Y(3,:),  X(3,:), X(4,:), ...
                    Y(2,:), Y(7,:), Y(4,:), X(5,:), X(6,:), Y(5,:)), ...
                WheelMfExtOut(t, X(7,:), X(8,:), Y(8,:), X(9,:), Y(9,:), Y(10,:), Y(13,:), ...
                    X(10,:), Y(11,:), Y(12,:), Y(14,:), Y(15,:), Y(16,:)), ...
                WheelMfExtOut(t, X(11,:), X(12,:), Y(17,:), X(13,:), Y(18,:), Y(19,:), Y(22,:), ...
                    X(14,:), Y(20,:), Y(21,:), Y(23,:), Y(24,:), Y(25,:)), ...
                SteerFoOut(t, X(15, :), Y(26, :)), ...
                CtrlLonOut(t, Y(27, :), Y(28, :), Y(29, :)), ...
                CtrlLatOut(t, Y(30,:), Y(31,:), Y(32,:)) ...
            );

        end

        %% Simulate --------------------------------------------------------------------------------
        % [Summary]
        %   This function simulates the planar nonlinear single track vehicle model with trajectory
        %   tracking controllers with C++ implementation.
        %
        % [Input]
        %   initCon  - initial conditions for the vehicle as VehStBndCon object
        %   input    - input as VehStClIn object
        %
        % [Output]
        %   output   - output as VehStClOut object
        % ------------------------------------------------------------------------------------------
        function output = SimulateMex(self, initCon, input)


            %% Check arguments
            if ~isa(initCon, 'ChassisStBndCon')
                error('ERROR: initCon must be a VehStBndCon object!');
            end
            if ~isa(input, 'VehStClIn')
                error('ERROR: input must be a VehStClIn object!');
            end

            %% Check self
            if ~isa(self.CtrlLon, 'CtrlLqsV')
                error('ERROR: Mex is only implemented for CtrlLqsV as longitudinal control!');
            end
            if ~isa(self.CtrlLat, 'CtrlLatSty')
                error('ERROR: Mex is only implemented for CtrlLatSty as lateral control!');
            end

            %% Parameters

            P = {self.Chassis.Mass;
                 self.Chassis.MomentOfIntertiaZ;
                 self.Chassis.DistanceFrontCOG;
                 self.Chassis.DistanceRearCOG;
                 self.Chassis.HeightCOG;
                 self.Chassis.DragCoefficient;
                 self.Chassis.FrontalArea;
                 self.Chassis.DrivingTorqueSplit;
                 self.WheelFront.Radius;
                 self.WheelFront.MomentOfInertiaY;
                 self.WheelFront.MfMaxX;
                 self.WheelFront.MfStiffnessX;
                 self.WheelFront.MfShapeX;
                 self.WheelFront.MfCurvatureX;
                 self.WheelFront.RollingResistanceConst;
                 self.WheelFront.RollingResistanceLin;
                 self.WheelFront.RollingResistanceSqr;
                 self.WheelFront.RollingResistanceVel;
                 self.WheelFront.RollingResistanceVelCorr;
                 self.WheelFront.BrakingTorqueVel;
                 self.WheelFront.RelaxationLength0X;
                 self.WheelFront.RelaxationLengthMinX;
                 self.WheelFront.SlipDamping0X;
                 self.WheelFront.VelSlowX;
                 self.WheelFront.MfMaxY;
                 self.WheelFront.MfStiffnessY;
                 self.WheelFront.MfShapeY;
                 self.WheelFront.MfCurvatureY;
                 self.WheelFront.RelaxationLength0Y;
                 self.WheelFront.RelaxationLengthMinY;
                 self.WheelFront.SlipDamping0Y;
                 self.WheelFront.VelSlowY;
                 self.WheelFront.MinSlip;
                 self.WheelRear.Radius;
                 self.WheelRear.MomentOfInertiaY;
                 self.WheelRear.MfMaxX;
                 self.WheelRear.MfStiffnessX;
                 self.WheelRear.MfShapeX;
                 self.WheelRear.MfCurvatureX;
                 self.WheelRear.RollingResistanceConst;
                 self.WheelRear.RollingResistanceLin;
                 self.WheelRear.RollingResistanceSqr;
                 self.WheelRear.RollingResistanceVel;
                 self.WheelRear.RollingResistanceVelCorr;
                 self.WheelRear.BrakingTorqueVel;
                 self.WheelRear.RelaxationLength0X;
                 self.WheelRear.RelaxationLengthMinX;
                 self.WheelRear.SlipDamping0X;
                 self.WheelRear.VelSlowX;
                 self.WheelRear.MfMaxY;
                 self.WheelRear.MfStiffnessY;
                 self.WheelRear.MfShapeY;
                 self.WheelRear.MfCurvatureY;
                 self.WheelRear.RelaxationLength0Y;
                 self.WheelRear.RelaxationLengthMinY;
                 self.WheelRear.SlipDamping0Y;
                 self.WheelRear.VelSlowY;
                 self.WheelRear.MinSlip;
                 self.Steering.Ratio;
                 self.Steering.TimeConstant;
                 self.CtrlLon.VelLin;
                 self.CtrlLon.SatVel;
                 self.CtrlLon.SatTorqueDrv;
                 self.CtrlLon.SatTorqueBrk;
                 self.CtrlLon.GainVelX;
                 self.CtrlLon.GainVelXTrackErrInt;
                 self.CtrlLon.GainVelXInit;
                 self.CtrlLat.VelLin;
                 self.CtrlLat.SatVel;
                 self.CtrlLat.SatSteerAng;
                 self.CtrlLat.GainSty;
                 self.CtrlLat.DistanceFrontCOG;
                 self.CtrlLat.TimeConstant;
                 input.PosXGRef;
                 input.PosYGRef;
                 input.YawZRef;
                 self.TimeInit};

            %% Input
            t = (input.Time(1) : self.SampleTime : input.Time(end))';
            if t(end) ~= input.Time(end)
                t(end + 1) = input.Time(end);
            end
            U = interp1q(input.Time, [input.VelXVRef, input.YawVelZRef, input.FrictionFront, input.FrictionRear], t)';

            %% Initial conditions

            X0 = [initCon.PosXG; initCon.VelXV; initCon.PosYG; initCon.VelYV; initCon.YawZ; initCon.YawVelZ];

            %% Calculation of the motion
            [X, Y] = VehStClMex(P, t', X0, U);

            output = VehStClOut(t, ...
                ChassisStOut(t, X(1,:), X(2,:), Y(1,:), Y(6,:), Y(3,:),  X(3,:), X(4,:), ...
                    Y(2,:), Y(7,:), Y(4,:), X(5,:), X(6,:), Y(5,:)), ...
                WheelMfExtOut(t, X(7,:), X(8,:), Y(8,:), X(9,:), Y(9,:), Y(10,:), Y(13,:), ...
                    X(10,:), Y(11,:), Y(12,:), Y(14,:), Y(15,:), Y(16,:)), ...
                WheelMfExtOut(t, X(11,:), X(12,:), Y(17,:), X(13,:), Y(18,:), Y(19,:), Y(22,:), ...
                    X(14,:), Y(20,:), Y(21,:), Y(23,:), Y(24,:), Y(25,:)), ...
                SteerFoOut(t, X(15, :), Y(26, :)), ...
                CtrlLonOut(t, Y(27, :), Y(28, :), Y(29, :)), ...
                CtrlLatOut(t, Y(30,:), Y(31,:), Y(32,:)) ...
            );
        end

        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the tracking errors during vehicle simulation.
        %
        % [Input]
        %       input  - input as VehStClIn object
        %       output - output as VehStClOut object
        %       name   - name for the plots
        % [Output]
        %       figPathTrack - figure handle of path tracking plot
        %       figSigTrack  - figure handle of signal tracking plot
        % ------------------------------------------------------------------------------------------
        function [figPathTrack, figSigTrack, figTrackError] = DisplayPlots(self, input, output, name)
            %% Constants
            FONTSIZE = 8;
            WIDTH = 5;
            HEIGHT = 10;
            WIDTH_PATH = 15;
            HEIGHT_PATH = 10;

            %% Check arguments
            if ~isa(input, 'VehStClIn')
                error('ERROR: input must be a VehStClIn object!');
            end
            if ~isa(output, 'VehStClOut')
                error('ERROR: output must be a VehStClOut object!');
            end

            %% Default arguments
            if nargin < 4
                name = class(self);
            end
            
            %% Calculate tracking errors at CoG
            self.CtrlLat.iClosestPrev = 0;
            eLat = zeros(size(output.Time));
            eYawZ = zeros(size(output.Time));
            for i = 1 : length(eLat)
                [eLat(i), eYawZ(i)] = self.CtrlLat.GetReferenceValues(input.PosXGRef, input.PosYGRef, ...
                    input.YawZRef, [], output.Chassis.PosXG(i), output.Chassis.PosYG(i), ...
                    output.Chassis.YawZ(i)); 
            end
            
            %% Path plot
            figPathTrack = nicefigure([name, '/Path'], WIDTH_PATH, HEIGHT_PATH);
            ax = niceaxis(figPathTrack, FONTSIZE);
            plot(ax, input.PosXGRef, input.PosYGRef, 'Color', nicecolors.YellowSunFlower, 'LineWidth', 4);
            plot(ax, output.Chassis.PosXG, output.Chassis.PosYG, 'Color', nicecolors.RedCarmine);
            axis(ax, 'equal');
            view(ax, -90, 90);
            xlabel(ax, '$x^G \mathrm{[m]}$');
            ylabel(ax, '$y^G \mathrm{[m]}$');
            title(ax, 'path tracking');
            nicelegend({'reference', 'actual'}, ax, FONTSIZE);
            
            %% Signal plot
            figSigTrack = nicefigure([name, '/SignalTracking'], WIDTH, HEIGHT);

            ax = niceaxis(figSigTrack, FONTSIZE);
            subplot(2, 1, 1, ax);
            plot(ax, input.Time, input.VelXVRef, 'Color', Colors.PeterRiver);
            plot(ax, output.Chassis.Time, output.Chassis.VelXV, 'Color', Colors.Pomegrante);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$v_x^V \mathrm{[m/s]}$');
            title('velocity tracking');
            nicelegend({'reference', 'actual'}, ax, FONTSIZE);

            ax = niceaxis(figSigTrack, FONTSIZE);
            subplot(2, 1, 2, ax);
            plot(ax, input.Time, input.YawVelZRef, 'Color', Colors.PeterRiver);
            plot(ax, output.Chassis.Time, output.Chassis.YawVelZ, 'Color', Colors.Pomegrante);
            xlabel('$t \mathrm{[s]}$');
            ylabel('$\dot{\phi}_z \mathrm{[rad/s]}$');
            title('yaw rate tracking');
            nicelegend({'reference', 'actual'}, ax, FONTSIZE);
            
            %% Error plot
            figTrackError = nicefigure([name, '/TrackingErrors'], WIDTH, HEIGHT);
            ax = niceaxis(figTrackError, FONTSIZE);
            subplot(2, 1, 1, ax);
            plot(ax, output.Time, eLat * 100, 'Color', Colors.PeterRiver);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$e_{lat} \mathrm{[cm]}$');
            title(ax, 'cross track error');

            ax = niceaxis(figTrackError, FONTSIZE);
            subplot(2, 1, 2, ax);
            plot(ax, output.Time, rad2deg(eYawZ), 'Color', Colors.Wisteria);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$e_{\psi} \mathrm{[^\circ]}$');
            title(ax, 'heading error');
            
        end
        
        function DisplayAnimation(self, in, out, saveGif)
            %% Constants
            LENGTH = 4.670;
            WIDHT = 2.017;
            SAMPLERATE = 1 / 25;
            
            %% Check arguments
            if ~isa(in, 'VehStClIn')
                error('ERROR: input must be a VehStClIn object!');
            end
            if ~isa(out, 'VehStClOut')
                error('ERROR: output must be a VehStClOut object!');
            end
            if nargin < 4
                saveGif = false;
            end
            
            %% Call animation generation
            lengthFront = LENGTH * self.Chassis.DistanceFrontCOG / ...
                (self.Chassis.DistanceFrontCOG + self.Chassis.DistanceRearCOG);
            lengthRear = LENGTH * self.Chassis.DistanceRearCOG / ...
                (self.Chassis.DistanceFrontCOG + self.Chassis.DistanceRearCOG);
            Util.TrajectoryTrackingAnimation(in.PosXGRef, in.PosYGRef, out.Time, out.Chassis.PosXG, ...
                out.Chassis.PosYG, out.Chassis.YawZ, out.Chassis.AccInrtXV, out.Chassis.AccInrtYV, ...
                out.WheelFront.SteerAng, lengthFront, lengthRear, WIDHT, SAMPLERATE, saveGif)
        end

    end
end
