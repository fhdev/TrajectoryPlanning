%% VehBaseOut **************************************************************************************
% [Summary]
%   This class represents a common base class for all vehicle model output classes.
%
% [Used in]
%   VehQrLonOut
%   VehStOlOut
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehBaseOut < ComTimeInOut
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Chassis related output (ChassisQrLonOut object)
        Chassis

    end
    
    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------
        function value = setChassis(value)
            if isstruct(value)
                value = ChassisQrLonOut(value);
            end
            if ~isa(value, 'ChassisQrLonOut')
                error('ERROR: Chassis must be a ChassisQrLonOut object!')
            end
        end

    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.Chassis(self, value)
            if ~isequal(self.Chassis, value)
                self.Chassis = self.setChassis(value);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = VehBaseOut(varargin)
            if nargin == 1
                comTimeInArgs = varargin(1);
            elseif nargin == 2
                comTimeInArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
           self@ComTimeInOut(comTimeInArgs{:});
            if nargin == 1
                self.Chassis = varargin{1}.Chassis;
            elseif nargin == 2
                self.Chassis = varargin{2};
            end
        end
        
    end
end