function plotVehParamEffect(axis, trj, color, marker, varargin)
    lenTrj = length(trj);
    errors = zeros(lenTrj,1);
    for i=1:lenTrj
        errors(i) = norm(trj(i).C);
    end
    if nargin > 4
        option = varargin{1};
        if strcmpi(option, 'errorparamlanechange')
            errors = not(errors > 0.6) .* errors + (errors > 0.6) .* (0.6 + 0.1 * (errors - 0.6));
        end
        if strcmpi(option, 'errorparamlanekeep')
            errors = not(errors > 5) .* errors + (errors > 5) .* (5 + 0.1 * (errors - 5));
        end
    end
    plot(axis, 1:lenTrj, errors, [':', marker], 'MarkerSize', 8.0, 'MarkerEdgeColor', color, 'LineWidth', 1, 'Color', color);
end