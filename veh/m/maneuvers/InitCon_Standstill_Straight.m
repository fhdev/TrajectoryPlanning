function initCon = InitCon_Standstill_Straight()

initCon.PosXG   = 0;
initCon.VelXV   = 0;
initCon.PosYG   = 0;
initCon.VelYV   = 0;
initCon.YawZ    = 0;
initCon.YawVelZ = 0;

end