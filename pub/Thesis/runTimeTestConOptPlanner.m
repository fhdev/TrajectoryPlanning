%% Parameters
% Velocity
v = 25; % [m/s]
% Longitudinal offset
x = 80;
% Lateral offsets
y_lanechange = 0:3:24; % [m]
y_lanekeep =  0:7:56; % [m]
% Number of time measurements
N = 100;

%% Do setup
% Base input
in = PlannerConIn(PlannerIn_LaneChange());
% Overwirte input
in.InitCon.VelXV = v;
in.FinalCon.PosXG = x;
in.FinalCon.YawZ = 0;
in.FinalCon.YawVelZ = 0;
in.OptVarInit.VelRef = 25;
in.OptVarInit.TravelTime = x / v;
% Planner
planner = PlannerCon(PlannerCon_Default());

%% Measure run-time
% Measure time
n = length(y_lanechange);
t_lanechange = zeros(n, 1);
t_lanekeep = zeros(n, 1);
for i = 1 : n
    % lane change
    in.FinalCon.PosYG = y_lanechange(i);
    in.FinalCon.YawZ = 0;
    in.FinalCon.YawVelZ = 0;
    for j = 1 : N
        out = planner.CalculateTrajectory(in);
        t_lanechange(i) = t_lanechange(i) + out.CalcTime;
    end
    t_lanechange(i) = t_lanechange(i) / N;
    % lane keep
    in.FinalCon.PosYG = y_lanekeep(i);
    [psi, dpsi] = PlannerConFcnVehState.CircConstr(0, 0, x, y_lanekeep(i), v);
    in.FinalCon.YawZ = psi;
    in.FinalCon.YawVelZ = dpsi;
    for j = 1 : N
        out = planner.CalculateTrajectory(in);
        t_lanekeep(i) = t_lanekeep(i) + out.CalcTime;
    end
    t_lanekeep(i) = t_lanekeep(i) / N;
end
