%% Clean up
clearvars();

%% Parameters
% Constant
SAMPLE_RATE_MDL_S = 1e-3;
SAMPLE_RATE_NN_S = 2e-2;
DATASET_NAME = 'drivedata';

% Modifiable
DATA_FOLDER = fullfile('E:', 'VehModel_NN_2020');
NAME = 'MotionDeltasV11';
START_INDEX = 1;
DECIMATION = 1;
BATCH_SIZE = 8196;
BUFFER_SIZE = 1024;
CHECK_SLICING = false; %#ok<*UNRCH>
DATASET_NUM_TRAIN = 1:10;
DATASET_NUM_VALIDATION = 11 : 14;

% Dependent
shift =  SAMPLE_RATE_NN_S / SAMPLE_RATE_MDL_S;
assert(round(shift) == shift);
trainingFolder = fullfile(DATA_FOLDER, NAME);

%% Create folder
if exist(trainingFolder, 'dir')
    error('Training folder %s already exists.', trainingFolder);
else
    mkdir(trainingFolder);
end

%% Write data
% Norms and feature sizes are calculated commonly for train and validation
maxAbsIn = 0;
maxAbsOut = 0;
numInputs = 0;
numOutputs = 0;

% Number of samples is calculated separately for train and validation
numSamplesPerBuffer = BATCH_SIZE * BUFFER_SIZE;
numSamplesTrain = 0;
numSamplesValidation = 0;

for type = {'train', 'validation'}
    type = type{1};
    
    % Choose file names and datasets
    switch type
        case 'train'
            fileInName = 'input_train';
            fileInExt = 'bin';
            fileOutName = 'output_train';
            fileOutExt = 'bin';
            dataSetNumbers = DATASET_NUM_TRAIN;
        case 'validation'
            fileInName = 'input_validation';
            fileInExt = 'bin';
            fileOutName = 'output_validation';
            fileOutExt = 'bin';
            dataSetNumbers = DATASET_NUM_VALIDATION;
    end
    
    % Create necessary variables
    numSamplesWrittenFromMat = 0;
    numSamplesWrittenToFile = 0;
    iFile = 1;
    
    % Open single files if needed
    if CHECK_SLICING
        fileIn1Path = fullfile(trainingFolder, sprintf('%s_single.%s', fileInName, fileInExt));
        fileIn1 = fopen(fileIn1Path, 'w');
        fileOut1Path = fullfile(trainingFolder, sprintf('%s_single.%s', fileOutName, fileOutExt));
        fileOut1 = fopen(fileOut1Path, 'w');
    end
    
    % Open first files
    fileIn = fopen(fullfile(trainingFolder, sprintf('%s_%03d.%s', fileInName, iFile, fileInExt)), 'w');
    fileOut = fopen(fullfile(trainingFolder, sprintf('%s_%03d.%s', fileOutName, iFile, fileOutExt)), 'w');
    
    % Process datasets
    for iDataSet = dataSetNumbers
        % Load dataset
        load(fullfile(DATA_FOLDER, sprintf('%s_%03d.mat', DATASET_NAME, iDataSet)), 'output');
        
        % Create indices for state variables to take
        numAllSamples = length(output.Time);
        indices = (START_INDEX : DECIMATION : numAllSamples - shift)';
        shiftedIndices = ((START_INDEX + shift) : DECIMATION : numAllSamples)';
        avgIndices = indices + (0 : (shift - 1));
        shiftedAvgIndices = shiftedIndices + (0 : (shift - 1));
        
        % I/O definition
        switch NAME
            case {'MotionDeltasV00', 'MotionDeltasV10'}
                vehGenTrainDataInOutV00;
            case 'MotionDeltasV01'
                vehGenTrainDataInOutV01;
            case 'MotionDeltasV02'
                vehGenTrainDataInOutV02;
            case 'MotionDeltasV03'
                vehGenTrainDataInOutV03;
            case 'MotionDeltasV04'
                vehGenTrainDataInOutV04;
            otherwise
                error('add case');
        end
        assert(length(inputNames) == size(matIn, 2));
        assert(length(outputNames) == size(matOut, 2));
        
        % Save scales (common for train and validation)
        maxAbsIn = max(max(abs(matIn)), maxAbsIn);
        maxAbsOut = max(max(abs(matOut)), maxAbsOut);
        
        % Save number of samples
        numSamples = size(matIn, 1);
        numInputs = size(matIn, 2);
        numOutputs = size(matOut, 2);
        switch type 
            case 'train'
                numSamplesTrain = numSamplesTrain + numSamples;
            case 'validation'
                numSamplesValidation = numSamplesValidation + numSamples;
        end
        
        % Reshape matrices in row-major manner for writing and save sizes
        matIn = reshape(matIn', numel(matIn), 1);
        matOut = reshape(matOut', numel(matOut), 1);
        
        % Write to single file if required for testing
        if CHECK_SLICING
            fwrite(fileIn1, matIn, 'float64', 0, 'l');
            fwrite(fileOut1, matOut, 'float64', 0, 'l');
        end
        
        % Write samples into file with determined size of numSamplesPerBuffer samples
        numSamplesWrittenFromMat = 0;
        while numSamplesWrittenFromMat < numSamples
            % Get number of samples that we can write out
            numSamplesWritable = min(numSamples - numSamplesWrittenFromMat, numSamplesPerBuffer - numSamplesWrittenToFile);
            
            % Write to file
            startIndexIn = numInputs *  numSamplesWrittenFromMat + 1;
            endIndexIn = numInputs * (numSamplesWrittenFromMat + numSamplesWritable);
            fwrite(fileIn, matIn(startIndexIn : endIndexIn), 'float64', 0, 'l');
            
            startIndexOut = numOutputs * numSamplesWrittenFromMat + 1;
            endIndexOut = numOutputs * ( numSamplesWrittenFromMat + numSamplesWritable);
            fwrite(fileOut, matOut(startIndexOut : endIndexOut), 'float64', 0, 'l');
            
            % Increase number of samples with the number that we have already written
            numSamplesWrittenToFile = numSamplesWrittenToFile + numSamplesWritable;
            numSamplesWrittenFromMat = numSamplesWrittenFromMat + numSamplesWritable;
            
            % Open new file if necessary because current one is full
            if numSamplesWrittenToFile == numSamplesPerBuffer
                iFile = iFile + 1;
                numSamplesWrittenToFile = 0;
                fclose(fileIn);
                fclose(fileOut);
                if numSamplesWrittenFromMat < numSamples
                    fileIn = fopen(fullfile(trainingFolder, sprintf('%s_%03d.%s', fileInName, iFile, fileInExt)), 'w');
                    fileOut = fopen(fullfile(trainingFolder, sprintf('%s_%03d.%s', fileOutName, iFile, fileOutExt)), 'w');
                end
            end
        end
        
        % Clean up
        clear matIn matOut;
        % Info
        fprintf(1, '%03d/%03d files processed.\n', length(dataSetNumbers), find(dataSetNumbers == iDataSet));
    end
    
    % Close files
    fclose(fileIn);
    fclose(fileOut);
    
    % Close single files if used for testing
    if CHECK_SLICING
        fclose(fileIn1);
        fclose(fileOut1);
    end
    
    %% Testing
    if CHECK_SLICING
        inputFilePaths = fullfile(trainingFolder, strcat(fileInName, '_', ...
            cellstr(reshape(sprintf('%03d', 1:iFile), 3, [])'), '.', fileInExt));
        fileInCatPath = fullfile(trainingFolder, sprintf('%s_cat.%s', fileInName, fileInExt));
        
        catCmd = ['copy /b /y ', strjoin(strcat('"', inputFilePaths, '"'), ' + '), ' "', fileInCatPath, '" 2>&1'];
        [exitCode, stdOutErr] = system(catCmd);
        assert(exitCode == 0, 'Check failed. Reason: %s.', stdOutErr);
        
        fcCmd = ['fc /b "', fileIn1Path, '" "', fileInCatPath, '" 2>&1'];
        [exitCode, stdOutErr] = system(fcCmd);
        assert((exitCode == 0) && contains(stdOutErr, 'no differences encountered'), 'Compare failed. Reason: %s.', stdOutErr);
        
        delete(fileIn1Path);
        delete(fileInCatPath);
        
        outputFilePaths = fullfile(trainingFolder, strcat(fileOutName, '_', ...
            cellstr(reshape(sprintf('%03d', 1:iFile), 3, [])'), '.', fileOutExt));
        fileOutCatPath = fullfile(trainingFolder, sprintf('%s_cat.%s', fileOutName, fileOutExt));
        
        catCmd = ['copy /b /y ', strjoin(strcat('"', outputFilePaths, '"'), ' + '), ' ', fileOutCatPath, ' 2>&1'];
        [exitCode, stdOutErr] = system(catCmd);
        assert(exitCode == 0, 'Check failed. Reason: %s.', stdOutErr);
        
        fcCmd = ['fc /b "', fileOut1Path, '" "',  fileOutCatPath, '" 2>&1'];
        [exitCode, stdOutErr] = system(fcCmd);
        assert((exitCode == 0) && contains(stdOutErr, 'no differences encountered'), 'Compare failed. Reason: %s.', stdOutErr);
        
        delete(fileOut1Path);
        delete(fileOutCatPath);
    end
end
%% Write scales
% Input
fileScaleIn = fopen(fullfile(trainingFolder, 'input_scale.bin'), 'w');
fwrite(fileScaleIn, reshape(maxAbsIn', numel(maxAbsIn), 1), 'float64', 0, 'l');
fclose(fileScaleIn);

% Output
fileScaleOut = fopen(fullfile(trainingFolder, 'output_scale.bin'), 'w');
fwrite(fileScaleOut, reshape(maxAbsOut', numel(maxAbsOut), 1), 'float64', 0, 'l');
fclose(fileScaleOut);

%% Create metadata
% Assemble metadata
metaData.InputNames = inputNames;
metaData.NumInputs = numInputs;
metaData.OutputNames = outputNames;
metaData.NumOutputs = numOutputs;
assert(length(metaData.OutputNames) == metaData.NumOutputs);
metaData.NumSamplesTrain = int64(numSamplesTrain);
metaData.NumSamplesValidation = int64(numSamplesValidation);
metaData.BatchSize = int64(BATCH_SIZE);
metaData.BufferSize = int64(BUFFER_SIZE);

% Write to JSON file
fileMeta = fopen(fullfile(trainingFolder, 'metadata.json'), 'w');
fprintf(fileMeta, '%s\n', jsonencode(metaData));
fclose(fileMeta);

