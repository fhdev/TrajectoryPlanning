%% PlannerOptVarVelPath ****************************************************************************
% [Summary]
%   This class represents the parameters of reference signal profiles used for motion planning.
%   For the longitudinal velocity reference signal, a trapezoidal profile is used, meanwhile the
%   yaw rate reference profile is a polynomial spline.
%
% [Used in]
%   PlannerConIn
%   PlannerCon
%
% [Subclasses]
%   PlannerOptVarVelPathFree
% **************************************************************************************************
classdef PlannerOptVarVelPath < handle
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % end velocity of linear speed reference (scalar) (start velocity is in initial condition)
        VelRef
        % inner knot points of quintic spline path spline (2 element vector)
        YRef         % optimized
        % sample distance between trajectory points (scalar)
        SampleDistance
        
    end
    %% Public instance methods =====================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.VelRef(self, value)
            if ~isequal(self.VelRef, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: VelRef must be a numeric scalar!')
                end
                self.VelRef = value;
            end
        end
        
        function set.YRef(self, value)
            if ~isequal(self.YRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YRef must be a numeric vector!')
                end
                self.YRef = value(:);
            end
        end
        
        function set.SampleDistance(self, value)
            if ~isequal(self.SampleDistance, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SampleDistance must be a numeric scalar!')
                end
                self.SampleDistance = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = PlannerOptVarVelPath(varargin)
            if nargin == 1
                self.VelRef         = varargin{1}.VelRef;
                self.YRef        = varargin{1}.YRef;
                self.SampleDistance = varargin{1}.SampleDistance;
            elseif nargin == 3
                self.VelRef         = varargin{1};
                self.YRef        = varargin{2};
                self.SampleDistance = varargin{3};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
 
        end
        
        %% Update ----------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------
        function updated = Update(self, vector)
            updated = false;
            if ~isequal(self.YRef, vector)
                self.YRef   = vector;
                updated = true;
            end
        end
        
        %% ToVector --------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------
        function vector = ToVector(self)
            vector = self.YRef;
        end
        
        %% ToVehIn ---------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------
        function vehIn = ToVehIn(self, input)
            [t, ~, x, y, r, v, dr] = PlannerGeom.SplineTrajLinVel(input.FinalCon.PosXG(1), ...
                [self.YRef; input.FinalCon.PosYG(1)], input.FinalCon.YawZ(1), input.InitCon.VelXV, ...
                self.VelRef, input.InitCon.YawVelZ, input.FinalCon.YawVelZ(1));
            muf = input.EnvIn.FrictionFront * ones(size(t));
            mur = input.EnvIn.FrictionRear * ones(size(t));
            vehIn = VehStClIn(t, x, y, r, v, dr, muf, mur); 
        end
        
        %% ToPlannerGeomOut ------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------
        function plannerGeomOut = ToPlannerGeomOut(self, input)
            [t, s, x, y, r, v, dr, acp] = PlannerGeom.SplineTrajLinVel(input.FinalCon.PosXG(1), ...
                [self.YRef; input.FinalCon.PosYG(1)], input.FinalCon.YawZ(1), input.InitCon.VelXV, ...
                self.VelRef, input.InitCon.YawVelZ, input.FinalCon.YawVelZ(1));
            plannerGeomOut = PlannerGeomOut(t, s, x, y, r, v, dr, acp);
        end
    end
end