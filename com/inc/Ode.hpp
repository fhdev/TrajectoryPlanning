#pragma once
#ifndef ODE_HPP
#define ODE_HPP

#include "Eigen/Dense"
#include "TypesTp.hpp"

namespace TrajectoryPlanning
{
    class DynamicSystem
    {
    public:

		virtual Int64_T nP() const = 0;
		virtual Int64_T nX() const = 0;
		virtual Int64_T nU() const = 0;
		virtual Int64_T nY() const = 0;

		void CheckSizes(const Eigen::Ref<const Eigen::VectorXd> X,
			const Eigen::Ref<const Eigen::VectorXd> U,
			const Eigen::Ref<const Eigen::VectorXd> dX,
			const Eigen::Ref<const Eigen::VectorXd> Y) const;

        virtual Int32_T Derivatives(Float64_T t,
            const Eigen::Ref<const Eigen::VectorXd> X,
            const Eigen::Ref<const Eigen::VectorXd> U,
            Eigen::Ref<Eigen::VectorXd> dX,
            Eigen::Ref<Eigen::VectorXd> Y) const = 0;
    };

    Int32_T Rk4(DynamicSystem& sys,
        const Eigen::Ref<const Eigen::VectorXd> t,
        const Eigen::Ref<const Eigen::VectorXd> X0,
        const Eigen::Ref<const Eigen::MatrixXd> U,
        Eigen::Ref<Eigen::MatrixXd> X,
        Eigen::Ref<Eigen::MatrixXd> Y);
}

#endif // !ODE_HPP

