%% WheelMfBndCon ***********************************************************************************
% [Summary]
%   This class represents boundary conditions for wheel model with Magic Formula tire.
%
% [Used in]
%   none
%
% [Subclasses]
%   WheelMfBndCon
% **************************************************************************************************
classdef WheelMfBndCon < WheelMfLonBndCon
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Lateral slip [1]
        SlipY
        
    end
    
    %% Instance methods ============================================================================    
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.SlipY(self, value)
            if ~isequal(self.SlipY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SlipY must be numeric scalar!')
                end
                self.SlipY = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------        
        function self = WheelMfBndCon(varargin)
            if nargin == 0
                superArgs = {};
            elseif nargin == 1
                superArgs = varargin(1);
            elseif nargin == 4
                superArgs = varargin(1:3);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@WheelMfLonBndCon(superArgs{:});
            if nargin == 0
                self.SlipY = 0;
            elseif nargin == 1
                self.SlipY = varargin{1}.SlipY;
            elseif nargin == 4
                self.SlipY = varargin{4};
            end
        end
        
    end
end