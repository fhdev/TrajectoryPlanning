%% PlannerConOut ************************************************************************************
% [Summary]
%   This class represents outputs of unconstrained / constrained nonlinear optimization based motion 
%   planner PlannerOpt / PlannerCopt.
%
% [Used in]
%   PlannerOpt
%   PlannerCopt
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef PlannerOptOut < PlannerConOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Cost function of trajectory tracking
        CostTrack
        % Cost function of dynamics
        CostState
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        
        function set.CostTrack(self, value)
            if ~isequal(self.CostTrack, value)
                if ~(isa(value, 'PlannerOptFcnTrack') && isscalar(value))
                    error('ERROR: CostTrack must be a PlannerOptFcnTrack object!')
                end
                self.CostTrack = value;
            end
        end
        
        function set.CostState(self, value)
            if ~isequal(self.CostState, value)
                if ~(isa(value, 'PlannerOptFcnVehState') && isscalar(value))
                    error('ERROR: CostState must be a PlannerOptFcnVehState object!')
                end
                self.CostState = value;
            end
        end
        
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerOptOut(calcTime, success, optVar, vehIn, vehOut, conState, costTrack, costState)
            self@PlannerConOut(calcTime, success, optVar, vehIn, vehOut, conState);
            %% Set properties
            self.CostTrack = costTrack;
            self.CostState = costState;
        end
    end
end
