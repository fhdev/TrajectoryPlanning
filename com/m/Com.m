%% Com *********************************************************************************************
% [Summary]
%   This class represents parameters and functions that are commonly used by all modules.
%
% [Used in]
%   All classes.
% 
% [Subclasses]
%   none
% **************************************************************************************************
classdef Com
    %% Constant properties =========================================================================
    properties (Constant)
        % Gravitational acceleration [m/s^2]
        Gravitation               = 9.80665;
        % Mass density of air [kg/m^3]
        AirDensity                = 1.225;
    end
    %% Public static methods =======================================================================
    methods (Static)
        %% InvalidPropertyMsg ----------------------------------------------------------------------
        % [Summary]
        %   This method creates a standard error message about an invalid value provoded for a 
        %   class property. This function is mainly used in class constructors.
        %
        % [Input]
        %   className - name of class for which the invalid property values is provided
        %   propName  - name of property for wtich the invalid value is provided 
        %
        % [Output]
        %    msg - error message
        % ------------------------------------------------------------------------------------------
        function msg = InvalidPropertyErrorMsg(className, propName)
            msg = [className, ': Invalid value provided for property ''', propName, '''!'];
        end
        %% MissingPropertyErrorMsg -----------------------------------------------------------------
        % [Summary]
        %   This method creates a standard error message if no value is provoded for a class
        %   property. This function is mainly used in class constructors.
        %
        % [Input]
        %   className - name of class for which property value is missing
        %   propName  - name of property for which no value is provided
        %
        % [Output]
        %    msg - error message
        % ------------------------------------------------------------------------------------------      
        function msg = MissingPropertyErrorMsg(className, propName)
            msg = [className, ': No value provided for property ''', propName, '''!'];
        end
        %% InvalidArgErrorMsg ----------------------------------------------------------------------
        % [Summary]
        %   This method creates a standard error message about invalid arguments provided to methods
        %   that are enabling polymorphism (variable number and type of arguments).
        %
        % [Input]
        %   className  - name of class
        %   methodName - name of method
        %   argName    - name of invalid argument
        %
        % [Output]
        %	msg - error message
        % ------------------------------------------------------------------------------------------
        function msg = InvalidArgErrorMsg(className, methodName, argName)
            if nargin == 2
                msg = [className, ': Invalid arguments provided to method ''', methodName, '''!'];
            elseif nargin == 3
                msg = [className, ': Invalid argument ''', argName, ''' provided to method ''', ...
                    methodName, '''!'];
            end
        end
        %% ArgNumErrorMsg --------------------------------------------------------------------------
        % [Summary]
        %   Tis method creates a standard error message about invalid number of arguments provided
        %   to methods that are enabling variable number of arguments.
        %
        % [Input]
        %   className  - name of class
        %   methodName - name of method
        %
        % [Output]
        %   msg - error message
        % ------------------------------------------------------------------------------------------
        function msg = ArgNumErrorMsg(className, methodName)
            msg = [className, ': Invalid number of arguments provided to method ''', methodName, ...
                '''!'];
        end
    end
end