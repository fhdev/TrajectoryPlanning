#include <cmath>
#include <chrono>
#include "PlannerGeom.hpp"
#include "MathTp.hpp"

using namespace Eigen;
using namespace TrajectoryPlanning;

int main(int argc, char *argv[])
{
	
	VectorXd x = VectorXd::LinSpaced(5, 1, 5);
	VectorXd y = VectorXd::LinSpaced(5, 2, 10);
	VectorXd xq(2);	xq << 1, 5;
	VectorXd yq(2);
	interp1(x, y, xq, yq);


	/*
	VectorXd x(4);
	x << 0, 1, 3, 5;
	VectorXd y(4);
	y << 0, -3, 10, 2;
	VectorXd dyb(2);
	dyb << 0.1, 0.1;
	VectorXd ddyb(2);
	ddyb << 0, 0;

	for (int i = 2; i <= 5; i++)
	{
		Int32_T lxq = pow(10, i) + 1;
		VectorXd xq = VectorXd::LinSpaced(lxq, 0, 5);
		std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
		for (int j = 0; j < 1e2; j++)
		{
			VectorXd yq = VectorXd::Zero(lxq);
			VectorXd dyq = VectorXd::Zero(lxq);
			VectorXd ddyq = VectorXd::Zero(lxq);
			interp4spline5(x, y, dyb, ddyb, xq, yq, dyq, ddyq);
		}
		std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
		Float64_T ellapsed = (std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()) / (1e6);
		printf("Elapsed time is %g seconds.\n", ellapsed);
	}
	*/

	/*
	Float64_T xf = 100;
	Float64_T yf = 10;
	VectorXd yp(3); yp << 1.5, 8.5, 10;
	Float64_T rf = 45.0 / 180.0 * 3.141592654;
	Float64_T vi = 10;
	Float64_T vf = 20;
	Float64_T dri = 0.1;
	Float64_T drf = 0.1;
	
	PlannerGeom p = PlannerGeom();
	std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
	for(int i = 0; i < 500; i++)
	{
		VectorXd t, s, x, y, r, v, dr, acp;
		p.SplineTrajLinVel(xf, yp, rf, vi, vf, dri, drf, t, s, x, y, r, v, dr, acp);
	}
	std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
	Float64_T ellapsed = (std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count()) / (1e6);
	printf("ellapsed=%g\n", ellapsed);
	*/
	return 0;
}