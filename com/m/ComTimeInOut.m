%% ComTimeInOut ************************************************************************************
% [Summary]
%   This class represents a common base class for all input and output classes.
%
% [Used in]
%   VehEnvIn1W
%   VehEnvIn2W
%   VehOlLonIn
%   VehClIn
%   ChassisQrLonOut
%   WheelMfLonOut
%   VehBaseOut
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef ComTimeInOut < handle
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % simulation time [s]
        Time
                        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------        
        function set.Time(self, value)
            if ~isequal(self.Time, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: Time must be a numeric vector!');
                end
                self.Time = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = ComTimeInOut(varargin)
            if nargin == 1
                if isstruct(varargin{1}) || isobject(varargin{1})
                    self.Time = varargin{1}.Time;
                else
                    self.Time = varargin{1};
                end
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end
        
    end
end