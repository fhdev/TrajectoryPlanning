%% VehStClOut **************************************************************************************
% [Summary]
%   This class represents output for closed-loop single track vehicle model. 
%
% [Used in]
%   VehStCl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehStClOut < VehStOlOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Longitudinal controller related outout (CtrlLonOut object)
        CtrlLon
        % Lateral controller related output (CtrlLatOut object)
        CtrlLat

    end

    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------        
        function set.CtrlLon(self, value)
            if ~isequal(self.CtrlLon, value)
                if isstruct(value)
                    value = CtrlLonOut(value);
                end
                if ~isa(value, 'CtrlLonOut')
                    error('ERROR: CtrlLon must be a CtrlLonOut object!')
                end
                self.CtrlLon = value;
            end
        end
        
        function set.CtrlLat(self, value)
            if ~isequal(self.CtrlLat, value)
                if isstruct(value)
                    value = CtrlLatOut(value);
                end
                if ~isa(value, 'CtrlLatOut')
                    error('ERROR: CtrlLat must be a CtrlLatOut object!')
                end
                self.CtrlLat = value;
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehStClOut(varargin)
            if nargin == 1
                vehStOutArgs = varargin(1);
            elseif nargin == 7
                vehStOutArgs = varargin(1:5);
            else
                 error('ERROR: Inappropriate number of arguments!');
            end
            self@VehStOlOut(vehStOutArgs{:});
            if nargin == 1
                self.CtrlLon = varargin{1}.CtrlLon;
                self.CtrlLat = varargin{1}.CtrlLat;
            elseif nargin == 7
                self.CtrlLon  = varargin{6};
                self.CtrlLat  = varargin{7};
            end
        end
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the single track vehicle model.
        %
        % [Input]
        %       name   - name for the plots
        %
        % [Output]
        %       figWheelDynF - figure handle of plot containing wheel dynamics related quantities
        %       figWheelInF  - figure handle of plot containing wheel input related quantities
        %       figWheelDynR - figure handle of plot containing wheel dynamics related quantities
        %       figWheelInR  - figure handle of plot containing wheel input related quantities
        %       figGlob      - figure handle of plot containing chassis related quantities in global
        %       figLoc       - figure handle of plot containing chassis related quantities in local
        %       figPath      - figure handle of vehicle path plot
        %       figLonCtrl   - figure handle of plot containing longitudinal control related quantities   
        %       figLatCtrl   - figure handle of plot containing lateral control related quantities  
        % ------------------------------------------------------------------------------------------
        function [figWheelDynF, figWheelInF, figWheelDynR, figWheelInR, figGlob, figLoc, ...
                figPath, figLonCtrl, figLatCtrl] = DisplayPlots(self, name)
            %% Constants
            WIDTH = 5;
            HEIGHT3 = 15;
            FONTSIZE = 8;

            %% Default arguments
            if nargin < 2
                name = class(self);
            end

            %% Create plots
            [figWheelDynF, figWheelInF, figWheelDynR, figWheelInR, figGlob, figLoc, figPath] = DisplayPlots@VehStOlOut(self, name);

            %% Longitudinal controller
            figLonCtrl = self.CtrlLon.DisplayPlots([name, '/CtrlLon']);
            
            %% Lateral controller
            figLatCtrl =  self.CtrlLat.DisplayPlots([name, '/CtrlLat']);

            % Activate path plot
            figure(figPath);
        end
    end
end