%% PlannerCoptIn *********************************************************************************
% [Summary]
%   This class represents inputs for consrained-optimal nonlinear optimization based motion planner 
%   PlannerCopt.
%
% [Used in]
%   PlannerCopt
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef PlannerCoptIn < PlannerOptIn
    %% Static methods ==============================================================================
    methods (Static)
        
        function value = setOptVarInit(value)
            if isstruct(value)
                value = PlannerOptVarVelPathFree(value);
            end
            if ~isa(value, 'PlannerOptVarVelPathFree')
                error('ERROR: OptVarInit must be a PlannerOptVarVelPathFree object!');
            end
        end
        
    end
    
    %% Instance methods ============================================================================    
    methods  
        %% Constructor  ----------------------------------------------------------------------------
       function self = PlannerCoptIn(varargin)
            self@PlannerOptIn(varargin{:});
        end
        
    end
    
end
