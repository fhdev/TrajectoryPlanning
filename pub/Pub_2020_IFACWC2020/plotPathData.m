% Subsampling factor
subs = @(x) x(1:50:end);

% Parameters
W = 7;
H = 4;
F = 8;

type = 'LaneKeep';

% Data
load(fullfile(pwd, 'data', 'Pub_2020_IFACWC2020', 'sampleTrajectories.mat'), 'plannerOpt', 'samplesOpt');


%% Path
fig1 = nicefigure('Path', W, H);
ax1 = niceaxis(fig1, F);

% Dummy for legends
plot(ax1, nan, nan, 'Color', nicecolors.BlueBelizeHole);
plot(ax1, nan, nan, 'Color', nicecolors.GreenEmerald);

if strcmpi(type, 'LaneChange')
    indices = 1 : 2 : length(samplesOpt);
elseif strcmpi(type, 'LaneKeep')
    indices = 2 : 2 : length(samplesOpt);
end

%indices = 9;

for i = indices
    plot(ax1, samplesOpt(i).Out.VehIn.PosXGRef, samplesOpt(i).Out.VehIn.PosYGRef, ...
        'Color', nicecolors.GreenEmerald);
    
    plot(ax1, subs(samplesOpt(i).Out.VehOut.Chassis.PosXG), subs(samplesOpt(i).Out.VehOut.Chassis.PosYG), ...
        'Color', nicecolors.BlueBelizeHole);
    
    plot(ax1, samplesOpt(i).In.InitCon.PosXG, samplesOpt(i).In.InitCon.PosYG, 'MarkerSize', 2.0, ...
        'Marker', 'o', 'MarkerEdgeColor', 'Black', 'MarkerFaceColor', 'Black');
    plot(ax1, samplesOpt(i).In.FinalCon.PosXG, samplesOpt(i).In.FinalCon.PosYG, 'MarkerSize', 2.0, ...
        'Marker', 'o', 'MarkerEdgeColor', 'Black', 'MarkerFaceColor', 'Black');
end
axis(ax1, 'equal');

if strcmpi(type, 'LaneChange')
    xlim(ax1, [-5, 45]);
    ylim(ax1, [-5, 15]);
elseif strcmpi(type, 'LaneKeep')
    xlim(ax1, [-5, 45]);
    ylim(ax1, [-5, 20]);
end


xlabel(ax1, '$x^G_v$ (pos. north) [m]');
ylabel(ax1, '$y^G_v$ (pos. west) [m]');

l = nicelegend({'reference', 'vehicle'}, ax1, F);
l.Orientation = 'Horizontal';

% Adjust by hand
fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

% Save
name = sprintf('CubicSplinePath_%s', type);
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

%% Path + heading
fig2 = nicefigure('PosDevAcc', W, H);
ax2 = niceaxis(fig2, F);

% Dummy for legends
plot(ax2, nan, nan, 'Color', nicecolors.BlueBelizeHole);
plot(ax2, nan, nan, 'Color', nicecolors.GreenEmerald);

dmax = [];
amax = [];
for i = indices
    dmax = [dmax; sqrt(samplesOpt(i).Out.ConState.Value.PosXG^2 + samplesOpt(i).Out.ConState.Value.PosYG^2)];
    amax = [amax; max(abs(samplesOpt(i).Out.VehOut.Chassis.AccInrtYV))];
end

yyaxis(ax2, 'left');
plot(ax2, 1:length(dmax), dmax, 'Color', nicecolors.PurpleAmethyst, 'LineStyle', '-', 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.PurpleAmethyst, 'MarkerFaceColor', nicecolors.PurpleAmethyst, ...
    'MarkerSize', 3);
ylabel(ax2, '$d_{max}$ (max. pos. dev.) [m]');
ax2.YAxis(1).Color = nicecolors.PurpleAmethyst;

yyaxis(ax2, 'right');
plot(ax2, 1:length(amax), amax, 'Color', nicecolors.RedAlizarin, 'LineStyle', '-', 'Marker', 's', ...
    'MarkerEdgeColor', nicecolors.RedAlizarin, 'MarkerFaceColor', nicecolors.RedAlizarin, ...
    'MarkerSize', 3);
ylabel(ax2, '$\mathrm{max} ( | \ddot{y}^V_{v,i} | )$ (lat. acc.) [m/s\textsuperscript{2}]');
ax2.YAxis(2).Color = nicecolors.RedAlizarin;

xlabel(ax2, '$i$ (no. of trajectory) [1]');
ax2.XTick = 0:10;

% Adjust by hand
fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

% Save
name = sprintf('PosDevAcc_%s', type);
savefig(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');


