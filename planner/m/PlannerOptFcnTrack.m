classdef PlannerOptFcnTrack < handle
    %% Instance properties =========================================================================    
    properties
        %% Internal properties ---------------------------------------------------------------------
        % property names for CtrlLatOut
        propNames
        
        %% Properties set in constructor -----------------------------------------------------------
        % cost function weights
        Weight
        % actual average tracking error values along trajectory 
        Value
    end
    
    %% Instance methods ============================================================================    
    methods
        
        %% Property getter and setter methods ------------------------------------------------------
        
        function set.Weight(self, value)
            if ~isequal(self.Weight, value)
                if isstruct(value)
                    value = CtrlLatOut(value);
                end
                if ~isa(value, 'CtrlLatOut')
                    error('ERROR: Weight must be a CtrlLatOut object!');
                end
                self.Weight = value;
            end
        end
        
        function set.Value(self, value)
            if ~isequal(self.Value, value)
                if isstruct(value)
                    value = CtrlLatOut(value);
                end
                if ~isa(value, 'CtrlLatOut')
                    error('ERROR: Value must be a CtrlLatOut object!');
                end
                self.Value = value;
            end
        end
                
        %% Constructor  ----------------------------------------------------------------------------
        
        function self = PlannerOptFcnTrack(weight)
            %% Check arguments
            if ~(isa(weight, 'CtrlLatOut'))
                error('ERROR: weight must be a CtrlLatOut object!');
            end
            %% Initialize
            self.Weight = CtrlLatOut(weight);
            nulls = repmat({0}, 1, 4);
            self.Value = CtrlLatOut(nulls{:});
            self.propNames = properties('CtrlLatOut');
        end
        
        %% Update ----------------------------------------------------------------------------------
        % [Summary]
        %   This function updates state tracking error related cost values according to given  
        %   vehicle simulation output.
        %
        % [Input]
        %   chassisOut - output of current vehicle simulation as CtrlLatOut object
        %
        % [Output]
        %   none
        % ------------------------------------------------------------------------------------------        
        function Update(self, ctrlOut)
            %% Check arguments
            if ~(isa(ctrlOut, 'CtrlLatOut'))
                error('ERROR: ctrlOut most be a CtrlLatOut object!');
            end
            %% Calculate state constraints
            for iProp = 1 : length(self.propNames)
                % There is weight specified
                if ~isnan(self.Weight.(self.propNames{iProp}))
                    % Calculate average deviation
                    self.Value.(self.propNames{iProp}) = ...
                        sqrt(trapz(ctrlOut.Time, ctrlOut.(self.propNames{iProp}).^2)) / ...
                        (ctrlOut.Time(end) - ctrlOut.Time(1));
                    
                    %self.Value.(self.propNames{iProp}) = max(abs(self.propNames{iProp}));  
                else
                    self.Value.(self.propNames{iProp}) = nan;                    
                end
            end
        end
        
        %% ToScalar --------------------------------------------------------------------------------
        % [Summary]
        %   This method provides scalar cost value as the weighted sum of average absolute tracking
        %   error values.
        %
        % [Input]
        %   none
        %
        % [Output]
        %   cost - scalar cost value
        % ------------------------------------------------------------------------------------------        
        function cost = ToScalar(self)
            cost = 0;
            for iProp = 1 : length(self.propNames)
                % There is weight specified
                if ~isnan(self.Weight.(self.propNames{iProp}))
                    cost = cost + self.Weight.(self.propNames{iProp}) * self.Value.(self.propNames{iProp});
                end
            end
        end
    end
end
