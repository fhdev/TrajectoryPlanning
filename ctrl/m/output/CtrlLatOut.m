%% CtrlLatOut **************************************************************************************
% [Summary]
%   This class represents common outputs for lateral (path tracking) control. 
%
% [Used in]
%   VehStClOut
%   PlannerOptIn
%   PlannerOptFcnTrack
%
% [Subclasses]
%
% **************************************************************************************************
classdef CtrlLatOut < VehOlLatIn   
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Cross track error during tracking [m]
        CrossTrackErr
        % Heading error during tracking [rad]
        HeadingErr
        
    end
    
    %% Instance methods ============================================================================
    methods        
        %% Property getter and setter methods ------------------------------------------------------        
        function set.CrossTrackErr(self, value)
            if ~isequal(self.CrossTrackErr, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: CrossTrackErr must be a numeric vector!');
                end
                self.CrossTrackErr = value(:);
            end
        end
        
        function set.HeadingErr(self, value)
            if ~isequal(self.HeadingErr, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: HeadingErr must be a numeric vector!');
                end
                self.HeadingErr = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------        
        function self = CtrlLatOut(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 4
                superArgs = varargin(1:2);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@VehOlLatIn(superArgs{:});
            if nargin == 1
                self.CrossTrackErr = varargin{1}.CrossTrackErr;
                self.HeadingErr    = varargin{1}.HeadingErr;
            elseif nargin == 4
                self.CrossTrackErr = varargin{3};
                self.HeadingErr    = varargin{4};
            end
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plot about the lateral controller.
        %
        % [Input]
        %       name   - name for the plot
        %
        % [Output]
        %       fig - figure handle of plot
        % ------------------------------------------------------------------------------------------          
        function fig = DisplayPlots(self, name)
            %% Constants
            WIDTH = 5;
            HEIGHT3 = 15;
            FONTSIZE = 8;
            
            %% Default arguments
            if nargin < 2
                name = class(self);
            end
            
            %% Create plot
            
            fig = nicefigure(name, WIDTH, HEIGHT3);

            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 1, 1, ax);
            plot(ax, self.Time, rad2deg(self.SteerWhlAng), 'Color', Colors.Nephritis);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$\delta_{sw} \mathrm{[^\circ]}$');
            title(ax, 'wheel level steering angle');

            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 1, 2, ax);
            plot(ax, self.Time, self.CrossTrackErr * 100, 'Color', Colors.PeterRiver);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$e_{lat} \mathrm{[cm]}$');
            title(ax, 'cross track error');

            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 1, 3, ax);
            plot(ax, self.Time, rad2deg(self.HeadingErr), 'Color', Colors.Wisteria);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$e_{\psi} \mathrm{[^\circ]}$');
            title(ax, 'heading error');
        end
        
    end
end
