close all;
clear;
%% Constants
FONTSIZE = 8;
AXISLABEL_TIME = '$t \mathrm{[s]}$';

%% Helpers
unitlabel = @(label, unit) sprintf('$%s \\mathrm{[%s]}$', label, unit);

%% Create plots for all measurements
measFolder = fullfile(Util.GetRepoRoot, '_data', 'ZalaZone_20190528');
list = dir(fullfile(measFolder, 'MEAS_*_S.mat'));


for iFile = 1 : length(list)
    %% Load measurement and create figures
    filePath = fullfile(measFolder, list(iFile).name);
    load(filePath);
    
    [~, fileName] = fileparts(list(iFile).name);
    figName1 = sprintf('%s_X%d_Y%d_Path', fileName, round(abs(data.plannerIn.FinalCon.PosXG) * 100), ...
        round(abs(data.plannerIn.FinalCon.PosYG) * 100));
    measFig1 = nicefigure(figName1, 8.8, 8.8);
    
    figName2 = sprintf('%s_X%d_Y%d_Err', fileName, round(abs(data.plannerIn.FinalCon.PosXG) * 100), ...
        round(abs(data.plannerIn.FinalCon.PosYG) * 100));
    measFig2 = nicefigure(figName1, 8.8, 8.8);
    
    %% Find start and end of maneuver
    bInd = find(~isnan(data.S.steer) & data.S.steer ~= 0, 1, 'first');
    if isempty(bInd)
        bInd = 1;
    end
    eInd = find(~isnan(data.S.steer) & data.S.steer ~= 0, 1, 'last');
    if isempty(eInd)
        eInd = length(data.S.steer);
    end
    
    %% Path plot
    sinYaw0 = sin(data.Planner.Vehicle.CtrlLat.YawZRef(1));
    cosYaw0 = cos(data.Planner.Vehicle.CtrlLat.YawZRef(1));
    xG = + data.Planner.vehOut.Chassis.PosXG * cosYaw0 - data.Planner.vehOut.Chassis.PosYG * sinYaw0 + ...
        data.Planner.Vehicle.CtrlLat.PosXRef(1);
    yG = + data.Planner.vehOut.Chassis.PosXG * sinYaw0 + data.Planner.vehOut.Chassis.PosYG * cosYaw0 + ...
        data.Planner.Vehicle.CtrlLat.PosYRef(2);
    pathAx = niceaxis(measFig1, FONTSIZE);
    expand(pathAx);
    subplot(2, 1, 1, pathAx);
    plot(pathAx, data.Planner.Vehicle.CtrlLat.PosXRef, data.Planner.Vehicle.CtrlLat.PosYRef, ...
        'Color', nicecolors.YellowSunFlower, 'LineWidth', 5);
    plot(pathAx, xG, yG, ...
        'Color', nicecolors.RedCarmine, 'LineWidth', 3, 'LineStyle', ':');
    plot(pathAx, data.S.x(bInd : eInd), data.S.y(bInd : eInd), ...
        'Color', nicecolors.BlueDeepKoamaru, 'LineWidth', 1.5);
    axis(pathAx, 'equal');
    xlabel(pathAx, unitlabel('West', 'm'));
    ylabel(pathAx, unitlabel('North', 'm'));
    nicelegend({'reference', 'prediction', 'actual'}, pathAx, FONTSIZE);
    
    %% Steering angle plot
    steerAx = niceaxis(measFig1, FONTSIZE);
    expand(steerAx);
    subplot(2, 1, 2, steerAx);
    plot(steerAx, data.plannerOut.VehOut.Input.Time + data.t(end, bInd), ...
        -16 * rad2deg(data.plannerOut.VehOut.Input.SteerAng), ...
        'Color', nicecolors.RedCarmine, 'LineStyle', '-', 'LineWidth', 1);
    plot(steerAx, data.t(end, (bInd : eInd)), data.S.steer(bInd : eInd), ...
        'Color', nicecolors.BlueDeepKoamaru, 'LineStyle', '-', 'LineWidth', 1);
    xlim(steerAx, data.t(end, [bInd, eInd]));
    ylim(steerAx, 1.5 * [min([steerAx.Children(1:2).YData]), max([steerAx.Children(1:2).YData])]);
    nicelegend({'prediction', 'actual'}, steerAx, FONTSIZE);
    xlabel(steerAx, unitlabel('t', 's'));
    ylabel(steerAx, unitlabel('\delta_{sw}', '^{\circ}'));
    
    %% Tracking error plot    
    errOffsAx = niceaxis(measFig2, FONTSIZE);
    expand(errOffsAx);
    subplot(2, 1, 1, errOffsAx);
    plot(errOffsAx, data.plannerOut.VehOut.Ctrl.Time + data.t(end, bInd), ...
        data.plannerOut.VehOut.Ctrl.CrossTrackErr * 100, ...
        'Color', nicecolors.RedCarmine, 'LineStyle', '-', 'LineWidth', 1);
    plot(errOffsAx, data.t(end, (bInd : eInd)), data.S.offsetError(bInd : eInd) * 100, ...
        'Color', nicecolors.BlueDeepKoamaru, 'LineStyle', '-', 'LineWidth', 1);
    xlim(errOffsAx, data.t(end, [bInd, eInd]));
    ylim(errOffsAx, 1.5 * [min([errOffsAx.Children(1:2).YData]), max([errOffsAx.Children(1:2).YData])]);
    nicelegend({'prediction', 'actual'}, errOffsAx, FONTSIZE);
    xlabel(errOffsAx, unitlabel('t', 's'));
    ylabel(errOffsAx, unitlabel('e_{lat}', 'cm'));
    
    errAngAx = niceaxis(measFig2, FONTSIZE);
    expand(errAngAx);
    subplot(2, 1, 2, errAngAx);
    plot(errAngAx, data.plannerOut.VehOut.Ctrl.Time + data.t(end, bInd), ...
        rad2deg(data.plannerOut.VehOut.Ctrl.HeadingErr), ...
        'Color', nicecolors.RedCarmine, 'LineStyle', '-', 'LineWidth', 1);
    plot(errAngAx, data.t(end, (bInd : eInd)), rad2deg(data.S.angleError(bInd : eInd)), ...
        'Color', nicecolors.BlueDeepKoamaru, 'LineStyle', '-', 'LineWidth', 1);
    xlim(errAngAx, data.t(end, [bInd, eInd]));
    ylim(errAngAx, 1.5 * [min([errAngAx.Children(1:2).YData]), max([errAngAx.Children(1:2).YData])]);
    nicelegend({'prediction', 'actual'}, errAngAx, FONTSIZE);
    xlabel(errAngAx, unitlabel('t', 's'));
    ylabel(errAngAx, unitlabel('e_{\psi}', '^{\circ}'));
    

    
    %% Save figures
    print(measFig1, fullfile(measFolder, [figName1, '.pdf']), '-dpdf', '-painters');
    print(measFig1, fullfile(measFolder, [figName1, '.png']), '-dpng', '-opengl');
    close(measFig1);
    print(measFig2, fullfile(measFolder, [figName2, '.pdf']), '-dpdf', '-painters');
    print(measFig2, fullfile(measFolder, [figName2, '.png']), '-dpng', '-opengl');
    close(measFig2);
    
end

function expand(ax)

end