// MATLAB SIMULINK S-FUNCTION

// Author:          Ferenc Hegedus
// Date:            14.12.2017.
// Version:         1.0
// Description:     Planar non-linear single track vehicle model.

// S-FUNCTION HEADER
// Macro S_FUNCTION_NAME must be specified as the name of the S-function
#define S_FUNCTION_NAME	 chassisStSfun
#define S_FUNCTION_LEVEL 2

// INCLUDE FILES
// Include simstruc.h for the definition of the SimStruct and its associated macro definitions. 
#include "simstruc.h"
#include <math.h>
#include "types_common.h"
#include "math_common.h"

// MACROS
// Parameters
#define m_p         rWork[ParamIndex_m]
#define theta_p     rWork[ParamIndex_theta]
#define lf_p        rWork[ParamIndex_lf]
#define lr_p        rWork[ParamIndex_lr]
#define h_p         rWork[ParamIndex_h]
#define cd_p        rWork[ParamIndex_cd]
#define Af_p        rWork[ParamIndex_Af]
// Initial conditions
#define d_x0_V_p    rWork[ParamIndex_d_x0_V]
#define x0_G_p      rWork[ParamIndex_x0_G]
#define d_y0_V_p    rWork[ParamIndex_d_y0_V]
#define y0_G_p      rWork[ParamIndex_y0_G]
#define d_rz0_p     rWork[ParamIndex_d_rz0]
#define rz0_p       rWork[ParamIndex_rz0]
// Stored variables
#define d_x_V_w     rWork[RWorkIndex_d_x_V]
#define d_y_V_w     rWork[RWorkIndex_d_y_V]
#define dd_xI_V_w    rWork[RWorkIndex_dd_xI_V]
#define dd_yI_V_w    rWork[RWorkIndex_dd_yI_V]
#define dd_rz_w     rWork[RWorkIndex_dd_rz]
#define Fzf_V_w     rWork[RWorkIndex_Fzf_V]
#define Fzr_V_w     rWork[RWorkIndex_Fzr_V]
// States
#define d_x_G_s     X[StateIndex_d_x_G]
#define x_G_s       X[StateIndex_x_G]
#define d_y_G_s     X[StateIndex_d_y_G]
#define y_G_s       X[StateIndex_y_G]
#define d_rz_s      X[StateIndex_d_rz]
#define rz_s        X[StateIndex_rz]
// State derivatives
// longitudinal acceleration of the chassis in global inertial coordinate system [m/s^2]
#define dd_x_G_d    dX[StateIndex_d_x_G]
// longitudinal velocity of the chassis in global intertial coordinate sytem [m/s]
#define d_x_G_d     dX[StateIndex_x_G]
// lateral acceleration of the chassis in global intertial coordinate system [m/s^2]
#define dd_y_G_d    dX[StateIndex_d_y_G]
// lateral velocity of the chassis in global intertial coordinate system [m/s]
#define d_y_G_d     dX[StateIndex_y_G]
// angular acceleration of the chassis about vertical axis [rad/s^2]
#define dd_rz_d     dX[StateIndex_d_rz]
// angular velocity of the vehicle about vertical axis [rad/s]
#define d_rz_d      dX[StateIndex_rz]
// Constant definitions

// CONSTANTS

// GLOBAL VARIABLES
// Indexes for real work vector elements.
enum RWorkIndex 
{
    // Basic parameters
    // total mass of vehicle [kg]
    ParamIndex_m,
    // moment of inertia of vehicle about vertical axis [kg.m^2]
    ParamIndex_theta,
    // horizontal distance between center of gravity of vehicle and front axle [m]
    ParamIndex_lf,
    // horizontal distance between center of gravity of vehicle and rear axle [m]
    ParamIndex_lr,
    // center of gravity height of vehicle [m]
    ParamIndex_h,
    // aerodynamic drag coefficient [-]
    ParamIndex_cd,
    // frontal area of vehicle [m^2]
    ParamIndex_Af,
    // Initial conditions
    // initial longitudinal velocity in vehicle-fixed coordinate system [m/s]
    ParamIndex_d_x0_V,
    // initial longitudinal position in ground-fixed coordinate sytem [m]
    ParamIndex_x0_G,
    // initial lateral velocity in vehicle-fixed coordinate system [m/s]
    ParamIndex_d_y0_V,
    // initial lateral position in ground-fixed coordinate sytem [m]
    ParamIndex_y0_G,
    // initial angular velocity about vertical axis [rad/s]
    ParamIndex_d_rz0,
    // initial heading angle [rad]
    ParamIndex_rz0,
    // SIZE OF PARAMETER VECTOR
    ParamIndex_SIZE,
    // Vehicle center of gravity velocities in vehicle-fixed coordinate system.
    RWorkIndex_d_x_V,
    RWorkIndex_d_y_V,
    // vehicle center of gravity accelerations in ground-fixed coordinate system [m/s^2]
    RWorkIndex_dd_xI_V,
    RWorkIndex_dd_yI_V,
    // vehicle angular acceleration about vertical axis [rad/s^2]
    RWorkIndex_dd_rz,
    // vertical wheel forces in vehicle-fixed coordinate system [N]
    RWorkIndex_Fzf_V,
    RWorkIndex_Fzr_V,
    // SIZE OF REAL WORK VECTOR
    RWorkIndex_SIZE
};
// Indexes for input.
enum InputIndex 
{
    // longutidnal and lateral tire forces of front tire in vehicle-fixed coordinate system [N]
    InputIndex_Fxf_V,
    InputIndex_Fyf_V,
    // lsongutidnal and lateral tire forces of rear tire in vehicle-fixed coordinate system [N]
    InputIndex_Fxr_V,
    InputIndex_Fyr_V,
    InputIndex_SIZE
};
// Indexes for output.
enum OutputIndex
{
    // longitudinal intertial acceleration of vehicle center of gravity in vehicle-fixed coordinate 
    // system [m/s^2]
    OutputIndex_dd_xI_V,
    // longitudinal velocity of vehicle center of gravity in vehicle-fixed coordinate system [m/s]
    OutputIndex_d_x_V,
    // longitudinal position of vehicle center of gravity in ground-fixed coordinate system [m]
    OutputIndex_x_G,
    // lateral intertial acceleration of vehicle center of gravity in vehicle-fixed coordinate 
    // system [m/s^2]
    OutputIndex_dd_yI_V, 
    // lateral velocity of vehicle center of gravity in vehicle-fixed coordinate system [m/s]
    OutputIndex_d_y_V,
    // lateral position of vehicle center of gravity in ground-fixed coordinate system [m]
    OutputIndex_y_G,
    // angular acceleration of vehicle about vertical axis (yaw) [rad/s^2]
    OutputIndex_dd_rz,
    // angular velocity of vehicle about vertical axis (yaw) [rad/s]
    OutputIndex_d_rz, 
    // Heading angle of vehicle about vertical axis (yaw) [rad].
    OutputIndex_rz,
    // vertical tire force of front tire in vehicle-fixed coordinate system [N]
    OutputIndex_Fzf_V,
    // front wheel centerpoint velocities in vehicle-fixed coordinate system [m/s]
    OutputIndex_d_xf_V,
    OutputIndex_d_yf_V,
    // vertical tire force of rear tire in vehicle-fixed coordinate system [N]
    OutputIndex_Fzr_V,
    // rear wheel centerpoint velocities in vehicle-fixed coordinate system [m/s]
    OutputIndex_d_xr_V,
    OutputIndex_d_yr_V,
    OutputIndex_SIZE
};
// Indexes for states.
enum StateIndex
{
    // longitudinal velocity of the chassis in global inertial coordinate system [m/s]
    StateIndex_d_x_G,
    // longitudinal position of the chassis in global intertial coordinate sytem [m]
    StateIndex_x_G,
    // lateral velocity of the chassis in global intertial coordinate system [m/s]
    StateIndex_d_y_G,
    // lateral position of the chassis in global intertial coordinate system [m]
    StateIndex_y_G, 
    // angular velocity of the chassis about vertical axis [rad/s]
    StateIndex_d_rz,
    // heading angle of the vehicle about vertical axis [rad]
    StateIndex_rz,
    StateIndex_SIZE
};

// FUNCTION DECLARATIONS

// FUNCTION DEFINITIONS
// S-function methods
// Parameter handling methods. These methods are not supported by RTW. 
#undef MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
// Function: mdlCheckParameters 
static void mdlCheckParameters(SimStruct *S)
{

}
#endif // MDL_CHECK_PARAMETERS 

#undef MDL_PROCESS_PARAMETERS
#if defined(MDL_PROCESS_PARAMETERS) && defined(MATLAB_MEX_FILE)
// Function: mdlProcessParameters
static void mdlProcessParameters(SimStruct *S)
{
}
#endif // MDL_PROCESS_PARAMETERS 

// Configuration and execution methods. 
// Function: mdlInitializeSizes
static void mdlInitializeSizes(SimStruct *S)
{
    // Set the number of expected parameters. 
    ssSetNumSFcnParams(S, ParamIndex_SIZE);
    // Return if the number of expected parameters != the number of actual parameters. Simulink will
    // generate error message about parameter mismatch.
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    // Set the tunability of parameters. 
    // ssSetSFcnParamTunable(S, 0, 0);
    // Set the number of continuous and discrete states. 
    ssSetNumContStates(S, StateIndex_SIZE);
    ssSetNumDiscStates(S, 0);
    // Set the number and width(s) of input port(s). 
    if (!ssSetNumInputPorts(S, InputIndex_SIZE))
    {
        return;
    }
    ssSetInputPortWidth(S, InputIndex_Fxf_V, 1);
    ssSetInputPortWidth(S, InputIndex_Fyf_V, 1);
    ssSetInputPortWidth(S, InputIndex_Fxr_V, 1);
    ssSetInputPortWidth(S, InputIndex_Fyr_V, 1);
    // Set if the signal on the port must be stored on a contiguous memory area.
    // If the option is set, you can use ssGetInputPortSignal to get the signal on the port.
    // Otherwise, ssGetInputSignalPtrs should be used.
    ssSetInputPortRequiredContiguous(S, InputIndex_Fxf_V, 1);
    ssSetInputPortRequiredContiguous(S, InputIndex_Fyf_V, 1);
    ssSetInputPortRequiredContiguous(S, InputIndex_Fxr_V, 1);
    ssSetInputPortRequiredContiguous(S, InputIndex_Fyr_V, 1);
    // Set direct feedthrough flag. 
    ssSetInputPortDirectFeedThrough(S, InputIndex_Fxf_V, 0);
    ssSetInputPortDirectFeedThrough(S, InputIndex_Fyf_V, 0);
    ssSetInputPortDirectFeedThrough(S, InputIndex_Fxr_V, 0);
    ssSetInputPortDirectFeedThrough(S, InputIndex_Fyr_V, 0);
    // Set input data types
    ssSetInputPortDataType(S, InputIndex_Fxf_V, SS_DOUBLE);
    ssSetInputPortDataType(S, InputIndex_Fyf_V, SS_DOUBLE);
    ssSetInputPortDataType(S, InputIndex_Fxr_V, SS_DOUBLE);
    ssSetInputPortDataType(S, InputIndex_Fyr_V, SS_DOUBLE);
    // Set the number and width(s) of output port(s). 
    if (!ssSetNumOutputPorts(S, OutputIndex_SIZE))
    {
        return;
    }
    ssSetOutputPortWidth(S, OutputIndex_dd_xI_V,    1);
    ssSetOutputPortWidth(S, OutputIndex_d_x_V,      1);
    ssSetOutputPortWidth(S, OutputIndex_x_G,        1);
    ssSetOutputPortWidth(S, OutputIndex_dd_yI_V,    1);
    ssSetOutputPortWidth(S, OutputIndex_d_y_V,      1);
    ssSetOutputPortWidth(S, OutputIndex_y_G,        1);
    ssSetOutputPortWidth(S, OutputIndex_dd_rz,     1);
    ssSetOutputPortWidth(S, OutputIndex_d_rz,      1);
    ssSetOutputPortWidth(S, OutputIndex_rz,        1);
    ssSetOutputPortWidth(S, OutputIndex_Fzf_V,      1);
    ssSetOutputPortWidth(S, OutputIndex_d_xf_V,     1);
    ssSetOutputPortWidth(S, OutputIndex_d_yf_V,     1);
    ssSetOutputPortWidth(S, OutputIndex_Fzr_V,      1);
    ssSetOutputPortWidth(S, OutputIndex_d_xr_V,     1);
    ssSetOutputPortWidth(S, OutputIndex_d_yr_V,     1);
    // Set output data types
    ssSetOutputPortDataType(S, OutputIndex_dd_xI_V, SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_d_x_V,   SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_x_G,     SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_dd_yI_V, SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_d_y_V,   SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_y_G,     SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_dd_rz,  SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_d_rz,   SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_rz,     SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_Fzf_V,   SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_d_xf_V,  SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_d_yf_V,  SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_Fzr_V,   SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_d_xr_V,  SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_d_yr_V,  SS_DOUBLE);
    // Set the number of sample times. 
    ssSetNumSampleTimes(S, 1);
    // Set the number of work vector elements. 
    ssSetNumRWork(S, RWorkIndex_SIZE);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    // Set the number of modes. 
    ssSetNumModes(S, 0);
    // Set the number of non-sampled zero crossings that need to be detected. 
    ssSetNumNonsampledZCs(S, 0);
    // Specify the sim state compliance to be same as a built-in block. 
    ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);
    // Set options. 
    ssSetOptions(S, 0);
}

#undef MDL_SET_INPUT_PORT_FRAME_DATA
#if defined(MDL_SET_INPUT_PORT_FRAME_DATA) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortFrameData 
static void mdlSetInputPortFrameData(SimStruct *S, int_T portIndex, Frame_T frameData)
{
}
#endif // MDL_SET_INPUT_PORT_FRAME_DATA 

#undef MDL_SET_INPUT_PORT_WIDTH
#if defined(MDL_SET_INPUT_PORT_WIDTH) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortWidth 
static void mdlSetInputPortWidth(SimStruct *S, int_T portIndex, int_T width)
{
}
#endif // MDL_SET_INPUT_PORT_WIDTH 

#undef MDL_SET_OUTPUT_PORT_WIDTH
#if defined(MDL_SET_OUTPUT_PORT_WIDTH) && defined(MATLAB_MEX_FILE)
// Function: mdlSetOutputPortWidth
static void mdlSetOutputPortWidth(SimStruct *S, int_T portIndex, int_T width)
{
}
#endif // MDL_SET_OUTPUT_PORT_WIDTH 

#undef MDL_SET_INPUT_PORT_DIMENSION_INFO
#if defined(MDL_SET_INPUT_PORT_DIMENSION_INFO) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortDimensionInfo
static void mdlSetInputPortDimensionInfo(SimStruct *S, int_T portIndex, const DimsInfo_T *dimsInfo)
{
}
#endif // MDL_SET_INPUT_PORT_DIMENSION_INFO 

#undef MDL_SET_OUTPUT_PORT_DIMENSION_INFO
#if defined(MDL_SET_OUTPUT_PORT_DIMENSION_INFO) && defined(MATLAB_MEX_FILE)
// Function: mdlSetOutputPortDimensionInfo 
static void mdlSetOutputPortDimensionInfo(SimStruct *S, int_T portIndex, const DimsInfo_T *dimsInfo)
{
}
#endif // MDL_SET_OUTPUT_PORT_DIMENSION_INFO 

#undef MDL_SET_DEFAULT_PORT_DIMENSION_INFO
#if defined(MDL_SET_DEFAULT_PORT_DIMENSION_INFO) && defined(MATLAB_MEX_FILE)
// Function: mdlSetDefaultPortDimensionInfo
static void mdlSetDefaultPortDimensionInfo(SimStruct *S)
{
}
#endif // MDL_SET_DEFAULT_PORT_DIMENSION_INFO 


#undef MDL_SET_INPUT_PORT_SAMPLE_TIME
#if defined(MDL_SET_INPUT_PORT_SAMPLE_TIME) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortSampleTime
static void mdlSetInputPortSampleTime(SimStruct *S,
    int_T		portIdx,
    real_T	sampleTime,
    real_T	offsetTime)
{
}
#endif // MDL_SET_INPUT_PORT_SAMPLE_TIME 

#undef MDL_SET_OUTPUT_PORT_SAMPLE_TIME
#if defined(MDL_SET_OUTPUT_PORT_SAMPLE_TIME) && defined(MATLAB_MEX_FILE)
// Function: mdlSetOutputPortSampleTime
static void mdlSetOutputPortSampleTime(SimStruct *S,
    int_T	 portIdx,
    real_T	 sampleTime,
    real_T	 offsetTime)
{
}
#endif // MDL_SET_OUTPUT_PORT_SAMPLE_TIME 

// Function: mdlInitializeSampleTimes
static void mdlInitializeSampleTimes(SimStruct *S)
{
    // Set continuous sample time without offset.
    ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
}

#undef MDL_SET_INPUT_PORT_DATA_TYPE
#if defined(MDL_SET_INPUT_PORT_DATA_TYPE) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortDataType
static void mdlSetInputPortDataType(SimStruct *S, int_T portIndex, DTypeId dType)
{
}
#endif // MDL_SET_INPUT_PORT_DATA_TYPE 

#undef MDL_SET_OUTPUT_PORT_DATA_TYPE
#if defined(MDL_SET_OUTPUT_PORT_DATA_TYPE) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortDataType
static void mdlSetOutputPortDataType(SimStruct *S, int_T portIndex, DTypeId dType)
{
}
#endif // MDL_SET_OUTPUT_PORT_DATA_TYPE 

#undef MDL_SET_DEFAULT_PORT_DATA_TYPES
#if defined(MDL_SET_DEFAULT_PORT_DATA_TYPES) && defined(MATLAB_MEX_FILE)
// Function: mdlSetDefaultPortDataTypes
static void mdlSetDefaultPortDataTypes(SimStruct *S)
{
}
#endif // MDL_SET_DEFAULT_PORT_DATA_TYPES 

#undef MDL_SET_INPUT_PORT_COMPLEX_SIGNAL
#if defined(MDL_SET_INPUT_PORT_COMPLEX_SIGNAL) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortComplexSignal
static void mdlSetInputPortComplexSignal(SimStruct *S, int_T portIndex, CSignal_T cSignalSetting)
{
}
#endif // MDL_SET_INPUT_PORT_COMPLEX_SIGNAL 

#undef MDL_SET_OUTPUT_PORT_COMPLEX_SIGNAL
#if defined(MDL_SET_OUTPUT_PORT_COMPLEX_SIGNAL) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortComplexSignal
static void mdlSetOutputPortComplexSignal(SimStruct *S, int_T portIndex, CSignal_T cSignalSetting)
{
}
#endif // MDL_SET_OUTPUT_PORT_COMPLEX_SIGNAL 

#undef MDL_SET_DEFAULT_PORT_COMPLEX_SIGNALS
#if defined(MDL_SET_DEFAULT_PORT_COMPLEX_SIGNALS) && defined(MATLAB_MEX_FILE)
// Function: mdlSetDefaultPortComplexSignals
static void mdlSetDefaultPortComplexSignals(SimStruct *S)
{
}
#endif // MDL_SET_DEFAULT_PORT_COMPLEX_SIGNALS 

#undef MDL_SET_WORK_WIDTHS
#if defined(MDL_SET_WORK_WIDTHS) && defined(MATLAB_MEX_FILE)
// Function: mdlSetWorkWidths
static void mdlSetWorkWidths(SimStruct *S)
{
}
#endif // MDL_SET_WORK_WIDTHS 

#define MDL_INITIALIZE_CONDITIONS
#if defined(MDL_INITIALIZE_CONDITIONS)
// Function: mdlInitializeConditions
static void mdlInitializeConditions(SimStruct *S)
{
}
#endif // MDL_INITIALIZE_CONDITIONS 

#define MDL_START
#if defined(MDL_START)
// Function: mdlStart
static void mdlStart(SimStruct *S)
{
    // Get real work vector.
    Float64_T *rWork = ssGetRWork(S);
    // Get state vector.
    Float64_T *X = ssGetContStates(S);
    // Obtain parameter values
    m_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_m)));
    theta_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_theta)));
    lf_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_lf)));
    lr_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_lr)));
    h_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_h)));
    cd_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_cd)));
    Af_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_Af)));
    // Initial conditions
    d_x0_V_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_d_x0_V)));
    x0_G_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_x0_G)));
    d_y0_V_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_d_y0_V)));
    y0_G_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_y0_G)));
    d_rz0_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_d_rz0)));
    rz0_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_rz0)));
    // Set initial conditions
    // Calculation of initial velocities in ground-fixed coordinate system.
    Float64_T sin_rz0 = sin(rz0_p);
    Float64_T cos_rz0 = cos(rz0_p);
    Float64_T d_x0_G = + cos_rz0 * d_x0_V_p - sin_rz0 * d_y0_V_p;
    Float64_T d_y0_G = + sin_rz0 * d_x0_V_p + cos_rz0 * d_y0_V_p;
    d_x_G_s = d_x0_G;
    x_G_s = x0_G_p;
    d_y_G_s = d_y0_G;
    y_G_s = y0_G_p;
    d_rz_s = d_rz0_p;
    rz_s = rz0_p;
    // Set initial values for stored variables
    d_x_V_w = d_x0_V_p;
    d_y_V_w = d_y0_V_p;
    dd_xI_V_w = 0.0;
    dd_yI_V_w = 0.0;
    dd_rz_w = 0.0;
    Fzf_V_w = m_p * g * lr_p / (lf_p + lr_p);
    Fzr_V_w = m_p * g * lf_p / (lf_p + lr_p);
}
#endif //  MDL_START 

#undef MDL_SIM_STATE
#if defined(MDL_SIM_STATE)
// Function: mdlGetSimState
static mxArray* mdlGetSimState(SimStruct* S)
{
}

// Function: mdlSetSimState
static void mdlSetSimState(SimStruct* S, const mxArray* inSimState)
{
}
#endif // MDL_SIM_STATE 

#undef MDL_GET_TIME_OF_NEXT_VAR_HIT
#if defined(MDL_GET_TIME_OF_NEXT_VAR_HIT) && (defined(MATLAB_MEX_FILE) || defined(NRT))
// Function: mdlGetTimeOfNextVarHit
static void mdlGetTimeOfNextVarHit(SimStruct *S)
{
    time_T timeOfNextHit = ssGetT(S) // + offset  ;
        ssSetTNext(S, timeOfNextHit);
}
#endif // MDL_GET_TIME_OF_NEXT_VAR_HIT 

#undef MDL_ZERO_CROSSINGS
#if defined(MDL_ZERO_CROSSINGS) && (defined(MATLAB_MEX_FILE) || defined(NRT))
// Function: mdlZeroCrossings
static void mdlZeroCrossings(SimStruct *S)
{
}
#endif // MDL_ZERO_CROSSINGS 

// Function: mdlOutputs
static void mdlOutputs(SimStruct *S, int_T tid)
{
    // Get real work vector (parameters, stored values)
    Float64_T *const rWork = ssGetRWork(S);
    // Get state vector
    const Float64_T *const X = ssGetContStates(S);
    // Get output signal pointers
    Float64_T *const dd_xI_V_o = ssGetOutputPortRealSignal(S, OutputIndex_dd_xI_V);
    Float64_T *const d_x_V_o = ssGetOutputPortRealSignal(S, OutputIndex_d_x_V);
    Float64_T *const x_G_o = ssGetOutputPortRealSignal(S, OutputIndex_x_G);
    Float64_T *const dd_yI_V_o = ssGetOutputPortRealSignal(S, OutputIndex_dd_yI_V);
    Float64_T *const d_y_V_o = ssGetOutputPortRealSignal(S, OutputIndex_d_y_V);
    Float64_T *const y_G_o = ssGetOutputPortRealSignal(S, OutputIndex_y_G);
    Float64_T *const dd_rz_o = ssGetOutputPortRealSignal(S, OutputIndex_dd_rz);
    Float64_T *const d_rz_o = ssGetOutputPortRealSignal(S, OutputIndex_d_rz);
    Float64_T *const rz_o = ssGetOutputPortRealSignal(S, OutputIndex_rz);
    Float64_T *const Fzf_V_o = ssGetOutputPortRealSignal(S, OutputIndex_Fzf_V);
    Float64_T *const d_xf_V_o = ssGetOutputPortRealSignal(S, OutputIndex_d_xf_V);
    Float64_T *const d_yf_V_o = ssGetOutputPortRealSignal(S, OutputIndex_d_yf_V);
    Float64_T *const Fzr_V_o = ssGetOutputPortRealSignal(S, OutputIndex_Fzr_V);
    Float64_T *const d_xr_V_o = ssGetOutputPortRealSignal(S, OutputIndex_d_xr_V);
    Float64_T *const d_yr_V_o = ssGetOutputPortRealSignal(S, OutputIndex_d_yr_V);

    // Copy inertial accelerations according to vehicle heading.
    dd_xI_V_o[0] = dd_xI_V_w;
    dd_yI_V_o[0] = dd_yI_V_w;

    // Copy vehicle center of gravity velocities in vehicle-fixed coordinate system to output.
    d_x_V_o[0] = d_x_V_w;
    d_y_V_o[0] = d_y_V_w;

    // Copy vehicle center of gravity positions in ground-fixed coordinate system to output.
    x_G_o[0] = x_G_s;
    y_G_o[0] = y_G_s;

    // Copy vehicle angular acceleration, velocity and heading angle about vertical axis to output.
    dd_rz_o[0] = dd_rz_w;
    d_rz_o[0] = d_rz_s;
    rz_o[0] = rz_s;

    // Copy vertical wheel forces in vehicle-fixed (z direction is equivalent) coordinate 
    // system.
    Fzf_V_o[0] = Fzf_V_w;
    Fzr_V_o[0] = Fzr_V_w;

    // Calculate front wheel centerpoint velocities in vehicle-fixed coordinate system
    d_xf_V_o[0] = d_x_V_w;
    d_yf_V_o[0] = d_y_V_w + lf_p * d_rz_s;

    // Calculate rear wheel centerpoint velocities in vehicle-fixed coordinate system
    d_xr_V_o[0] = d_x_V_w;
    d_yr_V_o[0] = d_y_V_w - lr_p * d_rz_s;
}

#undef MDL_UPDATE
#if defined(MDL_UPDATE)
// Function: mdlUpdate
static void mdlUpdate(SimStruct *S, int_T tid)
{
}
#endif // MDL_UPDATE 

#define MDL_DERIVATIVES
#if defined(MDL_DERIVATIVES)
// Function: mdlDerivatives
static void mdlDerivatives(SimStruct *S)
{
    // Get real work vector (parameters, stored values)
    Float64_T *const rWork = ssGetRWork(S);
    // Get state vector
    const Float64_T *const X = ssGetContStates(S);
    // Get state vector derivative
    Float64_T *const dX = ssGetdX(S);
    // Get input signal pointers
    const Float64_T *const Fxf_V_i = ssGetInputPortRealSignal(S, InputIndex_Fxf_V);
    const Float64_T *const Fyf_V_i = ssGetInputPortRealSignal(S, InputIndex_Fyf_V);
    const Float64_T *const Fxr_V_i = ssGetInputPortRealSignal(S, InputIndex_Fxr_V);
    const Float64_T *const Fyr_V_i = ssGetInputPortRealSignal(S, InputIndex_Fyr_V);

    // Cosine and sine of heading angle
    Float64_T cos_rz = cos(rz_s);
    Float64_T sin_rz = sin(rz_s);

    // Calculate front wheel forces in ground-fixed coordinate system
    Float64_T Fxf_G = +cos_rz * Fxf_V_i[0] - sin_rz * Fyf_V_i[0];
    Float64_T Fyf_G = +sin_rz * Fxf_V_i[0] + cos_rz * Fyf_V_i[0];
    // Calculate rear wheel forces in ground-fixed coordinate system
    Float64_T Fxr_G = +cos_rz * Fxr_V_i[0] - sin_rz * Fyr_V_i[0];
    Float64_T Fyr_G = +sin_rz * Fxr_V_i[0] + cos_rz * Fyr_V_i[0];

    // Calculate vehicle center of gravity velocities in vehicle-fixed coordinate system. 
    d_x_V_w = +cos_rz * d_x_G_s + sin_rz * d_y_G_s;
    d_y_V_w = -sin_rz * d_x_G_s + cos_rz * d_y_G_s;

    // Calculation of aerodynamic drag forces in vehicle-fixed coordinate system
    Float64_T Fxd_V = 0.5 * cd_p * Af_p * rhoa * d_x_V_w * 
        sqrt(d_x_V_w * d_x_V_w + d_y_V_w * d_y_V_w);
    Float64_T Fyd_V = 0.5 * cd_p * Af_p * rhoa * d_y_V_w * 
        sqrt(d_x_V_w * d_x_V_w + d_y_V_w * d_y_V_w);
    // Calculation of aerodynamic drag forces in ground-fixed coordinate system
    Float64_T Fxd_G = +cos_rz * Fxd_V - sin_rz * Fyd_V;
    Float64_T Fyd_G = +sin_rz * Fxd_V + cos_rz * Fyd_V;

    // Calculate vertical wheel forces in vehicle-fixed (z direction is equivalent) coordinate 
    // system.
    Fzf_V_w = (m_p * g * lr_p - h_p * (Fxf_V_i[0] + Fxr_V_i[0])) / (lf_p + lr_p);
    Fzr_V_w = (m_p * g * lf_p + h_p * (Fxf_V_i[0] + Fxr_V_i[0])) / (lf_p + lr_p);

    // Principle of linear momentum for the chassis in ground-fixed coordinate system
    dd_x_G_d = (1 / m_p) * (Fxf_G + Fxr_G - Fxd_G);
    dd_y_G_d = (1 / m_p) * (Fyf_G + Fyr_G - Fyd_G);
    
    // Principle of angular momentum for the chassis 
    dd_rz_d = (1 /theta_p) * (lf_p * Fyf_V_i[0] - lr_p * Fyr_V_i[0]);

    // Perform obvious integrations for positions and heading angle.
    d_x_G_d = d_x_G_s;
    d_y_G_d = d_y_G_s;
    d_rz_d = d_rz_s;

    // Rotate inertial accelerations according to vehicle heading.
    dd_xI_V_w = +cos_rz * dd_x_G_d + sin_rz * dd_y_G_d;
    dd_yI_V_w = -sin_rz * dd_x_G_d + cos_rz * dd_y_G_d;

    // Save accelerations to be able to output them.
    dd_rz_w = dd_rz_d;
}
#endif // MDL_DERIVATIVES 

// Function: mdlTerminate
static void mdlTerminate(SimStruct *S)
{
}

#undef MDL_RTW
#if defined(MDL_RTW) && defined(MATLAB_MEX_FILE)
// Function: mdlRTW
static void mdlRTW(SimStruct *S)
{
}
#endif // MDL_RTW 

// Custom methods



// S-FUNCTION TRAILER
#ifdef	MATLAB_MEX_FILE	// Is this file being compiled as a MEX-file? 
#include "simulink.c"	// MEX-file interface mechanism 
#else
#include "cg_sfun.h"	// Code generation registration function 
#endif