%% Constants
DO_PLOTS = false;

%% Vehicle parameters
% Gravitational acceleration [m/s^2]
g    = 9.8065;
% Basic parameters
% mass [kg]
m    = 1463;
% wheel radius [m]
r    = 0.315;
% Rolling resistance parameters
% Constant model [1]
frr  = 0.015;
% velocity independent rolling resistance coefficient [1]
Arr  = 0.0098;
% linearly velocity dependent rolling resistance coefficient [s/m]
Brr  = 0.00011;
% squarely velocity depenfent rolling resistance coefficient [s^2/m^2]
Crr  = 1.6e-06;
% Aerodynamic drag parameters
% aerodynamic drag coefficient [1]
cd   = 0.3;
% frontal area of the vehicle [m^2]
Af   = 2;
% mass density of air [kg/m^3]
rhoa = 1.225;
% velocity of linearization [m/s]
vlin = 25;

%% Controller parameters
% maximal driving torque [Nm]
Mmax = 3000;
% maximal braking torque [Nm]
Mmin = -5000;
% initial state gain 
Ksi = 4;

%% Input data
% Velocity domain
vmax = 250;
v = 0:0.1:vmax/3.6;
% initial velocity [m/s]
v0 = 20;
% final velocity [m/s]
v1 = 21;
%% Calculate linearized rolling resistance
% Frr = Fz * (Arr + Brr.|v| + Crr.v^2)  => Fz * (Arrlin + Brrlin.|v|)
% (calculate line which has same slope and value @ vlin as the parabola)
Brrlin = 2 * Crr * vlin + Brr;
Arrlin = Crr * vlin^2 + Brr * vlin + Arr - Brrlin * vlin;

%% Examination of rolling resistance components
% Nonlinear rolling resistance coefficient
crr = (Arr + Brr * abs(v) + Crr * v.^2);
% Linear rolling resistane coefficient
crrlin = Arrlin + Brrlin * v;

%% Calculate linerized aerodynamic drag
% (calculate line which has same value @ vlin as the parabola, and value 0 at 0)
Badlin = 0.5 * cd * Af * rhoa * vlin;

%% Examination of aerodynamic drag
% Nonlinear aerodinamic drag
cad = 0.5 * cd * Af * rhoa * v.^2;
% Total linear aerodynamic drag
cadlin = Badlin * v;

%% LTI model with transfer function
% Inputs
% U(1) => TorqueDrv: driving/braking wheel torque [Nm]
% Outputs
% Y(1) => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
%
% Model equations:
% m.d_v(t) = M(t)/r - (Brrlin * m * g + Badlin) * v - Arrlin * m * g
% [r * (m * s + Brrlin * m * g + Badlin) * V(s) +  Arrlin * m * g] = M(s)
% V(s) / M(s) = 1 / (r * (m * s + Brrlin * m * g + Badlin))  => 1 / (a1.s + a0)
a0 = r * (Brrlin * m * g + Badlin);
a1 = r * m;
% Amplification and time constant
AG = 1 / a0;
T = a1 / a0;
% Display results
fprintf('Longitudinal transfer function between driving torque and velocity.\n');
fprintf('(%gs + %g).V(s) = M(s)\n', a1, a0);
fprintf('G(s) = %g / (%gs + 1)\n', AG, T);

%% LTI model with state space representation
% Inputs
% U(1) => TorqueDrv: driving/braking wheel torque [Nm]
% States
% X(1) => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
% Outputs
% Y(1) => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
A = -(Brrlin * g + Badlin / m);
B = 1 / (m * r);
C = 1;
D = 0;
% Display results
fprintf('Longitudinal state space model: (state - velocity, input - driving torque).\n');
fprintf('dv(t) = %g.v(t) + %g.M(t)\n', A, B);

%% LQR control design for extended state space representation
% Inputs
% U(1) => TorqueDrv:   driving/braking wheel torque [Nm]
% States
% X(1) => VelXV:       longitudinal velocity in vehicle-fixed cs. [m/s]
% X(2) => TrackErrInt: integral of longitudinal velocity tracking error [m]
% Outputs
% Y(1) => VelXV:      longitudinal velocity in vehicle-fixed cs. [m/s]
Ae =[A, 0;
    -C, 0];
Be = [B;
      0];
Ce = [C, 0];
% Check controllability
if rank(ctrb(Ae, Be)) ~= size(Ae, 1)
    error('System is not controllable.');
end
% Choose weights
% velocity
q11 = 1 / 50^2;
% velocity tracking error
q22 = 1 / 0.1^2;
% torque
r11 = 1 / 400^2;
% Weighting matrices
Q0 = diag([q11, q22]);
R0 = r11;
Q1 = Q0;
R1 = R0;
% Design controller
K = lqr(Ae, Be, Q1, R1);
fprintf('LQR controller for longitudinal dymanics\n')
fprintf(['K = ', mat2str(K), ' (for negative feedback)\n']);
% Closed loop system
Scl = ss(Ae-Be*K, [0; 1], Ce, 0);
fprintf('Poles of closed loop system:\n');
fprintf(['P = ', mat2str(pole(Scl)), '\n']);
% Evaluate initial value of tracking error integrator so that initial output is Ki * Xsys0
% Initial velocity [m/s]
Xsys0 = v0;
% Initial value for tracking error integrator 
Xerr0 = K(end) \ -((K(1:end-1) + Ksi) * Xsys0);
fprintf('Initial value for tracking error integrator:\n');
fprintf('Xerr0 = %g\n', Xerr0);
% Check result
U0 = -K * [Xsys0; Xerr0];

%% PI control design
% C(s) = Kp + Ki / s = (Ki / s) * [(Kp / Ki) * s + 1]
% G(s) = A / (T * s + 1)
Kp = 2000;
Ki = Kp / T;
fprintf('PI controller for longitudinal dymanics\n')
fprintf('Kp = %g   Ki = %g (for negative feedback)\n', Kp, Ki);
% Closed loop system
Gsys = tf(AG, [T, 1]);
Gctrl = tf([Kp, Ki], [1, 0]);
Gh = Gsys * Gctrl;
Gcl = Gh / (Gh + 1);
fprintf('Poles of closed loop system:\n');
fprintf(['P = ', mat2str(pole(minreal(Gcl))), '\n']);
%% Plots
if DO_PLOTS
    fontSize = 8;
    
    %% Rolling resistance
    figure('Name', 'Linearizations for longitudinal dynamics', 'Color', [1, 1, 1], ...
        'Units', 'Centimeters', 'Position', [2, 2, 14, 6]);
    subplot(1,2,1);
    hold on;
    plot(v * 3.6, frr * ones(size(v)));
    plot(v * 3.6, crr);
    plot(v * 3.6, crrlin);
    xlim([0, vmax]);
    grid on;
    box on;
    set(gca, 'DefaultTextInterpreter', 'Latex', 'TickLabelInterpreter', 'Latex', ...
        'FontSize', fontSize);
    title('Rolling resistance coefficient');
    legend({'Constant', 'SAE J2452', 'Linearized'}, 'Interpreter', 'Latex');
    xlabel('$v$ [km/h]');
    ylabel('$c_{rr}$ [1]');
    
    %% Aerodynamic drag force
    subplot(1,2,2);
    hold on;
    plot(v * 3.6, cad);
    plot(v * 3.6, cadlin);
    grid on;
    box on;
    set(gca, 'DefaultTextInterpreter', 'Latex', 'TickLabelInterpreter', 'Latex', ...
        'FontSize', fontSize);
    title('Aerodynamic drag');
    legend({'Original', 'Linearized'}, 'Interpreter', 'Latex');
    xlabel('$v$ [km/h]');
    ylabel('$c_{ad}$ [1]');
    
    %% Step response of LQR controller
    figure('Name', 'Velocity tracking control', 'Color', [1, 1, 1], ...
        'Units', 'Centimeters', 'Position', [2, 2, 14, 8]);
    step(Scl);
    title('Step response of longitudinal LQR controller');
    box on;
    grid on;
    
    %% Step response of PID controller
    figure('Name', 'Velocity tracking control', 'Color', [1, 1, 1], ...
        'Units', 'Centimeters', 'Position', [2, 2, 14, 8]);
    step(Gcl);
    title('Step response of longitudinal PID controller');
    box on;
    grid on;
    
    figure('Name', 'Velocity tracking control', 'Color', [1, 1, 1], ...
        'Units', 'Centimeters', 'Position', [2, 2, 14, 14]);
    margin(Gh);
    box on;
    grid on;
end