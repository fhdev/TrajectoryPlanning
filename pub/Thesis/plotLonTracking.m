% To get the data, run model with CtrlLqsV and CtrlPidV controllers separately.
%
% pidLonTracking = obj2struct(VehTest.TestManeuver('VehStCl_Default', 'Maneuver_AccelerateBrake', true, false));
% lqrLonTracking = obj2struct(VehTest.TestManeuver('VehStCl_Default', 'Maneuver_AccelerateBrake', true, false));

% Subsampling factor
subs = @(x) x(1:50:end);

% Create plot
W = 10;
H = 8;
F = 8;
fig = nicefigure('TorqueDamping', W, H);

ax1 = niceaxis(fig, F);
subplot(2, 1, 1, ax1);
hold(ax1, 'on');
plot(ax1, subs(pidLonTracking.in.Time), subs(pidLonTracking.in.VelXVRef * 3.6), 'Color', nicecolors.BlueBelizeHole, ...
    'LineStyle', '-');
plot(ax1, subs(pidLonTracking.out.Chassis.Time), subs(pidLonTracking.out.Chassis.VelXV * 3.6), 'Color', nicecolors.RedAlizarin, ...
    'LineStyle', '-');
plot(ax1, subs(lqrLonTracking.out.Chassis.Time), subs(lqrLonTracking.out.Chassis.VelXV * 3.6), 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-');
ylim([30,80]);
xlabel(ax1, '$t_v$ (time) [s]');
ylabel(ax1, '$\dot{x}_r$, $\dot{x}_v$ (lon. vel.) [km/h]');
l = nicelegend({'Reference', 'PI', 'LQR'}, ax1, F);
l.Orientation = 'vertical';
l.Location = 'northeastoutside';

ax2 = niceaxis(fig, F);
subplot(2, 1, 2, ax2);
hold(ax2, 'on');
plot(ax2, subs(pidLonTracking.out.CtrlLon.Time), subs(pidLonTracking.out.CtrlLon.VErr * 3.6), 'Color', nicecolors.RedAlizarin, ...
    'LineStyle', '-');
plot(ax2, subs(lqrLonTracking.out.CtrlLon.Time), subs(lqrLonTracking.out.CtrlLon.VErr * 3.6), 'Color', nicecolors.PurpleAmethyst, ...
    'LineStyle', '-');
xlabel(ax2, '$t_v$ (time) [s]');
ylabel(ax2, '$\dot{x}_e$ (track. err.) [km/h]');
ax2.Position(3) = ax1.Position(3) * 0.63;

% Save
name = 'LonTracking';
savefig(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');