classdef PlannerGeom

    methods (Static)
        %% SplineTrajLinVel ------------------------------------------------------------------------
        % [Summary]
        %   This function evaluates a trajectory which has a quintic spline path function [y(x)] and
        %   linear longitudinal velocity as function of time [v(t)]. The trajectory is evaluated
        %   in the ego vehicle's coordinate system.
        %
        % [Input]
        %   xf      final longitudinal position
        %   yp      final positions at spline knot points (the first knot point is at 0, it is not
        %           to be provided), yp must have 3 values.
        %   rf      final yaw (heading angle)
        %   vi      initial longitudinal velocity
        %   vf      final longitudinal velocity
        %   dri     initial yaw rate
        %   drf     final yaw rate
        %   dx      optional, longitudinal coordinate resolution for numeric calculations 
        %           (numeric scalar), default is 1 cm
        %   ds      optional, arc length resolution for numeric calculations (numeric scalar),
        %           default is 10 cm
        %
        % [Output]
        %   ts       time values (numeric vector)
        %   s        arc length values (numeric vector)
        %   xs       longitudinal coordinates in vehicle-fixed coordinate system (numeric vector)
        %   ys       lateral coordinates in vehicle-fixed coordinate system (numeric vector)
        %   rs       yaw (heading) angle values (numeric vector)
        %   vs       longitudinal velocity values (numeric vector)
        %   drs      yaw rate values (numeric vector)
        % ------------------------------------------------------------------------------------------
        function [ts, s, xs, ys, rs, vs, drs, acps]  = SplineTrajLinVel(xf, yp, rf, vi, vf, dri, drf, dx, ds, optimflag)
            %% Default arguments
            if (nargin < 10) || isempty(optimflag)
                optimflag = false;
            end
            if (nargin < 9) || isempty(ds)
                ds = 0.1;
            end
            if (nargin < 8) || isempty(dx)
                dx = 0.01;
            end
            if (nargin < 7) || isempty(dri)
                dri = 0;
            end
            if (nargin < 6) || isempty(drf)
                drf = 0;
            end
            %% Check arguments
            if ~isnumeric(xf) || ~isscalar(xf)
                error('Final value for longitudinal position (xf) must be a numeric scalar.');
            end
            if ~isnumeric(yp) || ~isvector(yp) || (length(yp) ~= 3)
                error('Knot values for lateral position (yp) must be a numeric vector with 3 elements.');
            end
            yp = yp(:);
            if ~isnumeric(rf) || ~isscalar(rf)
                error('Final value for heading (rf) must be a numeric scalar.');
            end
            if ~isnumeric(vi) || ~isscalar(vi)
                error('Initial value for velocity (vi) must be a numeric scalar.');
            end
            if ~isnumeric(vf) || ~isscalar(vf)
                error('Final value for velocity (vf) must be a numeric scalar.');
            end
            if ~isnumeric(dri) || ~isscalar(dri)
                error('Initial value for yaw rate (dri) must be a numeric scalar.');
            end
            if ~isnumeric(drf) || ~isscalar(drf)
                error('Final value for yaw rate (drf) must be a numeric scalar.');
            end
            if ~isnumeric(dx) || ~isscalar(dx)
                error('Resolution for x (dx) must be a numeric scalar.');
            end
            if ~isnumeric(ds) || ~isscalar(ds)
                error('Resolution for arc length (ds) must be a numeric scalar.');
            end
                        
            %% Get equidistant samples in local coordinates
            nyp = length(yp);
            xp = linspace(0, xf, nyp + 1)';
            % trajectory begins at (0,0) CoG of ego vehicle
            yp = [0; yp];
            
            %% Evaluate path and curvature as function of longitudinal coordinate [ y(x), k(x) ]
            % longitudinal coordinate domain x
            nx = ceil(xf / dx) + 1;
            dxa = xf / (nx - 1);
            x = linspace(0, xf, nx)';
            
            % path y(x), path derivative y'(x), and path second derivative y''(x) functions
            dyb = [0; tan(rf)];
            ddyb = [dri / vi; drf / vf * (1 + tan(rf)^2)^(3/2)];
            [yx, dyx_dx, ddyx_dx] = interp4spline5(xp, yp, dyb, ddyb, x);
            
            % curvature function
            kx = ddyx_dx ./ ((1 + dyx_dx.^2).^(3 / 2));
            
            %% Calculate position and heading as function of arc length [ x(s), y(s), r(s) ]
            
            % calculate arc length as function of lon. position s(x)
            sx = cumsum([0; sqrt(dxa^2 + diff(yx).^2)]);
            
            % arc length domain s
            ns = ceil(sx(end) / ds) + 1;
            dsa = sx(end) / (ns - 1);
            s = linspace(0, sx(end), ns)';
            
            % calculate curvature as function of arc length k(s)
            ks = interp1q(sx, kx, s);
            
            % do not need to calculate this inside the optim problem
            if ~optimflag
                % calculate lon. and lat. positions as function of arc length x(s) and y(s)
                xs = interp1q(sx, x, s);
                ys = interp1q(sx, yx, s);
                
                % calculate heading as function of arc length r(s)
                rs = [0; atan(diff(ys) ./ diff(xs))];
            else
                xs = [];
                ys = [];
                rs = [];
            end
            
            %% Calculate time, velocity and yaw rate as function of arc length [ t(s), v(s), dr(s) ]
            
            % calculate time as function of arc length t(s)
            va = (vi + vf) / 2;
            tf = s(end) / va;
            a = (vf - vi) / tf;
            if a ~= 0
                % (a/2).t^2 + v0.t - s = 0 -> t
                ts = (-vi + sqrt(vi^2 + 2 * a * s)) / a;
            else
                ts = s / vi;
            end
            
            % calculate velocity and yaw rate as function of arc length dr(s), v(s)
            vs = vi + a * ts;
            drs = vs .* ks;
            acps = vs .* drs;
        end
        
        
        function [ts, s, xs, ys, rs, vs, drs, acps]  = SplineTrajLinVelOpt(xf, yf, rf, vi, vf, dri, drf)
            % Set up optimization problem
            yp0 = [0, 0];
            opts = optimoptions('fminunc', ...
                'Algorithm', 'quasi-newton', 'TypicalX', [1, 1], ...
                'StepTolerance', 1e-3, 'FunctionTolerance', 1e-3, ...
                'MaxIterations', 10, 'OptimalityTolerance', 1e-5, ...
                'MaxFunctionEvaluations', 300, 'Display', 'None');
            nfeval = 0;
            f = @cost;
            % Run optimization
            [yp, acpmax, exitflag] = fminunc(f, yp0, opts);
            % Evaluate optimal trajectory
            [ts, s, xs, ys, rs, vs, drs, acps] = ...
                PlannerGeom.SplineTrajLinVel(xf, [yp, yf], rf, vi, vf, dri, drf);
            nfeval = nfeval + 1;
            fprintf('exitflag=%d nfeval=%d\n', exitflag, nfeval);
            function c = cost(yp)
                [t, ~, ~, ~, ~, ~, ~, acp] = ...
                    PlannerGeom.SplineTrajLinVel(xf, [yp, yf], rf, vi, vf, dri, drf, [], [], true);
                c = sqrt(sum(acp(2 : end).^2 .* diff(t))) / t(end);
                nfeval = nfeval + 1;
            end
        end
        %% PolyTrajLinVel --------------------------------------------------------------------------
        % [Summary]
        %   This function evaluates a trajectory which has a quintic polynomial path function 
        %   (y(x) = p1.x^5 + p2.x^4 + p3.x^3 + p4.x^2 + p5.x + p6) and linear longitudinal velocity
        %   as function of time (v(t)). The trajectory is evaluated in the ego vehicle's coordiante
        %   system.
        %
        % [Input]
        %   xf      final longitudinal position
        %   yf      final lateral position
        %   rf      final yaw (heading angle)
        %   vi      initial longitudinal velocity
        %   vf      final longitudinal velocity
        %   dx      optional, longitudinal coordinate resolution for numeric calculations 
        %           (numeric scalar), default is 1 cm
        %   ds      optional, arc length resolution for numeric calculations (numeric scalar),
        %           default is 10 cm
        %
        % [Output]
        %   ts       time values (numeric vector)
        %   s        arc length values (numeric vector)
        %   xs       longitudinal coordinates in vehicle-fixed coordinate system (numeric vector)
        %   ys       lateral coordinates in vehicle-fixed coordinate system (numeric vector)
        %   rs       yaw (heading) angle values (numeric vector)
        %   vs       longitudinal velocity values (numeric vector)
        %   drs      yaw rate values (numeric vector)
        %   acps     lateral acceleration values (numeric values)
        % ------------------------------------------------------------------------------------------
        function [ts, s, xs, ys, rs, vs, drs, acps]  = PolyTrajLinVel(xf, yf, rf, vi, vf, dx, ds)
            %% Default arguments
            if (nargin < 7) || isempty(ds)
                ds = 0.1;
            end
            if (nargin < 6) || isempty(dx)
                dx = 0.01;
            end
            %% Check arguments
            if ~isnumeric(xf) || ~isscalar(xf)
                error('Final value for longitudinal position (xf) must be a numeric scalar.');
            end
            if ~isnumeric(yf) || ~isscalar(yf)
                error('Final value for lateral position (yf) must be a numeric scalar.');
            end
            yf = yf(:);
            if ~isnumeric(rf) || ~isscalar(rf)
                error('Final value for heading (rf) must be a numeric scalar.');
            end
            if ~isnumeric(vi) || ~isscalar(vi)
                error('Initial value for velocity (vi) must be a numeric scalar.');
            end
            if ~isnumeric(vf) || ~isscalar(vf)
                error('Final value for velocity (vf) must be a numeric scalar.');
            end
            if ~isnumeric(dx) || ~isscalar(dx)
                error('Resolution for x (dx) must be a numeric scalar.');
            end
            if ~isnumeric(dx) || ~isscalar(dx)
                error('Resolution for arc length (ds) must be a numeric scalar.');
            end
            
            %% Evaluate path and curvature as function of longitudinal coordinate [ y(x), k(x) ]
            % longitudinal coordinate domain x
            nx = ceil(xf / dx) + 1;
            dxa = xf / (nx - 1);
            x = linspace(0, xf, nx)';
            
            % path y(x), path derivative y'(x), and path second derivative y''(x) functions
            % interpolation in normalized space so that X is not becaming close to singular -> xf = 1
            xfs = 1;
            X = [       0,           0,         0 ,      0,   0, 1;      % p(0) = 0
                     xfs^5,      xfs^4,     xfs^3,   xfs^2, xfs, 1;      % p(xf) = yf
                        0,           0,         0,       0,   1, 0;      % p'(0) = 0
                 5 * xfs^4,  4 * xfs^3, 3 * xfs^2, 2 * xfs,   1, 0;      % p'(xf) = tan(rf)
                        0,           0,         0,       2,   0, 0;      % p''(0) = 0
                20 * xfs^3, 12 * xfs^2,   6 * xfs,       2,   0, 0];     % p''(xf) = 0
            Y = [0; yf; 0; tan(rf); 0; 0];
            p = X \ Y;
            % exponents
            e5 =  (5 : -1 : 0);
            e4 = [(4 : -1 : 0), 0];
            e3 = [(3 : -1 : 0), 0, 0];
            % normalized x values
            xs = x / xf;
            yx = sum(xs .^ e5 .* p', 2);
            dyx_dx = sum(xs  .^ e4 .* e5 .* p', 2) / xf;
            ddyx_dx = sum(xs .^ e3 .* e5 .* e4 .* p', 2) / xf^2;
            % curvature function
            kx = ddyx_dx ./ ((1 + dyx_dx.^2).^(3 / 2));
            
            %% Calculate position and heading as function of arc length [ x(s), y(s), r(s) ]
            
            % calculate arc length as function of lon. position s(x)
            sx = cumsum([0; sqrt(dxa^2 + diff(yx).^2)]);
            
            % arc length domain s
            ns = ceil(sx(end) / ds) + 1;
            dsa = sx(end) / (ns - 1);
            s = linspace(0, sx(end), ns)';
            
            % calculate lon. and lat. positions as function of arc length x(s) and y(s)
            xs = interp1q(sx, x, s);      
            ys = interp1q(sx, yx, s);
            
            % calculate curvature as function of arc length k(s)
            ks = interp1q(sx, kx, s);
            
            % calculate heading as function of arc length r(s)
            rs = [0; atan(diff(ys) ./ diff(xs))];
            
            %% Calculate time, velocity and yaw rate as function of arc length [ t(s), v(s), dr(s) ]
            
            % calculate time as function of arc length t(s)
            va = (vi + vf) / 2;
            tf = s(end) / va;
            a = (vf - vi) / tf;
            if a ~= 0
                % (a/2).t^2 + v0.t - s = 0 -> t
                ts = (-vi + sqrt(vi^2 + 2 * a * s)) / a;
            else
                ts = s / vi;
            end
            
            % calculate velocity and yaw rate as function of arc length dr(s), v(s)
            vs = vi + a * ts;
            drs = vs .* ks;
            acps = vs .* drs;
            
        end       

        %% CurvatureTrajI --------------------------------------------------------------------------
        % [Summary]
        %   This function evaluates a trajectory which has a picewise linear curvature as function
        %   of arc length (k(s)) and a piecewise linear longitudinal velocity as function of time
        %   (v(t)). The function calculates velocity in time with interpolation.
        %
        % [Input]
        %   sp      arc length sites (numeric vector)
        %   kp      curvature values at arc length sites (numeric vector)
        %   vp      longitudinal velocity values at arc length sites sp (numeric vector)
        %   dt      optional, time resolution for numeric calculations (numeric scalar), 
        %           default is 1 ms
        %   ds      optional, arc length resolution for numeric calculations (numeric scalar),
        %           default is 10 cm
        %
        % [Output]
        %   ts       time values (numeric vector)
        %   s        arc length values (numeric vector)
        %   xs       longitudinal coordinates in ground-fixed coordinate system (numeric vector)
        %   ys       lateral coordinates in ground fixed coordinate system (numeric vector)
        %   rs       yaw (heading) angle values (numeric vector)
        %   vs       longitudinal velocity values (numeric vector)
        %   drs      yaw rate values (numeric vector)
        %   acps     lateral acceleration values (numeric values)
        % ------------------------------------------------------------------------------------------
        function [ts, s, xs, ys, rs, vs, drs, acps] = CurvatureTrajI(sp, kp, vp, ds, dt)      
            %% Default arguments
            if nargin < 5
                dt = 0.001;
                if nargin < 4
                    ds = 0.1;
                end
            end
            
            %% Check and prepare arguments
            np = length(sp);
            if ~(isnumeric(sp) && isvector(sp) && (np >= 2) && all(diff(sp) > 0))
                error('ERROR: sp (arc length sites) must be a strictly monotonic increasing numeric vector with at least 2 elements.');
            end
            sp = sp(:);
            if ~(isnumeric(kp) && isvector(kp) && (length(kp) == np))
                error('ERROR: kp (curvature values) must be a numeric vector with the same number of elements as sp (arc length sites).');
            end
            kp = kp(:);
            if ~(isnumeric(vp) && isvector(vp) && (length(vp) == np))
                error('ERROR: vp (velocity values) must be a numeric vector with the same number of elements as sp (arc length sites).');
            end
            vp = vp(:);
            if ~(isnumeric(ds) && isscalar(ds))
                error('ERROR: ds (arc length resolution) must be a numeric scalar.');
            end
            if ~(isnumeric(dt) && isscalar(dt))
                error('ERROR: dt (time resolution) must be a numeric scalar.');
            end
            
            %% Determine velocity and arc length as function of time [t, v(t), s(t)]
            
            % Time needed to travel along specified path sections
            % length of sections / average velocities along sections
            tp = cumsum([0; diff(sp) ./ ((vp(1 : end - 1) + vp(2 : end)) / 2)]);
            
            % time domain t
            nt = ceil(tp(end)/dt) + 1;
            dtt = tp(end) / (nt - 1);
            t = linspace(0, tp(end), nt)';
            
            % velocity as function of time v(t) => should be linear
            vt = interp1q(tp, vp, t);
            
            % arc length as function of time s(t)
            st = cumsum([0; vt(1 : end - 1) * dtt + diff(vt) / 2 * dtt]);
            
            % add a last element so that st(end) surely reaches sp(end)
            t(end + 1) = tp(end);
            vt(end + 1) = vp(end);
            st(end + 1) = sp(end);     
            
            %% Transform velocity and time to arc length coordinates [s, t(s), v(s)]
            
            % arc length domain s
            ns = ceil(sp(end)/ds) + 1;
            dst = sp(end) / (ns - 1);
            s = linspace(0, sp(end), ns)';            
            
            % time as function of arc length t(s)
            ts = interp1q(st, t, s);
            
            % velocity as function of arc length v(s)
            vs = interp1q(st, vt, s);
            
            %% Calculate yaw angle, yaw rate, and coordinates [r(s), dr(s), x(s), y(s)]
            
            % curvature as function of arc length k(s) => should be linear
            ks = interp1q(sp, kp, s);
            
            % Yaw rate along path dr(s)/dt(s)
            drs = vs .* ks;
            
            % Centripetal acceleration along path
            acps = vs .* drs;
            
            % Heading (yaw angle) along path r(s)
            rs = cumsum([0; drs(1 : end - 1) .* diff(ts)]);
            
            % longitudinal coordinates x(s)
            dxs = dst * cos(rs(1 : end - 1));
            xs = [0; cumsum(dxs)];
            
            % lateral coordinates y(s)
            dys = dst .* sin(rs(1 : end - 1));
            ys = [0; cumsum(dys)];
            
            
        end
        
        %% CurvatureTraj ---------------------------------------------------------------------------
        % [Summary]
        %   This function evaluates a trajectory which has a picewise linear curvature as function
        %   of arc length (k(s)) and a piecewise linear longitudinal velocity as function of time
        %   (v(t)). The function calculates velocity in time without interpolation.
        %
        % [Input]
        %   sp      arc length sites (numeric vector)
        %   kp      curvature values at arc length sites (numeric vector)
        %   vp      longitudinal velocity values at arc length sites sp (numeric vector)
        %   ds      optional, arc length resolution for numeric calculations (numeric scalar),
        %           default is 10 cm
        %
        % [Output]
        %   t        time values (numeric vector)
        %   s        arc length values (numeric vector)
        %   x        longitudinal coordinates in ground-fixed coordinate system (numeric vector)
        %   y        lateral coordinates in ground fixed coordinate system (numeric vector)
        %   r        yaw (heading) angle values (numeric vector)
        %   v        longitudinal velocity values (numeric vector)
        %   dr       yaw rate values (numeric vector)
        %   acp      lateral acceleration values (numeric values)
        % ------------------------------------------------------------------------------------------
        function [t, s, x, y, r, v, dr, acp] = CurvatureTraj(sp, kp, vp, ds)
            %% Default arguments
            if nargin < 4
                ds = 0.1;
            end
            
            %% Check and prepare arguments
            np = length(sp);
            if ~(isnumeric(sp) && isvector(sp) && (np >= 2) && all(diff(sp) > 0))
                error('ERROR: sp (arc length sites) must be a strictly monotonic increasing numeric vector with at least 2 elements.');
            end
            sp = sp(:);
            if ~(isnumeric(kp) && isvector(kp) && (length(kp) == np))
                error('ERROR: kp (curvature values) must be a numeric vector with the same number of elements as sp (arc length sites).');
            end
            kp = kp(:);
            if ~(isnumeric(vp) && isvector(vp) && (length(vp) == np))
                error('ERROR: vp (velocity values) must be a numeric vector with the same number of elements as sp (arc length sites).');
            end
            vp = vp(:);
            if ~(isnumeric(ds) && isscalar(ds))
                error('ERROR: ds (arc length resolution) must be a numeric scalar.');
            end
            
            %% Calculate trajectory
            
            % time needed to travel along path sections
            % length of sections / average velocities along sections
            tp = cumsum([0; diff(sp) ./ ((vp(1 : end - 1) + vp(2 : end)) / 2)]);
            
            % acceleration at path sections
            ap = diff(vp) ./ diff(tp); 
            
            % arc length domain s
            ns = ceil(sp(end) / ds) + 1;
            dst = sp(end) / (ns - 1);
            s = linspace(0, sp(end), ns)';  
            
            % indices to every arc length value: which path section do the value belong to
            [~, ~, ip] = histcounts(s, sp);
            
            % determine time value for each arc length value            
            % (a/2).t^2 + v0.t - s = 0
            t = ((-vp(ip) + sqrt(vp(ip).^2 + 2 * ap(ip) .* (s - sp(ip)))) ./ ap(ip) + tp(ip));
            t2 = (s - sp(ip)) ./ vp(ip) + tp(ip);
            i = isnan(t);
            t(i) = t2(i);
            % determine velocity value for each arc length value
            v = vp(ip) + ap(ip) .* (t - tp(ip));
            
            % curvature as function of arc length k(s) => should be linear
            k = interp1q(sp, kp, s);
            
            % Yaw rate along path dr(s)/dt(s)
            dr = v .* k;
            
            % Centripetal acceleration along path
            acp = v .* dr;
            
            % Heading (yaw angle) along path r(s)
            r = cumsum([0; dr(1 : end - 1) .* diff(t)]);
            
            % longitudinal coordinates x(s)
            dx = dst * cos(r(1 : end - 1));
            x = [0; cumsum(dx)];
            
            % lateral coordinates y(s)
            dy = dst * sin(r(1 : end - 1));
            y = [0; cumsum(dy)];
                       
        end        
        
        

    end
end