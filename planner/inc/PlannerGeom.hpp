#pragma once
#ifndef PLANNERGEOM_HPP
#define PLANNERGEOM_HPP

#include "TypesTp.hpp"
#include "Eigen/Dense"
#include "optimization.h"

namespace TrajectoryPlanning
{
    
    class PlannerGeom
    {
    public:
        Int32_T SplineTrajLinVel(const Float64_T xf,
                                 const Eigen::VectorXd & yp0,
                                 const Float64_T rf,
                                 const Float64_T vi,
                                 const Float64_T vf,
                                 const Float64_T dri,
                                 const Float64_T drf,
                                 Eigen::VectorXd & ts,
                                 Eigen::VectorXd & s,
                                 Eigen::VectorXd & xs,
                                 Eigen::VectorXd & ys,
                                 Eigen::VectorXd & rs,
                                 Eigen::VectorXd & vs,
                                 Eigen::VectorXd & drs,
                                 Eigen::VectorXd & acps,
                                 Boolean_T optimflag = FALSE);

        std::tuple<Int32_T, Int32_T> SplineTrajLinVelOpt(const Float64_T &xf,
                                                         const Float64_T &yf,
                                                         const Float64_T &rf,
                                                         const Float64_T &vi,
                                                         const Float64_T &vf,
                                                         const Float64_T &dri,
                                                         const Float64_T &drf,
                                                         Eigen::VectorXd &ts,
                                                         Eigen::VectorXd &s,
                                                         Eigen::VectorXd &xs,
                                                         Eigen::VectorXd &ys,
                                                         Eigen::VectorXd &rs,
                                                         Eigen::VectorXd &vs,
                                                         Eigen::VectorXd &drs,
                                                         Eigen::VectorXd &acps);

    private:
        Float64_T xf_, yf_, rf_, dri_, drf_, vi_, vf_;
        Eigen::VectorXd t_, s_, x_, y_, r_, v_, dr_, acp_;
        Int32_T nfeval_;
        static void cost_fcn(const alglib::real_1d_array &x, double &fval,  alglib::real_1d_array &grad, void *ptr);
    };
}

#endif // PLANNERGEOM_HPP