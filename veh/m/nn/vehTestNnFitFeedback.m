%% Clean up
clearvars();

%% Parameters
% Constant
SAMPLE_RATE_MDL_S = 1e-3;
SAMPLE_RATE_NN_S = 1e-2;

% Modifiable
DATA_FOLDER = fullfile('E:', 'VehModel_NN_2020');
NAME = 'MotionDeltasV00';
TAG = 'n256l4v1_relu_00050';
DRIVE_DATA = 'drivedata_011';
NUM_VEH_OUT = 23;
DURATION_S = 10;

% Dependent
trainingFolder = fullfile(DATA_FOLDER, NAME);
modelFolder = fullfile(trainingFolder, ['training_', TAG]);
shift =  round(SAMPLE_RATE_NN_S / SAMPLE_RATE_MDL_S);
numSteps = round(DURATION_S / SAMPLE_RATE_NN_S);

%% Load input and output
% Drive data
if ~exist('output', 'var')
    % Drive data
    load(fullfile(DATA_FOLDER, sprintf('%s.mat', DRIVE_DATA)), 'output');
end

% Metadata
metadataPath = fullfile(trainingFolder, 'metadata.json');
metadata = jsondecode(fileread(metadataPath));

% Input scale
inputScalePath = fullfile(trainingFolder, 'input_scale.bin');
fileIn = fopen(inputScalePath, 'r');
scaleIn = fread(fileIn, metadata.NumInputs, 'float64=>float64', 0, 'l');
fclose(fileIn);
scaleIn = reshape(scaleIn, metadata.NumInputs, 1)';

% Output scale
outputScalePath = fullfile(trainingFolder, 'output_scale.bin');
fileOut = fopen(outputScalePath, 'r');
scaleOut = fread(fileOut, metadata.NumOutputs, 'float64=>float64', 0, 'l');
fclose(fileOut);
scaleOut = reshape(scaleOut, metadata.NumOutputs, 1)';


%% Load model
if ~exist('model', 'var')
    modelPath = fullfile(modelFolder, ['model_', TAG, '.json']);
    weightPath = fullfile(modelFolder, ['weights_', TAG, '.h5']);
    % Import Keras layers
    layers = importKerasLayers(modelPath, 'WeightFile', weightPath, 'ImportWeights', true);
    % Replace input layer from default imageInputLayer (MATLAB converts Keras Input to this) to
    % sequenceInputLayer and add final regressionLayer as MATLAB needs an output layer.
    layers = [sequenceInputLayer(max(layers(1).InputSize), 'Name', 'input_1'); layers(2:end); regressionLayer('Name', 'regression_1')];
    %layers = [layers; regressionLayer('Name', 'regression_1')];
    lGraph = layerGraph(layers);
    model = assembleNetwork(lGraph);
end

%% Evaluate
nSection = ceil(output.Time(end) / DURATION_S);
rmse = nan(nSection, NUM_VEH_OUT);
emax = nan(nSection, NUM_VEH_OUT);
mae = nan(nSection, NUM_VEH_OUT);
accFilter = true(nSection, 1);
totalRuntime = 0;
for iSection = 6%1 : nSection
    %% Calculate model output 
    startTime = (iSection - 1) * DURATION_S;
    endTime = min(startTime + DURATION_S, output.Time(end - 1) - SAMPLE_RATE_NN_S);   
    startIndex = floor(startTime / SAMPLE_RATE_MDL_S) + 1;
    endIndex = floor(endTime / SAMPLE_RATE_MDL_S) + 1;
    indices = startIndex : shift : endIndex;
    shiftedIndices = indices + shift;
    numSteps = floor((endTime - startTime) / SAMPLE_RATE_NN_S);
    
    accFilter(iSection) = ~any((abs(output.Chassis.AccInrtYV(startIndex : endIndex)) > 3) | ...
        (abs(output.Chassis.AccInrtXV(startIndex : endIndex)) > 3));
        
    switch NAME
        case {'MotionDeltasV00', 'MotionDeltasV00W'}
            vehTestNnFitWithFeedbackV00;
        case 'MotionDeltasV01'
            vehTestNnFitWithFeedbackV01;
        case {'MotionDeltasV02', 'MotionDeltasV02W'}
            vehTestNnFitWithFeedbackV02;
        case 'MotionDeltasV03'
            vehTestNnFitWithFeedbackV03;
        otherwise
            error('add case');
    end
    
    %% Evaluate results
    % Total runtime
    totalRuntime = totalRuntime + runTime;
    
    % Aboslute error
    err = vmOutMdl - vmOutNn;
    assert(all(err(1, :) == 0));
    rmse(iSection, :) = sqrt(mean(err.^2));
    emax(iSection, :) = max(abs(err));
    mae(iSection, :) = mean(abs(err));
    
end
% Runtime
averageRuntime = totalRuntime / nSection;

% Raw results
rmseMax = max(rmse);
emaxMax = max(emax);
maeMax = max(mae);

% Filtered results
rmseMaxFilt = max(rmse(accFilter, :));
emaxMaxFilt = max(emax(accFilter, :));
maeMaxFilt = max(mae(accFilter, :));

%% Print results
% File
fileId = fopen(fullfile(pwd, 'data', 'PlotTemp', [NAME, '_', TAG, '_FitFeedback.txt']), 'w');
% Raw
fprintf(fileId, 'RMSE_RAW     = ');
fprintf(fileId, '%.3e  ', rmseMax);
fprintf(fileId, '\n');
fprintf(fileId, 'EMAX_RAW     = ');
fprintf(fileId, '%.3e  ', emaxMax);
fprintf(fileId, '\n');
fprintf(fileId, 'MAE_RAW      = ');
fprintf(fileId, '%.3e  ', maeMax);
fprintf(fileId, '\n');
% Filtered
fprintf(fileId, 'RMSE_FILT    = ');
fprintf(fileId, '%.3e  ', rmseMaxFilt);
fprintf(fileId, '\n');
fprintf(fileId, 'EMAX_FILT    = ');
fprintf(fileId, '%.3e  ', emaxMaxFilt);
fprintf(fileId, '\n');
fprintf(fileId, 'MAE_FILT     = ');
fprintf(fileId, '%.3e  ', maeMaxFilt);
fprintf(fileId, '\n');
% Close file
fclose(fileId);

% Console
% Raw
fprintf(1, 'RMSE_RAW     = ');
fprintf(1, '%.3e  ', rmseMax);
fprintf(1, '\n');
fprintf(1, 'EMAX_RAW     = ');
fprintf(1, '%.3e  ', emaxMax);
fprintf(1, '\n');
fprintf(1, 'MAE_RAW      = ');
fprintf(1, '%.3e  ', maeMax);
fprintf(1, '\n');
% Filtered
fprintf(1, 'RMSE_FILT    = ');
fprintf(1, '%.3e  ', rmseMaxFilt);
fprintf(1, '\n');
fprintf(1, 'EMAX_FILT    = ');
fprintf(1, '%.3e  ', emaxMaxFilt);
fprintf(1, '\n');
fprintf(1, 'MAE_FILT     = ');
fprintf(1, '%.3e  ', maeMaxFilt);
fprintf(1, '\n');

%% Plot results
time = startTime : SAMPLE_RATE_NN_S : endTime;
names = {'PosXG', 'PosYG', 'YawZ', 'VelXV', 'VelYV', 'YawVelZ', 'PitchVelYF', 'SlipXF', 'SlipYF', ...
    'PitchVelYR', 'SlipXR', 'SlipYR', 'SteerAng' 'AccInrtXV', 'AccInrtYV', 'YawAccZ', ...
    'PitchAccYF', 'DSlipXF', 'DSlipYF', 'PitchAccYR', 'DSlipXR', 'DSlipYR', 'DSteerAng'};
fig = nicefigure('nn', 45, 25);
subPlotCols = 5;
subPlotRows = ceil(size(vmOutMdl, 2) / subPlotCols);
for iPlot = 1 : size(vmOutMdl, 2)
    ax = niceaxis(fig, 8, true);
    subplot(subPlotRows, subPlotCols, iPlot, ax);
    yyaxis(ax, 'left');
    plot(ax, time, vmOutMdl(:, iPlot), 'Color', nicecolors.RedAlizarin);
    plot(ax, time, vmOutNn(:, iPlot), 'Color', nicecolors.PurpleWisteria, 'LineWidth', 2,  'LineStyle', ':');
    ax.YAxis(1).Color = nicecolors.RedAlizarin;
    ax.YAxis(1).Label.String = names{iPlot};
    yyaxis(ax, 'right');
    plot(ax, time, vmOutMdl(:, iPlot) - vmOutNn(:, iPlot), 'Color', nicecolors.GreenEmerald);
    ax.YAxis(2).Color = nicecolors.GreenEmerald;
    ax.YAxis(2).Label.String = 'error';
    title(ax, sprintf('rmse=%.4e\nemax=%.4e', rmseMax(iPlot), emaxMax(iPlot)));
    nicelegend({'act', 'pred'}, ax, 8);
end
sgtitle(fig, sprintf('%s\n%s', NAME, strrep(TAG, '_', '\_')), 'Interpreter', 'Latex', 'FontSize', 8);
print(fig, fullfile(pwd, 'data', 'PlotTemp', [NAME, '_', TAG, '_FitFeedback.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [NAME, '_', TAG, '_FitFeedback.png']), '-dpng', '-opengl');
