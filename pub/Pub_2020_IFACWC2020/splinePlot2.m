close all;
clear;

xb = [10, 120];
yb = [10, 30];
rb = deg2rad([30, 30]);
vb = [10, 10];
yp = 0;

lx = diff(xb);
ly = diff(yb);

 [ts, s, xs, ys, rs, vs, drs] = SplineTrajLinVel2(xb, yb, rb, vb, yp);

figure();
axes();
hold on;
plot(xs, ys, 'r-', 'LineWidth', 2);

plot(xb, [yb(1), yb(1)], 'g-', 'LineWidth', 2);
plot([xb(1), xb(1)], yb, 'g-', 'LineWidth', 2);

plot([xb(1), xb(1) + lx * cos(rb(1))], [yb(1), yb(1) + lx * sin(rb(1))], 'b-', 'LineWidth', 2);
plot([xb(1), xb(1) - ly * sin(rb(1))], [yb(1), yb(1) + ly * cos(rb(1))], 'b-', 'LineWidth', 2);

axis equal

function [ts, s, xs, ys, rs, vs, drs]  = SplineTrajLinVel2(xb, yb, rb, vb, yp, dx, ds)
%% Default arguments
if nargin < 7
    ds = 0.1;
    if nargin < 6
        dx = 0.01;
    end
end

%% Check arguments
if ~(isnumeric(xb) && isvector(xb) && (length(xb) == 2))
    error('Boundary condition for x (xb) must be a numeric vector with 2 elements.');
end
if ~(isnumeric(yb) && isvector(yb) && (length(yb) == 2))
    error('Boundary condition for y (yb) must be a numeric vector with 2 elements.');
end
if ~(isnumeric(rb) && isvector(rb) && (length(rb) == 2))
    error('Boundary condition for r (rb) must be a numeric vector with 2 elements.');
end
if ~(isnumeric(vb) && isvector(vb) && (length(vb) == 2))
    error('Boundary condition for v (vb) must be a numeric vector with 2 elements.');
end
if ~(isnumeric(yp) && isvector(yp))
    error('Normalized values for y (ypn) must be a numeric vector.');
end
yp = yp(:);
if ~(isnumeric(dx) && isscalar(dx))
    error('Resolution for x (xres) must be a numeric scalar.');
end
if ~(isnumeric(dx) && isscalar(dx))
    error('Resolution for arc length (sres) must be a numeric scalar.');
end

%% Calculate final coordinates in local coordinate system
cos_rb1 = cos(rb(1));
sin_rb1 = sin(rb(1));
xfl = + cos_rb1 * diff(xb) + sin_rb1 * diff(yb);
yfl = - sin_rb1 * diff(xb) + cos_rb1 * diff(yb);

%% Get equidistant samples in local coordinates
nyp = length(yp);
xp = linspace(0, xfl, nyp + 2)';
yp = [0; yp; yfl];

%% Evaluate path and curvature as function of longitudinal coordinate [ y(x), k(x) ]
% longitudinal coordinate domain x
nx = ceil(xfl / dx) + 1;
dxa = xfl / (nx - 1);
x = linspace(0, xfl, nx)';

% path y(x), path derivative y'(x), and path second derivative y''(x) functions
[yx, dyx_dx, ddyx_dx] = interpspline3(xp, yp, x, [0; tan(diff(rb))]);

% curvature function
kx = ddyx_dx ./ ((1 + dyx_dx.^2).^(3 / 2));

%% Calculate position and heading as function of arc length [x(s), y(s), r(s) ]

% calculate arc length as function of lon. position s(x)
sx = cumsum([0; sqrt(dxa^2 + diff(yx).^2)]);

% arc length domain s
ns = ceil(sx(end) / ds) + 1;
dsa = sx(end) / (ns - 1);
s = linspace(0, sx(end), ns)';

% calculate lon. and lat. positions as function of arc length x(s) and y(s)
xsl = interp1q(sx, x, s);
ysl = interp1q(sx, yx, s);
xs = + cos_rb1 * xsl - sin_rb1 * ysl + xb(1);
ys = + sin_rb1 * xsl + cos_rb1 * ysl + yb(1);

% calculate curvature as function of arc length k(s)
ks = interp1q(sx, kx, s);

% calculate heading as function of arc length r(s)
rs = [0; atan(diff(ysl) ./ diff(xsl))] + rb(1);

%% Calculate time, velocity and yaw rate as function of arc length [ t(s), v(s), dr(s) ]
% calculate time as function of arc length t(s)
tf = s(end) / mean(vb);
a = diff(vb) / tf;
if a ~= 0
    % (a/2).t^2 + v0.t - s = 0 -> t
    ts = (-vb(1) + sqrt(vb(1)^2 + 2 * a * s)) / a;
else
    ts = s / vb(1);
end

% calculate velocity and yaw rate as function of arc length dr(s), v(s)
vs = vb(1) + a * ts;
drs = vs .* ks;

end