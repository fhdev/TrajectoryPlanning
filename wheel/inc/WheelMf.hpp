#pragma once
#ifndef WHEELMF_HPP
#define WHEELMF_HPP

#include "../../ext//eigen-3.3.7/Eigen/Dense"
#include "../../com/inc/TypesTp.hpp"
#include "../../com/inc/Ode.hpp"

namespace TrajectoryPlanning
{
	class WheelMf : DynamicSystem
	{
	private:
		// wheel radius [m]
		double radius;
		// moment of inertia [kgm^2]
		double momentOfInertiaY;
		// longitudinal maximum value (Dx) [1]
		double mfMaxX;
		// longitudinal stiffness factor (Bx) [1]
		double mfStiffnessX;
		// longitudinal shape factor (Cx) [1]
		double mfShapeX;
		// longitudinal curvature factor (Ex) [1]
		double mfCurvatureX;
		// velocity independent rolling resistance factor [1]
		double rollingResistanceConst;
		// linearly velocity dependent rolling resistance factor [s/m]
		double rollingResistanceLin;
		// squarely velocity dependent rollint resistance factor [s^2/m^2]
		double rollingResistanceSqr;
		// longitudinal velocity at which rolling resistance reaches it's final value [m/s]
		double rollingResistanceVel;
		// braking torque dependent correction in velocity at which rolling resistance reaches
		// it's final value [1/Ns]
		double rollingResistanceVelCorr;
		// longitudinal velocity at which braking torque reaches it's final value [m/s]
		double brakingTorqueVel;
		// longitudinal relaxation length at zero longitudinal slip [m]
		double relaxationLength0X;
		// minimal longitudinal relaxation length [m]
		double relaxationLengthMinX;
		// standstill longitudinal slip damping [Ns/m]
		double slipDamping0X;
		// longitudinal velocity at which slip damping switches off [m/s^2]
		double velSlowX;
		// longitudinal maximum value (Dy) [1]
		double mfMaxY;
		// lateral stiffness factors (By) [1]
		double mfStiffnessY;
		// lateral shape factors (Cy) [1]
		double mfShapeY;
		// lateral curvature factors (Ey) [1]
		double mfCurvatureY;
		// relaxation length at zero lateral slip in the lateral direction [m]
		double relaxationLength0Y;
		// minimal lateral relaxation length [m]
		double relaxationLengthMinY;
		// standstill lateral slip damping [Ns/m]
		double slipDamping0Y;
		// lateral velocity at which slip damping switches off [m/s^2]
		double velSlowY;
		// minimal slip at which slip superposition is considered [1]
		double minSlip;

	public:
		Float64_T Radius() const;

		Int64_T nP() const override;
		Int64_T nX() const override;
		Int64_T nU() const override;
		Int64_T nY() const override;

		WheelMf(const Eigen::Ref<const Eigen::VectorXd> P);

		WheelMf(
			Float64_T radius,
			Float64_T momentOfInertiaY,
			Float64_T mfMaxX,
			Float64_T mfStiffnessX,
			Float64_T mfShapeX,
			Float64_T mfCurvatureX,
			Float64_T rollingResistanceConst,
			Float64_T rollingResistanceLin,
			Float64_T rollingResistanceSqr,
			Float64_T rollingResistanceVel,
			Float64_T rollingResistanceVelCorr,
			Float64_T brakingTorqueVel,
			Float64_T relaxationLength0X,
			Float64_T relaxationLengthMinX,
			Float64_T slipDamping0X,
			Float64_T velSlowX,
			Float64_T mfMaxY,
			Float64_T mfStiffnessY,
			Float64_T mfShapeY,
			Float64_T mfCurvatureY,
			Float64_T relaxationLength0Y,
			Float64_T relaxationLengthMinY,
			Float64_T slipDamping0Y,
			Float64_T velSlowY,
			Float64_T minSlip);

		Int32_T	Derivatives(Float64_T t, 
			const Eigen::Ref<const Eigen::VectorXd> X,
			const Eigen::Ref<const Eigen::VectorXd> U,
			Eigen::Ref<Eigen::VectorXd> dX, 
			Eigen::Ref<Eigen::VectorXd> Y) const override;
	};
}

#endif // !WHEELMF_HPP