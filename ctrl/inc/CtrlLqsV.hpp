#pragma once
#ifndef CTRLLQSV_HPP
#define CTRLLQSV_HPP

#include "Eigen/Dense"
#include "TypesTp.hpp"
#include "Ode.hpp"


namespace TrajectoryPlanning
{
	class CtrlLqsV : DynamicSystem
	{
	private:
		// lon. velocity of linearization [m/s]
		Float64_T velLin;
		// lon. velocity sites for velocity dependent saturation of driving / braking torque [m/s]
		Eigen::VectorXd satVel;
		// saturation value for driving torque [Nm]
		Eigen::VectorXd satTorqueDrv;
		// saturation value for braking torque [Nm]
		Eigen::VectorXd satTorqueBrk;

		// gain for lon.velocity [Ns]
		Float64_T gainVelX;
		// gain for tracking error integral [N]
		Float64_T gainVelXTrackErrInt;
		// initial gain for lon.velocity [Ns]
		Float64_T gainVelXInit;

	public:

		Int64_T nP() const override;
		Int64_T nX() const override;
		Int64_T nU() const override;
		Int64_T nY() const override;

		CtrlLqsV(
			Float64_T velLin_, 
			const Eigen::Ref<const Eigen::VectorXd> satVel_,
			const Eigen::Ref<const Eigen::VectorXd> satTorqueDrv_,
			const Eigen::Ref<const Eigen::VectorXd> satTorqueBrk_,
			Float64_T gainVelX, 
			Float64_T gainVelXTrackErrInt, 
			Float64_T gainVelXInit);

		Int32_T Derivatives(
			Float64_T t, 
			const Eigen::Ref<const Eigen::VectorXd> X,
			const Eigen::Ref<const Eigen::VectorXd> U,
			Eigen::Ref<Eigen::VectorXd> dX,
			Eigen::Ref<Eigen::VectorXd> Y) const override;

		Int32_T Initialize(
			const Eigen::Ref<const Eigen::VectorXd> Xi,
			Eigen::Ref<Eigen::VectorXd> X) const;
	};
}

#endif // !CTRLLQSV_HPP

