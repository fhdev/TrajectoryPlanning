%% ChassisQrLon ************************************************************************************
% [Summary]
%   This class represents a chassis capable of longitudinal movement only, coupled to a single wheel
%   of a quarter vehicle model. 
%
% [Used in]
%   VehQrLon
%
% [Subclasses]
%   ChassisQr
%
% **************************************************************************************************
classdef ChassisQrLon < handle
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------        
        % Mass of quarter vehicle [kg]
        Mass
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.Mass(self, value)
            if ~isequal(self.Mass, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: Mass must be a numeric scalar!')
                end
                self.Mass = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = ChassisQrLon(varargin)
            if nargin == 1
                if isstruct(varargin{1})
                    self.Mass = varargin{1}.Mass;
                else
                    self.Mass = varargin{1};
                end
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of chassis capable of longitudinal movement 
        %   only, coupled to a single wheel.
        %
        %   The coordinate systems of the wheel, vehicle, and ground are coinciding!
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => PosXG: longitudinal position in ground-fixed coordinate system [m]
        %       X(2) => VelXG: longitudinal velocity in ground-fixed coordinate system [m/s]
        %
        %   U - input vector
        %       U(1) => ForceXV: longitudinal tire force in vehicle-fixed coordinate system [N]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %       Y(1)  => AccXG:     longitudinal acceleration in ground-fixed cs. [m/s^2]
        % ------------------------------------------------------------------------------------------        
        function [dX, Y] = StateEquation(self, ~, X, U)
            %% Calculation of state derivatives
            % Principle of linear momentum for the chassis
            accXG = U(1) / self.Mass;
            %% State derivative varaibles
            dX = [X(2);   % VelXG: longitudinal velocity in ground-fixed coordinate system [m/s]
                  accXG]; % AccXG: longitudinal acceleration in ground-fixed coordinate system [m/s^2]
            %% Output variables
            Y = accXG; % AccXG: longitudinal acceleration in ground-fixed cs. [m/s^2]
        end
    end
end