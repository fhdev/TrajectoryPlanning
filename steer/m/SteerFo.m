%% SteerFo *****************************************************************************************
% [Summary]
%   This class represets a simple steering model which is modelled as a first order transfer
%   function. The parameters are the setting time of the steering actuator and the ratio between  
%   steering wheel and vehicle wheel level steering angle.
%
% [Used in]
%   
%
% [Subclasses]
%   
%
% **************************************************************************************************
classdef SteerFo < handle
    %% Instance properties =========================================================================
    properties        
        %% Properties set in constructor -----------------------------------------------------------
        % Steering ratio from steering wheel level to wheel level [1]
        Ratio
        
        % Settling time of steering actuator [s]
        TimeConstant

    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.Ratio(self, value)
            if ~isequal(self.Ratio, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: Ratio must be a numeric vector!')
                end
                self.Ratio = value;
            end
        end
        
        function set.TimeConstant(self, value)
            if ~isequal(self.TimeConstant, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: TimeConstant must be a numeric vector!')
                end
                self.TimeConstant = value;
            end            
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = SteerFo(varargin)
            if nargin == 1
                self.Ratio = varargin{1}.Ratio;
                self.TimeConstant  = varargin{1}.TimeConstant;
            elseif nargin == 2
                self.Ratio = varargin{1};
                self.TimeConstant  = varargin{2};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end
        
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of chassis capable of longitudinal movement 
        %   only, coupled to a single wheel.
        %
        %   The coordinate systems of the wheel, vehicle, and ground are coinciding!
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => SteerAng: wheel level steering angle [rad]
        %
        %   U - input vector
        %       U(1) => SteerWheelAng: steering wheel angle [rad]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %       Y(1) => DSteerAng: derivative of wheel level steering angle [rad/s]
        % ------------------------------------------------------------------------------------------        
        function [dX, Y] = StateEquation(self, ~, X, U)
            %% Calculation of state derivatives
            dX = self.Ratio / self.TimeConstant * U - 1 / self.TimeConstant * X;
            %% Output variables
            Y = dX; % 
        end
        
    end
end