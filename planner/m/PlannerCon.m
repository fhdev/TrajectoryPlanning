%% PlannerCon **************************************************************************************
% [Summary]
%   This class represents a constrained nonlinear solver based motion planner.
%   The planner aims to reach a prescribed end state from the current initial state. 
%   This happens with the solution of the state constraint equation via Matlab fsolve.
%
% [Used in]
%   user
%
% [Subclasses]
%   PlannerOpt
% **************************************************************************************************
classdef PlannerCon < handle
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % Vehicle in- and outputs
        vehIn           % VehStClIn object
        vehOut          % VehStClOut object
        % Constraint function for states
        conState        % PlannerConFcnVehState object
        % Optimization variable (parameters of reference signal)
        optVar          % PlannerOptVarVelYawRate object
        
        %% Properties set in constructor -----------------------------------------------------------
        % Vehicle motion model
        Vehicle         % VehStCl object
        % Solver options
        SolverOptions   % optim.options.Fsolve object

    end
    
    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------
        
        function value = setVehicle(value)
            if isstruct(value)
                value = VehStCl(value);
            end
            if ~isa(value, 'VehStCl')
                error('ERROR: Vehicle must be a VehStCl object!')
            end
            if ~(isa(value.CtrlLon, 'CtrlLqsV') && isa(value.CtrlLat, 'CtrlLatSty'))
                error('ERROR: Vehicle must have CtrlLqsV longitudinal and CtrlLatSty lateral control!')
            end
        end
        
        function value = setSolverOptions(value)
            if ~isa(value, 'optim.options.Fsolve')
                error('ERROR: SolverOptions must be an optim.options.Fsolve object!')
            end
        end        
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.Vehicle(self, value)
            if ~isequal(self.Vehicle, value)
                self.Vehicle = self.setVehicle(value);
            end
        end
        
        function set.SolverOptions(self, value)
            if ~isequal(self.SolverOptions, value)
                self.SolverOptions = self.setSolverOptions(value);
            end
        end
        
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerCon(varargin)
            if nargin == 1
                self.Vehicle       = varargin{1}.Vehicle;
                self.SolverOptions = varargin{1}.SolverOptions;
            elseif nargin == 2
                self.Vehicle       = varargin{1};
                self.SolverOptions = varargin{2};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end

        %% conFcnState -----------------------------------------------------------------------------
        % [Summary]
        %   This function calculates and provides state constraint values in format required by 
        %   fsolve.
        %   
        % [Input]
        %   input    - planner input as PlannerConIn object
        %   optVarV  - optimization variable in a vector as provided by fsolve
        %   
        % [Output]
        %   eqCon    - nonlinear equality constraint vector as required by fsolve
        % ------------------------------------------------------------------------------------------
        function eqCon = conFcnState(self, input, optVarV)
            % Update optimized variable
            newOptVarFlag = self.optVar.Update(optVarV);
            % Run only at first time or if optimized variable changed
            if newOptVarFlag || (isempty(self.vehIn) && isempty(self.vehOut))        
                % Perform new simulation
                self.predictMotion(input);
            end
            %% Return with updated state constraint vector
            self.conState.Update(self.vehOut.Chassis);
            [~, eqCon] = self.conState.ToVector();
        end
        
        %% predictMotion ---------------------------------------------------------------------------
        % [Summary]
        %   This function simulates the vehicle's motion in case of the current reference signal
        %   parameters.
        %
        % [Input]
        %   input - planner input as PlannerConIn object
        %
        % [Output]
        %   none (works on internal properties)
        % ------------------------------------------------------------------------------------------
        function predictMotion(self, input)
            % Generate new vehicle inputs based on reference signal parameters
            self.vehIn = self.optVar.ToVehIn(input);
            % Simulate vehicle motion
            if isa(self.Vehicle.CtrlLat, 'CtrlLatSty') && isa(self.Vehicle.CtrlLon, 'CtrlLqsV')
                self.vehOut = self.Vehicle.SimulateMex(input.InitCon, self.vehIn);
            else
                self.vehOut = self.Vehicle.Simulate(input.InitCon, self.vehIn);
            end
        end
        
        %% CalculateTrajectory ---------------------------------------------------------------------
        % [Summary]
        %   This function calculates a vehicle trajectory according to the provided input.
        %
        % [Input]
        %   input  - planner input signals as PlannerConIn object
        %   
        % [Output]
        %   output - planner output signals as PlannerConOut object
        % ------------------------------------------------------------------------------------------
        function output = CalculateTrajectory(self, input)
            %% Check arguments
            % Serialize input 
            input = input(:);
            if ~(isa(input, 'PlannerConIn') && isscalar(input))
                error('ERROR: input must be a PlannerConIn object!');
            end
            %% Plan trajectory
            % Message to Command Window
            fprintf([repmat('=', 1, 120), '\n']);
            fprintf('Planning a constrained trajectory to x = %.4g m, y = %.4g m, r = %.4g rad, dr = %.4g rad ...\n', ...
                    input.FinalCon.PosXG(1), ...
                    input.FinalCon.PosYG(1), ...
                    input.FinalCon.YawZ(1), ...
                    input.FinalCon.YawVelZ(1));
            % Initialize
            self.conState = PlannerConFcnVehState(input.FinalCon);
            self.optVar = PlannerOptVarVelYawRate(input.OptVarInit);
            % Reset storage for vehicle input and output
            self.vehIn = [];
            self.vehOut = [];
            % Run solver
            optVarInitV = input.OptVarInit.ToVector();
            F = @(optVarV)self.conFcnState(input, optVarV);
            tic;
            [optVarOptV, eqConOpt, exitFlag] = fsolve(F, optVarInitV, self.SolverOptions);
            self.conFcnState(input, optVarOptV);
            ellapsedTime = toc;
            % Output results
            if exitFlag > 0
                fprintf('Success.\n');
            else
                fprintf('Failure.\n');
            end
            fprintf('Planning time:          %.4g\n', ellapsedTime);
            fprintf('Equality constraint:    %s\n', mat2str(eqConOpt, 4));
            fprintf('Optimization variable:  %s\n', mat2str(optVarOptV, 4));
            %% Set output
            output = PlannerConOut(ellapsedTime, exitFlag > 0, self.optVar, ...
                self.vehIn, self.vehOut, self.conState);
        end
        
    end
end
