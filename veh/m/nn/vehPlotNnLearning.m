%% Clean up
clearvars();

%% Parameters
% Modifiable
DATA_FOLDER = fullfile('E:', 'VehModel_NN_2020');
NAME = 'MotionDeltasV00';
% 1: all, 2: the best for every type, 3: the absolute best
PLOT_TYPE = 3;

% Dependent
trainingFolder = fullfile(DATA_FOLDER, NAME);

%% Load learning curves
learningCurves = {};
modelFolders = dir(trainingFolder);
modelFolders = {modelFolders([modelFolders.isdir]).name};
modelFolders = modelFolders(~ismember(modelFolders, {'.', '..'}));
modelNames = strrep(modelFolders, 'training_', '');
for iModel = 1 : length(modelFolders)
    logFilePath = fullfile(trainingFolder, modelFolders{iModel}, ['log_', modelNames{iModel}, '.csv']);
    learningCurves = [learningCurves, {readtable(logFilePath)}];
end
% Last values of validation losses for every result
lastValLosses = cellfun(@(x)x.val_loss(end), learningCurves);
% Names of different types of nn models
modelTypes = unique(cellfun(@(x)x(1 : find(x == '_', 1) - 1), modelNames, 'Uni', false));
% Indices of corresponding results for every modelType (we have multiple runs per modelType)
typeIndices = cellfun(@(y)find(cellfun(@(x)contains(x, y), modelNames)), modelTypes, 'Uni', false);
% Minimal value of validation loss among different runs for every modelType
minLastValLosses = sort(cellfun(@(x)min(lastValLosses(x)), typeIndices));
% Index of best results for every modelType
[minLastValLossIndices, ~] = find(lastValLosses' == minLastValLosses);
% Index of best result at all
[~, minLastValLossIndex] = min(lastValLosses);

%% Create plot
switch PLOT_TYPE
    case 1
        plotIndices = 1 : length(learningCurves);
    case 2
        plotIndices = minLastValLossIndices';
    case 3
        plotIndices = minLastValLossIndex;    
end
colors = {nicecolors.BlueBelizeHole, nicecolors.GreenEmerald, nicecolors.PurpleAmethyst, ...
    nicecolors.RedAlizarin, nicecolors.YellowSunFlower};
name = sprintf('%s_LearningProgress%d', NAME, PLOT_TYPE);
fig = nicefigure(name, 7, 8);
ax = niceaxis(fig, 8);
iColor = 1;
for iNet = plotIndices
    color1 = colors{mod(iColor - 1, length(colors)) + 1} * 1.1;
    color2 = colors{mod(iColor - 1, length(colors)) + 1} * 0.6;
    style = '-';
    iColor = iColor + 1;
    plot(ax, learningCurves{iNet}.epoch, learningCurves{iNet}.loss, 'LineStyle', style, ...
        'Color', color1);
    plot(ax, learningCurves{iNet}.epoch, learningCurves{iNet}.val_loss, 'LineStyle', style, ...
        'Color', color2);
end
xlabel(ax, 'epoch [1]');
ylabel(ax, 'loss [1]');
ax.YAxis.Scale = 'log';
legends = [strcat(modelNames(plotIndices), ' train'); ...
    strcat(modelNames(plotIndices), ' valid')];
legends = strrep(legends, '_', '\_');
l = nicelegend(legends(:), ax, 8);
l.Location = 'NorthOutSide';

%% Save plot
savefig(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');
