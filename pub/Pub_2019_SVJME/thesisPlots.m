%% Data
% load benchmarkingSamplesStruct.mat

%% Common
close all;

% Calculations
nInputs = length(input);
indexLinear = 1:nInputs;
maxAccelerationY = zeros(nInputs, 1);
for iInput = 1 : nInputs
    maxAccelerationY(iInput) = max(abs(outputOpt(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS));
end
[~, indexMaxAccelY] = sort(maxAccelerationY, 'ascend');

% Constants 
OPT_COLOR = nicecolors.BlueBelizeHole;
NN_COLOR = nicecolors.RedAlizarin;
INIT_COLOR = nicecolors.GreenEmerald;
FONT_SIZE = 8;
MARKER_SIZE = 10;

%% Runtimes normalized to plain optimal solution

% Calculations
calcTimeAvgOpt = mean([outputOpt(:).CalcTime]);
calcTimeAvgNN = mean([outputNN(:).CalcTime]);
calcTimeAvgInit = mean([outputOptInit(:).CalcTime]);

calcTimeNormOpt = [outputOpt(:).CalcTime] ./ calcTimeAvgOpt;
calcTimeNormNN = [outputNN(:).CalcTime] ./ calcTimeAvgOpt;
calcTimeNormInit = [outputOptInit(:).CalcTime] ./ calcTimeAvgOpt;

% Constants
NAME = 'Runtimes';
HEIGHT = 4;
WIDTH = 12;

% Plots
fig1 = nicefigure(NAME, WIDTH, HEIGHT);
ax11 = niceaxis(fig1, FONT_SIZE);
scatter(ax11, indexLinear, calcTimeNormOpt(indexMaxAccelY), MARKER_SIZE, 'Marker', 'o', 'MarkerEdgeColor', OPT_COLOR, 'MarkerFaceColor', OPT_COLOR);
scatter(ax11, indexLinear, calcTimeNormNN(indexMaxAccelY), MARKER_SIZE*1.2,  'Marker', 's', 'MarkerEdgeColor', NN_COLOR, 'MarkerFaceColor', NN_COLOR);
scatter(ax11, indexLinear, calcTimeNormInit(indexMaxAccelY), MARKER_SIZE, 'Marker', 'd', 'MarkerEdgeColor', INIT_COLOR, 'MarkerFaceColor', INIT_COLOR);
line(ax11, [0, nInputs], [1, 1], 'Color', OPT_COLOR);
line(ax11, [0, nInputs], repmat(calcTimeAvgNN / calcTimeAvgOpt, 2, 1), 'Color', NN_COLOR);
line(ax11, [0, nInputs], repmat(calcTimeAvgInit / calcTimeAvgOpt, 2, 1), 'Color', INIT_COLOR);
xlim(ax11, [0, nInputs]);
ax11.XTick = 0:10:nInputs;
ylim(ax11, [0, 1.5]);
ax11.YAxis.Scale = 'log';
ax11.YTick = [0.03, 0.5, 1];
xlabel(ax11, '$i$ (no. of trajectory) [1]');
ylabel(ax11, '$k_{np,t}$ (normalized run-time) [1]', 'Position', [-7.42, 0.14, -1]);
l = nicelegend({'optimal planner', 'hybrid nn planner', 'initialized optimal planner'}, ax11, FONT_SIZE);
l.Location = 'NorthEastOutside';

% Save
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

%% Deviation in reference signal parameters normalized to plain optimal solution

% Calculations
refParamDevNormNN = zeros(1, nInputs);
refParamDevNormInit = zeros(1, nInputs);
for iInput = 1 : nInputs
    refParamDevNormNN(iInput) = norm((outputOpt(iInput).RefParam.YawRate - ...
        outputNN(iInput).RefParam.YawRate) ./ outputOpt(iInput).RefParam.YawRate);
    refParamDevNormInit(iInput) = norm((outputOpt(iInput).RefParam.YawRate - ...
        outputOptInit(iInput).RefParam.YawRate) ./ ...
        outputOpt(iInput).RefParam.YawRate);
end
refParamDevNormAvgNN = mean(refParamDevNormNN, 2);
refParamDevNormAvgInit = mean(refParamDevNormInit, 2);

% Constants
NAME = 'RefParamDevNN';
HEIGHT = 4;
WIDTH = 7;

% Plots
fig2 = nicefigure(NAME, WIDTH, HEIGHT);
ax21 = niceaxis(fig2, FONT_SIZE);
scatter(ax21, indexLinear, refParamDevNormNN(indexMaxAccelY), MARKER_SIZE*1.2, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR, 'MarkerFaceColor', NN_COLOR);
line(ax21, [0, nInputs], repmat(refParamDevNormAvgNN, 1, 2), 'Color', NN_COLOR);
xlim(ax21, [0, nInputs]);
ax21.XTick = 0:10:nInputs;
xlabel(ax21, '$i$ (no. of trajectory) [1]');
ylabel(ax21, '$k_{np,p}$ (rel. param. dev.) [1]');
l = nicelegend({'hybrid nn planner'}, ax21, FONT_SIZE);
l.Location = 'NorthOutSide';
l.Position = [0.45,0.87,0.51,0.11];
ax21.Position = [0.17, 0.18, 0.79, 0.64];
ax21.YLabel.Position = [-9.105, 0.075, -1];

% Save
savefig(fig2, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

% --------

% Constants
NAME = 'RefParamDevInit';
HEIGHT = 4;
WIDTH = 7;

% Plots
fig3 = nicefigure(NAME, WIDTH, HEIGHT);
ax31 = niceaxis(fig3, FONT_SIZE);
scatter(ax31, indexLinear, refParamDevNormInit(indexMaxAccelY), MARKER_SIZE, 'Marker', 'd', 'MarkerEdgeColor', INIT_COLOR, 'MarkerFaceColor', INIT_COLOR);
line(ax31, [0, nInputs], repmat(refParamDevNormAvgInit, 1, 2), 'Color', INIT_COLOR);
xlim(ax31, [0, nInputs]);
ax31.XTick = 0:10:nInputs;
xlabel(ax31, '$i$ (no. of trajectory) [1]');
ylabel(ax31, '$k_{np,p}$ (rel. param. dev.) [1]');
l = nicelegend({'initialized optimal planner'}, ax31, FONT_SIZE);
l.Location = 'NorthOutSide';
l.Position = [0.3,0.87,0.66,0.11];
ax31.Position = [0.17, 0.18, 0.79, 0.64];
ax31.YLabel.Position = [-6.13, 0.000041025537677, -1];

% Save
savefig(fig3, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig3, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig3, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

%% Final value of state constraint

% Calculations
stateConstrOpt = zeros(1, nInputs);
stateConstrNN = zeros(1, nInputs);
stateConstrNNCalcTime = zeros(1, nInputs);
stateConstrNNCarMaker = zeros(1, nInputs);
stateConstrNNCarMakerCalcTime = zeros(1, nInputs);
stateConstrInit = zeros(1, nInputs);
for iInput = indexLinear
    % optimal
    c = [outputOpt(iInput).StateConstraint.Values.PositionX_GCS;
        outputOpt(iInput).StateConstraint.Values.PositionY_GCS;
        outputOpt(iInput).StateConstraint.Values.AngleZ;
        outputOpt(iInput).StateConstraint.Values.AngularVelocityZ];
    stateConstrOpt(iInput) = norm(c);
    % nn
    c = [outputNN(iInput).StateConstraint.Values.PositionX_GCS;
        outputNN(iInput).StateConstraint.Values.PositionY_GCS;
        outputNN(iInput).StateConstraint.Values.AngleZ;
        outputNN(iInput).StateConstraint.Values.AngularVelocityZ];
    stateConstrNN(iInput) = norm(c);
    % nn after planning time
    i = find(outputNN(iInput).VehicleOutput.Chassis.Time > outputNN(iInput).CalcTime, 1, 'first');
    c = [outputNN(iInput).VehicleOutput.Chassis.PositionX_GCS(i)    - outputOpt(iInput).VehicleOutput.Chassis.PositionX_GCS(i);
         outputNN(iInput).VehicleOutput.Chassis.PositionY_GCS(i)    - outputOpt(iInput).VehicleOutput.Chassis.PositionY_GCS(i);
         outputNN(iInput).VehicleOutput.Chassis.AngleZ(i)           - outputOpt(iInput).VehicleOutput.Chassis.AngleZ(i);
         outputNN(iInput).VehicleOutput.Chassis.AngularVelocityZ(i) - outputOpt(iInput).VehicleOutput.Chassis.AngularVelocityZ(i)];
    stateConstrNNCalcTime(iInput) = norm(c);
    % nn with carmaker
    c = [outputCarMaker(iInput).PositionX_GCS(end)    - input(iInput).FinalConditions.PositionX_GCS;
         outputCarMaker(iInput).PositionY_GCS(end)    - input(iInput).FinalConditions.PositionY_GCS;
         outputCarMaker(iInput).AngleZ(end)           - input(iInput).FinalConditions.AngleZ;
         outputCarMaker(iInput).AngularVelocityZ(end) - input(iInput).FinalConditions.AngularVelocityZ];
    stateConstrNNCarMaker(iInput) = norm(c); 
    % nn with carmaker after planning time
    i = find(outputCarMaker(iInput).Time > outputNN(iInput).CalcTime, 1, 'first');
    io = find(outputOpt(iInput).VehicleOutput.Chassis.Time > outputNN(iInput).CalcTime, 1, 'first');
    c = [outputCarMaker(iInput).PositionX_GCS(i)    - outputOpt(iInput).VehicleOutput.Chassis.PositionX_GCS(io);
         outputCarMaker(iInput).PositionY_GCS(i)    - outputOpt(iInput).VehicleOutput.Chassis.PositionY_GCS(io);
         outputCarMaker(iInput).AngleZ(i)           - outputOpt(iInput).VehicleOutput.Chassis.AngleZ(io);
         outputCarMaker(iInput).AngularVelocityZ(i) - outputOpt(iInput).VehicleOutput.Chassis.AngularVelocityZ(io)];
    stateConstrNNCarMakerCalcTime(iInput) = norm(c);    
    % initialized optimal  
    c = [outputOptInit(iInput).StateConstraint.Values.PositionX_GCS;
        outputOptInit(iInput).StateConstraint.Values.PositionY_GCS;
        outputOptInit(iInput).StateConstraint.Values.AngleZ;
        outputOptInit(iInput).StateConstraint.Values.AngularVelocityZ];
    stateConstrInit(iInput) = norm(c);
end
stateConstrAvgOpt = mean(stateConstrOpt);
stateConstrAvgNN = mean(stateConstrNN);
stateConstrAvgNNCalcTime = mean(stateConstrNNCalcTime);
stateConstrAvgNNCarMaker = mean(stateConstrNNCarMaker);
stateConstrAvgNNCarMakerCalcTime = mean(stateConstrNNCarMakerCalcTime);
stateConstrAvgInit = mean(stateConstrInit);

% Constants
HEIGHT = 4;
WIDTH = 7;

% Plots
NAME = 'StateConstrOpt';
fig4 = nicefigure(NAME, WIDTH, HEIGHT);
ax41 = niceaxis(fig4, FONT_SIZE);
scatter(ax41, indexLinear, stateConstrOpt(indexMaxAccelY), MARKER_SIZE, 'Marker', 'o', 'MarkerEdgeColor', OPT_COLOR, 'MarkerFaceColor', OPT_COLOR);
line(ax41, [0, nInputs], repmat(stateConstrAvgOpt, 1, 2), 'Color', OPT_COLOR);
xlim(ax41, [0, nInputs]);
ax41.XTick = 0:10:nInputs;
xlabel(ax41, '$i$ (no. of trajectory) [1]');
ylabel(ax41, '$||C_{np}||$ (constr. violation) [1]');
l = nicelegend({'optimal planner'}, ax41, FONT_SIZE);
l.Location = 'NorthOutside';
l.Position = [0.48,0.87,0.472,0.11];
ax41.Position = [0.17, 0.18, 0.79, 0.64];
ax41.YLabel.Position = [-6.130000000000003,0.000021231723244,-1];

% Save
savefig(fig4, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig4, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig4, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

% ------------------

NAME = 'StateConstrNN';
fig5 = nicefigure(NAME, WIDTH, HEIGHT);
ax51 = niceaxis(fig5, FONT_SIZE);
scatter(ax51, indexLinear, stateConstrNN(indexMaxAccelY), MARKER_SIZE*1.2, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR, 'MarkerFaceColor', NN_COLOR);
line(ax51, [0, nInputs], repmat(stateConstrAvgNN, 1, 2), 'Color', NN_COLOR);
xlim(ax51, [0, nInputs]);
ax51.XTick = 0:10:nInputs;
xlabel(ax51, '$i$ (no. of trajectory) [1]');
ylabel(ax51, '$||C_{np}||$ (constr. violation) [1]');
l = nicelegend({'hybrid nn planner'}, ax51, FONT_SIZE);
l.Location = 'NorthOutside';
l.Position = [0.45,0.87,0.51,0.11];
ax51.Position = [0.17, 0.18, 0.79, 0.64];
ax51.YLabel.Position = [-6.818995215310999,0.20814235333968,-1];


% Save
savefig(fig5, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig5, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig5, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

% ------------------
NAME = 'StateConstrNNCalcTime';
fig6 = nicefigure(NAME, WIDTH, HEIGHT);
ax61 = niceaxis(fig6, FONT_SIZE);
scatter(ax61, indexLinear, stateConstrNNCalcTime(indexMaxAccelY), MARKER_SIZE*1.2, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR, 'MarkerFaceColor', NN_COLOR);
line(ax61, [0, nInputs], repmat(stateConstrAvgNNCalcTime, 1, 2), 'Color', NN_COLOR);
xlim(ax61, [0, nInputs]);
ax61.XTick = 0:10:nInputs;
xlabel(ax61, '$i$ (no. of trajectory) [1]');
ylabel(ax61, '$||C_{np,t}||$ (constr. violation) [1]');
l = nicelegend({'hybrid nn planner'}, ax61, FONT_SIZE);
l.Location = 'NorthOutside';
l.Position = [0.45,0.87,0.51,0.11];
ax61.Position = [0.17, 0.18, 0.79, 0.64];
ax61.YLabel.Position = [-6.818995215310999,0.00075,-1];

% Save
savefig(fig6, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig6, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig6, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

% ------------------

NAME = 'StateConstrNNCarMaker';
fig7 = nicefigure(NAME, WIDTH, HEIGHT);
ax71 = niceaxis(fig7, FONT_SIZE);
scatter(ax71, indexLinear, stateConstrNNCarMaker(indexMaxAccelY), MARKER_SIZE*1.2, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR * 0.8, 'MarkerFaceColor', NN_COLOR * 0.8);
line(ax71, [0, nInputs], repmat(stateConstrAvgNNCarMaker, 1, 2), 'Color', NN_COLOR * 0.8);
xlim(ax71, [0, nInputs]);
ax71.XTick = 0:10:nInputs;
xlabel(ax71, '$i$ (no. of trajectory) [1]');
ylabel(ax71, '$||C_{np}||$ (constr. violation) [1]');
l = nicelegend({'hybrid nn planner'}, ax71, FONT_SIZE);
l.Location = 'NorthOutside';
l.Position = [0.45,0.87,0.51,0.11];
ax71.Position = [0.17, 0.18, 0.79, 0.64];
ax71.YLabel.Position = [-6.818995215310999,0.55814235333968,-1];

% Save
savefig(fig7, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig7, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig7, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

% ------------------

NAME = 'StateConstrNNCarMakerCalcTime';
fig8 = nicefigure(NAME, WIDTH, HEIGHT);
ax81 = niceaxis(fig8, FONT_SIZE);
scatter(ax81, indexLinear, stateConstrNNCarMakerCalcTime(indexMaxAccelY), MARKER_SIZE*1.2, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR * 0.8, 'MarkerFaceColor', NN_COLOR * 0.8);
line(ax81, [0, nInputs], repmat(stateConstrAvgNNCarMakerCalcTime, 1, 2), 'Color', NN_COLOR * 0.8);
xlim(ax81, [0, nInputs]);
ax81.XTick = 0:10:nInputs;
xlabel(ax81, '$i$ (no. of trajectory) [1]');
ylabel(ax81, '$||C_{np,t}||$ (constr. violation) [1]');
l = nicelegend({'hybrid nn planner'}, ax81, FONT_SIZE);
l.Location = 'NorthOutside';
l.Position = [0.45,0.87,0.51,0.11];
ax81.Position = [0.17, 0.18, 0.79, 0.64];
ax81.YLabel.Position = [-8.818995215310999,0.045,-1];

% Save
savefig(fig8, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig8, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig8, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

% ------------------

NAME = 'StateConstrInit';
fig9 = nicefigure(NAME, WIDTH, HEIGHT);
ax91 = niceaxis(fig9, FONT_SIZE);
scatter(ax91, indexLinear, stateConstrInit(indexMaxAccelY), MARKER_SIZE, 'Marker', 'd', 'MarkerEdgeColor', INIT_COLOR, 'MarkerFaceColor', INIT_COLOR);
line(ax91, [0, nInputs], repmat(stateConstrAvgInit, 1, 2), 'Color', INIT_COLOR);
xlim(ax91, [0, nInputs]);
ax91.XTick = 0:10:nInputs;
xlabel(ax91, '$i$ (no. of trajectory) [1]');
ylabel(ax91, '$||C_{np}||$ (constr. violation) [1]');
l = nicelegend({'initialized optimal planner'}, ax91, FONT_SIZE);
l.Location = 'NorthOutside';
l.Position = [0.3,0.87,0.66,0.11];
ax91.Position = [0.17, 0.18, 0.79, 0.64];
ax91.YLabel.Position = [-6.13, 0.0000225, -1];

% Save
savefig(fig9, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig9, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig9, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

% ------------------

%% Maximal lateral acceleration normalized to the optimal solution
% Jerk is almost the same, therefore it is not visualized.

% Calculation

maxAccYNN = zeros(nInputs, 1);
maxAccYNN_CarMaker = zeros(nInputs, 1);
maxAccYInit = zeros(nInputs, 1);
for iInput = 1 : nInputs
    maxAccYNN(iInput) = max(abs(outputNN(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS)) / max(abs(outputOpt(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS));
    maxAccYNN_CarMaker(iInput) = max(abs(outputCarMaker(iInput).AccelerationYIntertial_VCS)) / max(abs(outputOpt(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS));
    maxAccYInit(iInput) = max(abs(outputOptInit(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS)) / max(abs(outputOpt(iInput).VehicleOutput.Chassis.AccelerationYInertial_VCS));
end

maxAccYAvgNN = mean(maxAccYNN);
maxAccYAvgNN_CarMaker = mean(maxAccYNN_CarMaker);
maxAccYAvgInit = mean(maxAccYInit);

% Constants
HEIGHT = 4;
WIDTH = 7;

% Plots

NAME = 'MaxAccNN';
fig10 = nicefigure(NAME, WIDTH, HEIGHT);
ax101 = niceaxis(fig10, FONT_SIZE);
scatter(ax101, indexLinear, maxAccYNN(indexMaxAccelY), MARKER_SIZE*1.2, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR, 'MarkerFaceColor', NN_COLOR);
line(ax101, [0, nInputs], repmat(maxAccYAvgNN, 1, 2), 'Color', NN_COLOR);
xlim(ax101, [0, nInputs]);
ax101.XTick = 0:10:nInputs;
xlabel(ax101, '$i$ (no. of trajectory) [1]');
ylabel(ax101, '$k_{np,a}$ (max. rel. acc.) [1]');
l = nicelegend({'hybrid nn planner'}, ax101, FONT_SIZE);
l.Location = 'NorthOutside';
l.Position = [0.45,0.87,0.51,0.11];
ax101.Position = [0.17, 0.18, 0.79, 0.64];
ax101.YLabel.Position = [-8.818995215310999,1.025,-1];


% Save
savefig(fig10, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig10, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig10, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

% ---------------------------

NAME = 'MaxAccNNCarmaker';
fig11 = nicefigure(NAME, WIDTH, HEIGHT);
ax111 = niceaxis(fig11, FONT_SIZE);
scatter(ax111, indexLinear, maxAccYNN_CarMaker(indexMaxAccelY), MARKER_SIZE*1.2, 'Marker', 's', 'MarkerEdgeColor', NN_COLOR * 0.8, 'MarkerFaceColor', NN_COLOR * 0.8);
line(ax111, [0, nInputs], repmat(maxAccYAvgNN_CarMaker, 1, 2), 'Color', NN_COLOR);
xlim(ax111, [0, nInputs]);
ax111.XTick = 0:10:nInputs;
xlabel(ax111, '$i$ (no. of trajectory) [1]');
ylabel(ax111, '$k_{np,a}$ (max. rel. acc.) [1]');
l = nicelegend({'hybrid nn planner'}, ax111, FONT_SIZE);
l.Location = 'NorthOutside';
l.Position = [0.45,0.87,0.51,0.11];
ax111.Position = [0.17, 0.18, 0.79, 0.64];
ax111.YLabel.Position = [-8.818995215310999,1.05,-1];


% Save
savefig(fig11, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig11, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig11, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

% ------------------

NAME = 'MaxAccInit';
fig12 = nicefigure(NAME, WIDTH, HEIGHT);
ax121 = niceaxis(fig12, FONT_SIZE);
scatter(ax121, indexLinear, maxAccYInit(indexMaxAccelY), MARKER_SIZE, 'Marker', 'd', 'MarkerEdgeColor', INIT_COLOR, 'MarkerFaceColor', INIT_COLOR);
line(ax121, [0, nInputs], repmat(maxAccYAvgInit, 1, 2), 'Color', INIT_COLOR);
xlim(ax121, [0, nInputs]);
ax121.XTick = 0:10:nInputs;
xlabel(ax121, '$i$ (no. of trajectory) [1]');
ylabel(ax121, '$||C_{np}||$ (constr. violation) [1]');
ylabel(ax121, '$k_{np,a}$ (max. rel. acc.) [1]');
l = nicelegend({'initialized optimal planner'}, ax121, FONT_SIZE);
l.Location = 'NorthOutside';
l.Position = [0.3,0.87,0.66,0.11];
ax121.Position = [0.22, 0.18, 0.74, 0.64];
ax121.YLabel.Position = [-16, 0.999995, -1];

% Save
savefig(fig12, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.fig']), 'compact');
print(fig12, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.pdf']), '-dpdf', '-painters');
print(fig12, fullfile(pwd, 'data', 'PlotTemp', [NAME, '.png']), '-dpng', '-opengl');

%% Optimal trajectories
% Subsampling
subs = @(x) [x(1:10:end); x(end)]; 

% Constants
NAME_PATH = 'PathSamples';
NAME_TRAJ = 'TrajectorySamples';
CIRCULAR_COLOR =  nicecolors.BlueBelizeHole;
SYMMETRIC_COLOR = nicecolors.GreenEmerald;
HEIGHT = 4;
WIDTH = 7;
MARKER_SIZE = 5;
OFFSET = 5;
FONT_SIZE = 8;

% Plots
figPath = nicefigure(NAME_PATH, WIDTH, HEIGHT);
axPath = niceaxis(figPath, FONT_SIZE);
figTraj = nicefigure(NAME_TRAJ, WIDTH, HEIGHT);
axTraj = niceaxis(figTraj, FONT_SIZE);

xl = zeros(1, 2);
yl = zeros(1, 2);
for iOutput = 1 : length(outputOpt)
    doPlot = false;
    % Circular trajectories - upper side of figure
    if mod(iOutput, 2) == 1
        color = CIRCULAR_COLOR;
        if outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS(end) >= 0
            doPlot = true;
        end
    % Symmetric trajectories - lower side of figure
    else
        color = SYMMETRIC_COLOR;
        if outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS(end) <= 0
            doPlot = true;
        end
    end
    if doPlot
        % Dynamic limits
        xl(1) = min(xl(1), min(outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS));
        xl(2) = max(xl(2), max(outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS));
        yl(1) = min(yl(1), min(outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS));
        yl(2) = max(yl(2), max(outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS));
        plot(axPath, subs(outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS), ...
            subs(outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS), 'Color', color);
        scatter(axPath, outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS(end), ...
            outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS(end), MARKER_SIZE, ...
            'MarkerEdgeColor', color);
        plot3(axTraj, subs(outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS), ...
            subs(outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS), ...
            subs(outputOpt(iOutput).VehicleOutput.Chassis.AngleZ), ...
            'Color', color);
        scatter3(axTraj, outputOpt(iOutput).VehicleOutput.Chassis.PositionX_GCS(end), ...
            outputOpt(iOutput).VehicleOutput.Chassis.PositionY_GCS(end), ...
            outputOpt(iOutput).VehicleOutput.Chassis.AngleZ(end), MARKER_SIZE, ...
            'MarkerEdgeColor', color);
        if iOutput >= 59
            break
        end
    end
end
xl = round(xl + [-OFFSET, OFFSET]);
yl = round(yl + [-OFFSET, OFFSET]);

scatter(axPath, 0, 0, 5, 'MarkerEdgeColor', 'Black');
line(axPath, xl, [0, 0], 'Color', 'Black');
xlim(axPath, xl);
ylim(axPath, yl);
xlabel(axPath, '$x^G_v$ (pos. north) [m]');
ylabel(axPath, '$y^G_v$ (pos. west) [m]');

% Save
savefig(figPath, fullfile(pwd, 'data', 'PlotTemp', [NAME_PATH, '.fig']), 'compact');
print(figPath, fullfile(pwd, 'data', 'PlotTemp', [NAME_PATH, '.pdf']), '-dpdf', '-painters');
print(figPath, fullfile(pwd, 'data', 'PlotTemp', [NAME_PATH, '.png']), '-dpng', '-opengl');

scatter3(axTraj, 0, 0, 0, 5, 'MarkerEdgeColor', 'Black');
line(axTraj, xl, [yl(1), yl(1)], [0, 0], 'Color', 'Black');
line(axTraj, xl, [yl(2), yl(2)], [0, 0], 'Color', 'Black');
line(axTraj, [xl(1), xl(1)], yl, [0, 0], 'Color', 'Black');
line(axTraj, [xl(2), xl(2)], yl, [0, 0], 'Color', 'Black');
xlim(axTraj, xl);
ylim(axTraj, yl);
view(axTraj, [25, 15]);
xlabel(axTraj, '$x^G_v$ (pos. north) [m]');
ylabel(axTraj, '$y^G_v$ (pos. west) [m]');
zlabel(axTraj, '$\psi_v$ (yaw angle) [rad]');

% Save
savefig(figTraj, fullfile(pwd, 'data', 'PlotTemp', [NAME_TRAJ, '.fig']), 'compact');
print(figTraj, fullfile(pwd, 'data', 'PlotTemp', [NAME_TRAJ, '.pdf']), '-dpdf', '-painters');
print(figTraj, fullfile(pwd, 'data', 'PlotTemp', [NAME_TRAJ, '.png']), '-dpng', '-opengl');
