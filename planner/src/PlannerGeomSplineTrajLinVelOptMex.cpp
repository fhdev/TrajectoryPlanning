#include <math.h>
#include "mex.h"
#include "Eigen/Dense"
#include "TypesTp.hpp"
#include "MathTp.hpp"
#include "PlannerGeom.hpp"

#define xf_  (prhs[0])
#define yf_  (prhs[1])
#define rf_  (prhs[2])
#define vi_  (prhs[3])
#define vf_  (prhs[4])
#define dri_ (prhs[5])
#define drf_ (prhs[6])

#define t_   (plhs[0])
#define s_   (plhs[1])
#define x_   (plhs[2])
#define y_   (plhs[3])
#define r_   (plhs[4])
#define v_   (plhs[5])
#define dr_  (plhs[6])
#define acp_ (plhs[7])

using namespace Eigen;
using namespace TrajectoryPlanning;
using namespace std;

// [t, s, x, y, r, v, dr, acp] = PlannerGeomSplineTrajLinVelMex(xf, yp, rf, vi, vf, dri, drf);
void mexFunction(int nlhs, mxArray *plhs[],	int nrhs, const mxArray *prhs[])
{
	// Check number of input arguments
	if (nrhs != 7)
    {
		mexErrMsgIdAndTxt("PlannerGeomSplineTrajLinVelMex:nrhs", 
            "Argument list must be [t, s, x, y, r, v, dr, acp] = PlannerGeomSplineTrajLinVelMex(xf, yp, rf, vi, vf, dri, drf). Number of input parameters is not 7.");
	}
    // Check number of output arguments
	if (nlhs != 8) 
    {
        mexErrMsgIdAndTxt("PlannerGeomSplineTrajLinVelMex:nlhs", 
            "Argument list must be [t, s, x, y, r, v, dr, acp] = PlannerGeomSplineTrajLinVelMex(xf, yp, rf, vi, vf, dri, drf). Number of output parameters is not 8.");
	}
	// Check xf (final longitudinal position [m])
	if (!mxIsDouble(xf_) ||	mxIsComplex(xf_) || (!mxIsScalar(xf_)))
    {
		mexErrMsgIdAndTxt("PlannerGeomSplineTrajLinVelMex:invalidArgument", 
            "Parameter xf must be a numeric scalar.");
	}
    // Check xf (final longitudinal position [m])
	if (!mxIsDouble(yf_) ||	mxIsComplex(yf_) || (!mxIsScalar(yf_)))
    {
		mexErrMsgIdAndTxt("PlannerGeomSplineTrajLinVelMex:invalidArgument", 
            "Parameter yf must be a numeric scalar.");
	}
    // Check rf (final yaw angle [rad])
	if (!mxIsDouble(rf_) ||	mxIsComplex(rf_) || (!mxIsScalar(rf_)))
    {
		mexErrMsgIdAndTxt("PlannerGeomSplineTrajLinVelMex:invalidArgument", 
            "Parameter rf must be a numeric scalar.");
	}
    // Check vi (initial longitudinal velocity [m/s])
    if (!mxIsDouble(vi_) ||	mxIsComplex(vi_) || (!mxIsScalar(vi_)))
    {
		mexErrMsgIdAndTxt("PlannerGeomSplineTrajLinVelMex:invalidArgument", 
            "Parameter vi must be a numeric scalar.");
	}
    // Check vf (final longitudinal velocity [m/s])
	if (!mxIsDouble(vf_) ||	mxIsComplex(vf_) || (!mxIsScalar(vf_)))
    {
		mexErrMsgIdAndTxt("PlannerGeomSplineTrajLinVelMex:invalidArgument", 
            "Parameter vf must be a numeric scalar.");
	}
    // Check dri (initial yaw rate [rad/s])
	if (!mxIsDouble(dri_) ||	mxIsComplex(dri_) || (!mxIsScalar(dri_)))
    {
		mexErrMsgIdAndTxt("PlannerGeomSplineTrajLinVelMex:invalidArgument", 
            "Parameter dri must be a numeric scalar.");
	}
    // Check drf (final yaw rate [rad/s])
    if (!mxIsDouble(drf_) ||	mxIsComplex(drf_) || (!mxIsScalar(drf_)))
    {
		mexErrMsgIdAndTxt("PlannerGeomSplineTrajLinVelMex:invalidArgument", 
            "Parameter drf must be a numeric scalar.");
	}

    // Get input data pointers
    Float64_T xf = mxGetScalar(xf_);
    Float64_T yf = mxGetScalar(yf_);
    Float64_T rf = mxGetScalar(rf_);
    Float64_T vi = mxGetScalar(vi_);
    Float64_T vf = mxGetScalar(vf_);
    Float64_T dri = mxGetScalar(dri_);
    Float64_T drf = mxGetScalar(drf_);

    // Create output variables
    VectorXd t;
	VectorXd s;
	VectorXd x;
	VectorXd y;
	VectorXd r;
	VectorXd v;
	VectorXd dr;
	VectorXd acp;

    // Compute trajectory
	PlannerGeom p = PlannerGeom();
	tuple<Int32_T, Int32_T> rval = p.SplineTrajLinVelOpt(xf, yf, rf, vi, vf, dri, drf, t, s, x, y, r, v, dr, acp);
    mexPrintf("exitflag=%d nfeval=%d\n", get<0>(rval), get<1>(rval));

    // Allocate output
    t_ = mxCreateDoubleMatrix(t.size(), 1, mxREAL);
    s_ = mxCreateDoubleMatrix(s.size(), 1, mxREAL);
    x_ = mxCreateDoubleMatrix(x.size(), 1, mxREAL);
    y_ = mxCreateDoubleMatrix(y.size(), 1, mxREAL);
    r_ = mxCreateDoubleMatrix(r.size(), 1, mxREAL);
    v_ = mxCreateDoubleMatrix(v.size(), 1, mxREAL);
    dr_ = mxCreateDoubleMatrix(dr.size(), 1, mxREAL);
    acp_ = mxCreateDoubleMatrix(acp.size(), 1, mxREAL);

    // Get output data pointers
#if MX_HAS_INTERLEAVED_COMPLEX
    Float64_T *tptr = mxGetDoubles(t_);
	Float64_T *sptr = mxGetDoubles(s_);
	Float64_T *xptr = mxGetDoubles(x_);
	Float64_T *yptr = mxGetDoubles(y_);
	Float64_T *rptr = mxGetDoubles(r_);
	Float64_T *vptr = mxGetDoubles(v_);
	Float64_T *drptr = mxGetDoubles(dr_);
	Float64_T *acpptr = mxGetDoubles(acp_);
#else
    Float64_T *tptr = mxGetPr(t_);
	Float64_T *sptr = mxGetPr(s_);
	Float64_T *xptr = mxGetPr(x_);
	Float64_T *yptr = mxGetPr(y_);
	Float64_T *rptr = mxGetPr(r_);
	Float64_T *vptr = mxGetPr(v_);
	Float64_T *drptr = mxGetPr(dr_);
	Float64_T *acpptr = mxGetPr(acp_);
#endif

    // Copy data to MATLAB output
    memcpy(tptr, t.data(), t.size() * sizeof(Float64_T));
    memcpy(sptr, s.data(), s.size() * sizeof(Float64_T));
    memcpy(xptr, x.data(), x.size() * sizeof(Float64_T));
    memcpy(yptr, y.data(), y.size() * sizeof(Float64_T));
    memcpy(rptr, r.data(), r.size() * sizeof(Float64_T));
    memcpy(vptr, v.data(), v.size() * sizeof(Float64_T));
    memcpy(drptr, dr.data(), dr.size() * sizeof(Float64_T));
    memcpy(acpptr, acp.data(), acp.size() * sizeof(Float64_T));

}
