% For plot data run runTimeTestConOptPlanner

% Parameters
W = 7;
H = 4;
F = 8;
name = 'RuntimesConOptPlanner';

% Plot
fig1 = nicefigure(name, W, H);
ax1 = niceaxis(fig1, F);
hold(ax1, 'on');
plot(ax1, 1 : length(t_lanechange), t_lanechange, 'Color', nicecolors.PurpleAmethyst, 'MarkerSize', 3.0, 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.PurpleAmethyst, 'MarkerFaceColor', nicecolors.PurpleAmethyst);
plot(ax1, 1 : length(t_lanekeep), t_lanekeep, 'Color', nicecolors.OrangePumpkin,  'MarkerSize', 3.0, 'Marker', 'o', ...
    'MarkerEdgeColor', nicecolors.OrangePumpkin, 'MarkerFaceColor', nicecolors.OrangePumpkin);
xlabel(ax1, '$i$ (no. of trajectory) [1]');
ylabel(ax1, '$t_r$ (run-time) [s]');
l = nicelegend({'lane change', 'lane keep'}, ax1, F);

savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');