function initCon = InitCon_20mps_45deg()

initCon.PosXG   = 0;
initCon.VelXV   = 20;
initCon.PosYG   = 0;
initCon.VelYV   = 0;
initCon.YawZ    = 45 * pi / 180;
initCon.YawVelZ = 0;

end