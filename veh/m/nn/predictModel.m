function Y = predictModel(model, X)
for iLayer = 1 : length(model.Layers)
    switch class(model.Layers(iLayer))
        case 'nnet.cnn.layer.SequenceInputLayer'
            Y = X;
        case 'nnet.cnn.layer.FullyConnectedLayer'
            Y = model.Layers(iLayer).Weights * Y + model.Layers(iLayer).Bias;
        case 'nnet.cnn.layer.ReLULayer'
            Y = max(Y, 0);
    end
end
end