%% CtrlLatSty **************************************************************************************
% [Summary]
%   This class represents a state feedback based lateral (yaw rate tracking) control. 
%
% [Used in]
%   VehStCl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef CtrlLqsYr < CtrlLat    
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------                
        % Controller parameters 
        % State feedback gains for
        % lateral velocity
        GainVelY
        % yaw rate
        GainYawVelZ
        % integral of yaw rate tracking error
        GainYawVelZTrackErrInt
        
        % Gains for calculation of initial state value
        GainVelYInit
        GainYawVelZInit
        
    end
    
    %% Instance methods ============================================================================
    methods        
        %% Property getter and setter methods ------------------------------------------------------
        function set.GainVelY(self, value)
            if ~isequal(self.GainVelY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: GainVelY must be a numeric scalar!')
                end
                self.GainVelY = value(:);
            end
        end
        
        function set.GainYawVelZ(self, value)
            if ~isequal(self.GainYawVelZ, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: GainYawVelZ must be a numeric vector!')
                end
                self.GainYawVelZ = value(:);
            end
        end
        
        function set.GainYawVelZTrackErrInt(self, value)
            if ~isequal(self.GainYawVelZTrackErrInt, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: GainYawVelZTrackErrInt must be a numeric vector!')
                end
                self.GainYawVelZTrackErrInt = value(:);
            end
        end
        
        function set.GainVelYInit(self, value)
            if ~isequal(self.GainVelYInit, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: GainVelYInit must be a numeric vector!')
                end
                self.GainVelYInit = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------        
        function self = CtrlLqsYr(chassis, wheel, varargin)
            %% Assemble superclass arguments
            if ~isa(chassis, 'ChassisSt')
                error('ERROR: chassis must be a ChassisSt object!');
            end
            if ~isa(wheel, 'WheelMf')
                error('ERROR: wheel must be a WheelMf object!');
            end
            if nargin == 3
                superArgs = varargin(1);
            elseif nargin == 7
                superArgs = varargin(1:3);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            %% Call superclass constructor
            self@CtrlLat(superArgs{:});
            %% Set subclass properties
            if nargin == 3
                self.GainVelYInit    = varargin{1}.GainVelYInit;
                self.GainYawVelZInit = varargin{1}.GainYawVelZInit;
            elseif nargin == 7
                self.GainVelYInit    = varargin{4};
                self.GainYawVelZInit = varargin{5};
            end  
            
            % Control design
            %   Rajesh Rajamani: Vehicle Dynamics and Control, Chapter 2
                        
            % Parameters
            % mass [kg]
            m    = chassis.Mass;
            % moment of inertia about vertical axis [kg.m^2]
            J    = chassis.MomentOfIntertiaZ;
            % horizontal distance from front wheel center point to center of gravity [m]
            lf   = chassis.DistanceFrontCOG;
            % horizontal distance from rear wheel center point to center of gravity [m]
            lr   = chassis.DistanceRearCOG;
            % peak value for lateral adhesion coefficient [1]
            Dy   = 1;
            % shape factor lateral [1]
            Cy   = wheel.MfShapeY;
            % stiffness factor lateral [1]
            By   = wheel.MfStiffnessY;
            % curvature factor lateral [1]
            Ey   = wheel.MfCurvatureY;
            % gravitation [m/s^2]
            g = Com.Gravitation;
            
            % Linearize lateral slip model         
            % Calculate slip curve
            d_sy = 1e-3;
            sy = 0:d_sy:1e-1;
            gammay = Dy * sin(Cy * atan(By * sy - Ey * (By * sy - atan(By * sy))));
            d_gammay = diff(gammay) / d_sy;
            % Cornering stiffness in terms of adhesion coefficient distributed to the virtual front and rear
            % wheels => slope of slip curve @ 1% slip
            cf = d_gammay(sy == 1e-2);
            cr = cf;
            % Cornering stiffness in terms of lateral force distributed to the 4 wheels (Rajamani)
            Cf = cf * m * g * lr / (lf + lr) / 2;
            Cr = cr * m * g * lf / (lf + lr) / 2;
            

            %% LTI model with state space representation
            % Inputs
            % U(1) => SteerAng: steering angle [rad]
            % States
            % X(1) => VelYV:    lateral velocity in vehicle-fixed cs. [m/s]
            % X(2) => YawVelZ:  yaw (heading) angular velocity in ground-fixed system [rad/s]
            % Outputs
            % Y(1) => YawVelZ:  yaw (heading) angular velocity in ground-fixed system [rad/s]
            
            % System matrix
            a11 = - (g * (cf * lr + cr * lf) / (self.VelLin * (lf + lr)));
            a12 = - (self.VelLin + g * lf * lr * (cf - cr) / ( self.VelLin * (lf + lr)));
            a21 = - (m * g * lf * lr * (cf - cr) / (J * self.VelLin * (lf + lr)));
            a22 = - (m * g * lf * lr * (cf * lf + cr * lr) / (J * self.VelLin * (lf + lr)));
            A = [a11, a12;
                a21, a22];
            % As in Rajesh Rajamani: Vehicle dynamics and control Eq. 2.31 @ P. 30
            a11v = - (2 * Cf + 2 * Cr) / (m * self.VelLin);
            a12v = - (2 * Cf * lf - 2 * Cr * lr) / (m * self.VelLin) - self.VelLin;
            a21v = - (2 * Cf * lf - 2 * Cr * lr) / (J * self.VelLin);
            a22v = - (2 * Cf * lf^2 + 2 * Cr * lr^2) / (J * self.VelLin);
            Av = [a11v, a12v;
                a21v, a22v];
            
            % Input matrix
            % u = delta_f
            b11 =   g * cf * lr / (lf + lr);
            b21 =   m * g * cf * lf * lr / (J * (lf + lr));
            B = [b11;
                b21];
            % As in Rajesh Rajamani: Vehicle dynamics and control Eq. 2.31 @ P. 30
            b11v = 2 * Cf / m;
            b21v = 2 * Cf * lf / J;
            Bv = [b11v;
                  b21v];
            
            % Output matrix
            C = [0, 1];
            
            % Direct feedthrough matrix (no direct feedthrough)
            D =  0;
            
            %% LQ servo controller design for extended state space representation
            % Inputs
            % U(1) => SteerAng:    steering angle [rad]
            % States
            % X(1) => VelYV:       lateral velocity in vehicle-fixed cs. [m/s]
            % X(2) => YawVelZ:     yaw (heading) angular velocity in ground-fixed system [rad/s]
            % X(3) => TrackErrInt: integral of yaw rate tracking error [rad]
            % Outputs
            % Y(1) => YawVelZ:     yaw (heading) angular velocity in ground-fixed system [rad/s]
            
            % Complete state space representation with the integral of yaw rate reference
            % tracking error
            Ae = [ A, [0;0];
                  -C,    0];
            Be = [B;
                  0];
            Ce = [C, 0];
            % Check controllability
            if rank(ctrb(Ae, Be)) ~= length(Ae)
                error('System is not controllable.');
            end
            % Choose weights
            % lateral velocity, d_y
            q11 = 1 / 0.175^2;
            % yaw rate
            q22 = 1 / (10 / 180 * pi)^2;
            % yaw rate tracking error
            q33 = 1 / (0.1 / 180 * pi)^2;
            % steering angle
            r11 = 1 / (5 / 180 * pi)^2;
            % Weighting matrices
            Q0 = diag([q11, q22, q33]);
            R0 = r11;
            Q1 = diag([10 * q11, 10 * q22, q33]);
            R1 = q33;
            % Design controller
            K = lqr(Ae, Be, Q1, R1);
  
            
            %% Assign properties
            self.GainVelY               = K(1);
            self.GainYawVelZ            = K(2);
            self.GainYawVelZTrackErrInt = K(3);

        end
        
        %% Initialize -----------------------------------------------------------------------------
        % [Summary]
        %   This function calculates initial value of state vector.
        %       Let the output be Ki * U0r (self.GainVelXTrackErrInt * VelXV0) initially
        %           -Ke * X0 - K * U0r = Ki * U0r
        %            Ke * X0 = -(Ki + K) * U0r
        % [Input]
        %   U0r - initial input vector (= initial states of controlled system)
        %       U0r(1) => VelYV0:   initial lateral velocity in vehicle-fixed cs. [m/s]
        %       U0r(2) => YawVelZ0: initial yaw (heading) angular velocity in ground-fixed system [rad/s]
        %
        % [Output]
        %   X0 -  initial state vector
        %       X0(1) => YrErrInt0: initial integral of yaw rate tracking error [rad]
        % ------------------------------------------------------------------------------------------          
        function X0 = Initialize(self, U0r)
            X0 = - self.GainYawVelZTrackErrInt \ ((self.GainVelYInit + self.GainVelY) * U0r(1) + ...
                (self.GainYawVelZInit + self.GainYawVelZ) * U0r(2));
        end
        
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of the state feedback yaw-rate tracking
        %   controller. References:
        %       - Rajesh Rajamani: Vehicle Dynamics and Control, Chapter 2
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => YrErrInt:   integral of yaw rate tracking error [rad]
        %
        %   U - input vector
        %       U(1) => VelXV:      longitudinal velocity in vehicle-fixed cs. [m/s]
        %       U(2) => VelYV:      lateral velocity in vehicle-fixed cs. [m/s]
        %       U(3) => YawVelZ:    yaw (heading) angular velocity in ground-fixed system [rad/s]
        %       U(4) => YawVelZErr: tracking error of yaw angular velocity in ground-fixed system [rad/s]
        %
        % [Output]
        %   dX - state derivative vector
        %       dX(1) => YrErr:     yaw rate tracking error [rad/s]
        %
        %   Y  - output vector
        %       Y(1) => SteerAng:      steering angle output of controller [rad]
        % ------------------------------------------------------------------------------------------         
        function [dX, Y] = StateEquation(self, t, X, U) %#ok<INUSL> t used for debugging
            
            %% Calculate state feedback
            Y = -self.GainVelY * U(2) -self.GainYawVelZ * U(3) - self.GainYawVelZTrackErrInt * X(1);            
            
            %% Saturation with anti-windup (clamping)
            [dX, Y] = self.AntiWindup(U(1), Y, U(4));
            
        end
    end
end