%% VehEnvIn1W **************************************************************************************
% [Summary]
%   This class represents environmental inputs for single track vehicle models with two wheels.
%
% [Used in]
%   none
%
% [Subclasses]
%   VehStOlIn
%   VehStClIn
% **************************************************************************************************
classdef VehEnvIn2W < ComTimeInOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % friction coefficient between road and front tire [1]
        FrictionFront
        % friction coefficient between road and rear tire [1]
        FrictionRear

    end

    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.FrictionFront(self, value)
            if ~isequal(self.FrictionFront, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: FrictionFront must be a numeric vector!');
                end
                self.FrictionFront = value(:);
            end
        end

        function set.FrictionRear(self, value)
            if ~isequal(self.FrictionRear, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: FrictionRear must be a numeric vector!');
                end
                self.FrictionRear = value(:);
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehEnvIn2W(varargin)
            if nargin == 1
                comTimeInArgs = varargin(1);
            elseif nargin == 3
                comTimeInArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
           end
           self@ComTimeInOut(comTimeInArgs{:});
            if nargin == 1
                self.FrictionFront = varargin{1}.FrictionFront;
                self.FrictionRear  = varargin{1}.FrictionRear;
            elseif nargin == 3
                self.FrictionFront = varargin{2};
                self.FrictionRear  = varargin{3};
            end
        end

    end
end