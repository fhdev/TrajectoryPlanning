%% Parameters
T = 3;
%T = 10;

%% Data
if T == 10
Original.RMSE_FILT    = [6.023e-02  3.687e-02  2.213e-06  0.000e+00  8.245e-18  5.303e-18  0.000e+00  9.206e-19  1.377e-18  0.000e+00  9.630e-19  6.176e-19  5.353e-19  2.562e-02  9.192e-03  3.510e-03  1.682e-01  1.661e-03  2.622e-04  1.625e-01  1.650e-03  3.422e-04  4.348e-04];
Original.EMAX_FILT    = [9.420e-02  6.288e-02  4.927e-06  0.000e+00  1.388e-17  1.388e-17  0.000e+00  1.735e-18  1.735e-18  0.000e+00  1.735e-18  8.674e-19  9.216e-19  1.704e-01  4.105e-02  1.584e-02  3.567e+00  3.001e-02  1.319e-03  3.533e+00  3.008e-02  1.548e-03  2.414e-03];
else
Original.RMSE_FILT    = [2.037e-02  1.971e-02  8.725e-06  0.000e+00  1.273e-17  5.436e-18  0.000e+00  1.349e-18  2.542e-19  0.000e+00  1.520e-18  5.960e-19  1.678e-18  4.232e-02  1.780e-02  9.449e-03  2.998e-01  2.928e-03  7.827e-04  2.960e-01  2.898e-03  9.703e-04  1.711e-03];  
Original.EMAX_FILT    = [3.519e-02  3.522e-02  1.260e-05  0.000e+00  1.388e-17  1.388e-17  0.000e+00  1.735e-18  8.674e-19  0.000e+00  1.735e-18  8.674e-19  1.762e-18  2.193e-01  5.695e-02  3.436e-02  4.296e+00  3.884e-02  2.682e-03  4.247e+00  3.936e-02  3.106e-03  4.428e-03];  

end
Original.RUNTIME_FACTOR = [1, 3, 5, 10, 20;
    4.53548, 6.04396, 7.13756, 8.76033, 9.68333];

% MotionDeltasV00: n256l4v1_relu_00030 (n256l4v1_relu_00050, n256l4v2_relu_00100)
if T == 10
MotionDeltasV00.RMSE_FILT    = [9.599e-01  9.183e-01  3.441e-02  2.579e-02  1.060e-01  2.513e-02  2.288e-01  5.286e-04  3.156e-03  2.510e-01  2.654e-04  2.945e-03  2.749e-05  4.666e-02  4.146e-01  1.362e-02  4.363e-01  2.400e-03  2.261e-03  4.275e-01  2.340e-03  2.389e-03  1.406e-03];  
MotionDeltasV00.EMAX_FILT    = [2.357e+00  2.465e+00  8.891e-02  6.086e-02  1.645e-01  4.216e-02  3.808e-01  8.697e-04  5.946e-03  4.096e-01  4.764e-04  5.369e-03  9.164e-05  2.050e-01  7.499e-01  5.320e-02  6.523e+00  3.178e-02  6.405e-03  6.250e+00  3.033e-02  7.617e-03  4.103e-03];
else
MotionDeltasV00.RMSE_FILT    = [8.070e-02  7.939e-02  1.029e-02  8.415e-03  8.025e-02  1.086e-02  1.053e-01  4.730e-04  2.383e-03  8.511e-02  2.785e-04  2.052e-03  7.376e-05  8.397e-02  2.636e-01  1.834e-02  6.957e-01  3.165e-03  2.045e-03  6.817e-01  3.017e-03  2.343e-03  1.895e-03];
MotionDeltasV00.EMAX_FILT    = [2.171e-01  2.211e-01  1.915e-02  1.500e-02  1.287e-01  2.136e-02  1.567e-01  9.317e-04  3.803e-03  1.296e-01  5.727e-04  3.636e-03  1.837e-04  2.140e-01  4.719e-01  4.759e-02  6.452e+00  3.422e-02  6.405e-03  6.396e+00  3.395e-02  6.417e-03  4.936e-03];
end
MotionDeltasV00.RUNTIME_FACTOR = [1, 3, 5, 10, 20;
    11.7304045980038, 11.656245129539, 11.77652825495, 11.5647604590288, 11.5375243127142];

% MotionDeltasV00W: (n256l3v1_relu_00010, n256l3v1_relu_00100)

% MotionDeltasV01: n256l4v1_relu_00050
if T == 10
MotionDeltasV01.RMSE_FILT    = [1.072e+00  7.934e-01  6.808e-02  4.071e-02  8.880e-02  3.219e-02  2.123e-01  9.614e-04  4.373e-03  1.908e-01  4.490e-04  3.644e-03  8.213e-05  3.733e-02  4.048e-01  1.661e-02  4.531e-01  6.125e-03  2.628e-03  4.464e-01  6.208e-03  3.312e-03  1.446e-03];
MotionDeltasV01.EMAX_FILT    = [2.706e+00  2.361e+00  1.411e-01  5.972e-02  1.605e-01  5.320e-02  3.904e-01  1.455e-03  9.290e-03  3.351e-01  7.248e-04  7.656e-03  1.699e-04  2.185e-01  9.136e-01  6.229e-02  7.038e+00  3.140e-02  5.924e-03  6.744e+00  3.025e-02  7.819e-03  5.382e-03];
else
MotionDeltasV01.RMSE_FILT    = [9.968e-02  1.290e-01  1.478e-02  1.558e-02  8.138e-02  2.041e-02  1.424e-01  4.482e-04  2.468e-03  1.414e-01  2.568e-04  3.129e-03  8.329e-05  8.662e-02  3.497e-01  2.880e-02  6.649e-01  8.361e-03  3.082e-03  6.566e-01  8.528e-03  3.767e-03  2.045e-03]; 
MotionDeltasV01.EMAX_FILT    = [2.637e-01  3.324e-01  3.361e-02  3.201e-02  1.231e-01  3.443e-02  2.351e-01  7.817e-04  3.785e-03  2.277e-01  4.461e-04  4.961e-03  2.243e-04  2.251e-01  6.026e-01  8.076e-02  6.425e+00  3.260e-02  6.698e-03  6.323e+00  3.248e-02  7.008e-03  5.363e-03];  
end
MotionDeltasV01.RUNTIME_FACTOR = [1, 3, 5, 10, 20;
    11.1434095097272, 11.4280482183176, 11.5321236108437, 11.6878732981939, 11.6946720032898];

% MotionDeltasV02: n256l4v1_relu_00030
if T == 10
MotionDeltasV02.RMSE_FILT    = [4.961e-01  8.197e-01  3.215e-02  1.905e-02  5.476e-02  1.796e-02  1.751e-01  4.885e-04  2.097e-03  1.807e-01  2.378e-04  1.994e-03  3.618e-05  3.593e-02  3.387e-01  1.873e-02  4.599e-01  2.505e-03  1.925e-03  4.468e-01  2.294e-03  2.251e-03  1.486e-03];
MotionDeltasV02.EMAX_FILT    = [1.506e+00  1.844e+00  6.053e-02  4.433e-02  1.100e-01  3.450e-02  3.232e-01  7.931e-04  4.483e-03  3.473e-01  3.988e-04  4.637e-03  8.964e-05  2.104e-01  6.566e-01  6.744e-02  7.111e+00  3.296e-02  5.670e-03  6.825e+00  3.124e-02  6.512e-03  4.666e-03];
else
MotionDeltasV02.RMSE_FILT    = [8.763e-02  6.982e-02  1.806e-02  9.774e-03  6.932e-02  2.379e-02  1.174e-01  4.449e-04  2.571e-03  1.130e-01  2.203e-04  2.458e-03  9.831e-05  8.855e-02  2.580e-01  2.974e-02  7.369e-01  3.171e-03  3.016e-03  7.172e-01  3.106e-03  3.205e-03  1.954e-03];
MotionDeltasV02.EMAX_FILT    = [2.372e-01  1.943e-01  3.908e-02  1.737e-02  1.110e-01  3.138e-02  1.787e-01  7.874e-04  3.500e-03  1.872e-01  3.886e-04  3.441e-03  1.505e-04  2.425e-01  3.699e-01  6.978e-02  6.654e+00  3.584e-02  6.903e-03  6.546e+00  3.629e-02  7.602e-03  4.677e-03];

end
MotionDeltasV02.RUNTIME_FACTOR = [1, 3, 5, 10, 20;
    11.5186374728793, 11.4879198158624, 11.3833729833775, 11.3292638415306, 11.6125900243534];

% MotionDeltasV00W: (n256l4v2_relu_00050, n256l4v2_relu_00100)

% MotionDeltasV03: n256l3v1_relu_00100
if T == 10
MotionDeltasV03.RMSE_FILT    = [1.029e+00  9.856e-01  4.735e-02  4.159e-02  8.075e-02  3.374e-02  2.828e-01  1.352e-03  4.474e-03  2.578e-01  5.677e-04  3.640e-03  3.088e-05  3.667e-02  4.950e-01  2.280e-02  4.801e-01  2.493e-03  3.101e-03  4.714e-01  2.299e-03  3.177e-03  1.424e-03];
MotionDeltasV03.EMAX_FILT    = [2.115e+00  2.141e+00  1.077e-01  9.452e-02  1.369e-01  7.823e-02  5.575e-01  2.407e-03  1.112e-02  4.362e-01  1.009e-03  9.478e-03  1.442e-04  2.428e-01  1.252e+00  8.337e-02  7.439e+00  3.026e-02  7.076e-03  7.225e+00  2.848e-02  1.555e-02  4.295e-03];
else
MotionDeltasV03.RMSE_FILT    = [4.014e-01  5.197e-01  1.592e-02  2.072e-02  8.686e-02  1.777e-02  1.194e-01  8.044e-04  2.711e-03  1.304e-01  3.179e-04  3.311e-03  6.106e-05  1.024e-01  4.184e-01  2.646e-02  7.977e-01  3.194e-03  3.237e-03  7.762e-01  3.123e-03  3.911e-03  1.860e-03];
MotionDeltasV03.EMAX_FILT    = [6.944e-01  9.394e-01  3.093e-02  2.919e-02  1.362e-01  2.784e-02  2.026e-01  1.167e-03  4.113e-03  2.240e-01  4.625e-04  5.258e-03  1.442e-04  2.146e-01  7.243e-01  6.325e-02  6.528e+00  2.980e-02  7.253e-03  6.471e+00  2.999e-02  8.069e-03  5.073e-03];
end
MotionDeltasV03.RUNTIME_FACTOR = [1, 3, 5, 10, 20;
    11.4540570561558, 11.5919422018227, 11.5302157276493, 11.395476580833, 11.4560855521048];

%% Errors plot mae
% Plot
H = 7; % cm
W = 14.5; % cm
F = 8; % pt

fig1 = nicefigure('types', W, H);
ax11 = niceaxis(fig1, F, true);
b11 = bar(ax11, [MotionDeltasV00.EMAX_FILT; MotionDeltasV01.EMAX_FILT; MotionDeltasV02.EMAX_FILT; MotionDeltasV03.EMAX_FILT; Original.EMAX_FILT]');
b11(1).EdgeColor = nicecolors.BlueDeepKoamaru;
b11(1).FaceColor = nicecolors.BlueDeepKoamaru;
b11(2).EdgeColor = nicecolors.RedAlizarin;
b11(2).FaceColor = nicecolors.RedAlizarin;
b11(3).EdgeColor = nicecolors.GreenSea;
b11(3).FaceColor = nicecolors.GreenSea;
b11(4).EdgeColor = nicecolors.OrangeCarrot;
b11(4).FaceColor = nicecolors.OrangeCarrot;
b11(5).EdgeColor = nicecolors.PurpleAmethyst;
b11(5).FaceColor = nicecolors.PurpleAmethyst;
l11 = nicelegend({'V0', 'V1', 'V2', 'V3', 'ODE'}, ax11, F);
xlabel(ax11, 'output variable');
ylabel(ax11, 'max. abs. error');
ax11.XTick = 1:23;
ax11.XTickLabel = {'$x^G_v$ [m]', '$y^G_v$ [m]', '$\psi_v$ [rad]', '$\dot{x}^V_v$ [m/s]', '$\dot{y}^V_v$ [m/s]', ...
    '$\dot{\psi}_v$ [rad/s]', '$\dot{\rho}_f$ [rad/s]', '$s_{f,x}$ [1]', '$s_{f,y}$ [1]', ...
    '$\dot{\rho}_r$ [rad/s]', '$s_{r,x}$ [1]', '$s_{r,y}$ [1]', '$\delta_f$ [rad]', ...
    '$\ddot{x}^V_{v,I}$ [m/s\textsuperscript{2}]', '$\ddot{y}^V_{v,I}$ [m/s\textsuperscript{2}]', ...
    '$\ddot{\psi}_v$ [rad/s\textsuperscript{2}]', '$\ddot{\rho}_f$ [rad/s\textsuperscript{2}]', ...
    '$\dot{s}_{f,x}$ [1/s]', '$\dot{s}_{f,y}$ [1/s]', '$\ddot{\rho}_r$ [rad/s\textsuperscript{2}]', ...
    '$\dot{s}_{r,x}$ [1/s]', '$\dot{s}_{r,y}$ [1/s]', '$\dot{\delta}_f$ [rad/s]'};
xtickangle(ax11, 90);
ax11.YScale = 'log';
ax11.YLim = [1e-5, 1e1];
ax11.YTick = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e1];
ax11.YTickLabel = {'10\textsuperscript{-5}', '10\textsuperscript{-4}', '10\textsuperscript{-3}', '10\textsuperscript{-2}', ...
    '10\textsuperscript{-1}', '10\textsuperscript{0}', '10\textsuperscript{1}' };
ax11.Position(1) = 0.07;
ax11.Position(3) = 0.92;

fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

% Save plot
name = sprintf('OutputErrorsMax_%d.pdf', T);
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

%% Errors plot rmse
% Plot
H = 7; % cm
W = 14.5; % cm
F = 8; % pt

fig1 = nicefigure('types', W, H);
ax11 = niceaxis(fig1, F, true);
b11 = bar(ax11, [MotionDeltasV00.RMSE_FILT; MotionDeltasV01.RMSE_FILT; MotionDeltasV02.RMSE_FILT; MotionDeltasV03.RMSE_FILT; Original.RMSE_FILT]');
b11(1).EdgeColor = nicecolors.BlueDeepKoamaru;
b11(1).FaceColor = nicecolors.BlueDeepKoamaru;
b11(2).EdgeColor = nicecolors.RedAlizarin;
b11(2).FaceColor = nicecolors.RedAlizarin;
b11(3).EdgeColor = nicecolors.GreenSea;
b11(3).FaceColor = nicecolors.GreenSea;
b11(4).EdgeColor = nicecolors.OrangeCarrot;
b11(4).FaceColor = nicecolors.OrangeCarrot;
b11(5).EdgeColor = nicecolors.PurpleAmethyst;
b11(5).FaceColor = nicecolors.PurpleAmethyst;
l11 = nicelegend({'V0', 'V1', 'V2', 'V3', 'ODE'}, ax11, F);
xlabel(ax11, 'output variable');
ylabel(ax11, 'root mean squared error');
ax11.XTick = 1:23;
ax11.XTickLabel = {'$x^G_v$ [m]', '$y^G_v$ [m]', '$\psi_v$ [rad]', '$\dot{x}^V_v$ [m/s]', '$\dot{y}^V_v$ [m/s]', ...
    '$\dot{\psi}_v$ [rad/s]', '$\dot{\rho}_f$ [rad/s]', '$s_{f,x}$ [1]', '$s_{f,y}$ [1]', ...
    '$\dot{\rho}_r$ [rad/s]', '$s_{r,x}$ [1]', '$s_{r,y}$ [1]', '$\delta_f$ [rad]', ...
    '$\ddot{x}^V_{v,I}$ [m/s\textsuperscript{2}]', '$\ddot{y}^V_{v,I}$ [m/s\textsuperscript{2}]', ...
    '$\ddot{\psi}_v$ [rad/s\textsuperscript{2}]', '$\ddot{\rho}_f$ [rad/s\textsuperscript{2}]', ...
    '$\dot{s}_{f,x}$ [1/s]', '$\dot{s}_{f,y}$ [1/s]', '$\ddot{\rho}_r$ [rad/s\textsuperscript{2}]', ...
    '$\dot{s}_{r,x}$ [1/s]', '$\dot{s}_{r,y}$ [1/s]', '$\dot{\delta}_f$ [rad/s]'};
xtickangle(ax11, 90);
ax11.YScale = 'log';
ax11.YLim = [1e-5, 1e1];
ax11.YTick = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0, 1e1];
ax11.YTickLabel = {'10\textsuperscript{-5}', '10\textsuperscript{-4}', '10\textsuperscript{-3}', '10\textsuperscript{-2}', ...
    '10\textsuperscript{-1}', '10\textsuperscript{0}', '10\textsuperscript{1}' };
ax11.Position(1) = 0.07;
ax11.Position(3) = 0.92;

fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

% Save plot
name = sprintf('OutputErrorsRmse_%d.pdf', T);
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

%% Speed plot
H = 4; % cm
W = 8; % cm
F = 8; % pt

fig2 = nicefigure('types', W, H);
ax21 = niceaxis(fig2, F, true);
plot(ax21, MotionDeltasV00.RUNTIME_FACTOR(1,:), MotionDeltasV00.RUNTIME_FACTOR(2,:), ...
    'Color', nicecolors.BlueDeepKoamaru, 'LineStyle', '-', 'Marker', 'x');
plot(ax21, MotionDeltasV01.RUNTIME_FACTOR(1,:), MotionDeltasV01.RUNTIME_FACTOR(2,:), ...
    'Color', nicecolors.RedAlizarin, 'LineStyle', '-', 'Marker', 's');
plot(ax21, MotionDeltasV02.RUNTIME_FACTOR(1,:), MotionDeltasV02.RUNTIME_FACTOR(2,:), ...
    'Color', nicecolors.GreenSea, 'LineStyle', '-', 'Marker', 'd');
plot(ax21, MotionDeltasV03.RUNTIME_FACTOR(1,:), MotionDeltasV03.RUNTIME_FACTOR(2,:), ...
    'Color', nicecolors.OrangeCarrot, 'LineStyle', '-', 'Marker', '*');
plot(ax21, Original.RUNTIME_FACTOR(1,:), Original.RUNTIME_FACTOR(2,:), ...
    'Color', nicecolors.PurpleAmethyst, 'LineStyle', '-', 'Marker', 'o');
l12 = nicelegend({'V0', 'V1', 'V2', 'V3', 'ODE'}, ax21, F);
l12.Location = 'NorthEastOutside';
ylim(ax21, [0, 15]);
xlabel('$t_v$ (sim. time) [s]');
ylabel('$\tau_r$ (run time) [x real-time]');

% Save plot
name = 'RunTimesNnDyn';
savefig(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');