classdef Colors
    properties (Constant)
        % Green colors
        Turquoise    = [ 26, 188, 156] / 255;
        Emerald      = [ 46, 204, 113] / 255;
        GreenSea     = [ 22, 160, 133] / 255;
        Nephritis    = [ 39, 174,  96] / 255;
        % Blue colors
        PeterRiver   = [ 52, 152, 219] / 255;
        BelizeHole   = [ 41, 128, 185] / 255;
        % Purple colors
        Amethyst     = [155,  89, 182] / 255;
        Wisteria     = [142,  68, 173] / 255;
        % Yellow colors
        SunFlower    = [241, 196,  15] / 255;
        % Orange colors
        Carrot       = [230, 126,  34] / 255;
        Pumpkin      = [211,  84,   0] / 255;
        % Red colors
        Alizarin     = [231,  76,  60] / 255;
        Pomegrante   = [192,  57,  43] / 255;
    end
end