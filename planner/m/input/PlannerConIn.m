%% PlannerConIn ************************************************************************************
% [Summary]
%   This class represents inputs for constrained nonlinear solver based motion planner PlannerCon.
%
% [Used in]
%   PlannerCon
%
% [Subclasses]
%   PlannerOptIn
% **************************************************************************************************
classdef PlannerConIn < handle
    
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Initial conditions: the state of the vehicle at the begining of planning
        InitCon         % ChassisStBndCon object
        % Final conditions: the required state of the vehicle to reach
        FinalCon        % ChassisStOut object
        % Initial values of optimization variable
        OptVarInit      % PlannerOptVarVelYawRate or PlannerOptVarVelPath or PlannerOptVarVelPathFree object
        % Environmental input such as friction on the road
        EnvIn           % VehEnvIn2W object
    end
    
    %% Static methods ==============================================================================    
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------        
        function value = setOptVarInit(value)
            if isstruct(value)
                value = PlannerOptVarVelYawRate(value);
            end
            if ~isa(value, 'PlannerOptVarVelYawRate')
                error('ERROR: OptVarInit must be a PlannerOptVarVelYawRate object!');
            end
        end
        
    end
    
    %% Instance methods ============================================================================    
    methods
        %% Property getter and setter methods ------------------------------------------------------
        
        function set.InitCon(self, value)
            if ~isequal(self.InitCon, value)
                if isstruct(value)
                    value = ChassisStBndCon(value);
                end
                if ~isa(value, 'ChassisStBndCon')
                    error('ERROR: InitCon must be a ChassisStBndCon object!')
                end
                self.InitCon = value;
            end
        end
        
        function set.FinalCon(self, value)
            if ~isequal(self.FinalCon, value)
                if isstruct(value)
                    value = ChassisStOut(value);
                end
                if ~isa(value, 'ChassisStOut')
                    error('ERROR: FinalCon must be a ChassisStOut object!')
                end
                self.FinalCon = value;
            end
        end
        
        function set.OptVarInit(self, value)
            if ~isequal(self.OptVarInit, value)
                self.OptVarInit = self.setOptVarInit(value);
            end
        end
        
        function set.EnvIn(self, value)
            if ~isequal(self.EnvIn, value)
                if isstruct(value)
                    value = VehEnvIn2W(value);
                end
                if ~isa(value, 'VehEnvIn2W')
                    error('ERROR: EnvIn must be a VehEnvIn2W object!');
                end
                self.EnvIn = value;
            end
        end
        
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerConIn(varargin)
            if nargin == 1
                self.InitCon    = varargin{1}.InitCon;
                self.FinalCon   = varargin{1}.FinalCon;
                self.OptVarInit = varargin{1}.OptVarInit;
                self.EnvIn      = varargin{1}.EnvIn;
            elseif nargin == 4
                self.InitCon    = varargin{1};
                self.FinalCon   = varargin{2};
                self.OptVarInit = varargin{3};
                self.EnvIn      = varargin{4};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end
        
    end
end
