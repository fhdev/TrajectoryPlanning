%% References
% Controller for yaw rate tracking:
%   Rajesh Rajamani: Vehicle Dynamics and Control, Chapter 2
% Controller with respect to lateral and heading angle offset from lane centerline:
%   Rajesh Rajamani: Vehicle Dynamics and Control, Chapter 3
%   Jarrod M. Snider: Automatic Steering Methods for Autonomous Automobile Path Tracking,
%                     Chapter 4
%% Constants
DO_PLOTS = false;

%% Vehicle parameters
% Gravitational acceleration [m/s^2]
g    = 9.8065;
% Basic parameters
% mass [kg]
m    = 1463;
% moment of inertia about vertical axis [kg.m^2]
J    = 2152;
% horizontal distance from front wheel center point to center of gravity [m]
lf   = 1.12;
% horizontal distance from rear wheel center point to center of gravity [m]
lr   = 1.41;
% Tire model parameters
% stiffness factor lateral [1]
By   = 13;
% shape factor lateral [1]
Cy   = 1.85;
% curvature factor lateral [1]
Ey   = 0.5;
% peak value for lateral adhesion coefficient [1]
Dy   = 1;
% velocity of linearization [m/s]
vlin = 25;

%% Controller parameters
% maximal driving torque [Nm]
dmax = +30 / 180 * pi;
% maximal braking torque [Nm]
dmin = -30 / 180 * pi;
% initial state gain 
Ksi = [0, 0.1212];

%% Input data
% initial yaw rate [rad/s]
psi0 = 0;
% final yaw rate [rad/s]
psi1 = 0.01;

%% Linearize lateral slip model
% Calculate slip curve
d_sy = 1e-3;
sy = 0:d_sy:1e-1;
gammay = Dy * sin(Cy * atan(By * sy - Ey * (By * sy - atan(By * sy))));
d_gammay = diff(gammay) / d_sy;
% Cornering stiffness in terms of adhesion coefficient distributed to the virtual front and rear
% wheels => slope of slip curve @ 1% slip
cf = d_gammay(sy == 1e-2);
cr = cf;
% Cornering stiffness in terms of lateral force distributed to the 4 wheels
Cf = cf * m * g * lr / (lf + lr) / 2;
Cr = cr * m * g * lf / (lf + lr) / 2;

%% LTI model with state space representation
% Inputs
% U(1) => SteerAng: steering angle [rad]
% States
% X(1) => VelYV:    lateral velocity in vehicle-fixed cs. [m/s]
% X(2) => YawVelZ:  yaw (heading) angular velocity in ground-fixed system [rad/s]
% Outputs
% Y(1) => YawVelZ:  yaw (heading) angular velocity in ground-fixed system [rad/s]

% System matrix
a11 = - (g * (cf * lr + cr * lf) / (vlin * (lf + lr)));
a12 = - (vlin + g * lf * lr * (cf - cr) / ( vlin * (lf + lr)));
a21 = - (m * g * lf * lr * (cf - cr) / (J * vlin * (lf + lr)));
a22 = - (m * g * lf * lr * (cf * lf + cr * lr) / (J * vlin * (lf + lr)));
A = [a11, a12;
    a21, a22];
% As in Rajesh Rajamani: Vehicle dynamics and control Eq. 2.31 @ P. 30
a11v = - (2 * Cf + 2 * Cr) / (m * vlin);
a12v = - (2 * Cf * lf - 2 * Cr * lr) / (m * vlin) - vlin;
a21v = - (2 * Cf * lf - 2 * Cr * lr) / (J * vlin);
a22v = - (2 * Cf * lf^2 + 2 * Cr * lr^2) / (J * vlin);
Av = [a11v, a12v;
    a21v, a22v];

% Input matrix
% u = delta_f
b11 =   g * cf * lr / (lf + lr);
b21 =   m * g * cf * lf * lr / (J * (lf + lr));
B = [b11;
    b21];
% As in Rajesh Rajamani: Vehicle dynamics and control Eq. 2.31 @ P. 30
b11v = 2 * Cf / m;
b21v = 2 * Cf * lf / J;
b21v = (cf * m * g * lr) * lf / (J * (lf + lr));
Bv = [b11v;
    b21v];

% Output matrix
C = [0, 1];

% Direct feedthrough matrix (no direct feedthrough)
D =  0;

%% LQ servo controller design for extended state space representation
% Inputs
% U(1) => SteerAng:    steering angle [rad]
% States
% X(1) => VelYV:       lateral velocity in vehicle-fixed cs. [m/s]
% X(2) => YawVelZ:     yaw (heading) angular velocity in ground-fixed system [rad/s]
% X(3) => TrackErrInt: integral of yaw rate tracking error [rad]
% Outputs
% Y(1) => YawVelZ:     yaw (heading) angular velocity in ground-fixed system [rad/s]

% Complete state space representation with the integral of yaw rate reference
% tracking error
Ae = [ A, [0;0];
      -C,    0];
Be = [B;
      0];
Ce = [C, 0];
% Check controllability
if rank(ctrb(Ae, Be)) ~= length(Ae)
    error('System is not controllable.');
end
% Choose weights
% lateral velocity, d_y
q11 = 1 / 0.175^2;
% yaw rate
q22 = 1 / (10 / 180 * pi)^2;
% yaw rate tracking error
q33 = 1 / (0.1 / 180 * pi)^2;
% steering angle
r11 = 1 / (5 / 180 * pi)^2;
% Weighting matrices
Q0 = diag([q11, q22, q33]);
R0 = r11;
Q1 = diag([10 * q11, 10 * q22, q33]);
R1 = q33;
% Design controller
K = lqr(Ae, Be, Q1, R1);
fprintf('LQR controller for lateral dymanics (yaw rate tracking)\n')
fprintf(['K = ', mat2str(K), ' (for negative feedback)\n']);
% Closed loop system
Scl = ss(Ae-Be*K, [0; 0; 1], Ce, 0);
fprintf('Poles of closed loop system:\n');
fprintf(['P = ', mat2str(pole(Scl)), '\n']);
% Evaluate initial value of tracking error integrator so that initial output is Ki * Xsys0
% Initial yaw rate [m/s]
Xsys0 = [0; psi0];
% Initial value for tracking error integrator 
Xerr0 = K(end) \ -((K(1:end-1) + Ksi) * Xsys0);
fprintf('Initial value for tracking error integrator:\n');
fprintf('Xerr0 = %g\n', Xerr0);
% Check result
U0 = -K * [Xsys0; Xerr0];

%% LTI model with state space representation with respect to tracking errors
% Inputs
% U(1) => SteerAng:   steering angle [rad]
% U(2) => YawVelZRef: desired yaw rate along path [rad/s]
% States
% X(1) => ErrLatDev:  position offset from lane centerline (perpendicular to vehicle x-axis)
% X(2) => DErrLatDev: derivative of position offset from lane centerline
% X(3) => ErrYawDev:  yaw (heading) angle offset from lane centerline
% X(4) => DErrYawDev: derivative of yaw (heading) angle offset from lane centerline
% Outputs
% Y(1) => ErrSum:     sum of errors

% System matrix
a22 = -((2 * Cf + 2 * Cr) / (m * vlin));
a23 = (2 * Cf + 2 * Cr) / m;
a24 =  -((2 * Cf * lf + 2 * Cr * lr) / (m * vlin));
a42 = -((2 * Cf * lf - 2 * Cr * lr) / (J * vlin));
a43 = (2 * Cf * lf - 2 * Cr * lr) / J;
a44 = -((2 * Cf * lf^2 + 2 * Cr * lr^2) / (J * vlin));
A2 = [ 0,   1,   0,   0;
      0, a22, a23, a24;
      0,   0,   0,   1;
      0, a42, a43, a44];

% Input matrix
b21 = 2 * Cf / m;
b22 = -(((2 * Cf * lf - 2 * Cr * lr) / (m * vlin)) - vlin);
b41 = 2 * Cf * lf / J;
b42 =  -((2 * Cf * lf^2 + 2 * Cr * lr^2) / (J * vlin));
B2 = [  0,   0;
     b21, b22;
       0,   0;
     b41, b42];

% Output matrix
C2 = [1, 0, 1, 0];

% Feedthrough matrix
D2 = 0;

%% LQ controller design for above state space representation
% Check controllability
if rank(ctrb(A2, B2(:,1))) ~= length(A2)
    error('System is not controllable.');
end
% Weighting matrices
Q0 = diag([1, 1, 100, 100]);
R0 = 10;
% Design controller
K2 = lqr(A2, B2(:,1), Q0, R0);
fprintf('LQR controller for lateral dymanics (path tracking)\n')
fprintf(['K = ', mat2str(K2), ' (for negative feedback)\n']);
% Closed loop system
Scl2 = ss(A2-B2(:,1)*K2, B2(:,2), C2, D2);
fprintf('Poles of closed loop system:\n');
fprintf(['P = ', mat2str(pole(Scl2)), '\n']);

%% Feedforward term for controller
% Understeer gradient (CtrlLatTrk.UnderSteerGrad)
KV = lr * m / (2 * Cf * (lf + lr)) - lf * m / (2 * Cr * (lf + lr));
% Constant to calculate e2_ss (CtrlLatTrk.YawDevConst)
Ke2 = lf * m / (2 * Cr * (lf + lr));
%% Display plots
if DO_PLOTS
    fontSize = 8;
    % Cornering stiffness
    figure('Name', 'Linearization for lateral dynamics', 'Color', [1, 1, 1], ...
        'Units', 'Centimeters', 'Position', [2, 2, 7, 6]);
    hold on;
    plot(sy, gammay);
    plot(sy, cf * sy);
    grid on;
    box on;
    set(gca, 'DefaultTextInterpreter', 'Latex', 'TickLabelInterpreter', 'Latex', ...
        'FontSize', fontSize);
    title('Lateral addhesion coefficient');
    legend({'Magic Formula', 'Linearized'}, 'Interpreter', 'Latex');
    xlabel('$s_y$ [1]');
    ylabel('$\gamma_y$ [1]');
end