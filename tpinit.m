function tpinit(opt)
rootDir = fileparts(mfilename('fullpath'));
%% Check and prepare arguments
if nargin < 1
    if ~contains(path, rootDir)
        opt = 'add';
    else
        opt = 'remove';
    end
end
if ~any(strcmpi(opt, {'add'; 'remove'; 'clear'; 'reset'}))
    error('ERROR: Option should be ''add'', ''remove'', ''clear'', or ''reset''!');
end
%% Add or remove trajectory planning to path
% Select build directory
buildDir = fullfile(rootDir, 'build');
switch lower(computer)
    case 'pcwin64'
        buildDirPlatform = fullfile(buildDir, 'windows');
    case 'glnxa64'
        buildDirPlatform = fullfile(buildDir, 'linux');
    otherwise
        error('ERROR: Only implemented for Windows and Linux!');
end
buildDirPlatformConfig = fullfile(buildDirPlatform, 'Release');
% MATLAB and built modules
modules = {'chassis', 'com', 'ctrl', 'demo', 'obst', 'planner', 'pub', 'steer', 'util', 'veh', 'wheel'};
buildModules = {'veh', 'planner'};
switch opt
    case 'add'
        addpath(buildDir);
        addpath(buildDirPlatform);
        addpath(buildDirPlatformConfig);
        for i = 1 : length(buildModules)
            switch lower(computer)
                case 'pcwin64'
                    addpath(fullfile(buildDirPlatformConfig, buildModules{i}, 'Release'));
                case 'glnxa64'
                    addpath(fullfile(buildDirPlatformConfig, buildModules{i}));
            end
        end
        for i = 1 : length(modules)
            addpath(fullfile(rootDir, modules{i}));
            addpath(genpath(fullfile(rootDir, modules{i}, 'm')));
        end
    case 'remove'
        rmpath(buildDir);
        rmpath(buildDirPlatform);
        rmpath(buildDirPlatformConfig);
        for i = 1 : length(buildModules)
            switch lower(computer)
                case 'pcwin64'
                    rmpath(fullfile(buildDirPlatformConfig, buildModules{i}, 'Release'));
                case 'glnxa64'
                    rmpath(fullfile(buildDirPlatformConfig, buildModules{i}));
            end
        end
        for i = 1 : length(modules)
            rmpath(fullfile(rootDir, modules{i}));
            rmpath(genpath(fullfile(rootDir, modules{i}, 'm')));
        end
    case 'clear'
        evalin('base', 'clearvars');
    case 'reset'
        if ~isempty(license('inuse', 'simulink'))
            bdclose('all');
        end
        diary('off');
        close('all', 'force');
        fclose('all');
        clear('all'); %#ok<CLALL>
        delete(timerfindall());
        clc;
end
end