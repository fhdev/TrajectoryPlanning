%% VehQrOut ****************************************************************************************
% [Summary]
%   This class represents output for quarter vehicle model. 
%
% [Used in]
%   VehQr
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehQrOut < VehQrLonOut
    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------
        function value = setChassis(value)
            if isstruct(value)
                value = ChassisQrOut(value);
            end
            if ~isa(value, 'ChassisQrOut')
                error('ERROR: Chassis must be a ChassisQrOut object!')
            end
        end

        function value = setWheel(value)
            if isstruct(value)
                value = WheelMfLon(value);
            end
            if ~isa(value, 'WheelMfOut')
                error('ERROR: Wheel must be a struct or a WheelMfOut object!')
            end
        end

    end
    
    %% Instance methods ============================================================================
    methods
        %% Constructor -----------------------------------------------------------------------------
        function self = VehQrOut(varargin)
            self@VehQrLonOut(varargin{:});
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the quarter vehicle model.
        %
        % [Input]
        %       name   - name for the plots
        % [Output]
        %       figWheel    - figure handle of plot containing wheel related quantities
        %       figGlob     - figure handle of plot containing chassis related quantities in global
        %       figLoc      - figure handle of plot containing chassis related quantities in local
        %       figPath     - figure handle of vehicle path plot
        % ------------------------------------------------------------------------------------------
        function [figWheel, figGlob, figLoc, figPath] = DisplayPlots(self, name)
            %% Constants

            %% Default arguments
            if nargin < 2
                name = class(self);
            end

            %% Create plots
            figWheel = self.Wheel.DisplayPlots([name, '/Wheel']);
            [figGlob, figLoc, figPath] = self.Chassis.DisplayPlots([name, '/Chassis']);
        end
    end
end