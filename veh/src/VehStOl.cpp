#include <cstdlib>
#include "VehStOl.hpp"
#include "MathTp.hpp"

using namespace Eigen;

// Parameters ======================================================================================
// Chassis -----------------------------------------------------------------------------------------
// mass [kg]
#define Mass_P                     (P(0))
// moment of inertia about vertical axis [kgm^2]
#define MomentOfInertiaZ_P         (P(1))
// horizontal distance from front wheel center point to center of gravity [m]
#define DistanceFrontCOG_P         (P(2))
// horizontal distance from rear wheel center point to center of gravity [m]
#define DistanceRearCOG_P          (P(3))
// center of gravity height [m]
#define HeightCOG_P                (P(4))
// aerodynamic drag coefficient [1]
#define DragCoefficient_P          (P(5))
// frontal area [m^2]
#define FrontalArea_P              (P(6))
// driving torque distribution between front and rear axles [1]
#define DrivingTorqueSplit_P       (P(7))
// WheelFront --------------------------------------------------------------------------------------
#define OFFSET_PWF 8
// wheel radius [m]
#define RadiusFront_P                   (P(8))
// moment of inertia [kgm^2]
#define MomentOfInertiaYFront_P         (P(9))
// longitudinal max value (Dx) [1]
#define MfMaxXFront_P					(P(10))
// longitudinal stiffness factor (Bx) [1]
#define MfStiffnessXFront_P             (P(11))
// longitudinal shape factor (Cx) [1]
#define MfShapeXFront_P                 (P(12))
// longitudinal curvature factor (Ex) [1]
#define MfCurvatureXFront_P             (P(13))
// velocity independent rolling resistance factor [1]
#define RollingResistanceConstFront_P   (P(14))
// linearly velocity dependent rolling resistance factor [s/m]
#define RollingResistanceLinFront_P     (P(15))
// squarely velocity dependent rollint resistance factor [s^2/m^2]
#define RollingResistanceSqrFront_P     (P(16))
// longitudinal velocity at which rolling resistance reaches it's final value [m/s]
#define RollingResistanceVelFront_P     (P(17))
// braking torque dependent correction in velocity at which rolling resistance reaches
// it's final value [1/Ns]
#define RollingResistanceVelCorrFront_P (P(18))
// longitudinal velocity at which braking torque reaches it's final value [m/s]
#define BrakingTorqueVelFront_P         (P(19))
// longitudinal relaxation length at zero longitudinal slip [m]
#define RelaxationLength0XFront_P       (P(20))
// minimal longitudinal relaxation length [m]
#define RelaxationLengthMinXFront_P     (P(21))
// standstill longitudinal slip damping [Ns/m]
#define SlipDamping0XFront_P            (P(22))
// longitudinal velocity at which slip damping switches off [m/s^2]
#define VelSlowXFront_P                 (P(23))
// lateral max value (Dy) [1]
#define MfMaxFrontY_P					(P(24))
// lateral stiffness factors (By) [1]
#define MfStiffnessYFront_P             (P(25))
// lateral shape factors (Cy) [1]
#define MfShapeYFront_P                 (P(26))
// lateral curvature factors (Ey) [1]
#define MfCurvatureYFront_P             (P(27))
// relaxation length at zero lateral slip in the lateral direction [m]
#define RelaxationLength0YFront_P       (P(28))
// minimal lateral relaxation length [m]
#define RelaxationLengthMinYFront_P     (P(29))
// standstill lateral slip damping [Ns/m]
#define SlipDamping0YFront_P            (P(30))
// lateral velocity at which slip damping switches off [m/s^2]
#define VelSlowYFront_P                 (P(31))
// minimal slip at which slip superposition is considered [1]
#define MinSlipFront_P                  (P(32))
// WheelRear --------------------------------------------------------------------------------------
#define OFFSET_PWR 33
// wheel radius [m]
#define RadiusRear_P                    (P(33))
// moment of inertia [kgm^2]
#define MomentOfInertiaYRear_P          (P(34))
// longitudinal max value (Dx) [1]
#define MfMaxXRear_P					(P(35))
// longitudinal stiffness factor (Bx) [1]
#define MfStiffnessXRear_P              (P(36))
// longitudinal shape factor (Cx) [1]
#define MfShapeXRear_P                  (P(37))
// longitudinal curvature factor (Ex) [1]
#define MfCurvatureXRear_P              (P(38))
// velocity independent rolling resistance factor [1]
#define RollingResistanceConstRear_P    (P(39))
// linearly velocity dependent rolling resistance factor [s/m]
#define RollingResistanceLinRear_P      (P(40))
// squarely velocity dependent rollint resistance factor [s^2/m^2]
#define RollingResistanceSqrRear_P      (P(41))
// longitudinal velocity at which rolling resistance reaches it's final value [m/s]
#define RollingResistanceVelRear_P      (P(42))
// braking torque dependent correction in velocity at which rolling resistance reaches
// it's final value [1/Ns]
#define RollingResistanceVelCorrRear_P  (P(43))
// longitudinal velocity at which braking torque reaches it's final value [m/s]
#define BrakingTorqueVelRear_P          (P(44))
// longitudinal relaxation length at zero longitudinal slip [m]
#define RelaxationLength0XRear_P        (P(45))
// minimal longitudinal relaxation length [m]
#define RelaxationLengthMinXRear_P      (P(46))
// standstill longitudinal slip damping [Ns/m]
#define SlipDamping0XRear_P             (P(47))
// longitudinal velocity at which slip damping switches off [m/s^2]
#define VelSlowXRear_P                  (P(48))
// lateral max value (Dy) [1]
#define MfMaxYRear_P					(P(49))
// lateral stiffness factors (By) [1]
#define MfStiffnessYRear_P              (P(50))
// lateral shape factors (Cy) [1]
#define MfShapeYRear_P                  (P(51))
// lateral curvature factors (Ey) [1]
#define MfCurvatureYRear_P              (P(52))
// relaxation length at zero lateral slip in the lateral direction [m]
#define RelaxationLength0YRear_P        (P(53))
// minimal lateral relaxation length [m]
#define RelaxationLengthMinYRear_P      (P(54))
// standstill lateral slip damping [Ns/m]
#define SlipDamping0YRear_P             (P(55))
// lateral velocity at which slip damping switches off [m/s^2]
#define VelSlowYRear_P                  (P(56))
// minimal slip at which slip superposition is considered [1]
#define MinSlipRear_P                   (P(57))
// Steering ----------------------------------------------------------------------------------------
#define OFFSET_PS 58
// mass [kg]
#define Ratio_P						   (P(58))
// moment of inertia about vertical axis [kgm^2]
#define TimeConstant_P                 (P(59))
// number of parameters
#define NP 60

// Inputs ==========================================================================================
// driving torque [Nm]
#define TorqueDrv_U     (U(0))
// braking torque [Nm]
#define TorqueBrk_U     (U(1))
// friction coefficient front [1]
#define FrictionFront_U (U(2))
// friction coefficient rear [1]
#define FrictionRear_U  (U(3))
// steering angle [rad]
#define SteerWhlAng_U   (U(4))
// number of inputs
#define NU 5

// Specified initial states ========================================================================
// Chassis -----------------------------------------------------------------------------------------
// longitudinal position in ground - fixed coordinate system [m]
#define PosXG_Xi	(Xi(0))
// longitudinal velocity in vehicle - fixed coordinate system [m/s]
#define VelXV_Xi	(Xi(1))
// lateral position in ground - fixed coordinate system [m]
#define PosYG_Xi	(Xi(2))
// lateral velocity in vehicle - fixed coordinate system [m/s]
#define VelYV_Xi	(Xi(3))
// yaw (heading) angle in ground - fixed coordinate system [rad]
#define YawZ_Xi	    (Xi(4))
// yaw (heading) angular velocity in ground - fixed system [rad/s]
#define YawVelZ_Xi  (Xi(5))
// number of specified initial states
#define NXi 6

// Specified initial states for model learning =====================================================
// Chassis -----------------------------------------------------------------------------------------
// longitudinal velocity in vehicle - fixed coordinate system [m/s]
#define VelXV_Xil			(Xil(0))
// lateral velocity in vehicle - fixed coordinate system [m/s]
#define VelYV_Xil			(Xil(1))
// yaw (heading) angular velocity in ground - fixed system [rad/s]
#define YawVelZ_Xil			(Xil(2))
// WheelFront --------------------------------------------------------------------------------------
// angular velocity of the wheel [rad/s]
#define PitchVelYFront_Xil  (Xil(3))
// longitudinal slip[1]
#define SlipXFront_Xil      (Xil(4))
// lateral slip[1]
#define SlipYFront_Xil      (Xil(5))
// WheelRear ---------------------------------------------------------------------------------------
// angular velocity of the wheel [rad/s]
#define PitchVelYRear_Xil   (Xil(6))
// longitudinal slip[1]
#define SlipXRear_Xil       (Xil(7))
// lateral slip[1]
#define SlipYRear_Xil       (Xil(8))
// Steering ----------------------------------------------------------------------------------------
#define SteerAng_Xil        (Xil(9))
// number of specified initial states
#define NXil 10

// States and derivatives ==========================================================================
// Chassis -----------------------------------------------------------------------------------------
// longitudinal position in ground - fixed coordinate system [m]
#define PosXG_X     (X(0))
#define DPosXG_dX   (dX(0))
// longitudinal velocity in ground - fixed coordinate system [m/s]
#define VelXG_X     (X(1))
#define DVelXG_dX   (dX(1))
// lateral position in ground - fixed coordinate system [m]
#define PosYG_X     (X(2))
#define DPosYG_dX   (dX(2))
// lateral velocity in ground - fixed coordinate system [m/s]
#define VelYG_X     (X(3))
#define DVelYG_dX   (dX(3))
// yaw(heading) angle in ground - fixed coordinate system [rad]
#define YawZ_X      (X(4))
#define DYawZ_dX    (dX(4))
// yaw(heading) angular velocity in ground - fixed system [rad/s]
#define YawVelZ_X   (X(5))
#define DYawVelZ_dX (dX(5))
// WheelFront --------------------------------------------------------------------------------------
#define OFFSET_XWF 6
// angle of the wheel [rad]
#define PitchYFront_X      (X(6))
#define DPitchYFront_dX    (dX(6))
// angular velocity of the wheel [rad/s]
#define PitchVelYFront_X   (X(7))
#define DPitchVelYFront_dX (dX(7))
// longitudinal slip[1]
#define SlipXFront_X       (X(8))
#define DSlipXFront_dX     (dX(8))
// lateral slip[1]
#define SlipYFront_X       (X(9))
#define DSlipYFront_dX     (dX(9))
// WheelRear ---------------------------------------------------------------------------------------
#define OFFSET_XWR 10
// angle of the wheel [rad]
#define PitchYRear_X      (X(10))
#define DPitchYRear_dX    (dX(10))
// angular velocity of the wheel [rad/s]
#define PitchVelYRear_X   (X(11))
#define DPitchVelYRear_dX (dX(11))
// longitudinal slip[1]
#define SlipXRear_X       (X(12))
#define DSlipXRear_dX     (dX(12))
// lateral slip[1]
#define SlipYRear_X       (X(13))
#define DSlipYRear_dX     (dX(13))
// Steering ----------------------------------------------------------------------------------------
#define OFFSET_XS 14
// wheel level steering angle [rad]
#define SteerAng_X        (X(14))
#define DSteerAng_dX      (dX(14))
// number of states
#define NX 15

// Outputs =========================================================================================
// Chassis -----------------------------------------------------------------------------------------
// longitudinal acceleration in ground - fixed cs.[m / s ^ 2]
#define AccXG_Y     (Y(0))
// lateral acceleration in ground - fixed cs. [m/s^2]
#define AccYG_Y     (Y(1))
// longitudinal inertial acceleration in vehicle - fixed cs. [m/s^2]
#define AccInrtXV_Y (Y(2))
// lateral inertial acceleration in vehicle - fixed cs. [m/s^2]
#define AccInrtYV_Y (Y(3))
// yaw(heading) angular acceleration in ground - fixed system [rad/s^2]
#define YawAccZ_Y   (Y(4))
// longitudinal velocity in vehicle - fixed cs. [m/s]
#define VelXV_Y     (Y(5))
// lateral velocity in vehicle - fixed cs. [m/s]
#define VelYV_Y     (Y(6))
// WheelFront --------------------------------------------------------------------------------------
#define OFFSET_YWF 7
// angular acceleration [rad/s]
#define PitchAccYFront_Y (Y(7))
// longitudinal slip derivative [1/s]
#define DSlipXFront_Y    (Y(8))
// longitudinal force in wheel - fixed coordinate system [N]
#define ForceXWFront_Y   (Y(9))
// lateral slip derivative [1/s]
#define DSlipYFront_Y    (Y(10))
// lateral force in wheel - fixed coordinate system [N]
#define ForceYWFront_Y   (Y(11))
// vertical force in wheel - fixed coordinate system [N]
#define ForceZWFront_Y   (Y(12))
// wheel level driving torque [Nm]
#define TorqueDrvFront_Y (Y(13))
// wheel level braking torque [Nm]
#define TorqueBrkFront_Y (Y(14))
// wheel level steering angle [rad]
#define SteerAngFront_Y  (Y(15))
// WheelRear ---------------------------------------------------------------------------------------
#define OFFSET_YWR 16
// angular acceleration [rad/s]
#define PitchAccYRear_Y (Y(16))
// longitudinal slip derivative [1/s]
#define DSlipXRear_Y    (Y(17))
// longitudinal force in wheel - fixed coordinate system [N]
#define ForceXWRear_Y   (Y(18))
// lateral slip derivative [1/s]
#define DSlipYRear_Y    (Y(19))
// lateral force in wheel - fixed coordinate system [N]
#define ForceYWRear_Y   (Y(20))
// vertical force in wheel - fixed coordinate system [N]
#define ForceZWRear_Y   (Y(21))
// wheel level driving torque [Nm]
#define TorqueDrvRear_Y (Y(22))
// wheel level braking torque [Nm]
#define TorqueBrkRear_Y (Y(23))
// wheel level steering angle [rad]
#define SteerAngRear_Y  (Y(24))
// Steering ----------------------------------------------------------------------------------------
#define OFFSET_YS 25
// wheel level steering angle derivative [rad/s]
#define DSteerAng_Y      (dX(25))
// number of outputs
#define NY 26

namespace TrajectoryPlanning
{
	Int64_T VehStOl::nP() const { return NP; }
	Int64_T VehStOl::nX() const { return NX; }
	Int64_T VehStOl::nU() const { return NU; }
	Int64_T VehStOl::nY() const { return NY; }

	VehStOl::VehStOl(const Ref<const VectorXd> P) :
		chassis(ChassisSt(P.segment(0, OFFSET_PWF))), 
		wheelFront(WheelMf(P.segment(OFFSET_PWF, OFFSET_PWR - OFFSET_PWF))),
		wheelRear(WheelMf(P.segment(OFFSET_PWR, OFFSET_PS - OFFSET_PWR))),
		steering(SteerFo(P.segment(OFFSET_PS, NP - OFFSET_PS))),
		forceXVFront(0.0),
		forceXVRear(0.0)
	{
	}

	VehStOl::VehStOl(const ChassisSt &chassis, const WheelMf &wheelFront, const WheelMf &wheelRear, const SteerFo &steering) :
		chassis(chassis),
		wheelFront(wheelFront),
		wheelRear(wheelRear),
		steering(steering),
		forceXVFront(0.0),
		forceXVRear(0.0)
	{
	}

	Int32_T VehStOl::Derivatives(Float64_T t, 
		const Ref<const VectorXd> X,
		const Ref<const VectorXd> U,
		Ref<VectorXd> dX, 
		Ref<VectorXd> Y) const
	{
        // Check sizes
		CheckSizes(X, U, dX, Y);

		// Sine and cosine of steering angle
		Float64_T cosSteerAng = cos(SteerAng_X);
		Float64_T sinSteerAng = sin(SteerAng_X);

		// Sine and cosine of yaw angle
		Float64_T cosYawV = cos(YawZ_X);
		Float64_T sinYawV = sin(YawZ_X);

		// Wheels
		// Chassis velocities in vehicle - fixed coordinate system
		VelXV_Y = + cosYawV * VelXG_X + sinYawV * VelYG_X;
		VelYV_Y = - sinYawV * VelXG_X + cosYawV * VelYG_X;

		// Wheel velocities in vehicle - fixed coordinate system
		Float64_T velFrontXV = VelXV_Y;
		Float64_T velFrontYV = VelYV_Y + chassis.DistanceFrontCOG() * YawVelZ_X;
		Float64_T velRearXV = VelXV_Y;
		Float64_T velRearYV = VelYV_Y - chassis.DistanceRearCOG() * YawVelZ_X;

		// Wheel velocities in wheel - fixed coordinate system
		Float64_T velFrontXW = + cosSteerAng * velFrontXV + sinSteerAng * velFrontYV;
		Float64_T velFrontYW = - sinSteerAng * velFrontXV + cosSteerAng * velFrontYV;
		Float64_T velRearXW = velRearXV;
		Float64_T velRearYW = velRearYV;

		// Tire loads (vertical forces)
		ForceZWFront_Y = (chassis.Mass() * G * chassis.DistanceRearCOG() - 
			chassis.HeightCOG()	* (forceXVFront + forceXVRear)) /
			(chassis.DistanceFrontCOG() + chassis.DistanceRearCOG());
        ForceZWRear_Y = (chassis.Mass() * G  * chassis.DistanceFrontCOG() + 
			chassis.HeightCOG()	* (forceXVFront + forceXVRear)) / 
			(chassis.DistanceFrontCOG() + chassis.DistanceRearCOG());

		// Driving torque distribution
		Float64_T coeffFront = wheelFront.Radius() * ForceZWFront_Y;
		Float64_T coeffRear = wheelRear.Radius() * ForceZWRear_Y;
		TorqueDrvFront_Y = coeffFront / (coeffFront + coeffRear) * TorqueDrv_U;
		TorqueDrvRear_Y = coeffRear / (coeffFront + coeffRear) * TorqueDrv_U;

		// Braking torque distribution
		TorqueBrkFront_Y = coeffFront / (coeffFront + coeffRear) * TorqueBrk_U;
		TorqueBrkRear_Y = coeffRear / (coeffFront + coeffRear) * TorqueBrk_U;

		// Wheels
		VectorXd UWF(6);
		UWF << TorqueDrvFront_Y, TorqueBrkFront_Y, FrictionFront_U, ForceZWFront_Y, velFrontXW, velFrontYW;
		wheelFront.Derivatives(t, 
			X.segment(OFFSET_XWF, OFFSET_XWR - OFFSET_XWF), 
			UWF,
			dX.segment(OFFSET_XWF, OFFSET_XWR - OFFSET_XWF),
			Y.segment(OFFSET_YWF, OFFSET_YWR - OFFSET_YWF));

		VectorXd UWR(6);
		UWR << TorqueDrvRear_Y, TorqueBrkRear_Y, FrictionRear_U, ForceZWRear_Y, velRearXW , velRearYW;
		wheelRear.Derivatives(t,
			X.segment(OFFSET_XWR, OFFSET_XS - OFFSET_XWR),
			UWR, 
			dX.segment(OFFSET_XWR, OFFSET_XS - OFFSET_XWR),
			Y.segment(OFFSET_YWR, OFFSET_YS - OFFSET_YWR));
		
		// Chassis
		// Forces in vehicle - fixed coordinate system
		forceXVFront = +cosSteerAng * ForceXWFront_Y - sinSteerAng * ForceYWFront_Y;
		Float64_T forceYVFront = +sinSteerAng * ForceXWFront_Y + cosSteerAng * ForceYWFront_Y;

		forceXVRear = ForceXWRear_Y;
		Float64_T forceYVRear = ForceYWRear_Y;

		VectorXd UC(6);
		UC << forceXVFront, forceYVFront, forceXVRear, forceYVRear, VelXV_Y, VelYV_Y;
		chassis.Derivatives(t,
			X.segment(0, OFFSET_XWF),
			UC,
			dX.segment(0, OFFSET_XWF),
			Y.segment(0, OFFSET_YWF));

		// Steering
		VectorXd US(1);
		US << SteerWhlAng_U;
		steering.Derivatives(t, 
			X.segment(OFFSET_XS, NX - OFFSET_XS), 
			US, 
			dX.segment(OFFSET_XS, NX - OFFSET_XS), 
			Y.segment(OFFSET_YS, NY - OFFSET_YS));

		// Set additional outputs
		SteerAngFront_Y = SteerAng_X;
		SteerAngRear_Y = 0;

		return 0;
	}

	Int32_T VehStOl::Initialize(const Ref<const VectorXd> Xi, Ref<VectorXd> X) const
	{
		// Check sizes
		if (Xi.size() != NXi)
		{
			throw std::invalid_argument("VehStOl.Initialize: Length of Xi is not appropriate!");
		}
		if (X.size() != NX)
		{
			throw std::invalid_argument("VehStOl.Initialize: Length of X is not appropriate!");
		}
		// Reset internal variables
		forceXVFront = 0.0;
		forceXVRear = 0.0;

		// Initial state vector
		Float64_T cosYawZ0 = cos(YawZ_Xi);
		Float64_T sinYawZ0 = sin(YawZ_Xi);

		Float64_T velXG0 = +cosYawZ0 * VelXV_Xi - sinYawZ0 * VelYV_Xi;
		Float64_T velYG0 = +sinYawZ0 * VelXV_Xi + cosYawZ0 * VelYV_Xi;

		PosXG_X   = PosXG_Xi;
		VelXG_X   = velXG0;
		PosYG_X   = PosYG_Xi;
		VelYG_X   = velYG0;
		YawZ_X    = YawZ_Xi;
		YawVelZ_X = YawVelZ_Xi;

		PitchYFront_X    = 0.0;
		PitchVelYFront_X = VelXV_Xi / wheelFront.Radius();
		SlipXFront_X     = 0.0;
		SlipYFront_X     = 0.0;

		PitchYRear_X    = 0.0;
		PitchVelYRear_X = VelXV_Xi / wheelRear.Radius();
		SlipXRear_X     = 0.0;
		SlipYRear_X     = 0.0;

		SteerAng_X = 0.0;

		return 0;
	}

	Int32_T VehStOl::InitConL(const Ref<const VectorXd> Xil, Ref<VectorXd> X) const
	{
		// Check sizes
		if (Xil.size() != NXil)
		{
			throw std::invalid_argument("VehStOl.Initialize: Length of Xil is not appropriate!");
		}
		if (X.size() != NX)
		{
			throw std::invalid_argument("VehStOl.Initialize: Length of X is not appropriate!");
		}

		PosXG_X = 0;
		VelXG_X = VelXV_Xil;
		PosYG_X = 0;
		VelYG_X = VelYV_Xil;
		YawZ_X = 0;
		YawVelZ_X = YawVelZ_Xil;

		PitchYFront_X = 0;
		PitchVelYFront_X = PitchVelYFront_Xil;
		SlipXFront_X = SlipXFront_Xil;
		SlipYFront_X = SlipYFront_Xil;

		PitchYRear_X = 0;
		PitchVelYRear_X = PitchVelYRear_Xil;
		SlipXRear_X = SlipXRear_Xil;
		SlipYRear_X = SlipYRear_Xil;

		SteerAng_X = SteerAng_Xil;

		return 0;
	}
}
