%% CtrlLonOut **************************************************************************************
% [Summary]
%   This class represents common outputs for lateral (path tracking) control. 
%
% [Used in]
%   VehStClOut
%
% [Subclasses]
%
% **************************************************************************************************
classdef CtrlLonOut < VehOlLonIn  
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Longitudinal velocity tracking error [m]
        VErr
        
    end
    
    %% Instance methods ============================================================================
    methods        
        %% Property getter and setter methods ------------------------------------------------------        
        function set.VErr(self, value)
            if ~isequal(self.VErr, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: VErr must be a numeric vector!');
                end
                self.VErr = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------        
        function self = CtrlLonOut(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 4
                superArgs = varargin(1:3);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@VehOlLonIn(superArgs{:});
            if nargin == 1
                self.VErr = varargin{1}.VErr;
            elseif nargin == 4
                self.VErr = varargin{4};
            end
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plot about the longitudinal controller.
        %
        % [Input]
        %       name   - name for the plot
        %
        % [Output]
        %       fig - figure handle of plot
        % ------------------------------------------------------------------------------------------        
        function fig = DisplayPlots(self, name)
            %% Constants
            WIDTH = 5;
            HEIGHT3 = 15;
            FONTSIZE = 8;
            
            %% Default arguments
            if nargin < 2
                name = class(self);
            end
            
            %% Create plot
            
            fig = nicefigure(name, WIDTH, HEIGHT3);

            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 1, 1, ax);
            plot(ax, self.Time, self.TorqueDrv, 'Color', Colors.Nephritis);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$M_d \mathrm{[Nm]}$');
            title(ax, 'driving torque');

            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 1, 2, ax);
            plot(ax, self.Time, self.TorqueBrk, 'Color', Colors.PeterRiver);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$M_b \mathrm{[Nm]}$');
            title(ax, 'braking torque');

            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 1, 3, ax);
            plot(ax, self.Time, self.VErr * 3.6, 'Color', Colors.Wisteria);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$v_{err} \mathrm{[km/h]}$');
            title(ax, 'velocity tracking error');
        end
        
    end
end