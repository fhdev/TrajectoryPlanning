%% ChassisQr ***************************************************************************************
% [Summary]
%   This class represents a chassis coupled to a single wheel of a quarter vehicle model. 
%
% [Used in]
%   VehQr
%
% [Subclasses]
%   ChassisSt
%
% **************************************************************************************************
classdef ChassisQr < ChassisQrLon
    %% Instance methods ============================================================================
    methods
        %% Constructor -----------------------------------------------------------------------------
        function self = ChassisQr(varargin)
            self@ChassisQrLon(varargin{:});
        end
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of chassis coupled to a single wheel.
        %
        %   The coordinate systems of the vehicle, and the wheel are coinciding and rotated by the
        %   steering angle compared to the ground-fixed system!
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => PosXG: longitudinal position in ground-fixed coordinate system [m]
        %       X(2) => VelXG: longitudinal velocity in ground-fixed coordinate system [m/s]
        %       X(3) => PosYG: lateral position in ground-fixed coordinate system [m]
        %       X(4) => VelYG: lateral velocity in ground-fixed coordinate system [m/s]
        %
        %   U - input vector
        %       U(1) => ForceXG:  longitudinal tire force in ground-fixed coordinate system [N];
        %       U(2) => ForceYG:  lateral tire force in ground-fixed coordinate system [N]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %       Y(1)  => AccXG: longitudinal acceleration in ground-fixed cs. [m/s^2]
        %       Y(2)  => AccYG: lateral acceleration in ground-fixed cs. [m/s^2]    
        % ------------------------------------------------------------------------------------------       
        function [dX, Y] = StateEquation(self, ~, X, U)
            %% Calculation of state derivatives
            % Principle of linear momentum for the chassis
            accXG = U(1) / self.Mass;
            accYG = U(2) / self.Mass;
            %% State derivative varaibles
            dX = [X(2);   % VelXG: longitudinal velocity in ground-fixed coordinate system [m/s]
                  accXG;  % AccXG: longitudinal acceleration in ground-fixed cs. [m/s^2]
                  X(4);   % VelYG: lateral velocity in ground-fixed coordinate system [m/s]
                  accYG]; % AccYG: lateral acceleration in ground-fixed cs. [m/s^2]
            %% Output variables
            Y = [accXG;      % AccXG: longitudinal acceleration in ground-fixed cs. [m/s^2]
                 accYG];     % AccYG: lateral acceleration in ground-fixed cs. [m/s^2]              
        end
    end
end