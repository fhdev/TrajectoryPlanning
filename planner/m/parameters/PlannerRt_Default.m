function planner = PlannerRt_Default()
planner.Planner           = PlannerCon_Default();
planner.SampleTimeCtrl    = 0.02;
planner.SampleTimePlanner = 2;
planner.MaxRunTime        = 10;
planner.VirtualCan        = true;
planner.Wheel2SteerWheel  = 25;
end
