W = 7;
H = 5;
F = 8;

colors = {nicecolors.BlueBelizeHole, nicecolors.GreenEmerald, nicecolors.PurpleAmethyst, ...
    nicecolors.RedAlizarin, nicecolors.YellowSunFlower};

fig1 = nicefigure('LearningProcessTraining', W, H);
ax1 = niceaxis(fig1, F);

fig2 = nicefigure('LearningProcessTesting', W, H);
ax2 = niceaxis(fig2, F);

iCnt = 1;
for i = [1, 3, 5, 7, 17, 19]
    color = colors{mod(iCnt - 1, length(colors)) + 1};
    plot(ax1, Networks3(i).traindata.perf, 'Color', color, 'LineStyle', '-');
    plot(ax2, Networks3(i).traindata.tperf, 'Color', color, 'LineStyle', '-.');
    iCnt = iCnt + 1;
end
ylim(ax1, [1e-5, 1e1]);
xlim(ax1, [0, 35]);
ax1.YScale = 'log';
ax1.YTick = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 1e1];

ylim(ax2, [1e-5, 1e1]);
xlim(ax2, [0, 35]);
ax2.YScale = 'log';
ax2.YTick = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 1e1];

xlabel(ax1, 'epoch [1]');
ylabel(ax1, 'training loss [1]');

xlabel(ax2, 'epoch [1]');
ylabel(ax2, 'testing loss [1]');

l1 = nicelegend({'n20l1 feedforward', 'n20l1 cascade', 'n28l2 feedforward', 'n28l2 cascade', 'n38l3v1 feedforward' , 'n38l3v1 cascade'}, ax1, F);
l2 = nicelegend({'n20l1 feedforward', 'n20l1 cascade', 'n28l2 feedforward', 'n28l2 cascade', 'n38l3v1 feedforward' , 'n38l3v1 cascade'}, ax2, F);

name = 'LearningProcessTraining';
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');

name = 'LearningProcessTesting';
savefig(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig2, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');