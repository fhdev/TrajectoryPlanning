%% CtrlPidV ****************************************************************************************
% [Summary]
%   This class represents a SISO PID controller for longitudinal velocity tracking. 
%
% [Used in]
%   VehStCl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef CtrlPidV < CtrlLon    
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % Previous input, output and time stamp
        up
        yp
        tp
        
        %% Properties set in constructor -----------------------------------------------------------
        % Controller gains
        PropGain 
        IntGain
        DerGain
        % Derivative filter coefficient
        DerFilterCon
        
    end
    
    %% Instance methods ============================================================================
    methods      
        %% Property getter and setter methods ------------------------------------------------------
        function set.PropGain(self, value)
            if ~isequal(self.PropGain, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: PropGain must be a numeric scalar!')
                end
                self.PropGain = value(:);
            end
        end
        
         function set.IntGain(self, value)
            if ~isequal(self.IntGain, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: IntGain must be a numeric scalar!')
                end
                self.IntGain = value(:);
            end
         end
        
        function set.DerGain(self, value)
            if ~isequal(self.DerGain, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DerGain must be a numeric scalar!')
                end
                self.DerGain = value(:);
            end
        end         
        
        function set.DerFilterCon(self, value)
            if ~isequal(self.DerFilterCon, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DerFilterCon must be a numeric scalar!')
                end
                self.DerFilterCon = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------       
        function self = CtrlPidV(chassis, wheel, varargin)
            %% Assemble superclass arguments
            if nargin == 3
                superArgs = varargin(1);
            elseif nargin == 6
                superArgs = varargin(1:4);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            %% Call superclass constructor
            self@CtrlLon(superArgs{:});
            %% Set subclass properties
            % Parameters
            % mass [kg]
            m    = chassis.Mass;
            % Aerodynamic drag parameters
            % aerodynamic drag coefficient [1]
            cd   = chassis.DragCoefficient;
            % frontal area of the vehicle [m^2]
            Af   = chassis.FrontalArea;
            % wheel radius [m]
            r    = wheel.Radius;
            % Rolling resistance parameters
            % velocity independent rolling resistance coefficient [1]
            Arr  = wheel.RollingResistanceConst;
            % linearly velocity dependent rolling resistance coefficient [s/m]
            Brr  = wheel.RollingResistanceLin;
            % squarely velocity depenfent rolling resistance coefficient [s^2/m^2]
            Crr  = wheel.RollingResistanceSqr;
            % gravitation [m/s^2]
            g = Com.Gravitation;
            % mass density of air [kg/m^3]
            rhoa = Com.AirDensity;
            
            %% Calculate linearized rolling resistance
            % Frr = Fz * (Arr + Brr.|v| + Crr.v^2)  => Fz * (Arrlin + Brrlin.|v|)
            % (calculate line which has same slope and value @ self.VelLin as the parabola)
            Brrlin = 2 * Crr * self.VelLin + Brr;
            Arrlin = Crr * self.VelLin^2 + Brr * self.VelLin + Arr - Brrlin * self.VelLin;
            
            %% Calculate linerized aerodynamic drag
            % (calculate line which has same value @ self.VelLin as the parabola, and value 0 at 0)
            Badlin = 0.5 * cd * Af * rhoa * self.VelLin;
            
            %% LTI model with transfer function
            % Inputs
            % U(1) => TorqueDrv: driving/braking wheel torque [Nm]
            % Outputs
            % Y(1) => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
            %
            % Model equations:
            % m.d_v(t) = M(t)/r - (Brrlin * m * g + Badlin) * v - Arrlin * m * g
            % [r * (m * s + Brrlin * m * g + Badlin) * V(s) +  Arrlin * m * g] = M(s)
            % V(s) / M(s) = 1 / (r * (m * s + Brrlin * m * g + Badlin))  => 1 / (a1.s + a0)
            a0 = r * (Brrlin * m * g + Badlin);
            a1 = r * m;
            % Amplification and time constant
            AG = 1 / a0;
            T = a1 / a0;
            
            %% PI control design
            % C(s) = Kp + Ki / s = (Ki / s) * [(Kp / Ki) * s + 1]
            % G(s) = A / (T * s + 1)
            Kp = 2500;
            Ki = Kp / T;
            Kd = 0;
            N = 1000;
            
            %% Assign computed property values
            self.PropGain = Kp;
            self.IntGain = Ki;
            self.DerGain = Kd;
            self.DerFilterCon = N;
            
            %% Initial value for internal variables
            self.tp = 0;
            self.yp = 0;
            self.up = 0;
            
        end
        
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of the SISO PID controller for longitudinal 
        %   velocity tracking. 
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => VErrInt:        integral of longitudinal velocity tracking error [m]
        %
        %   U - input vector
        %       U(1) => VelXV:          longitudinal velocity in vehicle-fixed cs. [m/s]
        %       U(2) => VelRef:         target longitudinal velocity in vehicle-fixed cs. [m/s]
        %
        % [Output]
        %   dX - state derivative vector
        %       dX(1) => VErr:          longitudinal velocity tracking error [m]
        %
        %   Y  - output vector
        %       Y(1) => TorqueDrv: driving torque [Nm]
        %       Y(2) => TorqueBrk: braking torque [Nm]
        %       Y(3) => VErr:      longitudinal velocity tracking error [m]
        % ------------------------------------------------------------------------------------------        
        function [dX, Y] = StateEquation(self, t, X, U) 
            %% Calculation
            % Tracking error
            vErr = U(2) - U(1);
            % Calculate output in parallel form
            proportional = self.PropGain * vErr;
            integral = self.IntGain * X(1);
            dt = (t - self.tp);
            if dt > 0
                dU = (vErr - self.up) / dt;
                derivative = self.DerGain * (dU + self.yp / (self.DerFilterCon * dt)) / ...
                    (1 + self.DerGain / (self.DerFilterCon * dt));
            else
                derivative = 0;
            end
            %% Calculate driving / braking torque
            torque = proportional + integral + derivative;
            
            %% Saturation with anti-windup (clamping)
            [dX(1), torque] = self.AntiWindup(U, torque);
            
            %% Save previous input and output
            self.up  = vErr;
            self.yp  = torque;
            self.tp  = t;
            
            %% Set output
            Y = [max(0, torque); max(0, -torque); vErr];
        end
        
        %% Initialize -----------------------------------------------------------------------------
        % [Summary]
        %   This function calculates initial value of state vector.
        %
        % [Input]
        %   none
        %
        % [Output]
        %   X0 -  initial state vector
        %       X0(1) => VErrInt:  initial integral of longitudinal velocity tracking error [m]
        % ------------------------------------------------------------------------------------------         
        function X0 = Initialize(self)
            X0 = 0;
        end
    end
end