%% VehQrLonIn **************************************************************************************
% [Summary]
%   This class represents inputs for single track closed loop vehicle model.
%
% [Used in]
%   VehStCl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehStClIn < VehClIn & VehEnvIn2W
    %% Instance methods ============================================================================
    methods
        %% Constructor -----------------------------------------------------------------------------
       function self = VehStClIn(varargin)
           if nargin == 1
               vehClInArgs = varargin(1);
               vehEnvIn2WArgs = varargin(1);
           elseif nargin == 8
               vehClInArgs = varargin(1:6);
               vehEnvIn2WArgs = varargin([1, 7, 8]);
           else
               error('ERROR: Inappropriate number of arguments!');
           end
           self@VehClIn(vehClInArgs{:});
           self@VehEnvIn2W(vehEnvIn2WArgs{:});
       end

    end
end