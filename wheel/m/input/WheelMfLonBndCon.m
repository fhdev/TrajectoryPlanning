%% WheelMfLonBndCon ********************************************************************************
% [Summary]
%   This class represents boundary conditions for longitudinal wheel model with Magic Formula tire.
%
% [Used in]
%   none
%
% [Subclasses]
%   WheelMfBndCon
% **************************************************************************************************
classdef WheelMfLonBndCon < handle
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Pitch (turn) angle of the wheel [rad]
        PitchY
        % Pitch (turn) rate of the wheel [rad/s]
        PitchVelY
        % Longitudinal slip [1]
        SlipX
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.PitchY(self, value)
            if ~isequal(self.PitchY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: PitchY must be numeric scalar!')
                end
                self.PitchY = value;
            end
        end
        
        function set.PitchVelY(self, value)
            if ~isequal(self.PitchVelY, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: PitchVelY must be numeric scalar!')
                end
                self.PitchVelY = value;
            end
        end
        
        function set.SlipX(self, value)
            if ~isequal(self.SlipX, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: SlipX must be numeric scalar!')
                end
                self.SlipX = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = WheelMfLonBndCon(varargin)
            if nargin == 0
                self.PitchY = 0;
                self.PitchVelY = 0;
                self.SlipX = 0;
            elseif nargin == 1
                self.PitchY     = varargin{1}.PitchY;
                self.PitchVelY = varargin{1}.PitchVelY;
                self.SlipX      = varargin{1}.SlipX;
            elseif nargin == 3
                self.PitchY     = varargin{1};
                self.PitchVelY = varargin{2};
                self.SlipX      = varargin{3};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end
        
    end
end