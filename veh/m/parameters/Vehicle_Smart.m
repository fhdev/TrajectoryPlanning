function vehicle = Vehicle_Smart()

vehicle.SampleTime = 1e-3;

% Only for VehStRt
vehicle.SampleTimeRt = 0.02;
vehicle.MaxSimTime = 30;
vehicle.VirtualCan = true;

%% Chassis
vehicle.Chassis.Mass               =  900;
vehicle.Chassis.MomentOfIntertiaZ  = 1000;
vehicle.Chassis.DistanceFrontCOG   =    1.04;
vehicle.Chassis.DistanceRearCOG    =    0.83;
vehicle.Chassis.HeightCOG          =    0.42;
vehicle.Chassis.DragCoefficient    =    0.3;
vehicle.Chassis.FrontalArea        =    1.4;
vehicle.Chassis.DrivingTorqueSplit =    0.5;

%% Wheels

% For 1 wheeled models
vehicle.Wheel.Radius                   =   0.287;
vehicle.Wheel.MomentOfInertiaY         =   0.9;
vehicle.Wheel.MfStiffnessX             =  15.0;
vehicle.Wheel.MfShapeX                 =   1.45;
vehicle.Wheel.MfCurvatureX             =   0.4;
vehicle.Wheel.RollingResistanceConst   =   9.8e-3;
vehicle.Wheel.RollingResistanceLin     =   1.1e-4;
vehicle.Wheel.RollingResistanceSqr     =   1.6e-6;
vehicle.Wheel.RollingResistanceVel     =   0.05;
vehicle.Wheel.RollingResistanceVelCorr =   1e-4;
vehicle.Wheel.BrakingTorqueVel         =   0.05;
vehicle.Wheel.RelaxationLength0X       =   0.2;
vehicle.Wheel.RelaxationLengthMinX     =   0.01;
vehicle.Wheel.SlipDamping0X            = 980;
vehicle.Wheel.VelSlowX                 =   5.0;
vehicle.Wheel.MfStiffnessY             =   13.0;
vehicle.Wheel.MfShapeY                 =    1.85;
vehicle.Wheel.MfCurvatureY             =    0.5;
vehicle.Wheel.RelaxationLength0Y       =    0.2;
vehicle.Wheel.RelaxationLengthMinY     =    0.01;
vehicle.Wheel.SlipDamping0Y            =  980;
vehicle.Wheel.VelSlowY                 =    2.5;
vehicle.Wheel.MinSlip                  =    0;

% For 2 wheeled models
vehicle.WheelFront.Radius                   =   0.287;
vehicle.WheelFront.MomentOfInertiaY         =   0.9;
vehicle.WheelFront.MfStiffnessX             =  15.0;
vehicle.WheelFront.MfShapeX                 =   1.45;
vehicle.WheelFront.MfCurvatureX             =   0.4;
vehicle.WheelFront.RollingResistanceConst   =   9.8e-3;
vehicle.WheelFront.RollingResistanceLin     =   1.1e-4;
vehicle.WheelFront.RollingResistanceSqr     =   1.6e-6;
vehicle.WheelFront.RollingResistanceVel     =   0.05;
vehicle.WheelFront.RollingResistanceVelCorr =   1e-4;
vehicle.WheelFront.BrakingTorqueVel         =   0.05;
vehicle.WheelFront.RelaxationLength0X       =   0.2;
vehicle.WheelFront.RelaxationLengthMinX     =   0.01;
vehicle.WheelFront.SlipDamping0X            = 980;
vehicle.WheelFront.VelSlowX                 =   5.0;
vehicle.WheelFront.MfStiffnessY             =   13.0;
vehicle.WheelFront.MfShapeY                 =    1.85;
vehicle.WheelFront.MfCurvatureY             =    0.5;
vehicle.WheelFront.RelaxationLength0Y       =    0.2;
vehicle.WheelFront.RelaxationLengthMinY     =    0.01;
vehicle.WheelFront.SlipDamping0Y            =  980;
vehicle.WheelFront.VelSlowY                 =    2.5;
vehicle.WheelFront.MinSlip                  =    0;

vehicle.WheelRear.Radius                   =   0.287;
vehicle.WheelRear.MomentOfInertiaY         =   0.9;
vehicle.WheelRear.MfStiffnessX             =  15.0;
vehicle.WheelRear.MfShapeX                 =   1.45;
vehicle.WheelRear.MfCurvatureX             =   0.4;
vehicle.WheelRear.RollingResistanceConst   =   9.8e-3;
vehicle.WheelRear.RollingResistanceLin     =   1.1e-4;
vehicle.WheelRear.RollingResistanceSqr     =   1.6e-6;
vehicle.WheelRear.RollingResistanceVel     =   0.05;
vehicle.WheelRear.RollingResistanceVelCorr =   1e-4;
vehicle.WheelRear.BrakingTorqueVel         =   0.05;
vehicle.WheelRear.RelaxationLength0X       =   0.2;
vehicle.WheelRear.RelaxationLengthMinX     =   0.01;
vehicle.WheelRear.SlipDamping0X            = 980;
vehicle.WheelRear.VelSlowX                 =   5.0;
vehicle.WheelRear.MfStiffnessY             =   13.0;
vehicle.WheelRear.MfShapeY                 =    1.85;
vehicle.WheelRear.MfCurvatureY             =    0.5;
vehicle.WheelRear.RelaxationLength0Y       =    0.2;
vehicle.WheelRear.RelaxationLengthMinY     =    0.01;
vehicle.WheelRear.SlipDamping0Y            =  980;
vehicle.WheelRear.VelSlowY                 =    2.5;
vehicle.WheelRear.MinSlip                  =    0;

%% Controllers

% Type selector: CtrlLqsYr or CtrlLatTrk or CtrlLatSty
vehicle.CtrlLat.Type            = 'CtrlLatSty';
% Common
vehicle.CtrlLat.VelLin          = 25;
vehicle.CtrlLat.SatVel          = [0; 10; 20; 30; 50];
vehicle.CtrlLat.SatSteerAng     = [28; 28; 5; 2; 1] * pi / 180;
% Only for CtrlLqsYr
vehicle.CtrlLat.GainVelYInit    = 0.0000;
vehicle.CtrlLat.GainYawVelZInit = 0.1212;
% Only for CtrlLatSty
vehicle.CtrlLat.GainSty         = 0.25;

% Type selector: CtrlLqsV or CtrlPidV
vehicle.CtrlLon.Type            = 'CtrlLqsV';
% Common
vehicle.CtrlLon.VelLin          = 25;
vehicle.CtrlLon.SatVel          = [0; 10; 20; 30; 50];
vehicle.CtrlLon.SatTorqueDrv    = [2000; 1800; 1500; 1300; 1000];
vehicle.CtrlLon.SatTorqueBrk    = [6000; 5800; 5800; 5800; 5000];
% Only for CtrlLqsV
vehicle.CtrlLon.GainVelXInit    = 0.5;

end