%% VehOlIn *****************************************************************************************
% [Summary]
%   This class represents open loop dynamics inputs for vehicle models.
%
% [Used in]
%   none
%
% [Subclasses]
%   VehQrIn
%   VehStOlIn
% **************************************************************************************************
classdef VehOlIn < VehOlLonIn & VehOlLatIn
    %% Instance methods ============================================================================
    methods
        %% Constructor -----------------------------------------------------------------------------
        function self = VehOlIn(varargin)
            if nargin == 1
                velOlLonInArgs = varargin(1);
                velOlLatInArgs = varargin(1);
            elseif nargin == 4
                velOlLonInArgs = varargin(1:3);
                velOlLatInArgs = varargin([1, 4]);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@VehOlLonIn(velOlLonInArgs{:});
            self@VehOlLatIn(velOlLatInArgs{:});
            if nargin == 1
                
            elseif nargin == 4
                
            end
        end

    end
end