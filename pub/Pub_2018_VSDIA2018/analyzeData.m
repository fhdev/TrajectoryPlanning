nPathes = length(t);

devAvgLin   = zeros(nPathes, 1);
devAvg      = zeros(nPathes, 1);
devAvgCross = zeros(nPathes, 1);

devMaxLin   = zeros(nPathes, 1);
devMax      = zeros(nPathes, 1);
devMaxCross = zeros(nPathes, 1);

slipMaxLin  = zeros(nPathes, 1);
slipMax     = zeros(nPathes, 1);

accMaxLin   = zeros(nPathes, 1);
accMax      = zeros(nPathes, 1);
accMaxTraj  = zeros(nPathes, 1);

steeringAngleGainLin  = zeros(nPathes, 1);
steeringAngleGain  = zeros(nPathes, 1);

for iPath = 1 : nPathes
    %%  analyze
    nSteps   = length(t(iPath).Time);
    
    devLin   = zeros(nSteps - 1, 1);
    dev      = zeros(nSteps - 1, 1);
    devCross = zeros(nSteps - 1, 1);
    slip     = zeros(nSteps - 1, 1);
    accLin   = zeros(nSteps - 1, 1);
    acc      = zeros(nSteps - 1, 1);
    
    for iStep = 1 : nSteps - 1
        
        stateRef = [t(iPath).PositionX_GCS(iStep);
            t(iPath).PositionY_GCS(iStep);
            t(iPath).AngleZ(iStep);
            t(iPath).AngularVelocityZ(iStep)];
        
        stateLin = [interp1q(outLin(iPath).Chassis.Time, outLin(iPath).Chassis.PositionX_GCS, t(iPath).Time(iStep));
            interp1q(outLin(iPath).Chassis.Time, outLin(iPath).Chassis.PositionY_GCS, t(iPath).Time(iStep));
            interp1q(outLin(iPath).Chassis.Time, outLin(iPath).Chassis.AngleZ, t(iPath).Time(iStep));
            interp1q(outLin(iPath).Chassis.Time, outLin(iPath).Chassis.AngularVelocityZ, t(iPath).Time(iStep))];
        
        state = [interp1q(out(iPath).Time, out(iPath).Chassis.PositionX_GCS, t(iPath).Time(iStep));
            interp1q(out(iPath).Time, out(iPath).Chassis.PositionY_GCS, t(iPath).Time(iStep));
            interp1q(out(iPath).Time, out(iPath).Chassis.AngleZ, t(iPath).Time(iStep));
            interp1q(out(iPath).Time, out(iPath).Chassis.AngularVelocityZ, t(iPath).Time(iStep))];
        
        posRef = [t(iPath).PositionX_GCS(iStep);
                  t(iPath).PositionY_GCS(iStep)];
        
        posLin = [interp1q(outLin(iPath).Chassis.PositionX_GCS, outLin(iPath).Chassis.PositionX_GCS, t(iPath).PositionX_GCS(iStep));
            interp1q(outLin(iPath).Chassis.PositionX_GCS, outLin(iPath).Chassis.PositionY_GCS, t(iPath).PositionX_GCS(iStep))];
        
        pos = [interp1q(out(iPath).Chassis.PositionX_GCS, out(iPath).Chassis.PositionX_GCS, t(iPath).PositionX_GCS(iStep));
            interp1q(out(iPath).Chassis.PositionX_GCS, out(iPath).Chassis.PositionY_GCS, t(iPath).PositionX_GCS(iStep))];
        
        errLin = stateLin - stateRef;
        err = state - stateRef;
        errCross = state - stateLin;
        
        weight = diag([1; 1; 10; 10]);
        
        devLin(iStep) = sqrt(errLin' * weight * errLin);
        dev(iStep) = sqrt(err' * weight * err);
        devCross(iStep) = sqrt(errCross' * weight * errCross);
        
        posDevLin(iStep) = norm(posLin - posRef);
        posDev(iStep) = norm(pos - posRef);
        
        slip(iStep) = norm([interp1q(out(iPath).Time, out(iPath).WheelFront.SlipX, t(iPath).Time(iStep));
            interp1q(out(iPath).Time, out(iPath).WheelFront.SlipY, t(iPath).Time(iStep))]);
        
        accLin(iStep) = norm([interp1q(outLin(iPath).Chassis.Time, outLin(iPath).Chassis.AccelerationXInertial_VCS, t(iPath).Time(iStep));
            interp1q(outLin(iPath).Chassis.Time, outLin(iPath).Chassis.AccelerationYInertial_VCS, t(iPath).Time(iStep))]);
        
        acc(iStep) = norm([interp1q(out(iPath).Time, out(iPath).Chassis.AccelerationXInertial_VCS, t(iPath).Time(iStep));
            interp1q(out(iPath).Time, out(iPath).Chassis.AccelerationYInertial_VCS, t(iPath).Time(iStep))]);
        
    end
    
    devAvgLin(iPath)   = mean(devLin);
    devAvg(iPath)      = mean(dev);
    devAvgCross(iPath) = mean(devCross);
    
    devMaxLin(iPath)   = max(devLin);
    devMax(iPath)      = max(dev);
    devMaxCross(iPath) = max(devCross);
    
    posDevMaxLin(iPath) = max(posDevLin);
    posDevMax(iPath) = max(posDev);
    
    slipMaxLin(iPath)  = max(sqrt(outLin(iPath).Slip).^2);
    slipMax(iPath)     = max(slip);
    
    
    accMaxLin(iPath)   = max(accLin);
    accMax(iPath)      = max(acc);
    accMaxTraj(iPath)  = max(sqrt((t(iPath).AngularVelocityZ(1:end-1) .* t(iPath).VelocityX_VCS(1:end-1)).^2 + (diff(t(iPath).VelocityX_VCS)./diff(t(iPath).Time)).^2));
    
    steeringAngleGainLin(iPath) = mean(abs(outLin(iPath).SteeringAngle)) ./ mean(abs(outLin(iPath).Chassis.AngularVelocityZ));
    steeringAngleGain(iPath) = mean(abs(out(iPath).CalcInput.SteeringAngle)) ./ mean(abs(out(iPath).Chassis.AngularVelocityZ));
end