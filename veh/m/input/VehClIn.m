%% VehStClIn ***************************************************************************************
% [Summary]
%   This class represents inputs for a planar nonlinear single track vehicle model with trajectory
%   tracking controllers.
%
% [Used in]
%   VehStCl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehClIn < ComTimeInOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % longitudinal position reference [m]
        PosXGRef
        % lateral position reference [m]
        PosYGRef
        % yaw angle (heading) reference [rad]
        YawZRef
        % longitudinal velocity reference [m/s]
        VelXVRef
        % yaw rate (turn rate) reference [rad/s]
        YawVelZRef

    end

    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.PosXGRef(self, value)
            if ~isequal(self.PosXGRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PosXGRef must be a numeric vector!');
                end
                self.PosXGRef = value(:);
            end
        end

        function set.PosYGRef(self, value)
            if ~isequal(self.PosYGRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PosYGRef must be a numeric vector!');
                end
                self.PosYGRef = value(:);
            end
        end

        function set.YawZRef(self, value)
            if ~isequal(self.YawZRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawZRef must be a numeric vector!');
                end
                self.YawZRef = value(:);
            end
        end

        function set.VelXVRef(self, value)
            if ~isequal(self.VelXVRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: VelXVRef must be a numeric vector!');
                end
                self.VelXVRef = value(:);
            end
        end

        function set.YawVelZRef(self, value)
            if ~isequal(self.YawVelZRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawVelZRef must be a numeric vector!');
                end
                self.YawVelZRef = value(:);
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehClIn(varargin)
            if nargin == 1
                comTimeInArgs = varargin(1);
            elseif nargin == 6
                comTimeInArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@ComTimeInOut(comTimeInArgs{:});
            if nargin == 1
                self.PosXGRef      = varargin{1}.PosXGRef;
                self.PosYGRef      = varargin{1}.PosYGRef;
                self.YawZRef       = varargin{1}.YawZRef;
                self.VelXVRef      = varargin{1}.VelXVRef;
                self.YawVelZRef    = varargin{1}.YawVelZRef;
            elseif nargin == 6
                self.PosXGRef      = varargin{2};
                self.PosYGRef      = varargin{3};
                self.YawZRef       = varargin{4};
                self.VelXVRef      = varargin{5};
                self.YawVelZRef    = varargin{6};

            end
        end

    end
end
