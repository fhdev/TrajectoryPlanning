%% ChassisQrLonBndCon ******************************************************************************
% [Summary]
%   This class represents boundary conditions for longitudinal quarter vehicle chassis model.
%
% [Used in]
%   VehQrLonBndCon
%
% [Subclasses]
%   ChassisQrBndCon
% **************************************************************************************************
classdef ChassisQrLonBndCon < handle
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % longitudinal position in ground-fixed cs. [m]
        PosXG
        % longitudinal velocity in vehicle-fixed cs. [m/s]
        VelXV
        
    end
    
    %% Instance methods ============================================================================    
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.PosXG(self, value)
            if ~isequal(self.PosXG, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: PosXG must be a numeric scalar!')
                end
                self.PosXG = value;
            end
        end
        
        function set.VelXV(self, value)
            if ~isequal(self.VelXV, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: VelXV must be a numeric scalar!')
                end
                self.VelXV = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = ChassisQrLonBndCon(varargin)
            if nargin == 0
                self.PosXG = 0;
                self.VelXV = 0;
            elseif nargin == 1
                self.PosXG = varargin{1}.PosXG;
                self.VelXV = varargin{1}.VelXV;
            elseif nargin == 2
                self.PosXG = varargin{1};
                self.VelXV = varargin{2};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end
    end
end