%% Cleanup
clear;
%% Create optimal and constrained-optimal planner
plannerOpt = PlannerOpt(PlannerOpt_Default());

%% Generate ranges for trajectories
xRange = 40;
yMultRange = linspace(0, 8, 9)' / 40;
vRange = 10;
rMultRange = [0; 1];

nSamples = length(xRange) * length(yMultRange) * length(vRange) * length(rMultRange);
samplesOpt = repmat(struct('In', [], 'Out', []), nSamples, 1);

%% Generate samples
iSample = 1;
for ix = 1 : length(xRange)
    xf = xRange(ix);
    for iy = 1 : length(yMultRange)
        yf = yMultRange(iy) * xf;
        for iv = 1 : length(vRange)
            vi = vRange(iv);
            vf = vi;
            [rCirc, drCirc] = PlannerConFcnVehState.CircConstr(0, 0, xf, 2 * yf, vf);
            for ir = 1 : length(rMultRange)
                rf = rMultRange(ir) * rCirc;
                dri = 0;
                if rf == 0
                    drf = 0;
                else
                    yf = 2 * yf;       
                    drf = drCirc;
                end
                
                %% Create trajectory plan
                % environment input
                input.EnvIn.Time          = 1;
                input.EnvIn.FrictionFront = 1;
                input.EnvIn.FrictionRear  = 1;

                % initial condition
                input.InitCon.PosXG       = 0;    
                input.InitCon.PosYG       = 0;          
                input.InitCon.YawZ        = 0;
                input.InitCon.VelYV       = 0;
                input.InitCon.VelXV         = vi;
                input.InitCon.YawVelZ       = dri;
                % final condition
                input.FinalCon.Time         = nan;
                input.FinalCon.PosXG        = xf;
                input.FinalCon.VelXV        = vf;
                input.FinalCon.PosYG        = yf;
                input.FinalCon.AccInrtYV    = nan;
                input.FinalCon.YawZ         = rf;
                input.FinalCon.YawVelZ      = drf;
                input.FinalCon.VelXG        = nan;  
                input.FinalCon.AccXG        = nan;
                input.FinalCon.AccInrtXV    = nan;
                input.FinalCon.VelYG        = nan;
                input.FinalCon.VelYV        = nan;
                input.FinalCon.AccYG        = nan;
                input.FinalCon.YawAccZ      = nan;
                % optimization variable
                input.OptVarInit.VelRef             = vi;
                input.OptVarInit.YRef               = [0; 0];
                input.OptVarInit.SampleDistance     = 1e-2;
                input.OptVarInit.YEnd               = yf;
                input.OptVarInit.YawEnd             = rf;
                input.OptVarInit.YawRateEnd         = drf;
                % cost function
                input.CostTrackWeight.Time          = nan;
                input.CostTrackWeight.SteerWhlAng   = nan;
                input.CostTrackWeight.CrossTrackErr = 1;
                input.CostTrackWeight.HeadingErr    = rad2deg(0.2);
                input.CostStateWeight.Time          = nan;
                input.CostStateWeight.PosXG         = nan;
                input.CostStateWeight.VelXG         = nan;
                input.CostStateWeight.VelXV         = nan;
                input.CostStateWeight.AccXG         = nan;
                input.CostStateWeight.AccInrtXV     = nan;
                input.CostStateWeight.PosYG         = nan;
                input.CostStateWeight.VelYG         = nan;
                input.CostStateWeight.VelYV         = nan;
                input.CostStateWeight.AccYG         = nan;
                input.CostStateWeight.AccInrtYV     = 1;
                input.CostStateWeight.YawZ          = nan;
                input.CostStateWeight.YawVelZ       = nan;
                input.CostStateWeight.YawAccZ       = nan;
                
                % Calculate optimal trajectory
                plannerOptIn = PlannerOptIn(input);
                plannerOptOut = plannerOpt.CalculateTrajectory(plannerOptIn);
                
                %% Save results
                samplesOpt(iSample).In = obj2struct(plannerOptIn);
                samplesOpt(iSample).Out = obj2struct(plannerOptOut);
                
                %% Increase counter
                iSample = iSample + 1;
                
            end
        end
    end
end

% Save to file as well
plannerOpt = obj2struct(plannerOpt);

fileName = 'sampleTrajectories.mat';
filePath = fullfile(pwd, 'data', 'Pub_2020_IFACWC2020', fileName);
save(filePath, 'samplesOpt', 'plannerOpt', '-v7.3');
