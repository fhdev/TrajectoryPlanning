// MATLAB SIMULINK S-FUNCTION
//
// Author:          Ferenc Heged�s
// Date:            14.12.2017.
// Version:         1.0
// Description:     Simple wheel model with Magic Formula tire model.
// Equation and page numbers are from Hans Pacejka - Tyre and Vehicle Dynamics (2nd edition), 2006

// S-FUNCTION HEADER
// Macro S_FUNCTION_NAME must be specified as the name of the S-function
#define S_FUNCTION_NAME	 wheelMfSfun
#define S_FUNCTION_LEVEL 2

// INCLUDE FILES
// Include simstruc.h for the definition of the SimStruct and its associated macro definitions. 
#include "simstruc.h"
#include <math.h>
#include "types_common.h"
#include "math_common.h"

// MACROS
// PARAMETERS
// Basic parameters
#define r_p                 rWork[ParamIndex_r]
#define theta_p             rWork[ParamIndex_theta]
// Parameters of magic formula
#define Bx_p                rWork[ParamIndex_Bx]
#define By_p                rWork[ParamIndex_By]
#define Cx_p                rWork[ParamIndex_Cx]
#define Cy_p                rWork[ParamIndex_Cy]
#define Ex_p                rWork[ParamIndex_Ex]
#define Ey_p                rWork[ParamIndex_Ey]
// Parameters of rolling resistance model
#define Arr_p               rWork[ParamIndex_Arr]
#define Brr_p               rWork[ParamIndex_Brr]
#define Crr_p               rWork[ParamIndex_Crr]
#define vr_p                rWork[ParamIndex_vr]
// Parameters of transient slip model
#define lx0_p               rWork[ParamIndex_lx0]
#define lxm_p               rWork[ParamIndex_lxm]
#define klx0_p              rWork[ParamIndex_klx0]
#define vlx_p               rWork[ParamIndex_vlx]
#define ly0_p               rWork[ParamIndex_ly0]
#define lym_p               rWork[ParamIndex_lym]
// Parameters of brake torque application model
#define vb_p                rWork[ParamIndex_vb]
#define vbc_p               rWork[ParamIndex_vbc]
// Initial conditions
#define d_ry0_p             rWork[ParamIndex_d_ry0]
#define ry0_p               rWork[ParamIndex_ry0]
#define sx0_p               rWork[ParamIndex_sx0]
#define sy0_p               rWork[ParamIndex_sy0]
// STORED VARIABLES
#define v_w                 rWork[RWorkIndex_v]
#define dd_ry_w             rWork[RWorkIndex_dd_ry]
#define Fx_V              rWork[RWorkIndex_Fx_V]
#define Fy_V              rWork[RWorkIndex_Fy_V]
// STATES
#define d_ry_s              X[StateIndex_d_ry]
#define ry_s                X[StateIndex_ry]
#define sx_s                X[StateIndex_sx]
#define sy_s                X[StateIndex_sy]
// STATE DERIVATIVES
// angular acceleration of the wheel [rad/s^2]
#define dd_ry_d             dX[StateIndex_d_ry]
// angular velocity of the wheel [rad/s]
#define d_ry_d              dX[StateIndex_ry]
// derivative of transient longitudinal slip [1/s]
#define d_sx_d              dX[StateIndex_sx]
// derivative of transient lateral slip [1/s]
#define d_sy_d              dX[StateIndex_sy]
// CONSTANT DEFINITIONS

// CONSTANTS

// GLOBAL VARIABLES
// Indexes for real work vector.
enum RWorkIndex
{
    // PARAMETERS
    // Basic parameters
    // wheel radius [m]
    ParamIndex_r,
    // wheel moment of inertia about own y-axis [kg.m^2]
    ParamIndex_theta,
    // Parameters of Magic Formula
    // stiffness factors in the longitudinal and lateral directions [1]
    ParamIndex_Bx,
    ParamIndex_By,
    // shape factors in the longitudinal and lateral directions [1]
    ParamIndex_Cx, 
    ParamIndex_Cy,
    // curvature factor in the longitudinal and lateral directions [1]
    ParamIndex_Ex,
    ParamIndex_Ey,
    // Parameters of rolling resistance model
    // velocity independent rolling resistance factor [1]
    ParamIndex_Arr,
    // linearly velocity dependent rolling resistance factor [s/m]
    ParamIndex_Brr,
    // squarely velocity dependent rollint resistance factor [s^2/m^2]
    ParamIndex_Crr,
    // longitudinal velocity at which rolling resistance reaches its final value [m/s]
    ParamIndex_vr,
    // Parameters of transient slip model
    // longitudinal relaxation length at zero longitudinal slip [m]
    ParamIndex_lx0,
    // minimal longitudinal relaxation length [m]
    ParamIndex_lxm,
    // standstill longitudinal slip damping [Ns/m]
    ParamIndex_klx0,
    // longitudinal velocity at which slip damping is off [m/s^2]
    ParamIndex_vlx,
    // lateral relaxation length at zero longitudinal slip [m]
    ParamIndex_ly0,
    // minimal lateral relaxation length [m]
    ParamIndex_lym,
    // Parameters of brake torque application model
    // longitudinal velocity at which braking torque reaches it's final value [m/s]
    ParamIndex_vb,
    // braking torque dependent correction in longitudinal velocity at which braking torque reaches
    // it's final value [1 / Ns]
    ParamIndex_vbc,
    // Initial conditions
    // initial angular velocity [rad/s]
    ParamIndex_d_ry0,
    // initial angle of rotation [rad]
    ParamIndex_ry0,
    // initial longitudinal transient slip [1]
    ParamIndex_sx0,
    // initial lateral transient slip [1]
    ParamIndex_sy0,
    // NUMBER OF PARAMETERS
    ParamIndex_SIZE,
    // STORED VARIABLES
    // longitudinal wheel speed from angular velocity (r.omega) [m/s]
    RWorkIndex_v, 
    // angular acceleration of the wheel [rad/s^2]
    RWorkIndex_dd_ry,
    // longitudinal and lateral tire forces in vehicle-fixed coordinate system [N]
    RWorkIndex_Fx_V,
    RWorkIndex_Fy_V,
    // SIZE OF REAL WORK VECTOR
    RWorkIndex_SIZE
};
// Indexes for inputs
enum InputIndex
{
    // driving torque [Nm]
    InputIndex_Md,
    // braking torque [Nm]
    InputIndex_Mb,
    // friction coefficient between road surface and tire [1]
    InputIndex_mu,
    // steering angle at wheel [rad]
    InputIndex_delta,
    // vertical force on the wheel in vehicle-fixed coordinate system [N]
    InputIndex_Fz_V,
    // wheel centerpoint velocities in vehicle-fixed coordinate system [m/s]
    InputIndex_d_x_V,
    InputIndex_d_y_V,
    // SIZE OF INPUT VECTOR
    InputIndex_SIZE
};
// Indexes for outputs.
enum OutputIndex
{
    // longitudinal and lateral forces in vehicle-fixed coordinate system [N]
    OutputIndex_Fx_V,
    OutputIndex_Fy_V,
    // angular acceleration of the wheel [rad/s^2]
    OutputIndex_dd_ry,
    // angular velocity of the wheel [rad/s]
    OutputIndex_d_ry,
    // rotation angle of the wheel [rad]
    OutputIndex_ry,
    // longitudinal and lateral transient slips [1]
    OutputIndex_sx,
    OutputIndex_sy,
    // SIZE OF OUTPUT VECTOR
    OutputIndex_SIZE
};
// Indexes for state vector.
enum StateIndex
{
    // angular velocity of the wheel [rad/s]
    StateIndex_d_ry,
    // rotation angule of the wheel [rad]
    StateIndex_ry,
    // longitudinal and lateral transient slips [1]
    StateIndex_sx,
    StateIndex_sy,
    // SIZE OF STATE VECTOR
    StateIndex_SIZE
};

// FUNCTION DECLARATIONS

// FUNCTION DEFINITIONS
// S-function methods
// Parameter handling methods. These methods are not supported by RTW. 
#undef MDL_CHECK_PARAMETERS
#if defined(MDL_CHECK_PARAMETERS) && defined(MATLAB_MEX_FILE)
// Function: mdlCheckParameters 
static void mdlCheckParameters(SimStruct *S)
{

}
#endif // MDL_CHECK_PARAMETERS 

#undef MDL_PROCESS_PARAMETERS
#if defined(MDL_PROCESS_PARAMETERS) && defined(MATLAB_MEX_FILE)
// Function: mdlProcessParameters
static void mdlProcessParameters(SimStruct *S)
{
}
#endif // MDL_PROCESS_PARAMETERS 

// Configuration and execution methods. 
// Function: mdlInitializeSizes
static void mdlInitializeSizes(SimStruct *S)
{
    // Set the number of expected parameters. 
    ssSetNumSFcnParams(S, ParamIndex_SIZE);
    // Return if the number of expected parameters != the number of actual parameters. Simulink will
    // generate error message about parameter mismatch.
    if (ssGetNumSFcnParams(S) != ssGetSFcnParamsCount(S))
    {
        return;
    }
    // Set the tunability of parameters. 
    // ssSetSFcnParamTunable(S, 0, 0);
    // Set the number of continuous and discrete states. 
    ssSetNumContStates(S, StateIndex_SIZE);
    ssSetNumDiscStates(S, 0);
    // Set the number and width(s) of input port(s). 
    if (!ssSetNumInputPorts(S, InputIndex_SIZE))
    {
        return;
    }
    ssSetInputPortWidth(S, InputIndex_Md, 1);
    ssSetInputPortWidth(S, InputIndex_Mb, 1);
    ssSetInputPortWidth(S, InputIndex_mu, 1);
    ssSetInputPortWidth(S, InputIndex_delta, 1);
    ssSetInputPortWidth(S, InputIndex_Fz_V, 1);
    ssSetInputPortWidth(S, InputIndex_d_x_V, 1);
    ssSetInputPortWidth(S, InputIndex_d_y_V, 1);
    // Set if the signal on the port must be stored on a contiguous memory area.
    // If the option is set, you can use ssGetInputPortSignal to get the signal on the port.
    // Otherwise, ssGetInputSignalPtrs should be used.
    ssSetInputPortRequiredContiguous(S, InputIndex_Md, 1);
    ssSetInputPortRequiredContiguous(S, InputIndex_Mb, 1);
    ssSetInputPortRequiredContiguous(S, InputIndex_mu, 1);
    ssSetInputPortRequiredContiguous(S, InputIndex_delta, 1);
    ssSetInputPortRequiredContiguous(S, InputIndex_Fz_V, 1);
    ssSetInputPortRequiredContiguous(S, InputIndex_d_x_V, 1);
    ssSetInputPortRequiredContiguous(S, InputIndex_d_y_V, 1);   
    // Set direct feedthrough flag. 
    ssSetInputPortDirectFeedThrough(S, InputIndex_Md, 0);
    ssSetInputPortDirectFeedThrough(S, InputIndex_Mb, 0);
    ssSetInputPortDirectFeedThrough(S, InputIndex_mu, 0);
    ssSetInputPortDirectFeedThrough(S, InputIndex_delta, 0);
    ssSetInputPortDirectFeedThrough(S, InputIndex_Fz_V, 0);
    ssSetInputPortDirectFeedThrough(S, InputIndex_d_x_V, 0);
    ssSetInputPortDirectFeedThrough(S, InputIndex_d_y_V, 0);
    // Set input data types
    ssSetInputPortDataType(S, InputIndex_Md, SS_DOUBLE);
    ssSetInputPortDataType(S, InputIndex_Mb, SS_DOUBLE);
    ssSetInputPortDataType(S, InputIndex_mu, SS_DOUBLE);
    ssSetInputPortDataType(S, InputIndex_delta, SS_DOUBLE);
    ssSetInputPortDataType(S, InputIndex_Fz_V, SS_DOUBLE);
    ssSetInputPortDataType(S, InputIndex_d_x_V, SS_DOUBLE);
    ssSetInputPortDataType(S, InputIndex_d_y_V, SS_DOUBLE);
    // Set the number and width(s) of output port(s). 
    if (!ssSetNumOutputPorts(S, OutputIndex_SIZE))
    {
        return;
    }
    ssSetOutputPortWidth(S, OutputIndex_Fx_V, 1);
    ssSetOutputPortWidth(S, OutputIndex_Fy_V, 1);
    ssSetOutputPortWidth(S, OutputIndex_dd_ry, 1);
    ssSetOutputPortWidth(S, OutputIndex_d_ry, 1);
    ssSetOutputPortWidth(S, OutputIndex_ry, 1);
    ssSetOutputPortWidth(S, OutputIndex_sx, 1);
    ssSetOutputPortWidth(S, OutputIndex_sy, 1);
    // Set output data types
    ssSetOutputPortDataType(S, OutputIndex_Fx_V, SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_Fy_V, SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_dd_ry, SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_d_ry, SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_ry, SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_sx, SS_DOUBLE);
    ssSetOutputPortDataType(S, OutputIndex_sy, SS_DOUBLE);
    // Set the number of sample times. 
    ssSetNumSampleTimes(S, 1);
    // Set the number of work vector elements. 
    ssSetNumRWork(S, RWorkIndex_SIZE);
    ssSetNumIWork(S, 0);
    ssSetNumPWork(S, 0);
    // Set the number of modes. 
    ssSetNumModes(S, 0);
    // Set the number of non-sampled zero crossings that need to be detected. 
    ssSetNumNonsampledZCs(S, 0);
    // Specify the sim state compliance to be same as a built-in block. 
    ssSetSimStateCompliance(S, USE_DEFAULT_SIM_STATE);
    // Set options. 
    ssSetOptions(S, 0);
}

#undef MDL_SET_INPUT_PORT_FRAME_DATA
#if defined(MDL_SET_INPUT_PORT_FRAME_DATA) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortFrameData 
static void mdlSetInputPortFrameData(SimStruct *S, int portIndex, Frame_T frameData)
{
}
#endif // MDL_SET_INPUT_PORT_FRAME_DATA 

#undef MDL_SET_INPUT_PORT_WIDTH
#if defined(MDL_SET_INPUT_PORT_WIDTH) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortWidth 
static void mdlSetInputPortWidth(SimStruct *S, int portIndex, int width)
{
}
#endif // MDL_SET_INPUT_PORT_WIDTH 

#undef MDL_SET_OUTPUT_PORT_WIDTH
#if defined(MDL_SET_OUTPUT_PORT_WIDTH) && defined(MATLAB_MEX_FILE)
// Function: mdlSetOutputPortWidth
static void mdlSetOutputPortWidth(SimStruct *S, int portIndex, int width)
{
}
#endif // MDL_SET_OUTPUT_PORT_WIDTH 

#undef MDL_SET_INPUT_PORT_DIMENSION_INFO
#if defined(MDL_SET_INPUT_PORT_DIMENSION_INFO) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortDimensionInfo
static void mdlSetInputPortDimensionInfo(SimStruct *S, int_T portIndex, const DimsInfo_T *dimsInfo)
{
}
#endif // MDL_SET_INPUT_PORT_DIMENSION_INFO 

#undef MDL_SET_OUTPUT_PORT_DIMENSION_INFO
#if defined(MDL_SET_OUTPUT_PORT_DIMENSION_INFO) && defined(MATLAB_MEX_FILE)
// Function: mdlSetOutputPortDimensionInfo 
static void mdlSetOutputPortDimensionInfo(SimStruct *S, int_T portIndex, const DimsInfo_T *dimsInfo)
{
}
#endif // MDL_SET_OUTPUT_PORT_DIMENSION_INFO 

#undef MDL_SET_DEFAULT_PORT_DIMENSION_INFO
#if defined(MDL_SET_DEFAULT_PORT_DIMENSION_INFO) && defined(MATLAB_MEX_FILE)
// Function: mdlSetDefaultPortDimensionInfo
static void mdlSetDefaultPortDimensionInfo(SimStruct *S)
{
}
#endif // MDL_SET_DEFAULT_PORT_DIMENSION_INFO 


#undef MDL_SET_INPUT_PORT_SAMPLE_TIME
#if defined(MDL_SET_INPUT_PORT_SAMPLE_TIME) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortSampleTime
static void mdlSetInputPortSampleTime(SimStruct *S, 
                                      int_T		portIdx,
                                      real_T	sampleTime,
                                      real_T	offsetTime)
{
}
#endif // MDL_SET_INPUT_PORT_SAMPLE_TIME 

#undef MDL_SET_OUTPUT_PORT_SAMPLE_TIME
#if defined(MDL_SET_OUTPUT_PORT_SAMPLE_TIME) && defined(MATLAB_MEX_FILE)
// Function: mdlSetOutputPortSampleTime
static void mdlSetOutputPortSampleTime(SimStruct *S,
                                       int_T	 portIdx,
                                       real_T	 sampleTime,
                                       real_T	 offsetTime)
{
}
#endif // MDL_SET_OUTPUT_PORT_SAMPLE_TIME 

// Function: mdlInitializeSampleTimes
static void mdlInitializeSampleTimes(SimStruct *S)
{
    // Set continuous sample time without offset.
    ssSetSampleTime(S, 0, CONTINUOUS_SAMPLE_TIME);
    ssSetOffsetTime(S, 0, 0.0);
}

#undef MDL_SET_INPUT_PORT_DATA_TYPE
#if defined(MDL_SET_INPUT_PORT_DATA_TYPE) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortDataType
static void mdlSetInputPortDataType(SimStruct *S, int portIndex,DTypeId dType)
{
}
#endif // MDL_SET_INPUT_PORT_DATA_TYPE 

#undef MDL_SET_OUTPUT_PORT_DATA_TYPE
#if defined(MDL_SET_OUTPUT_PORT_DATA_TYPE) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortDataType
static void mdlSetOutputPortDataType(SimStruct *S, int portIndex, DTypeId dType)
{
}
#endif // MDL_SET_OUTPUT_PORT_DATA_TYPE 

#undef MDL_SET_DEFAULT_PORT_DATA_TYPES
#if defined(MDL_SET_DEFAULT_PORT_DATA_TYPES) && defined(MATLAB_MEX_FILE)
// Function: mdlSetDefaultPortDataTypes
static void mdlSetDefaultPortDataTypes(SimStruct *S)
{
}
#endif // MDL_SET_DEFAULT_PORT_DATA_TYPES 

#undef MDL_SET_INPUT_PORT_COMPLEX_SIGNAL
#if defined(MDL_SET_INPUT_PORT_COMPLEX_SIGNAL) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortComplexSignal
static void mdlSetInputPortComplexSignal(SimStruct *S, int portIndex, CSignal_T cSignalSetting)
{
}
#endif // MDL_SET_INPUT_PORT_COMPLEX_SIGNAL 

#undef MDL_SET_OUTPUT_PORT_COMPLEX_SIGNAL
#if defined(MDL_SET_OUTPUT_PORT_COMPLEX_SIGNAL) && defined(MATLAB_MEX_FILE)
// Function: mdlSetInputPortComplexSignal
static void mdlSetOutputPortComplexSignal(SimStruct *S, int portIndex, CSignal_T cSignalSetting)
{
}
#endif // MDL_SET_OUTPUT_PORT_COMPLEX_SIGNAL 

#undef MDL_SET_DEFAULT_PORT_COMPLEX_SIGNALS
#if defined(MDL_SET_DEFAULT_PORT_COMPLEX_SIGNALS) && defined(MATLAB_MEX_FILE)
// Function: mdlSetDefaultPortComplexSignals
static void mdlSetDefaultPortComplexSignals(SimStruct *S)
{
}
#endif // MDL_SET_DEFAULT_PORT_COMPLEX_SIGNALS 

#undef MDL_SET_WORK_WIDTHS
#if defined(MDL_SET_WORK_WIDTHS) && defined(MATLAB_MEX_FILE)
// Function: mdlSetWorkWidths
static void mdlSetWorkWidths(SimStruct *S)
{
}
#endif // MDL_SET_WORK_WIDTHS 

#define MDL_INITIALIZE_CONDITIONS
#if defined(MDL_INITIALIZE_CONDITIONS)
// Function: mdlInitializeConditions
static void mdlInitializeConditions(SimStruct *S)
{
}
#endif // MDL_INITIALIZE_CONDITIONS 

#define MDL_START
#if defined(MDL_START)
// Function: mdlStart
static void mdlStart(SimStruct *S)
{
    // Get real work vector.
    Float64_T *rWork = ssGetRWork(S);
    // Get state vector.
    Float64_T *X = ssGetContStates(S);
    // Obtain parameter values
    r_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_r)));
    theta_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_theta)));
    Bx_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_Bx)));
    By_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_By)));
    Cx_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_Cx)));
    Cy_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_Cy)));
    Ex_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_Ex)));
    Ey_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_Ey)));
    Arr_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_Arr)));
    Brr_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_Brr)));
    Crr_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_Crr)));
    vr_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_vr)));
    lx0_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_lx0)));
    lxm_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_lxm)));
    klx0_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_klx0)));
    vlx_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_vlx)));
    ly0_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_ly0)));
    lym_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_lym)));
    vb_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_vb)));
    vbc_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_vbc)));
    // Initial state values
    d_ry0_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_d_ry0)));
    ry0_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_ry0)));
    sx0_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_sx0)));
    sy0_p = *(mxGetPr(ssGetSFcnParam(S, ParamIndex_sy0)));
    // Set initial values for stored variables
    v_w = d_ry0_p * r_p;
    dd_ry_w = 0.0;
    Fx_V = 0.0;
    Fy_V = 0.0;
    // Set initial conditions
    d_ry_s = d_ry0_p;
    ry_s = ry0_p;
    sx_s = sx0_p;
    sy_s = sy0_p;
}
#endif //  MDL_START 

#undef MDL_SIM_STATE
#if defined(MDL_SIM_STATE)
// Function: mdlGetSimState
static mxArray* mdlGetSimState(SimStruct* S)
{
}

// Function: mdlSetSimState
static void mdlSetSimState(SimStruct* S, const mxArray* inSimState)
{
}
#endif // MDL_SIM_STATE 

#undef MDL_GET_TIME_OF_NEXT_VAR_HIT
#if defined(MDL_GET_TIME_OF_NEXT_VAR_HIT) && (defined(MATLAB_MEX_FILE) || defined(NRT))
// Function: mdlGetTimeOfNextVarHit
static void mdlGetTimeOfNextVarHit(SimStruct *S)
{
  time_T timeOfNextHit = ssGetT(S) // + offset  ;
  ssSetTNext(S, timeOfNextHit);
}
#endif // MDL_GET_TIME_OF_NEXT_VAR_HIT 

#undef MDL_ZERO_CROSSINGS
#if defined(MDL_ZERO_CROSSINGS) && (defined(MATLAB_MEX_FILE) || defined(NRT))
// Function: mdlZeroCrossings
static void mdlZeroCrossings(SimStruct *S)
{
}
#endif // MDL_ZERO_CROSSINGS 

// Function: mdlOutputs
static void mdlOutputs(SimStruct *S, int_T tid)
{
    // Get real work vector (parameters, stored values)
    const Float64_T *const rWork = ssGetRWork(S);
    // Get state vector
    const Float64_T *const X = ssGetContStates(S);
    // Get output signal pointers
    Float64_T *const Fx_V_o = ssGetOutputPortRealSignal(S, OutputIndex_Fx_V);
    Float64_T *const Fy_V_o = ssGetOutputPortRealSignal(S, OutputIndex_Fy_V);
    Float64_T *const dd_phi_o = ssGetOutputPortRealSignal(S, OutputIndex_dd_ry);
    Float64_T *const d_phi_o = ssGetOutputPortRealSignal(S, OutputIndex_d_ry);
    Float64_T *const phi_o = ssGetOutputPortRealSignal(S, OutputIndex_ry);
    Float64_T *const sx_o = ssGetOutputPortRealSignal(S, OutputIndex_sx);
    Float64_T *const sy_o = ssGetOutputPortRealSignal(S, OutputIndex_sy);
    // Copy tire forces in vehicle-fixed coordinate system to output
    Fx_V_o[0] = Fx_V;
    Fy_V_o[0] = Fy_V;
    // Copy angular position, velocity, and acceleration of wheel to output
    dd_phi_o[0] = dd_ry_w;
    d_phi_o[0] = d_ry_s;
    phi_o[0] = ry_s;
    // Copy slip values to output
    sx_o[0] = sx_s;
    sy_o[0] = sy_s;
}

#undef MDL_UPDATE
#if defined(MDL_UPDATE)
// Function: mdlUpdate
static void mdlUpdate(SimStruct *S, int_T tid)
{
}
#endif // MDL_UPDATE 

#define MDL_DERIVATIVES
#if defined(MDL_DERIVATIVES)
// Function: mdlDerivatives
static void mdlDerivatives(SimStruct *S)
{
    // Get real work vector (parameters, stored values)
    Float64_T *const rWork = ssGetRWork(S);
    // Get state vector
    Float64_T *const X = ssGetContStates(S);
    // Get state vector derivative
    Float64_T *const dX = ssGetdX(S);
    // Get input signal pointers
    const Float64_T *const Md_i = ssGetInputPortRealSignal(S, InputIndex_Md);
    const Float64_T *const Mb_i = ssGetInputPortRealSignal(S, InputIndex_Mb);
    const Float64_T *const mu_i = ssGetInputPortRealSignal(S, InputIndex_mu);
    const Float64_T *const delta_i = ssGetInputPortRealSignal(S, InputIndex_delta);
    const Float64_T *const Fz_V_i = ssGetInputPortRealSignal(S, InputIndex_Fz_V);
    const Float64_T *const d_x_V_i = ssGetInputPortRealSignal(S, InputIndex_d_x_V);
    const Float64_T *const d_y_V_i = ssGetInputPortRealSignal(S, InputIndex_d_y_V);
    // Calculation of velocities (longitudinal and lateral) in wheel-fixed coordinate system.
    Float64_T sin_delta = sin(delta_i[0]);
    Float64_T cos_delta = cos(delta_i[0]);
    Float64_T d_x_W = +cos_delta * d_x_V_i[0] + sin_delta * d_y_V_i[0];
    Float64_T d_y_W = -sin_delta * d_x_V_i[0] + cos_delta * d_y_V_i[0];
    // Calculation of slip damping factors (Eq. 8.128 @ P. 407)
    Float64_T klx = 0.0;
    Float64_T abs_d_x_W = fabs(d_x_W);
    if (abs_d_x_W <= vlx_p)
    {
        klx = 0.5 * klx0_p * (1 + cos(pi * abs_d_x_W / vlx_p));
    }
    // Calculation of slip stiffness values (Eq. 8.115 @ P. 405 & Eq. 1.6 @ P. 6)
    Float64_T Fz_W = Fz_V_i[0];
    Float64_T Dx = Fz_W * mu_i[0];
    Float64_T Dy = Fz_W * mu_i[0];
    Float64_T CFx = Bx_p * Cx_p * Dx;
    Float64_T CFy = By_p * Cy_p * Dy;
    // Calculation of longitudinal wheel speed according to angular velocity.
    v_w = r_p * d_ry_s;
    // Calculation of damped slip values (Eq. 8.127 @ P. 407)
    Float64_T sxl = sx_s + klx / CFx * (v_w - d_x_W);
    Float64_T syl = sy_s;
    // Calculation of tire forces in wheel-fixed coordinate system (Eq. 8.109 @ P. 404)
    Float64_T Fx_W = Dx * sin(Cx_p * atan(Bx_p * sxl - Ex_p * (Bx_p * sxl - atan(Bx_p * sxl))));
    Float64_T Fy_W = Dy * sin(Cy_p * atan(By_p * syl - Ey_p * (By_p * syl - atan(By_p * syl))));
    // Calculation of slip superposition (own deduction based on Coulomb-friction)
    if ((fabs(sxl) > 0.0) && (fabs(syl) > 0.0))
    {
        Float64_T Fx_W2 = Fx_W * Fx_W;
        Float64_T Fy_W2 = Fy_W * Fy_W;
        Float64_T sxl2 = sxl * sxl;
        Float64_T syl2 = syl * syl;
        Fx_W = sqrt(Fx_W2 * Fy_W2 / (Fy_W2 + syl2 / sxl2 * Fx_W2)) * fsign(sxl);
        Fy_W = sqrt(Fx_W2 * Fy_W2 / (Fx_W2 + sxl2 / syl2 * Fy_W2)) * fsign(syl);
    }
    // Calculation of slip - dependent relaxation lengths(Eq. 8.121 @ P. 406 & Eq. 7.36 @ P. 349)
    Float64_T lx = fmax(lx0_p * (1 - CFx / (3 * Dx) * fabs(sx_s)), lxm_p);
    Float64_T ly = fmax(ly0_p * (1 - CFy / (3 * Dy) * fabs(sy_s)), lym_p);
    // Calculation of rolling resistance torque
    // (https://www.mathworks.com/help/physmod/sdl/ref/rollingresistance.html)
    Float64_T abs_v_w = fabs(v_w);
    Float64_T sign_v_w = fsign(v_w);
    Float64_T Mrr = Fz_W * r_p * (Arr_p + Brr_p * abs_v_w + Crr_p * v_w * v_w) * sign_v_w;
    if (abs_v_w < vr_p)
    {
        Mrr *= 0.5 * (1 - cos(pi * abs_v_w / vr_p));
    }
    // Calculation of acting braking torque
    Float64_T vb = vb_p + vbc_p * fabs(Mb_i[0]);
    Float64_T Mba = Mb_i[0] * sign_v_w;
    if (abs_v_w < vb)
    {
        Mba *= 0.5 * (1 - cos(pi * abs_v_w / vb));
    }
    // Calculation of tire forces in vehicle-fixed coordinate system.
    Fx_V = +cos_delta * Fx_W - sin_delta * Fy_W;
    Fy_V = +sin_delta * Fx_W + cos_delta * Fy_W;
    // Calculation of agular acceleration
    dd_ry_d = (Md_i[0] - Mba - r_p * Fx_W - Mrr) / theta_p;
    // Calculaton of agular velocity (obvious integration)
    d_ry_d = d_ry_s;
    // Calculation of transient longitudinal slip(Eq. 8.119 @ P. 406)
    d_sx_d = (r_p * d_ry_s - d_x_W - abs_d_x_W * sx_s) / lx;
    // Calculation of transient lateral slip(Eq. 7.35 @ P. 349)
    d_sy_d = (-d_y_W - abs_d_x_W * sy_s) / ly;
    // Store angular acceleration to be able to copy it to output
    dd_ry_w = dd_ry_d;
}
#endif // MDL_DERIVATIVES 

// Function: mdlTerminate
static void mdlTerminate(SimStruct *S)
{
}

#undef MDL_RTW
#if defined(MDL_RTW) && defined(MATLAB_MEX_FILE)
// Function: mdlRTW
static void mdlRTW(SimStruct *S)
{
}
#endif // MDL_RTW 

// Custom methods



// S-FUNCTION TRAILER
#ifdef	MATLAB_MEX_FILE	// Is this file being compiled as a MEX-file? 
#include "simulink.c"	// MEX-file interface mechanism 
#else
#include "cg_sfun.h"	// Code generation registration function 
#endif