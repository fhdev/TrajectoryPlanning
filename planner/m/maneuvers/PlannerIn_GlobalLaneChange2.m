function input = PlannerIn_GlobalLaneChange2()
% Global initial state
Xi = 100;
Yi = 50;
Ri = deg2rad(45);
% Get local input
input = PlannerIn_LaneChange();
% Transform
input.FinalCon = PlannerConIn.BndConGlobalFromLocal(input.FinalCon, Xi, Yi, Ri);
end