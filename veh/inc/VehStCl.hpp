#pragma once
#ifndef VEHSTCL_HPP
#define VEHSTCL_HPP

#include "Eigen/Dense"
#include "TypesTp.hpp"
#include "Ode.hpp"
#include "CtrlLqsV.hpp"
#include "CtrlLatSty.hpp"
#include "VehStOl.hpp"


namespace TrajectoryPlanning
{
	class VehStCl : public VehStOl
	{
	private:
		CtrlLqsV ctrlLon;
		CtrlLatSty ctrlLat;
		Float64_T timeInit;

		mutable Float64_T torqueDrv;
		mutable Float64_T torqueBrk;
		mutable Float64_T steerWhlAng;

	public:

		Int64_T nP() const override;
		Int64_T nX() const override;
		Int64_T nU() const override;
		Int64_T nY() const override;

		VehStCl(
			const ChassisSt &chassis, 
			const WheelMf &wheelFront, 
			const WheelMf &wheelRear,
			const SteerFo &steering,
			const CtrlLqsV &ctrlLon,
			const CtrlLatSty &ctrlLat,
			Float64_T timeInit);

		Int32_T	Derivatives(
			Float64_T t, 
			const Eigen::Ref<const Eigen::VectorXd> X,
			const Eigen::Ref<const Eigen::VectorXd> U,
			Eigen::Ref<Eigen::VectorXd> dX, 
			Eigen::Ref<Eigen::VectorXd> Y) const override;

		Int32_T Initialize(
			const Eigen::Ref<const Eigen::VectorXd> Xi, 
			Eigen::Ref<Eigen::VectorXd> X) const;
	};
}

#endif // !VEHSTCL_HPP