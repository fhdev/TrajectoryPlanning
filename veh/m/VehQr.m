%% VehQr *******************************************************************************************
% [Summary]
%   This class represents a quarter vehicle model that consists of a wheel and a coupled mass.
%
% [Used in]
%   user
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef VehQr < VehQrLon
    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------
        function value = setChassis(value)
            if isstruct(value)
                value = ChassisQr(value);
            end
            if ~isa(value, 'ChassisQr')
                error('ERROR: Chassis must be a ChassisQr object!')
            end
        end

        function value = setWheel(value)
            if isstruct(value)
                value = WheelMf(value);
            end
            if ~isa(value, 'WheelMf')
                error('ERROR: Wheel must be a struct or a WheelMf object!')
            end
        end

    end

    %% Instance methods ============================================================================
    methods
        %% Constructor -----------------------------------------------------------------------------
        function self = VehQr(varargin)
            self@VehQrLon(varargin{:});
        end

        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of the quarter vehicle model.
        %
        %   The coordinate systems of the vehicle, and the wheel are coinciding and rotated by the
        %   steering angle around the z-axis compared to the ground-fixed system!
        %   The vehicle-fixed coordinate system is: x points forward (driving direction); y points
        %   left; z points upwards.
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %   Chassis ------------------------------------------------------------------------------
        %       X(1) => PosXG:     longitudinal position in ground-fixed cs. [m]
        %       X(2) => VelXG:     longitudinal velocity in ground-fixed cs. [m/s]
        %       X(3) => PosYG:     lateral position in ground-fixed cs. [m]
        %       X(4) => VelYG:     lateral velocity in ground-fixed cs. [m/s]
        %   Wheel --------------------------------------------------------------------------------
        %       X(5) => PitchY:    angle of the wheel [rad]
        %       X(6) => PitchVelY: angular velocity of the wheel [rad/s]
        %       X(7) => SlipX:     longitudinal slip [1]
        %       X(8) => SlipY:     lateral slip [1]
        %
        %   U - input vector
        %       U(1) => TorqueDrv: driving torque [Nm]
        %       U(2) => TorqueBrk: braking torque [Nm]
        %       U(3) => Friction:  friction coefficient [1]
        %       U(4) => SteerAng:  steering angle [rad]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %   Chassis ------------------------------------------------------------------------------
        %       Y(1)  => AccXG:     longitudinal acceleration in ground-fixed cs. [m/s^2]
        %       Y(2)  => AccYG:     lateral acceleration in ground-fixed cs. [m/s^2]
        %       Y(3)  => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
        %       Y(4)  => AccInrtXV: longitudinal inertial acceleration in vehicle-fixed cs. [m/s^2]
        %       Y(5)  => VelYV:     lateral velocity in vehicle-fixed cs. [m/s]
        %       Y(6)  => AccInrtYV: lateral inertial acceleration in vehicle-fixed cs. [m/s^2]
        %   Wheel --------------------------------------------------------------------------------
        %       Y(7)  => PitchAccY: angular acceleration [rad/s]
        %       Y(8)  => DSlipX:    longitudinal slip derivative [1/s]
        %       Y(9)  => ForceXW:   longitudinal force in wheel-fixed cs. [N]
        %       Y(10) => DSlipY:    lateral slip derivative [1/s]
        %       Y(11) => ForceYW:   lateral force in wheel-fixed cs. [N]
        %       Y(12) => ForceZW:   vertical force in wheel-fixed cs. [N]
        % ------------------------------------------------------------------------------------------
        function [dX, Y] = StateEquation(self, t, X, U)
            %% Common

            % Vertical force
            Fz = self.Chassis.Mass * Com.Gravitation;

            % Sine and cosine of steering angle
            cosSteerAng = cos(U(4));
            sinSteerAng = sin(U(4));

            %% Wheel

            % Velocities in vehicle-fixed (:= wheel-fixed) cs.
            velXV = + cosSteerAng * X(2) + sinSteerAng * X(4);
            velYV = - sinSteerAng * X(2) + cosSteerAng * X(4);

            % Inputs
            % U(1) => TorqueDrv: driving torque [Nm]
            % U(2) => TorqueBrk: braking torque [Nm]
            % U(3) => Friction:  friction coefficient [1]
            % U(4) => ForceZW:   vertical force (tire load) in wheel-fixed cs. [N]
            % U(5) => VelXW:     lon. velocity of the wheel center point in wheel-fixed cs. [m/s]
            % U(6) => VelYW:     lat. velocity of the wheel center point in wheel-fixed cs. [m/s]

            [dXW, YW] = self.Wheel.StateEquation(t, X(5:8), [U(1:3); Fz; velXV; velYV]);

            % Outputs
            % Y(1) => PitchAccY: angular acceleration [rad/s]
            % Y(2) => DSlipX:    longitudinal slip derivative [1/s]
            % Y(3) => ForceXW:   longitudinal force in wheel-fixed cs. [N]
            % Y(4) => DSlipY:    lateral slip derivative [1/s]
            % Y(5) => ForceYW:   lateral force in wheel-fixed cs. [N]

            %% Chassis

            % Forces in ground-fixed cs.
            forceXG = + cosSteerAng * YW(3) - sinSteerAng * YW(5);
            forceYG = + sinSteerAng * YW(3) + cosSteerAng * YW(5);

            % Inputs
            % U(1) => ForceXG:  longitudinal tire force in ground-fixed cs. [N];
            % U(2) => ForceYG:  lateral tire force in ground-fixed cs. [N]

            [dXC, YC] = self.Chassis.StateEquation(t, X(1:4), [forceXG; forceYG]);

            % Outputs
            % Y(1)  => AccXG:     longitudinal acceleration in ground-fixed cs. [m/s^2]
            % Y(2)  => AccYG:     lateral acceleration in ground-fixed cs. [m/s^2]

            % Inertial acceleration in vehicle-fixed (wheel-fixed) cs.
            accInrtXV = + cosSteerAng * YC(1) + sinSteerAng * YC(2);
            accInrtYV = - sinSteerAng * YC(1) + cosSteerAng * YC(2);

            %% Set state derivatives
            dX = [dXC; dXW];

            %% Set outputs
            Y = [YC; velXV; accInrtXV; velYV; accInrtYV; YW; Fz];
        end

        %% Simulate --------------------------------------------------------------------------------
        % [Summary]
        %   This function simulates the quarter vehicle model.
        %
        % [Input]
        %   initCon  - initial conditions for the vehicle as VehQrBndCon object
        %   input    - input as VehQrIn object
        %
        % [Output]
        %   output   - output as VehQrOut object
        % ------------------------------------------------------------------------------------------
        function output = Simulate(self, initCon, input)
            %% Check arguments
            if ~isa(initCon, 'VehQrBndCon')
                error('ERROR: initCon must be a VehQrBndCon object!');
            end
            if ~isa(input, 'VehQrIn')
                error('ERROR: input must be a VehQrIn object!');
            end

            %% Input

            t = (input.Time(1) : self.SampleTime : input.Time(end))';
            if t(end) ~= input.Time(end)
                t(end + 1) = input.Time(end);
            end
            U = interp1q(input.Time, [input.TorqueDrv, input.TorqueBrk, input.Friction, input.SteerWhlAng / 18], t)';

            %% Initial conditions

            % Sine and cosine of steering angle
            cosSteerAng = cos(U(4, 1));
            sinSteerAng = sin(U(4, 1));

            % Velocities in ground-fixed cs.
            velXG0 = + cosSteerAng * initCon.VelXV - sinSteerAng * initCon.VelYV;
            velYG0 = + sinSteerAng * initCon.VelXV + cosSteerAng * initCon.VelYV;

            % Initial value of state vector
            X0 = [initCon.PosXG;
                  velXG0;
                  initCon.PosYG;
                  velYG0;
                  0;
                  initCon.VelXV / self.Wheel.Radius;
                  0;
                  0];

            %% Calculation of the motion
            [X, Y] = ode4(self, t, X0, U);
            %% Get results
            output = VehQrOut(t, ...
                ChassisQrOut(t, X(1,:), X(2,:), Y(1,:), Y(3,:), Y(4,:), X(3,:), X(4,:), Y(2,:), Y(5,:), Y(6,:)), ...
                WheelMfOut(t, X(5,:), X(6,:), Y(7, :), X(7, :), Y(8, :), Y(9, :), Y(12,:), X(8,:), Y(10,:), Y(11,:)));
        end

    end
end