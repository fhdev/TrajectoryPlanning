%% VehStOlNn *******************************************************************************************
% [Summary]
%   This class represents a planar nonlinear single track vehicle model.
%
% [Used in]
%   user
%
% [Subclasses]
%   VehStCl
% **************************************************************************************************
classdef VehStOlNn < handle
    %% Constant properties =========================================================================    
    properties (Constant)
        % number of inputs
        N_INPUT = 12
        % number of outputs
        N_OUTPUT = 10
        % data folder
        DATA_FOLDER = fullfile('data','VehModel_NN_2020', 'StateEq_V1');
    end
    
    %% Instance properties =========================================================================
    properties
        %% Internal properties ---------------------------------------------------------------------
        % neural network
        net
        % mean and standard deviation for input
        inputNorms
        % mean and standard deviation for output
        outputNorms
        
        %% Properties set in constructor -----------------------------------------------------------     
        % step size for ode solution
        SampleTime
        % path of .json file of net model
        ModelPath
        % path of .h5 file of net weights
        WeightPath


    end

    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.ModelPath(self, value)
            if ~isequal(self.ModelPath, value)
                if ~ischar(value) || ~isvector(value) || (size(value, 1) ~= 1)
                    error('ERROR: ModelPath must be a row vector of chars!')
                end
                self.ModelPath = value;
            end
        end

        function set.WeightPath(self, value)
            if ~isequal(self.WeightPath, value)
                if ~ischar(value) || ~isvector(value) || (size(value, 1) ~= 1)
                    error('ERROR: WeightPath must be a row vector of chars!')
                end
                self.WeightPath = value;
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehStOlNn(modelName, scaleMatIn, scaleMatOut)
            %% Set sample time
            self.SampleTime = 1e-3;
            
            %% Assemble pathes
            modelFolder = fullfile(self.DATA_FOLDER, ['training_', modelName]);
            modelPath = fullfile(modelFolder,  ['model_', modelName, '.json']);
            weightPath = fullfile(modelFolder,  ['weights_', modelName, '.h5']);
            self.ModelPath = modelPath;
            self.WeightPath = weightPath;
            
            %% Import input and output norms
            self.inputNorms = scaleMatIn;
            self.outputNorms = scaleMatOut;
            
            %% Import Keras model
            layers = importKerasLayers(modelPath, 'WeightFile', weightPath, 'ImportWeights', true);
            % Check imported model input and output size
            nInputs = max(layers(1).InputSize);
            assert(isequal(self.N_INPUT, nInputs), 'Number of inputs of Keras model is not %d but %d!', ...
                self.N_INPUT, nInputs);
            nOutputs = max(layers(end).OutputSize);
            assert(isequal(self.N_OUTPUT, nOutputs), 'Number of inputs of Keras model is not %d but %d!', ...
                self.N_OUTPUT, nOutputs);
            % Replace input layer from default imageInputLayer (MATLAB converts Keras Input to this) to
            % sequenceInputLayer and add final regressionLayer as MATLAB needs an output layer.
            layers = [sequenceInputLayer(self.N_INPUT, 'Name', 'input_1');
                layers(2:end);
                regressionLayer('Name', 'regression_1')];
            lGraph = layerGraph(layers);
            self.net = assembleNetwork(lGraph);
        end

        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of the planar nonlinear single track vehicle
        %   model.
        %
        %   The coordinate system of the vehicle is rotated around z-axis by the yaw angle
        %   compared to the ground-fixed system! The coordinate systems of the wheels are rotated
        %   by the steering angle compared to the vehicle-fixed system.
        %   The vehicle-fixed coordinate system is: x points forward (driving direction); y points
        %   left; z points upwards.
        %
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %   Chassis ------------------------------------------------------------------------------
        %       X(1)  => PosXG:     longitudinal position in ground-fixed cs. [m]
        %       X(2)  => VelXG:     longitudinal velocity in ground-fixed cs. [m/s]
        %       X(3)  => PosYG:     lateral position in ground-fixed cs. [m]
        %       X(4)  => VelYG:     lateral velocity in ground-fixed cs. [m/s]
        %       X(5)  => YawZ:      yaw (heading) angle in ground-fixed cs. [rad]
        %       X(6)  => YawVelZ:   yaw (heading) angular velocity in ground-fixed cs. [rad/s]
        %   WheelFront ---------------------------------------------------------------------------
        %       X(7)  => PitchY:    angle of the wheel [rad]
        %       X(8)  => PitchVelY: angular velocity of the wheel [rad/s]
        %       X(9)  => SlipX:     longitudinal slip [1]
        %       X(10) => SlipY:     lateral slip [1]
        %   WheelRear ----------------------------------------------------------------------------
        %       X(11) => PitchY:    angle of the wheel [rad]
        %       X(12) => PitchVelY: angular velocity of the wheel [rad/s]
        %       X(13) => SlipX:     longitudinal slip [1]
        %       X(14) => SlipY:     lateral slip [1]
        %   Steering -----------------------------------------------------------------------------
        %       X(15) => SteerAng:  wheel level steering angle [rad]
        %
        %   U - input vector
        %       U(1) => TorqueDrv:     driving torque [Nm]
        %       U(2) => TorqueBrk:     braking torque [Nm]
        %       U(3) => FrictionFront: friction coefficient front [1]
        %       U(4) => FrictionRear:  friction coefficient rear [1]
        %       U(5) => SteerWhlAng:   steering wheel angle [rad]
        %
        % [Output]
        %   dX - state derivative vector
        %
        %   Y  - output vector
        %   Chassis ------------------------------------------------------------------------------
        %       Y(1)  => AccXG:     longitudinal acceleration in ground-fixed cs. [m/s^2]
        %       Y(2)  => AccYG:     lateral acceleration in ground-fixed cs. [m/s^2]
        %       Y(3)  => AccInrtXV: longitudinal inertial acceleration in vehicle-fixed cs. [m/s^2]
        %       Y(4)  => AccInrtYV: lateral inertial acceleration in vehicle-fixed cs. [m/s^2]
        %       Y(5)  => YawAccZ:   yaw (heading) angular acceleration in ground-fixed cs. [rad/s^2]
        %       Y(6)  => VelXV:     longitudinal velocity in vehicle-fixed cs. [m/s]
        %       Y(7)  => VelYV:     lateral velocity in vehicle-fixed cs. [m/s]
        %   WheelFront ---------------------------------------------------------------------------
        %       Y(8)  => PitchAccY: angular acceleration [rad/s]
        %       Y(9)  => DSlipX:    longitudinal slip derivative [1/s]
        %       Y(10) => ForceXW:   longitudinal force in wheel-fixed cs. [N]
        %       Y(11) => DSlipY:    lateral slip derivative [1/s]
        %       Y(12) => ForceYW:   lateral force in wheel-fixed cs. [N]
        %       Y(13) => ForceZW:   vertical force in wheel-fixed cs. [N]
        %       Y(14) => TorqueDrv: wheel level driving torque [Nm]
        %       Y(15) => TorqueBrk: wheel level braking torque [Nm]
        %       Y(16) => SteerAng:  wheel level steering angle [rad]
        %   WheelRear ----------------------------------------------------------------------------
        %       Y(17) => PitchAccY: angular acceleration [rad/s]
        %       Y(18) => DSlipX:    longitudinal slip derivative [1/s]
        %       Y(19) => ForceXW:   longitudinal force in wheel-fixed cs. [N]
        %       Y(20) => DSlipY:    lateral slip derivative [1/s]
        %       Y(21) => ForceYW:   lateral force in wheel-fixed cs. [N]
        %       Y(22) => ForceZW:   vertical force in wheel-fixed cs. [N]
        %       Y(23) => TorqueDrv: wheel level driving torque [Nm]
        %       Y(24) => TorqueBrk: wheel level braking torque [Nm]
        %       Y(25) => SteerAng:  wheel level steering angle [rad]
        %   Steering -----------------------------------------------------------------------------
        %       Y(26) => DSteerAng: wheel level steering angle derivative [rad/s]
        % ------------------------------------------------------------------------------------------
        function [dX, Y] = StateEquation(self, t, X, U)
            %% Calculate necessary inputs
            % Sine and cosine of yaw angle
            cosYawV = cos(X(5));
            sinYawV = sin(X(5));
            
            % Chassis velocities in vehicle-fixed cs.
            velXV = + cosYawV * X(2) + sinYawV * X(4);
            velYV = - sinYawV * X(2) + cosYawV * X(4);
            
            %% Predict with net
            Xnet = [U(1) - U(2); U(5); velXV; velYV; X([6, 8:10, 12:15])] ./ self.inputNorms(1, :)';
            Ynet = double(self.net.predict(Xnet)) .* self.outputNorms(1, :)';
            
            %% Calculate additional variables
            % Inertial acceleration in vehicle-fixed coordinate system
            accXG = + cosYawV * Ynet(1) - sinYawV * Ynet(2);
            accYG = + sinYawV * Ynet(1) + cosYawV * Ynet(2);
            
            %% Assign output
            dX = [X(2); accXG; X(4); accYG; X(6); Ynet(3); ...
                X(8); Ynet(4:6); ...
                X(12); Ynet(7:9); ...
                Ynet(10)];
            Y = [accXG; accYG; Ynet(1); Ynet(2); Ynet(3); velXV; velYV; ...
                Ynet(4:5); 0; Ynet(6); 0; 0; 0; 0; 0; ...
                Ynet(7:8); 0; Ynet(9); 0; 0; 0; 0; 0; ...
                Ynet(10)];
        end

        %% Initialize -----------------------------------------------------------------------------
        % [Summary]
        %   This function provides the initial state vector of the planar nonlinear single track
        %   vehicle model based on the provided initial conditios.
        %
        % [Input]
        %   initCon - initial conditions as ChassisStBndCon object.
        %
        % [Output]
        %   X0      - initial state vector of vehicle model
        % ------------------------------------------------------------------------------------------
        function X0 = Initialize(self, initCon)
            %% Check arguments
            if ~isa(initCon, 'ChassisStBndCon')
                error('ERROR: input must be a ChassisStBndCon object!');
            end
            
            %% Initial state vector

            % Sine and cosine of yaw angle
            cosYawZ0 = cos(initCon.YawZ);
            sinYawZ0 = sin(initCon.YawZ);

            % Velocities in ground-fixed cs.
            velXG0 = + cosYawZ0 * initCon.VelXV - sinYawZ0 * initCon.VelYV;
            velYG0 = + sinYawZ0 * initCon.VelXV + cosYawZ0 * initCon.VelYV;

            % Initial value of state vector
            X0 = [initCon.PosXG;
                  velXG0;
                  initCon.PosYG;
                  velYG0;
                  initCon.YawZ;
                  initCon.YawVelZ;
                  0;
                  initCon.VelXV / 0.315;
                  0;
                  0;
                  0;
                  initCon.VelXV / 0.315;
                  0;
                  0;
                  0];
        end

        %% Simulate --------------------------------------------------------------------------------
        % [Summary]
        %   This function simulates the planar nonlinear single track vehicle model.
        %
        % [Input]
        %   initCon  - initial conditions for the vehicle as VehStBndCon object
        %   input    - input as VehStOlIn object
        %
        % [Output]
        %   output   - output as VehStOlOut object
        % ------------------------------------------------------------------------------------------
        function output = Simulate(self, initCon, input)
            %% Check arguments
            if ~isa(initCon, 'VehStBndCon')
                error('ERROR: initCon must be a VehStBndCon object!');
            end
            if ~isa(input, 'VehStOlIn')
                error('ERROR: input must be a VehStOlIn object!');
            end

            %% Input

            t = (input.Time(1) : self.SampleTime : input.Time(end))';
            if t(end) ~= input.Time(end)
                t(end + 1) = input.Time(end);
            end
            U = interp1q(input.Time, [input.TorqueDrv,  input.TorqueBrk, input.FrictionFront, ...
                input.FrictionRear, input.SteerWhlAng], t)';

            %% Initial conditions

            X0 = self.Initialize(initCon);

            %% Calculation of the motion
            [X, Y] = ode4(self, t, X0, U);

            output = VehStOlOut(t, ...
                ChassisStOut(t, X(1,:), X(2,:), Y(1,:), Y(6,:), Y(3,:),  X(3,:), X(4,:), ...
                    Y(2,:), Y(7,:), Y(4,:), X(5,:), X(6,:), Y(5,:)), ...
                WheelMfExtOut(t, X(7,:), X(8,:), Y(8,:), X(9,:), Y(9,:), Y(10,:), Y(13,:), ...
                    X(10,:), Y(11,:), Y(12,:), Y(14,:), Y(15,:), Y(16,:)), ...
                WheelMfExtOut(t, X(11,:), X(12,:), Y(17,:), X(13,:), Y(18,:), Y(19,:), Y(22,:), ...
                    X(14,:), Y(20,:), Y(21,:), Y(23,:), Y(24,:), Y(25,:)), ...
                SteerFoOut(t, X(15, :), Y(26, :)) ...
            );
        end
        
    end
end