classdef VehStLpv
    properties
        Mass
        WheelRadius
        RollingResistanceCoeff
        DragCoeff
        MomentOfInertia
        DistanceFront
        DistanceRear
        CorneringStiffnessFront
        CorneringStiffnessRear
    end
    %% Public instance methods =====================================================================
    methods
        %% Property access methods -----------------------------------------------------------------
        function self = set.Mass(self, value)
            if ~isequal(self.Mass, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: Mass must be numeric scalar!')
                end
                self.Mass = value;
            end
        end
        function self = set.WheelRadius(self, value)
            if ~isequal(self.WheelRadius, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: WheelRadius must be numeric scalar!')
                end
                self.WheelRadius = value;
            end
        end
        function self = set.RollingResistanceCoeff(self, value)
            if ~isequal(self.RollingResistanceCoeff, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: RollingResistanceCoeff must be numeric scalar!')
                end
                self.RollingResistanceCoeff = value;
            end
        end
        function self = set.DragCoeff(self, value)
            if ~isequal(self.DragCoeff, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DragCoeff must be numeric scalar!')
                end
                self.DragCoeff = value;
            end
        end
        function self = set.MomentOfInertia(self, value)
            if ~isequal(self.MomentOfInertia, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: MomentOfInertia must be numeric scalar!')
                end
                self.MomentOfInertia = value;
            end
        end
        function self = set.DistanceFront(self, value)
            if ~isequal(self.DistanceFront, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DistanceFront must be numeric scalar!')
                end
                self.DistanceFront = value;
            end
        end
        function self = set.DistanceRear(self, value)
            if ~isequal(self.DistanceRear, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DistanceRear must be numeric scalar!')
                end
                self.DistanceRear = value;
            end
        end
        function self = set.CorneringStiffnessFront(self, value)
            if ~isequal(self.CorneringStiffnessFront, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: CorneringStiffnessFront must be numeric scalar!')
                end
                self.CorneringStiffnessFront = value;
            end
        end
        function self = set.CorneringStiffnessRear(self, value)
            if ~isequal(self.CorneringStiffnessRear, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: CorneringStiffnessRear must be numeric scalar!')
                end
                self.CorneringStiffnessRear = value;
            end
        end

        function self = VehStLpv(varargin)
            if nargin == 0
                self.Mass = 1500;
                self.WheelRadius = 0.315;
                self.RollingResistanceCoeff = 0.000175;
                self.DragCoeff = 7.35;
                self.MomentOfInertia = 1850;
                self.DistanceFront = 1.12;
                self.DistanceRear = 1.31;
                self.CorneringStiffnessFront = 17.56;
                self.CorneringStiffnessRear = 17.56;
            elseif nargin == 1
                if ~isstruct(varargin{1})
                    error('ERROR: varargin{1} is not a sruct!');
                end
                for property = properties(self)
                    self.(property{1}) = varargin{1}.(property{1});
                end
            elseif nargin == 9
                self.Mass = varargin{1};
                self.WheelRadius = varargin{2};
                self.RollingResistanceCoeff = varargin{3};
                self.DragCoeff = varargin{4};
                self.MomentOfInertia = varargin{5};
                self.DistanceFront = varargin{6};
                self.DistanceRear = varargin{7};
                self.CorneringStiffnessFront = varargin{8};
                self.CorneringStiffnessRear = varargin{9};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end

        function [dX, Y] = StateEquation(self, t, X, U)
            %% Parameters
            g = Com.Gravitation;
            m = self.Mass;
            r = self.WheelRadius;
            fr = self.RollingResistanceCoeff;
            cd = self.DragCoeff;
            J = self.MomentOfInertia;
            lf = self.DistanceFront;
            lr = self.DistanceRear;
            cf = self.CorneringStiffnessFront;
            cr = self.CorneringStiffnessRear;

            %% State variables
            % vxV: longitudinal velocity in vehicle-fixed coordinate system
            vxV = X(1);
            % vyV: lateral velocity in vehicle-fixed coordinate system
            vyV = X(2);
            % wzV: yaw rate in vehicle-fixed coordinate system
            wzV = X(3);
            % txG: longitudinal position in ground-fixed coordinate system
            %txG = X(4);
            % tyG: lateral position in ground-fixed coordinate system
            %tyG = X(5);
            % rzG: yaw angle in ground-fixed coordinate system
            rzG = X(6);

            %% Input variables
            % delta: steering angle of front wheel
            delta = U(1);
            % M: driving / braking torque
            M = U(2);

            %% State equations
            % State matrix
            a11 = -(fr * g * m + cd) / m;
            a22 = -(g * (cf * lr + cr * lf) / (vxV * (lf + lr)));
            a23 = -(vxV + g * lf * lr * (cf - cr) / ( vxV * (lf + lr)));
            a32 = -(m * g * lf * lr * (cf - cr) / (J * vxV * (lf + lr)));
            a33 = -(m * g * lf * lr * (cf * lf + cr * lr) / (J * vxV * (lf + lr)));
            % Input matrix
            b11 = 1 / (m * r);
            b22 = g * cf * lr / (lf + lr);
            b32 = m * g * cf * lf * lr / (J * (lf + lr));
            % Velocities in global coordinate system
            vxG = cos(rzG) * vxV - sin(rzG) * vyV;
            vyG = sin(rzG) * vxV + cos(rzG) * vyV;
            % Turn rates in global coordinate system
            wzG = wzV;
            % Principle of linear momentum in vehicle-fixed coordinate system
            axV = a11 * vxV + b11 * M;
            ayV = a22 * vyV + a23 * wzV + b22 * delta;
            % Principle of angular momentum in vehicle-fixed coordinate system
            ezV = a32 * vyV + a33 * wzV + b32 * delta;
            % State derivative vector
            dX = [axV; ayV; ezV; vxG; vyG; wzG];

            %% Output equations
            % Inertial accelerations in vehicle-fixed coordinate system
            axIV = axV - vyV * wzV;
            ayIV = ayV + vxV * wzV;
            % Output vector
            Y = [axIV; ayIV; ezV];
        end

    end
end