%% VehEnvIn1W **************************************************************************************
% [Summary]
%   This class represents environmental inputs for quarter vehicle models with a single wheel.
%
% [Used in]
%   none
%
% [Subclasses]
%   VehQrLonIn
%   VehQrIn
% **************************************************************************************************
classdef VehEnvIn1W < ComTimeInOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % friction coefficient between road and tire [1]
        Friction

    end

    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.Friction(self, value)
            if ~isequal(self.Friction, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: Friction must be a numeric vector!');
                end
                self.Friction = value(:);
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehEnvIn1W(varargin)
            if nargin == 1
                comTimeInArgs = varargin(1);
            elseif nargin == 2
                comTimeInArgs = varargin(1);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@ComTimeInOut(comTimeInArgs{:});
            if nargin == 1
                self.Friction = varargin{1}.Friction;
            elseif nargin == 2
                self.Friction = varargin{2};
            end
        end

    end
end