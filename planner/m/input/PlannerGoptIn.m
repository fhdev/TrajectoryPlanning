%% PlannerGoptIn ***********************************************************************************
% [Summary]
%   This class represents inputs for constrained nonlinear solver based motion planner PlannerCon.
%
% [Used in]
%   PlannerCon
%
% [Subclasses]
%   PlannerOptIn
% **************************************************************************************************
classdef PlannerGoptIn < handle
    
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Initial conditions: the state of the vehicle at the begining of planning
        InitCon
        % Final conditions: the required state of the vehicle to reach
        FinalCon
        % Initial values of optimization variable
        OptVarInit
        % Weights of geometric planner objective function
        CostGeomWeight

    end
    
    %% Instance methods ============================================================================    
    methods
        %% Property getter and setter methods ------------------------------------------------------
        
        function set.InitCon(self, value)
            if ~isequal(self.InitCon, value)
                if isstruct(value)
                    value = PlannerGeomBndCon(value);
                end
                if ~isa(value, 'PlannerGeomBndCon')
                    error('ERROR: InitCon must be a PlannerGeomBndCon object!')
                end
                self.InitCon = value;
            end
        end
        
        function set.FinalCon(self, value)
            if ~isequal(self.FinalCon, value)
                if isstruct(value)
                    value = PlannerGeomOut(value);
                end
                if ~isa(value, 'PlannerGeomOut')
                    error('ERROR: FinalCon must be a PlannerGeomOut object!')
                end
                self.FinalCon = value;
            end
        end
        
        function set.OptVarInit(self, value)
            if ~isequal(self.OptVarInit, value)
                if isstruct(value)
                    value = PlannerOptVarVelPath(value);
                end
                if ~isa(value, 'PlannerOptVarVelPath')
                    error('ERROR: FinalCon must be a PlannerOptVarVelPath object!')
                end
                self.OptVarInit = value;
            end
        end
        
        function set.CostGeomWeight(self, value)
            if ~isequal(self.CostGeomWeight, value)
                if isstruct(value)
                    value = PlannerGeomOut(value);
                end
                if ~isa(value, 'PlannerGeomOut')
                    error('ERROR: EnvIn must be a PlannerGeomOut object!');
                end
                self.CostGeomWeight = value;
            end
        end
        
        %% Constructor  ----------------------------------------------------------------------------
        function self = PlannerGoptIn(varargin)
            if nargin == 1
                self.InitCon        = varargin{1}.InitCon;
                self.FinalCon       = varargin{1}.FinalCon;
                self.OptVarInit     = varargin{1}.OptVarInit;
                self.CostGeomWeight = varargin{1}.CostGeomWeight;
            elseif nargin == 4
                self.InitCon        = varargin{1};
                self.FinalCon       = varargin{2};
                self.OptVarInit     = varargin{3};
                self.CostGeomWeight = varargin{4};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end
        
    end
end
