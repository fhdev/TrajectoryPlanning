#include <math.h>
#include "mex.h"
#include "Eigen/Dense"
#include "TypesTp.hpp"
#include "MathTp.hpp"
#include "VehStOl.hpp"
#include "ChassisSt.hpp"
#include "WheelMf.hpp"
#include "Ode.hpp"

// number of parameters
#define NP 60
// number of inputs
#define NU 5
// number of provided initial states
#define NXi 6
// number of states
#define NX 15
// number of outputs
#define NY 26

#define P_  (prhs[0])
#define t_  (prhs[1])
#define Xi_ (prhs[2])
#define U_  (prhs[3])

#define X_ (plhs[0])
#define Y_ (plhs[1])

using namespace Eigen;
using namespace TrajectoryPlanning;

VehStOl *v = NULL; 

// [X, Y] = VehStMex(P, t, X0, U)
void mexFunction(int nlhs, mxArray *plhs[],	int nrhs, const mxArray *prhs[])
{
	// Check number of input arguments
	if (nrhs != 4)
    {
		mexErrMsgIdAndTxt("VehStMex:nrhs", 
            "Argument list must be [X, Y] = VehStMex(P, t, Xi, U). Number of input parameters is not 4.");
	}
    // Check number of output arguments
	if (nlhs != 2) 
    {
        mexErrMsgIdAndTxt("VehStMex:nlhs", 
            "Argument list must be [X, Y] = VehStMex(P, t, Xi, U). Number of output parameters is not 2.");
	}
	// Check P (parameter array)
    Int64_T rowsP = mxGetM(P_);
    Int64_T colsP = mxGetN(P_);
	if (!mxIsDouble(P_) ||	mxIsComplex(P_) || (rowsP != NP) || (colsP != 1))
    {
		mexErrMsgIdAndTxt("VehStMex:invalidArgument", 
            "P must be a column vector of doubles with 54 elements.");
	}
    // Check t (time sites)
    Int64_T rowst = mxGetM(t_);
    Int64_T colst = mxGetN(t_);
    if (!mxIsDouble(t_) || mxIsComplex(t_) || (rowst != 1))
    {
        mexErrMsgIdAndTxt("VehStMex:invalidArgument",
            "t must be a row vector of doubles.");
    }
    // Check X0 (time sites)
    Int64_T rowsX0 = mxGetM(Xi_);
    Int64_T colsX0 = mxGetN(Xi_);
    if (!mxIsDouble(Xi_) || mxIsComplex(Xi_) || (rowsX0 != NXi) || (colsX0 != 1))
    {
        mexErrMsgIdAndTxt("VehStMex:invalidArgument",
            "Xi must be a column vector of doubles with 6 elements.");
    }
    // Check U (input data)
    Int64_T rowsU = mxGetM(U_);
    Int64_T colsU = mxGetN(U_);
    if (!mxIsDouble(U_) || mxIsComplex(U_) || (rowsU != NU) || (colsU != colst))
    {
        mexErrMsgIdAndTxt("VehStMex:invalidArgument",
            "U must be a matrix of doubles with 5 rows and same number of column as t.");
    }

    // Get input data pointers
#if MX_HAS_INTERLEAVED_COMPLEX
    Float64_T *Pptr = mxGetDoubles(P_);
    Float64_T *tptr = mxGetDoubles(t_);
    Float64_T *Xiptr = mxGetDoubles(Xi_);
    Float64_T *Uptr = mxGetDoubles(U_);
#else
    Float64_T *Pptr = mxGetPr(P_);
    Float64_T *tptr = mxGetPr(t_);
    Float64_T *Xiptr = mxGetPr(Xi_);
    Float64_T *Uptr = mxGetPr(U_);
#endif

    // Allocate output
    X_ = mxCreateDoubleMatrix(NX, colst, mxREAL);
    Y_ = mxCreateDoubleMatrix(NY, colst, mxREAL);

    // Get output data pointers
#if MX_HAS_INTERLEAVED_COMPLEX
    Float64_T *Xprt = mxGetDoubles(X_);
    Float64_T *Yprt = mxGetDoubles(Y_);
#else
    Float64_T *Xprt = mxGetPr(X_);
    Float64_T *Yprt = mxGetPr(Y_);
#endif

    // Create mappings for Eigen usage
    Map<VectorXd> P(Pptr, NP);
    Map<VectorXd> t(tptr, colst);
	Map<VectorXd> Xi(Xiptr, NXi);
    Map<MatrixXd, ColMajor> U(Uptr, NU, colst);
    Map<MatrixXd, ColMajor> X(Xprt, NX, colst);
    Map<MatrixXd, ColMajor> Y(Yprt, NY, colst);

    // Create vehicle
    if (v == NULL)
	    v = new VehStOl(P);

	// Calculate initial state
	VectorXd X0(NX);
	v->Initialize(Xi, X0);

    // Compute motion
    Rk4(*v, t, X0, U, X, Y);

}
