#pragma once
#ifndef STEERFO_HPP
#define STEERFO_HPP

#include "Eigen/Dense"
#include "TypesTp.hpp"
#include "Ode.hpp"


namespace TrajectoryPlanning
{
	class SteerFo : DynamicSystem
	{
	private:
		// steering ratio from steering wheel level to wheel level [1]
		Float64_T ratio;
		// settling time of steering actuator [s]
		Float64_T timeConstant;

	public:
		Float64_T Ratio() const;

		Int64_T nP() const override;
		Int64_T nX() const override;
		Int64_T nU() const override;
		Int64_T nY() const override;

		SteerFo(
			Float64_T ratio,
			Float64_T timeConstant);

		SteerFo(const Eigen::Ref<const Eigen::VectorXd> P);

		Int32_T Derivatives(Float64_T t, 
			const Eigen::Ref<const Eigen::VectorXd> X,
			const Eigen::Ref<const Eigen::VectorXd> U,
			Eigen::Ref<Eigen::VectorXd> dX,
			Eigen::Ref<Eigen::VectorXd> Y) const override;
	};
}

#endif // !STEERFO_HPP

