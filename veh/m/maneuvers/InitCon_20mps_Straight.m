function initCon = InitCon_20mps_Straight()

%% Do not reset these
initCon.PosXG   = 0;
initCon.PosYG   = 0;
initCon.YawZ    = 0;

%% Adjustable indeed
initCon.VelXV   = 20;
initCon.VelYV   = 0;
initCon.YawVelZ = 0;
end