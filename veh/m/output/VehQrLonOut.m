%% VehQrLonOut *************************************************************************************
% [Summary]
%   This class represents output for longitudinal quarter vehicle model. 
%
% [Used in]
%   VehQrLon
%
% [Subclasses]
%   VehQrOut
% **************************************************************************************************
classdef VehQrLonOut < VehBaseOut
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Wheel related outout (WheelMfLonOut object)
        Wheel

    end
    
    %% Static methods ==============================================================================
    methods (Static)
        %% Property checker methods ----------------------------------------------------------------
        function value = setChassis(value)
            if isstruct(value)
                value = ChassisQrLonOut(value);
            end
            if ~isa(value, 'ChassisQrLonOut')
                error('ERROR: Chassis must be a ChassisQrLonOut object!')
            end
        end

        function value = setWheel(value)
            if isstruct(value)
                value = WheelMfLonOut(value);
            end
            if ~isa(value, 'WheelMfLonOut')
                error('ERROR: Wheel must be a struct or a WheelMfLonOut object!')
            end
        end

    end
    
    %% Instance methods ============================================================================
    methods
        
        %% Property getter and setter methods ------------------------------------------------------
        function set.Wheel(self, value)
            if ~isequal(self.Wheel, value)
                self.Wheel = self.setWheel(value);
            end
        end

        %% Constructor -----------------------------------------------------------------------------
        function self = VehQrLonOut(varargin)
            if nargin == 1
                vehBaseOutAgs = varargin(1);
            elseif nargin == 3
                vehBaseOutAgs = varargin(1:2);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@VehBaseOut(vehBaseOutAgs{:});
            if nargin == 1
                self.Wheel   = varargin{1}.Wheel;
            elseif nargin == 3
                self.Wheel   = varargin{3};
            end
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the longitudinal quarter vehicle model.
        %
        % [Input]
        %       name   - name for the plots
        % [Output]
        %       figWheel    - figure handle of plot containing wheel related quantities
        %       figChassis  - figure handle of plot containing chassis related quantities
        % ------------------------------------------------------------------------------------------
        function [figWheel, figChassis] = DisplayPlots(self, name)
            %% Default arguments
            if nargin < 2
                name = class(self);
            end

            %% Create plots
            figWheel = self.Wheel.DisplayPlots([name, '/Wheel']);
            figChassis = self.Chassis.DisplayPlots([name, '/Chassis']);
        end
    end
end