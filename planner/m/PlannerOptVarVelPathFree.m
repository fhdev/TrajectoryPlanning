%% PlannerOptVarVelPathFree ************************************************************************
% [Summary]
%   This class represents the parameters of reference signal profiles used for motion planning.
%   For the longitudinal velocity reference signal, a trapezoidal profile is used, meanwhile the
%   yaw rate reference profile is a polynomial spline.
%
% [Used in]
%   PlannerConIn
%   PlannerCon
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef PlannerOptVarVelPathFree < PlannerOptVarVelPath
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        YEnd
        
        YawEnd
        
        YawRateEnd
    end
    
    %% Public instance methods =====================================================================    
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.YEnd(self, value)
            if ~isequal(self.YEnd, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: YEnd must be a numeric scalar!')
                end
                self.YEnd = value;
            end
        end
        
        function set.YawEnd(self, value)
            if ~isequal(self.YawEnd, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: YawEnd must be a numeric scalar!')
                end
                self.YawEnd = value;
            end
        end
        
        function set.YawRateEnd(self, value)
            if ~isequal(self.YawRateEnd, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: YawRateEnd must be a numeric scalar!')
                end
                self.YawRateEnd = value;
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = PlannerOptVarVelPathFree(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 6
                superArgs = varargin(1:3);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@PlannerOptVarVelPath(superArgs{:});
            if nargin == 1
                self.YEnd       = varargin{1}.YEnd;
                self.YawEnd     = varargin{1}.YawEnd;
                self.YawRateEnd = varargin{1}.YawRateEnd;
            elseif nargin == 6
                self.YEnd       = varargin{4};
                self.YawEnd     = varargin{5};
                self.YawRateEnd = varargin{6};
            end
        end
        
        %% Update ----------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------        
        function updated = Update(self, vector)
            updated = false;
            if ~isequal(self.ToVector(), vector)
                self.YRef        = vector(1 : end-3);
                self.YEnd        = vector(end-2);
                self.YawEnd      = vector(end-1);
                self.YawRateEnd  = vector(end);
                updated = true;
            end
        end

        %% ToVector --------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------        
        function vector = ToVector(self)
            vector = [self.YRef; self.YEnd; self.YawEnd; self.YawRateEnd];
        end
        
        %% ToVehIn ---------------------------------------------------------------------------------
        % [Summary]
        %   This function 
        %
        % [Input]
        %      
        % [Output]
        %   
        % ------------------------------------------------------------------------------------------        
        function input = ToVehIn(self, input)
            [t, ~, x, y, r, v, dr] = PlannerGeom.SplineTrajLinVel(input.FinalCon.PosXG(1), ...
                [self.YRef; self.YEnd], self.YawEnd, input.InitCon.VelXV, ...
                self.VelRef, input.InitCon.YawVelZ, self.YawRateEnd);
            
            muf = input.EnvIn.FrictionFront * ones(size(t));
            mur = input.EnvIn.FrictionRear * ones(size(t));
            input = VehStClIn(t, x, y, r, v, dr, muf, mur); 
        end
    end
end
