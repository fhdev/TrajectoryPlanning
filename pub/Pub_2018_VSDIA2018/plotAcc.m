% Load one of VSDIA2018 mat files and run analyzeData for plotting data

% Sizes
W = 7;
H = 6;
F = 8;

fig1 = nicefigure('MaxSlips', W, H);
ax1 = niceaxis(fig1, F);
% Lane change
plot(ax1, accMaxTraj(1:2:end), accMaxLin(1:2:end), 'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-.');
plot(ax1, accMaxTraj(1:2:end), accMax(1:2:end), 'Color', nicecolors.BlueBelizeHole, 'LineStyle', '-');
% Lane keep
plot(ax1, accMaxTraj(2:2:end), accMaxLin(2:2:end), 'Color', nicecolors.GreenEmerald, 'LineStyle', '-.');
plot(ax1, accMaxTraj(2:2:end), accMax(2:2:end), 'Color', nicecolors.GreenEmerald, 'LineStyle', '-');
xlim(ax1, [0, max(accMaxTraj)]);
xlabel(ax1, '$\max(a_{geom})$ (max acc. ref.) [m/s\textsuperscript{2}]');
ylabel(ax1, '$\max(a_v)$ (max acc.) [m/s\textsuperscript{2}]');
l = nicelegend({'linear lane change', 'nonlinear lane change', 'linear lane keep', 'nonlinear lane keep'}, ax1, F);
l.Location = 'NorthOutSide';

fprintf('Adjust by hand...\n');
pause();
fprintf('Saving...\n');

name = 'MaxAcc';
savefig(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.fig']), 'compact');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.pdf']), '-dpdf', '-painters');
print(fig1, fullfile(pwd, 'data', 'PlotTemp', [name, '.png']), '-dpng', '-opengl');
