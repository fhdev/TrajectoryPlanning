param(
    [string]$drive="C"
)
$repoRoot = [System.IO.Path]::GetFullPath("$PSScriptRoot\..\..")
$env:Path += ";C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.1\bin;C:\Program Files\NVIDIA GPU Computing Toolkit\CUDA\v10.1\extras\CUPTI\lib64"
$env:PYTHONPATH = "$repoRoot;$repoRoot\veh\py"
& "$repoRoot\.wvenv\Scripts\Activate.ps1"
& tensorboard --logdir "$drive`:\VehModel_NN_2020\"