%% CtrlLon *****************************************************************************************
% [Summary]
%   This class represents common features for longitudinal velocity tracking control. 
%
% [Used in]
%   none
%
% [Subclasses]
%   CtrlPidV
% **************************************************************************************************
classdef CtrlLon < handle    
    %% Instance properties =========================================================================
    properties   
        %% Properties set in constructor -----------------------------------------------------------
        % Linearization velocity
        VelLin
        % Saturation velocity, driving and breaking torque values
        SatVel       
        SatTorqueDrv      
        SatTorqueBrk
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------        
        function set.VelLin(self, value)
            if ~isequal(self.VelLin, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: VelLin must be a numeric scalar!')
                end
                self.VelLin = value(:);
            end
        end
        
        function set.SatVel(self, value)
            if ~isequal(self.SatVel, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: SatVel must be a numeric vector!')
                end
                self.SatVel = value(:);
            end
        end
        
        function set.SatTorqueDrv(self, value)
            if ~isequal(self.SatTorqueDrv, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: SatTorqueDrv must be a numeric vector!')
                end
                self.SatTorqueDrv = value(:);
            end
        end
        
        function set.SatTorqueBrk(self, value)
            if ~isequal(self.SatTorqueBrk, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: SatTorqueBrk must be a numeric vector!')
                end
                self.SatTorqueBrk = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = CtrlLon(varargin)
            if nargin == 1
                self.VelLin       = varargin{1}.VelLin;
                self.SatVel       = varargin{1}.SatVel;
                self.SatTorqueDrv = varargin{1}.SatTorqueDrv;
                self.SatTorqueBrk = varargin{1}.SatTorqueBrk;
            elseif nargin == 4
                self.VelLin       = varargin{1};
                self.SatVel       = varargin{2};
                self.SatTorqueDrv = varargin{3};
                self.SatTorqueBrk = varargin{4};
            else
                error('ERROR: Inappropriate number of arguments!');
            end
        end
        
        %% AntiWindup ------------------------------------------------------------------------------
        % [Summary]
        %   This function calculates saturation and anti-windup by clamping for longitudinal
        %   velocity tracking control.
        %
        % [Input]
        %   U - input vector
        %       U(1) => VelXV:          longitudinal velocity in vehicle-fixed cs. [m/s]
        %       U(2) => VelRef:         target longitudinal velocity in vehicle-fixed cs. [m/s]
        %
        %   Y - output vector
        %       Y(1) => TorqueDrvBrk:    driving (> 0) or braking (< 0) torque [Nm]
        %
        % [Output]
        %   dX - state derivative vector
        %       dX(1) => VErr:           longitudinal velocity tracking error [m]
        %
        %   Y  - output vector
        %       Y(1) => TorqueDrvBrkSat: saturated driving (> 0) or braking (< 0) torque [Nm]
        % ------------------------------------------------------------------------------------------         
        function [dX, Y] = AntiWindup(self, U, Y)
            %% Saturation with anti-windup (clamping)
            
            torqueDrvMax = Util.LinInterp(self.SatVel, self.SatTorqueDrv, abs(U(1)));
            torqueBrkMax = Util.LinInterp(self.SatVel, self.SatTorqueBrk, abs(U(1)));
            
            if Y > torqueDrvMax
                Y = torqueDrvMax;
                dX = 0;
            elseif Y < -torqueBrkMax
                Y = -torqueBrkMax;
                dX = 0;
            else
                dX = U(2) - U(1);
            end
        end
        
    end
end