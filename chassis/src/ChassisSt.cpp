#include <cstdlib>
#include "../inc/ChassisSt.hpp"
#include "../../com/inc/MathTp.hpp"

using namespace Eigen;

// Parameters ======================================================================================
// mass [kg]
#define Mass_P                     (P(0))
// moment of inertia about vertical axis [kgm^2]
#define MomentOfInertiaZ_P        (P(1))
// horizontal distance from front wheel center point to center of gravity [m]
#define DistanceFrontCOG_P         (P(2))
// horizontal distance from rear wheel center point to center of gravity [m]
#define DistanceRearCOG_P          (P(3))
// center of gravity height [m]
#define HeightCOG_P                (P(4))
// aerodynamic drag coefficient [1]
#define DragCoefficient_P          (P(5))
// frontal area [m^2]
#define FrontalArea_P              (P(6))
// driving torque distribution between front and rear axles [1]
#define DrivingTorqueSplit_P       (P(7))
// number of parameters
#define NP 8

// Inputs ==========================================================================================
// longitudinal front tire force in vehicle - fixed coordinate system [N]
#define ForceXVFront_U (U(0))
// lateral front tire force in vehicle - fixed coordinate system [N]
#define ForceYVFront_U (U(1))
// longitudinal rear tire force in vehicle - fixed coordinate system [N]
#define ForceXVRear_U  (U(2))
// lateral rear tire force in vehicle - fixed coordinate system [N]
#define ForceYVRear_U  (U(3))
// longitudinal velocity in vehicle - fixed coordintate system [m/s]
#define VelXV_U        (U(4))
// lateral velocity in vehicle - fixed coordintate system[m/s]
#define VelYV_U        (U(5))    
// number of inputs
#define NU 6

// States and derivatives ==========================================================================
// longitudinal position in ground - fixed coordinate system [m]
#define PosXG_X     (X(0))
#define DPosXG_dX   (dX(0))
// longitudinal velocity in ground - fixed coordinate system [m/s]
#define VelXG_X     (X(1))
#define DVelXG_dX   (dX(1))
// lateral position in ground - fixed coordinate system [m]
#define PosYG_X     (X(2))
#define DPosYG_dX   (dX(2))
// lateral velocity in ground - fixed coordinate system [m/s]
#define VelYG_X     (X(3))
#define DVelYG_dX   (dX(3))
// yaw (heading) angle in ground - fixed coordinate system [rad]
#define YawZ_X      (X(4))
#define DYawZ_dX    (dX(4))
// yaw (heading) angular velocity in ground - fixed system [rad/s]
#define YawVelZ_X   (X(5))
#define DYawVelZ_dX (dX(5))
// number of states
#define NX 6

// Outputs =========================================================================================
// longitudinal acceleration in ground - fixed cs. [m/s^2]
#define AccXG_Y     (Y(0))
// lateral acceleration in ground - fixed cs. [m/s^2]
#define AccYG_Y     (Y(1))
// longitudinal inertial acceleration in vehicle - fixed cs. [m/s^2]
#define AccInrtXV_Y (Y(2))
// lateral inertial acceleration in vehicle - fixed cs. [m/s^2]
#define AccInrtYV_Y (Y(3))
// yaw(heading) angular acceleration in ground - fixed system [rad/s^2]
#define YawAccZ_Y   (Y(4))
// longitudinal velocity in vehicle - fixed cs. [m/s]
#define VelXV_Y     (Y(5))
// lateral velocity in vehicle - fixed cs. [m/s]
#define VelYV_Y     (Y(6))
// number of outputs
#define NY 7

namespace TrajectoryPlanning
{
	Float64_T ChassisSt::Mass()	const { return mass; }
	Float64_T ChassisSt::DistanceFrontCOG() const { return distanceFrontCOG; }
	Float64_T ChassisSt::DistanceRearCOG() const { return distanceRearCOG; }
	Float64_T ChassisSt::HeightCOG() const { return heightCOG; }

	Int64_T ChassisSt::nP() const { return NP; }
	Int64_T ChassisSt::nX() const { return NX; }
	Int64_T ChassisSt::nU() const { return NU; }
	Int64_T ChassisSt::nY() const { return NY; }

	ChassisSt::ChassisSt(const Ref<const VectorXd> P) :

		mass(Mass_P),
		momentOfInertiaZ(MomentOfInertiaZ_P),
		distanceFrontCOG(DistanceFrontCOG_P),
		distanceRearCOG(DistanceRearCOG_P),
		heightCOG(HeightCOG_P),
		dragCoefficient(DragCoefficient_P),
		frontalArea(FrontalArea_P),
		drivingTorqueSplit(DrivingTorqueSplit_P)
	{
	}

	ChassisSt::ChassisSt(
		Float64_T mass, 
		Float64_T momentOfInertiaZ,
		Float64_T distanceFrontCOG, 
		Float64_T distanceRearCOG,
		Float64_T heightCOG,
		Float64_T dragCoefficient,
		Float64_T frontalArea,
		Float64_T drivingTorqueSplit) :

		mass(mass),
		momentOfInertiaZ(momentOfInertiaZ),
		distanceFrontCOG(distanceFrontCOG),
		distanceRearCOG(distanceRearCOG),
		heightCOG(heightCOG),
		dragCoefficient(dragCoefficient),
		frontalArea(frontalArea),
		drivingTorqueSplit(drivingTorqueSplit)
	{
	}

	Int32_T ChassisSt::Derivatives(Float64_T t, 
		const Ref<const VectorXd> X,
		const Ref<const VectorXd> U,
		Ref<VectorXd> dX, 
		Ref<VectorXd> Y) const
	{
        // Check sizes
		CheckSizes(X, U, dX, Y);

		// Calculation of necessary variables
		Float64_T sinYawZ = sin(YawZ_X);
		Float64_T cosYawZ = cos(YawZ_X);
		// Tire forces in ground - fixed coordinate system
		Float64_T forceFrontXG = +cosYawZ * ForceXVFront_U - sinYawZ * ForceYVFront_U;
		Float64_T forceFrontYG = +sinYawZ * ForceXVFront_U + cosYawZ * ForceYVFront_U;
		Float64_T forceRearXG = +cosYawZ * ForceXVRear_U - sinYawZ * ForceYVRear_U;
		Float64_T forceRearYG = +sinYawZ * ForceXVRear_U + cosYawZ * ForceYVRear_U;
		// Aerodynamic drag force in vehicle - fixed and then in ground - fixed coordinate system
		Float64_T velAbs = sqrt(SQR(VelXV_U) + SQR(VelYV_U));
		Float64_T forceDragXV = 0.5 * dragCoefficient * frontalArea * RHOA * VelXV_U * velAbs;
		Float64_T forceDragYV = 0.5 * dragCoefficient * frontalArea * RHOA * VelYV_U * velAbs;
		Float64_T forceDragXG = +cosYawZ * forceDragXV - sinYawZ * forceDragYV;
		Float64_T forceDragYG = +sinYawZ * forceDragXV + cosYawZ * forceDragYV;

		// Calculation of state derivatives
		// Principle of linear momentum for the chassis
		DPosXG_dX = VelXG_X;
		DVelXG_dX = (forceFrontXG + forceRearXG - forceDragXG) / mass;
		DPosYG_dX = VelYG_X;
		DVelYG_dX = (forceFrontYG + forceRearYG - forceDragYG) / mass;
		// Principle of angular momentum for the chassis around z - axis
		DYawZ_dX = YawVelZ_X;
		DYawVelZ_dX = (distanceFrontCOG * ForceYVFront_U - distanceRearCOG * ForceYVRear_U) 
			/ momentOfInertiaZ;

		// Calculation of additional output quantitites and storage of output quantities
		AccXG_Y = DVelXG_dX;
		AccYG_Y = DVelYG_dX;
		// Inertial acceleration in vehicle - fixed coordinate system
		AccInrtXV_Y = +cosYawZ * AccXG_Y + sinYawZ * AccYG_Y;
		AccInrtYV_Y = -sinYawZ * AccXG_Y + cosYawZ * AccYG_Y;
		YawAccZ_Y = DYawVelZ_dX;

		return 0;
	}
}
