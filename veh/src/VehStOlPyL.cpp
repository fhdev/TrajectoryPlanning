#include <math.h>
#include <stdlib.h>
#include "../../ext//eigen-3.3.7/Eigen/Dense"
#include "../../com/inc/TypesTp.hpp"
#include "../../com/inc/MathTp.hpp"
#include "../inc/VehStOl.hpp"
#include "../../chassis/inc/ChassisSt.hpp"
#include "../../wheel/inc/WheelMf.hpp"
#include "../../com/inc/Ode.hpp"

extern "C" __declspec(dllexport) double dummy(double scalarIn, double* vectIn, double* vectOut)
{
	double scalarOut = 2 * scalarIn;
	vectOut[0] = 2 * vectIn[0];
	vectOut[1] = 2 * vectIn[1];
	return scalarOut;
}