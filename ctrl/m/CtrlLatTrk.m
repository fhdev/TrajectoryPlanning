%% CtrlLatTrk **************************************************************************************
% [Summary]
%   This class represents a state feedback and feedforward based lateral (path tracking) control. 
%
% [Used in]
%   VehStCl
%
% [Subclasses]
%   none
% **************************************************************************************************
classdef CtrlLatTrk < CtrlLat
    %% Instance properties =========================================================================    
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % State feedback gains for
        % lateral distance from track reference
        GainLatErr
        % lateral velocity from track reference
        GainDLatErr
        % yaw deviation from track reference
        GainYawErr
        % yaw rate derivation from track reference
        GainDYawErr
        
        
        % Controlled vehicle related parameters
        DistanceFrontCOG
        DistanceRearCOG
        % understeer gradient
        UnderSteerGrad
        % steady state yaw angle error feedforward factor
        YawDevSSFactor
        
        % Reference trajectory 
        PosXGRef
        PosYGRef
        YawZRef
        YawVelZRef
                
    end
    
    %% Instance methods ============================================================================
    methods    
        %% Property getter and setter methods ------------------------------------------------------        
        function set.GainLatErr(self, value)
            if ~isequal(self.GainLatErr, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: GainLatErr must be a numeric scalar!')
                end
                self.GainLatErr = value;
            end
        end
         
         function set.GainDLatErr(self, value)
            if ~isequal(self.GainDLatErr, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: GainDLatErr must be a numeric scalar!')
                end
                self.GainDLatErr = value;
            end
         end
         
        function set.GainYawErr(self, value)
            if ~isequal(self.GainYawErr, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: GainYawErr must be a numeric scalar!')
                end
                self.GainYawErr = value;
            end
        end
         
        function set.GainDYawErr(self, value)
            if ~isequal(self.GainDYawErr, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: GainDYawErr must be a numeric scalar!')
                end
                self.GainDYawErr = value;
            end
        end                          
        
        function set.DistanceFrontCOG(self, value)
            if ~isequal(self.DistanceFrontCOG, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DistanceFrontCOG must be a numeric scalar!')
                end
                self.DistanceFrontCOG = value;
            end
         end
         
        function set.DistanceRearCOG(self, value)
            if ~isequal(self.DistanceRearCOG, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: DistanceRearCOG must be a numeric scalar!')
                end
                self.DistanceRearCOG = value;
            end
        end  
        
        function set.UnderSteerGrad(self, value)
            if ~isequal(self.UnderSteerGrad, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: UnderSteerGrad must be a numeric scalar!')
                end
                self.UnderSteerGrad = value;
            end
        end
        
        function set.YawDevSSFactor(self, value)
            if ~isequal(self.YawDevSSFactor, value)
                if ~(isnumeric(value) && isscalar(value))
                    error('ERROR: YawDevSSFactor must be a numeric scalar!')
                end
                self.YawDevSSFactor = value;
            end
        end
        
        function set.PosXGRef(self, value)
            if ~isequal(self.PosXGRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PosXGRef must be a numeric vector!')
                end
                self.PosXGRef = value(:);
            end
        end

        function set.PosYGRef(self, value)
            if ~isequal(self.PosYGRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: PosYGRef must be a numeric vector!')
                end
                self.PosYGRef = value(:);
            end
        end
        
        function set.YawZRef(self, value)
            if ~isequal(self.YawZRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawZRef must be a numeric vector!')
                end
                self.YawZRef = value(:);
            end
        end
        
        function set.YawVelZRef(self, value)
            if ~isequal(self.YawVelZRef, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: YawVelZRef must be a numeric vector!')
                end
                self.YawVelZRef = value(:);
            end
        end
       
        %% Constructor -----------------------------------------------------------------------------        
        function self = CtrlLatTrk(chassis, wheel, varargin)
            %% Assemble superclass arguments
            if ~isa(chassis, 'ChassisSt')
                error('ERROR: chassis must be a ChassisSt object!');
            end
            if ~isa(wheel, 'WheelMf')
                error('ERROR: wheel must be a WheelMf object!');
            end
            if nargin == 3
                superArgs = varargin(1);
            elseif nargin == 5
                superArgs = varargin(1:3);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            
            %% Call superclass constructor
            self@CtrlLat(superArgs{:});
            
            %% Set subclass properties
            
            % Control design
            %   Rajesh Rajamani: Vehicle Dynamics and Control, Chapter 3
            %   Jarrod M. Snider: Automatic Steering Methods for Autonomous Automobile Path Tracking,
            %                     Chapter 4
            
            % Parameters
            % mass [kg]
            m    = chassis.Mass;
            % moment of inertia about vertical axis [kg.m^2]
            J    = chassis.MomentOfIntertiaZ;
            % horizontal distance from front wheel center point to center of gravity [m]
            lf   = chassis.DistanceFrontCOG;
            % horizontal distance from rear wheel center point to center of gravity [m]
            lr   = chassis.DistanceRearCOG;
            % peak value for lateral adhesion coefficient [1]
            Dy   = 1;
            % shape factor lateral [1]
            Cy   = wheel.MfShapeY;
            % stiffness factor lateral [1]
            By   = wheel.MfStiffnessY;
            % curvature factor lateral [1]
            Ey   = wheel.MfCurvatureY;
            
            % Linearize lateral slip model
            g = Com.Gravitation;
            
            % Calculate slip curve
            d_sy = 1e-3;
            sy = 0:d_sy:1e-1;
            gammay = Dy * sin(Cy * atan(By * sy - Ey * (By * sy - atan(By * sy))));
            d_gammay = diff(gammay) / d_sy;
            % Cornering stiffness in terms of adhesion coefficient distributed to the virtual front 
            % and rear wheels => slope of slip curve @ 1% slip
            cf = d_gammay(sy == 1e-2);
            cr = cf;
            % Cornering stiffness in terms of lateral force distributed to the 4 wheels (Rajamani)
            Cf = cf * m * g * lr / (lf + lr) / 2;
            Cr = cr * m * g * lf / (lf + lr) / 2;
            
            % LTI model with state space representation with respect to tracking errors
            % Inputs
            % U(1) => SteerAng:   steering angle [rad]
            % U(2) => YawVelZRef: desired yaw rate along path [rad/s]
            % States
            % X(1) => LatErr:  position offset from lane centerline (perpendicular to vehicle x-axis)
            % X(2) => DLatErr: derivative of position offset from lane centerline
            % X(3) => YawErr:  yaw (heading) angle offset from lane centerline
            % X(4) => DYawErr: derivative of yaw (heading) angle offset from lane centerline
            % Outputs
            % Y(1) => ErrSum:     sum of errors
            % System matrix
            a22 = -((2 * Cf + 2 * Cr) / (m * self.VelLin));
            a23 = (2 * Cf + 2 * Cr) / m;
            a24 =  -((2 * Cf * lf + 2 * Cr * lr) / (m * self.VelLin));
            a42 = -((2 * Cf * lf - 2 * Cr * lr) / (J * self.VelLin));
            a43 = (2 * Cf * lf - 2 * Cr * lr) / J;
            a44 = -((2 * Cf * lf^2 + 2 * Cr * lr^2) / (J * self.VelLin));
            A = [ 0,   1,   0,   0;
                0, a22, a23, a24;
                0,   0,   0,   1;
                0, a42, a43, a44];
            % Input matrix
            b21 = 2 * Cf / m;
            b22 = -(((2 * Cf * lf - 2 * Cr * lr) / (m * self.VelLin)) - self.VelLin);
            b41 = 2 * Cf * lf / J;
            b42 =  -((2 * Cf * lf^2 + 2 * Cr * lr^2) / (J * self.VelLin));
            B = [  0,   0;
                b21, b22;
                0,   0;
                b41, b42];
            % Output matrix
            C = [1, 0, 1, 0];
            % Feedthrough matrix
            D = 0;
            
            % LQ controller design
            % Check controllability
            if rank(ctrb(A, B(:,1))) ~= length(A)
                error('System is not controllable.');
            end
            % Weighting matrices
            Q0 = diag([1, 1, 100, 100]);
            R0 = 100;
            % Design controller
            K = lqr(A, B(:,1), Q0, R0);
            
            % Understeer gradient
            kV = lr * m / (2 * Cf * (lf + lr)) - lf * m / (2 * Cr * (lf + lr));
            % Constant to calculate steady state yaw error
            ke2 = lf * m / (2 * Cr * (lf + lr));
            
            %% Set subclass properties
            
            % Controller parameters 
            self.GainLatErr  = K(1);
            self.GainDLatErr = K(2);
            self.GainYawErr  = K(3);
            self.GainDYawErr = K(4);
            
            % Vehicle parameters
            self.DistanceFrontCOG = lf;
            self.DistanceRearCOG  = lr;
            self.UnderSteerGrad   = kV;
            self.YawDevSSFactor   = ke2;
            
            % Default values for references
            self.PosXGRef    = [0; 100];
            self.PosYGRef    = [0; 0];
            self.YawZRef    = [0; 0];
            self.YawVelZRef = [0; 0];
        end
        
        %% StateEquation ---------------------------------------------------------------------------
        % [Summary]
        %   This function constains the state equation of the state feedback / feedforward lateral
        %   path tracking controller. References:
        %       - Rajesh Rajamani: Vehicle Dynamics and Control, Chapter 3
        %       - Jarrod M. Snider: Automatic Steering Methods for Autonomous Automobile Path 
        %         Tracking, Chapter 4
        % [Input]
        %   t - current time stamp
        %
        %   X - state vector
        %       X(1) => IntVelXVYawErrVelDev: integral of longitudinal velocity multiplied by 
        %                                     errYawVelDev
        %
        %   U - input vector
        %       U(1) => PosXG:    longitudinal position in ground-fixed coordinate system [m]
        %       U(2) => PosYG:    lateral position in ground-fixed coordinate system [m]
        %       U(3) => YawZ:     yaw (heading) angle in ground-fixed coordinate system [rad]
        %       U(4) => YawVelZ:  yaw (heading) angular velocity in ground-fixed system [rad/s]
        %       U(5) => VelXV:    longitudinal velocity in vehicle-fixed cs. [m/s] 
        %       U(6) => VelYV:    lateral velocity in vehicle-fixed cs. [m/s]
        %
        % [Output]
        %   dX - state derivative vector
        %       dX(1) => VelXVYawErrVelDev: longitudinal velocity multiplied by errYawVelDev
        %
        %   Y  - output vector
        %       Y(1) => SteerAng: steering angle [rad]
        %       Y(2) => ErrCrossTrack: cross track error [m]
        %       Y(3) => ErrHeading:    heading error [rad]
        % ------------------------------------------------------------------------------------------        
        function [dX, Y] = StateEquation(self, t, X, U) %#ok<INUSL> t used for debugging
            %% Calculate tracking errors
            % error = track - ego vehicle            
            [eLat, eYawZ, eYawVelZ] = ...
                self.GetReferenceValues(self.PosXGRef, self.PosYGRef, self.YawZRef, self.YawVelZRef, ...
                U(1), U(2), U(3), U(4));

            dLatErr = X(1) - U(6);
            yawVelZRef = eYawVelZ + U(4);
            
            %% Calculate state derivative
            dX = -U(5) * eYawVelZ;
            
            %% Calculate state feedback
            steerAngStFb = self.GainLatErr * eLat + self.GainDLatErr * dLatErr ...
                + self.GainYawErr * eYawZ + self.GainDYawErr * eYawVelZ;
            
            %% Calculate feedforward
            if yawVelZRef == 0
                radiusRef = inf;
            else
                radiusRef = U(5) / yawVelZRef;
            end
            accCpRef = U(5)^2 / radiusRef;
            yawErrSS = - self.DistanceRearCOG / radiusRef + self.YawDevSSFactor * accCpRef;
            steerAngFfw = (self.DistanceFrontCOG + self.DistanceRearCOG) / radiusRef + self.UnderSteerGrad * accCpRef + self.GainYawErr * yawErrSS;
            
            steerAng = steerAngStFb + steerAngFfw;
            
            %% Saturation
            [~, steerAng] = self.AntiWindup(U(5), steerAng, []);
            
            %% Set 
            Y = [steerAng; eLat; eYawZ];
        end
        
        %% Initialize -----------------------------------------------------------------------------
        % [Summary]
        %   This function calculates initial value of state vector.
        %
        % [Input]
        %   None
        %
        % [Output]
        %   X0 -  initial state vector
        %       X0(1) => IntVelXVYawErrVelDev: integral of longitudinal velocity multiplied by 
        %                                      errYawVelDev
        % ------------------------------------------------------------------------------------------          
        function X0 = Initialize(self)
            %% Reset invernal variables
            self.iClosestPrev = 0;
            
            %% Set initial state vector
            X0 = 0;
            
        end
    end
 end
