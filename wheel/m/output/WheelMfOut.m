%% WheelMfOut **************************************************************************************
% [Summary]
%   This class represents outputs for longitudinal wheel model with Magic Formula tire.
%
% [Used in]
%   VehQrOut
%   VehQr
%
% [Subclasses]
%   WheelMfExtOut
% **************************************************************************************************
classdef WheelMfOut < WheelMfLonOut
    %% Instance properties =========================================================================
    properties
        %% Properties set in constructor -----------------------------------------------------------
        % Lateral slip [1]
        SlipY
        % Lateral slip derivative [1]
        DSlipY
        % Lateral tire force [N]
        ForceYW
        
    end
    
    %% Instance methods ============================================================================
    methods
        %% Property getter and setter methods ------------------------------------------------------
        function set.SlipY(self, value)
            if ~isequal(self.SlipY, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: SlipY must be a numeric vector!')
                end
                self.SlipY = value(:);
            end
        end
        
        function set.DSlipY(self, value)
            if ~isequal(self.DSlipY, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: DSlipY must be a numeric vector!')
                end
                self.DSlipY = value(:);
            end
        end
        
        function set.ForceYW(self, value)
            if ~isequal(self.ForceYW, value)
                if ~(isnumeric(value) && isvector(value))
                    error('ERROR: ForceYW must be a numeric vector!')
                end
                self.ForceYW = value(:);
            end
        end
        
        %% Constructor -----------------------------------------------------------------------------
        function self = WheelMfOut(varargin)
            if nargin == 1
                superArgs = varargin(1);
            elseif nargin == 11
                superArgs = varargin(1:8);
            else
                error('ERROR: Inappropriate number of arguments!');
            end
            self@WheelMfLonOut(superArgs{:});
            if nargin == 1
                self.SlipY   = varargin{1}.SlipY;
                self.DSlipY  = varargin{1}.DSlipY;
                self.ForceYW = varargin{1}.ForceYW;
            elseif nargin == 11
                self.SlipY   = varargin{9};
                self.DSlipY  = varargin{10};
                self.ForceYW = varargin{11};
            end           
        end
        
        %% DisplayPlots ----------------------------------------------------------------------------
        % [Summary]
        %   This function creates plots about the motion of the wheel model.
        %
        % [Input]
        %       name - name for the plots
        %
        % [Output]
        %       fig  - figure handle of plot
        % ------------------------------------------------------------------------------------------        
        function fig = DisplayPlots(self, name)
            %% Constants
            FONTSIZE = 8;
            
            %% Default arguments
            if nargin < 2
                name = class(self);
            end
            
            %% Create plots
            
            % Plot superclass properties
            fig = DisplayPlots@WheelMfLonOut(self, name);
            
            % Plot own properties
            figure(fig);
            
            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 3, ax);
            plot(ax, self.Time, 100 * self.SlipY, 'Color', Colors.Pumpkin);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$s_y \mathrm{[\%]}$');
            title(ax, 'lat. slip');
            
            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 6, ax);
            plot(ax, self.Time, 100 * self.DSlipY, 'Color', Colors.Pumpkin);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$\dot{s}_y \mathrm{[\%/s]}$');
            title(ax, 'lat. slip derivative');
            
            ax = niceaxis(fig, FONTSIZE);
            subplot(3, 3, 9, ax);
            plot(ax, self.Time, self.ForceYW, 'Color', Colors.Alizarin);
            xlabel(ax, '$t \mathrm{[s]}$');
            ylabel(ax, '$F_x \mathrm{[N]}$');
            title(ax, 'lat. force');
        end
    end
end