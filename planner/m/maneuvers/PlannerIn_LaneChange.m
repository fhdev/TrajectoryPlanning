function input = PlannerIn_LaneChange()

%% Environment input
% Con, Opt, Copt
input.EnvIn.Time          = 1;
input.EnvIn.FrictionFront = 1;
input.EnvIn.FrictionRear  = 1;

%% Initial conditions
% Con, Opt, Copt => must be 0
input.InitCon.PosXG       = 0;    
input.InitCon.PosYG       = 0;          
input.InitCon.YawZ        = 0;
input.InitCon.VelYV       = 0;
 % Con, Opt, Copt, Gopt
input.InitCon.VelXV       = kph2mps(30);
input.InitCon.YawVelZ     = 0;

%% Final conditions
% Gopt
input.FinalCon.ArcLenS    = nan;
% Con, Opt, Copt, Gopt
input.FinalCon.PosXG      = +37.50;
input.FinalCon.PosYG      = -[ 2.50;  3.00];
input.FinalCon.YawZ       = -[ 0.00;  0.01];
input.FinalCon.YawVelZ    = -[ 0.00;  0.01];
input.FinalCon.Time       = nan;
input.FinalCon.VelXV      = nan;
input.FinalCon.AccInrtYV  = nan;
% Con, Opt, Copt
input.FinalCon.VelXG      = nan;  
input.FinalCon.AccXG      = nan;
input.FinalCon.AccInrtXV  = nan;
input.FinalCon.VelYG      = nan;
input.FinalCon.VelYV      = nan;
input.FinalCon.AccYG      = nan;
input.FinalCon.YawAccZ    = nan;

%% Initial reference parameters
% Con, Opt, Copt, Gopt
input.OptVarInit.VelRef             = kph2mps(30);
% Con
input.OptVarInit.YawRateRef         = [0; 0; 0];
input.OptVarInit.TravelTime         = input.FinalCon.PosXG(1) / input.InitCon.VelXV;
input.OptVarInit.SampleTime         = 1e-2;
% Opt, Gopt
input.OptVarInit.YRef               = [0; 0];
input.OptVarInit.SampleDistance     = 1e-2;
% Copt
input.OptVarInit.YEnd               = input.FinalCon.PosYG(1);
input.OptVarInit.YawEnd             = input.FinalCon.YawZ(1);
input.OptVarInit.YawRateEnd         = input.FinalCon.YawVelZ(1);

%% Cost function weights
% Opt, Copt
input.CostTrackWeight.Time          = nan;
input.CostTrackWeight.SteerWhlAng   = nan;
input.CostTrackWeight.CrossTrackErr = 1;
input.CostTrackWeight.HeadingErr    = rad2deg(0.2);
input.CostStateWeight.Time          = nan;
input.CostStateWeight.PosXG         = nan;
input.CostStateWeight.VelXG         = nan;
input.CostStateWeight.VelXV         = nan;
input.CostStateWeight.AccXG         = nan;
input.CostStateWeight.AccInrtXV     = nan;
input.CostStateWeight.PosYG         = nan;
input.CostStateWeight.VelYG         = nan;
input.CostStateWeight.VelYV         = nan;
input.CostStateWeight.AccYG         = nan;
input.CostStateWeight.AccInrtYV     = 0.05;
input.CostStateWeight.YawZ          = nan;
input.CostStateWeight.YawVelZ       = nan;
input.CostStateWeight.YawAccZ       = nan;
% Gopt
input.CostGeomWeight.Time           = nan;
input.CostGeomWeight.PosXG          = nan;
input.CostGeomWeight.VelXV          = nan;
input.CostGeomWeight.PosYG          = nan;
input.CostGeomWeight.AccInrtYV      = 1;
input.CostGeomWeight.YawZ           = nan;
input.CostGeomWeight.YawVelZ        = nan;
input.CostGeomWeight.ArcLenS        = nan;
end
