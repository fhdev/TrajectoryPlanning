%% Clean up
clearvars;
%close all;

%% Parameters
nTraj = 14;
direction = 1;

veh = VehStCl(Vehicle_Default);
%% Load generated trajectory
outputDir = fullfile('E:', 'VehModel_NN_2020');
load(fullfile(outputDir, sprintf('trajectory_%03d.mat', nTraj)), 'trajectory');

%% Initial conditions
% Do not reset these
initCon.PosXG   = 0;
initCon.PosYG   = 0;
initCon.YawZ    = 0;
% Adjustable indeed
initCon.VelXV   = trajectory.VelXV(1);
initCon.VelYV   = 0;
initCon.YawVelZ = trajectory.YawVelZ(1) * sign(direction);
initCon = VehStBndCon(initCon);

%% Inputs
% Common
input.Time = trajectory.Time;
input.Friction = ones(size(input.Time));
input.FrictionFront = ones(size(input.Time));
input.FrictionRear = ones(size(input.Time));
% Closed loop with yaw rate tracking or path tracking
input.VelXVRef = trajectory.VelXV;
input.YawVelZRef = trajectory.YawVelZ * sign(direction);
input.PosXGRef = trajectory.PosXG;
input.PosYGRef = trajectory.PosYG * sign(direction);
input.YawZRef = trajectory.YawZ * sign(direction);
input = VehStClIn(input);

%% Simulate
tic;
output = veh.SimulateMex(initCon, input);
tSim = toc;
fprintf('Simulation time is %g s.\n', tSim);
fprintf('Simulation speed is %g x RT.\n', input.Time(end) / tSim);

%% Plot
output.Chassis.DisplayPlots();
output.WheelFront.DisplayPlots();

%% Save
veh = obj2struct(veh);
input = obj2struct(input);
output = obj2struct(output);
if sign(direction) == 1
    save(fullfile(outputDir, sprintf('drivedata_%03d.mat', nTraj)), '-v7.3', 'veh', 'input', 'output');
else
    save(fullfile(outputDir, sprintf('drivedata_%03db.mat', nTraj)), '-v7.3', 'veh', 'input', 'output');
end
