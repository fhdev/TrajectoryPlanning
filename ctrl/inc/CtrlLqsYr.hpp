#pragma once
#ifndef CTRLLQSYR_HPP
#define CTRLLATSTY_HPP

#include "Eigen/Dense"
#include "TypesTp.hpp"
#include "Ode.hpp"


namespace TrajectoryPlanning
{
	class CtrlLqsYr : DynamicSystem
	{
	private:
		// lon. velocity of linearization [m/s]
		Float64_T velLin;
		// lon. velocity sites for velocity dependent saturation of driving / braking torque [m/s]
		Eigen::VectorXd satVel;
		// saturation steering angle values [rad]
		Eigen::VectorXd satSteerAng;

		// gain for lat. velocity [s.rad/m]
		Float64_T gainVelY;
		// gain for yaw rate [s]
		Float64_T gainYawVelZ;
		// gain for yaw rate tracking error integral [1]
		Float64_T gainYawVelZTrackErrInt;
		// initial gain for lat. velocity [s.rad/m]
		Float64_T gainVelYInit;
		// initial gain for yaw rate [s]
		Float64_T gainYawVelZInit;

	public:
		Int64_T nP() const override;
		Int64_T nX() const override;
		Int64_T nU() const override;
		Int64_T nY() const override;

		CtrlLqsYr(
			Float64_T velLin,
			const Eigen::Ref<const Eigen::VectorXd> satVel,
			const Eigen::Ref<const Eigen::VectorXd> satSteerAng,
			Float64_T gainVelY,
			Float64_T gainYawVelZ_,
			Float64_T gainYawVelZTrackErrInt,
			Float64_T gainVelYInit,
			Float64_T gainYawVelZInit);

		Int32_T Derivatives(
			Float64_T t, 
			const Eigen::Ref<const Eigen::VectorXd> X,
			const Eigen::Ref<const Eigen::VectorXd> U,
			Eigen::Ref<Eigen::VectorXd> dX,
			Eigen::Ref<Eigen::VectorXd> Y) const override;
	};
}

#endif // !CTRLLQSYR_HPP

